
/**
 * Author: Sivaram Nimishakavi
 * Description: Information & Support Implemetation
 * Contains Information Regarding:
 * 1) News
 * 2) Contacts
 * 3) FAQ
**/

var faqArr = [];

var newsArr=[];

var searched_data=[];

var fromForm = "";

function frmInformationKApostshow(){
  if(fromForm === "otp"){
    frmInformationKA.flxFAQ.setVisibility(true);
    frmInformationKA.flxFAQSearch.setVisibility(false);
    frmInformationKA.searchField.text="";
    switchToContactDetailsTab();
    if(kony.application.getPreviousForm().id === "frmLoginKA" || kony.application.getPreviousForm().id === "frmMore")
    	closeAll();
  }
  else{
    frmInformationKA.flxFAQ.setVisibility(true);
    frmInformationKA.flxFAQSearch.setVisibility(false);
    frmInformationKA.searchField.text="";
    switchToNewsDetailsTab();
    if(kony.application.getPreviousForm().id === "frmLoginKA" || kony.application.getPreviousForm().id === "frmMore")
    	closeAll();
  }
}

function switchToContactDetailsTab(){
  /*************
  frmInformationKA.flcContentSivaram.animate(
    kony.ui.createAnimation({100:
                             {
                               "left": '-100%',
                               "stepConfig":{"timingFunction": easeOut}}}),
    {fillMode: forwards ,duration:0.3},
    {animationEnd: function() {} });
  ***************/
  frmInformationKA.flxNewsSivaram.setVisibility(false);
  frmInformationKA.flxFAQInfo.setVisibility(false);
  frmInformationKA.flxContactUsSivaram.setVisibility(true);
  frmInformationKA.btnContacts.skin = sknBtnBGWhiteBlue105Rd10;
  frmInformationKA.btnNews.skin = sknBtnBGBlueWhite105Rd10;
  frmInformationKA.btnFAQ.skin = sknBtnBGBlueWhite105Rd10;
}

/**
 * Author: Sivaram Nimishakavi
 * Description: Switch to FAQ Details tab
**/

function switchToFAQDetailsTab(){
  kony.print("switch to FAQ Layout fired");
  /*********
  if(kony.store.getItem("langPrefObj") == "ar"){
    frmInformationKA.flcContentSivaram.animate(
      kony.ui.createAnimation({100:
                               {
                                 "left": '0%',
                                 "stepConfig":{"timingFunction": easeOut}}}),
      {fillMode: forwards ,duration:0.3},
      {animationEnd: function() {} });
  }
  else{
    frmInformationKA.flcContentSivaram.animate(
      kony.ui.createAnimation({100:
                               {
                                 "left": '-200%',
                                 "stepConfig":{"timingFunction": easeOut}}}),
      {fillMode: forwards ,duration:0.3},
      {animationEnd: function() {} });
  }
	**************/
  frmInformationKA.flxContactUsSivaram.setVisibility(false);
  frmInformationKA.flxNewsSivaram.setVisibility(false);
  frmInformationKA.flxFAQInfo.setVisibility(true);
  frmInformationKA.btnContacts.skin = sknBtnBGBlueWhite105Rd10;
  frmInformationKA.btnNews.skin = sknBtnBGBlueWhite105Rd10;
  frmInformationKA.btnFAQ.skin = sknBtnBGWhiteBlue105Rd10;
}


/**
 * Author: Sivaram Nimishakavi
 * Description: Switch to News Details tab
**/

function switchToNewsDetailsTab() {
  /***************
  if(kony.store.getItem("langPrefObj") == "ar"){

    frmInformationKA.flcContentSivaram.animate(
      kony.ui.createAnimation({100:
                               {
                                 "left": '-200%',
                                 "stepConfig":{"timingFunction": easeOut}}}),
      {fillMode: forwards ,duration:0.3},
      {animationEnd: function() {} });
  }
  else{
    frmInformationKA.flcContentSivaram.animate(
      kony.ui.createAnimation({100:
                               {
                                 "left": '0%',
                                 "stepConfig":{"timingFunction": easeOut}}}),
      {fillMode: forwards ,duration:0.3},
      {animationEnd: function() {} });
  }
*************/
  frmInformationKA.flxFAQInfo.setVisibility(false);
  frmInformationKA.flxContactUsSivaram.setVisibility(false);
  frmInformationKA.flxNewsSivaram.setVisibility(true);
  frmInformationKA.btnContacts.skin = sknBtnBGBlueWhite105Rd10;
  frmInformationKA.btnNews.skin = sknBtnBGWhiteBlue105Rd10;
  frmInformationKA.btnFAQ.skin = sknBtnBGBlueWhite105Rd10;

}

function viewAllOffersInfo(){

  if(frmInformationKA.flxInnerNews.height=="93%"){
    frmInformationKA.flxInnerNews.animate(
      kony.ui.createAnimation({100:
                               {
                                 "height": '57%',
                                 "stepConfig":{"timingFunction": easeOut}}}),
      {fillMode: forwards ,duration:0.3},
      {animationEnd: function() {} });  

    frmInformationKA.btnMoreOfferNews.text=geti18nkey("i18n.common.shw3More");
  }else{
    frmInformationKA.flxInnerNews.animate(
      kony.ui.createAnimation({100:
                               {
                                 "height": '93%',
                                 "stepConfig":{"timingFunction": easeOut}}}),
      {fillMode: forwards ,duration:0.3},
      {animationEnd: function() {} });

    frmInformationKA.btnMoreOfferNews.text=geti18nkey("i18n.common.showless3");
  }
} 
/*
Service Call to fetch News & FAQ Details.
*/
function getNewsInfo(from){

  fromForm = from;

  ShowLoadingScreen();
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var options = {"access": "online"};
  var headers = {};
  objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
  var dataObject = new kony.sdk.dto.DataObject("Informationcontent");
  var selectedLanguage=kony.store.getItem("langPrefObj");
  selectedLanguage=selectedLanguage.toUpperCase();

  dataObject.addField("language", selectedLanguage);
  var serviceOptions = {
    "dataObject": dataObject,
    "headers": headers
  };

  kony.print("dataObject::"+JSON.stringify(dataObject));
  if (kony.sdk.isNetworkAvailable()) {
    objectService.customVerb("getInfoSupportData", serviceOptions, fetchNewsDetailsSuccess, fetchNewsDetailsError);
  }
}

function fetchNewsDetailsSuccess(response){

  var tmpNewsData=[];
  var tmpFaqData=[];
  kony.print("**News Response In fetchNewsDetailsSuccess "+JSON.stringify(response));

  var newsListData=response.newslist;

  kony.print("**News Response In fetchNewsDetailsSuccess "+JSON.stringify(newsListData));

  faqListData=response.faqlist;

  kony.print("**News Response In faqListData "+JSON.stringify(faqListData));

  //Start of FAQ Information
  if(faqListData!==undefined){

    for(var a=0;a<faqListData.length;a++){

      var gblFaqQuestionsData=[];
      var gblFaqData=[];

      var faqData={
        "lblSectionName":faqListData[a].category
      };

      gblFaqData.push(faqData);
      if(faqListData[a].faqdata!==undefined){

        for(var j=0;j<faqListData[a].faqdata.length;j++){

          tmpFaqData={
            "lblQuestion":faqListData[a].faqdata[j].faqQ,
            "lblAnswer":faqListData[a].faqdata[j].faqA
          };
          gblFaqQuestionsData.push(tmpFaqData);
        }
        kony.print("**gblFaqQuestionsData**"+JSON.stringify(gblFaqQuestionsData));
        gblFaqData.push(gblFaqQuestionsData);
        kony.print("**gblFaqData*** "+JSON.stringify(gblFaqData));
        faqArr[a]=[].concat(gblFaqData);
      }

    }

    kony.print("**Saved FAQ Data in Global Array*** "+JSON.stringify(faqArr));
  }
  //End of FAQ Information

  //Start of BANK and LATEST News
  if(newsListData!==undefined){

    for(var b=0;b<newsListData.length;b++){

      var gblNewsListData=[];
      var gblNewsData=[];

      var listData={
        "lblSectionName":newsListData[b].category
      };

      gblNewsData.push(listData);

      if(newsListData[b].newsdata!==undefined){

        for(var k=0;k<newsListData[b].newsdata.length;k++){
          var tt= "";
          if(newsListData[b].newsdata[k].imageData!== "" && newsListData[b].newsdata[k].imageData !== undefined && newsListData[b].newsdata[k].imageData != null)
            tt =newsListData[b].newsdata[k].imageData;
          else
            tt= "";


          tmpNewsData={
            "categoryDescription":newsListData[b].newsdata[k].description,
            "categorySubTitle":newsListData[b].newsdata[k].title,
            "imagePreview":{"base64":tt}
          };
          gblNewsListData.push(tmpNewsData);
        }
        kony.print("**gblNewsListData**"+JSON.stringify(gblNewsListData));
        gblNewsData.push(gblNewsListData);
        kony.print("**gblNewsData*** "+JSON.stringify(gblNewsData));
        newsArr[b]=[].concat(gblNewsData);
      }
    }
  }
  //End of BANK and LATEST News

  kony.application.dismissLoadingScreen();
  frmInformationKA.flxFAQ.setVisibility(true);
  frmInformationKA.flxFAQSearch.setVisibility(false);
  frmInformationKA.searchField.text="";
  /************PRESHOW CONTENT************
  	frmInformationKA.flcContentSivaram.showFadingEdges = false;
    frmInformationKA.flxNewsSivaram.showFadingEdges = false;
    frmInformationKA.flxNewsAndBankInfo.showFadingEdges = false;
    frmInformationKA.flxContactUsSivaram.showFadingEdges = false;
    frmInformationKA.mainContent.showFadingEdges = false;
    frmInformationKA.flxFAQInfo.showFadingEdges = false;
    frmInformationKA.flxFAQMainContent.showFadingEdges = false;
    frmInformationKA.flxSearchInfo.showFadingEdges = false;
    frmInformationKA.flxFAQ.showFadingEdges = false;
    frmInformationKA.flxFAQSearch.showFadingEdges = false;
    var currentAppVersion = appConfig.appVersion;
    frmInformationKA.lblVersionCode.text = "V " + currentAppVersion;
    frmInformationKApostshow();
  ************************/

  frmInformationKA.flxNewsSivaram.setVisibility(true);
  frmInformationKA.flxContactUsSivaram.setVisibility(false);
  frmInformationKA.show();
}
function fetchNewsDetailsError(response){
  kony.print("**News Response in fetchNewsDetailsError"+JSON.stringify(response));
  customAlertPopup(geti18Value("i18n.Bene.Failed"),  geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
  kony.application.dismissLoadingScreen();
  fetchApplicationPropertiesWithoutLoading();
}

//Bank ANd Latest - Start

function getNewsLatestDetails(){
  var gblNumSections = newsArr.length;

  for(var i=1;i<=gblNumSections;i++){
    var segFre = new kony.ui.SegmentedUI2({
      "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
      "data": [],
      "groupCells": false,
      "id": "segNews"+i,
      "isVisible": true,
      "left": "5%",
      "needPageIndicator": true,
      "pageOffDotImage": "pageoffdot.png",
      "pageOnDotImage": "pageondot.png",
      "onRowClick": onRowClickNews,
      "retainSelection": false,
      "rowFocusSkin": "seg0b1b665cbd23642",
      "rowSkin": "seg0b1b665cbd23642",
      "rowTemplate": flxInformation	,
      "sectionHeaderTemplate": Copycontainer087f3a0994b334a,
      "scrollingEvents": {},
      "sectionHeaderSkin": "CopysliPhoneSegmentHeader0e27d37c2968140",
      "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
      "separatorRequired": true,
      "separatorThickness": 1,
      "separatorColor":"052c49",
      "showScrollbars": false,
      "indicator": constants.SEGUI_NONE,
      "top": "0%",
      "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
      "width": "90%",
      "zIndex": 10,
      "enableReordering": false
    }, {
      "padding": [5, 5, 5, 5],
      "paddingInPixel": false
    }, {"bounces": false});
    frmInformationKA.flxNewsAndBankInfo.add(segFre);
    addNewsDataIntoSeg("segNews"+i);
  }
}

/*
Add News Data into News Segment.
*/

function addNewsDataIntoSeg(segName){

  var temp=[];
  var temp1=[];

  var segNum = segName.substring(7,segName.length);
  kony.print("segNum :: " + segNum);

  frmInformationKA[segName].widgetDataMap = {
    lblHeaderKA:"lblSectionName",
    lblHeader:"categorySubTitle",
    lblDescription:"categoryDescription",
    imgNews:"imagePreview"
  };

  var data = [];
  if(newsArr[segNum-1][1].length <= 2){
    data.push(newsArr[segNum-1]);
  }
  else{
    kony.print("newsArr bef :: "+JSON.stringify(newsArr));

    for(var i in newsArr){
      for(var j in newsArr[i]){
        if(j==0){
          temp1[j] = newsArr[i][j];
        }
        else{
          temp1[j]=[].concat(newsArr[i][j]);
          temp1[j].splice(2,temp1[j].length);
        }
      }
      temp[i]=[].concat(temp1);
    }

    kony.print("temp aft :: "+JSON.stringify(temp));
    kony.print("newsArr aft :: "+JSON.stringify(newsArr));

    data.push(temp[segNum-1]);
    var btnScreen = new kony.ui.Button({
      "centerX": "50%",
      "focusSkin": "sknBtnBGBlueWhite105Rd10",
      "height": "32dp",
      "id": "btnShowMore"+segNum,
      "isVisible": true,
      "maxWidth": "90%",
      "onClick": onClickSeeMoreNews,
      "skin": "SKNsHOWmORE",
      "text": geti18nkey("i18n.common.shw3More"),
      "top": "2%",
      "bottom": "2%",
      "width": "40%",
      "zIndex": 1
    }, {
      "contentAlignment": constants.CONTENT_ALIGN_CENTER,
      "displayText": true,
      "padding": [0, 0, 0, 0],
      "paddingInPixel": false
    }, {});

    frmInformationKA.flxNewsAndBankInfo.add(btnScreen);
  }
  kony.print("data @ add :: " + JSON.stringify(data));

  frmInformationKA[segName].setData(data);

}

/*
On Row Click operation of News Segment.
*/

function onRowClickNews(seguiWidget, sectionNumber, rowNumber, selectedState){

  kony.print("seguiWidget @ add :: " + seguiWidget.id);
  kony.print("sectionNumber @ add :: " + JSON.stringify(sectionNumber));
  kony.print("rowNumber @ add :: " + JSON.stringify(rowNumber));
  kony.print("selectedState @ add :: " + JSON.stringify(selectedState));

  var segName = seguiWidget.id;
  var segNum = segName.substring(7,segName.length);
  var dataNews = [];
  var dataNewsSeg= frmInformationKA[segName].data;
  dataNews.push(newsArr[segNum-1]);

  kony.print("dataNews :: " + JSON.stringify(dataNews));

  var informationRecord=dataNews[0][1][rowNumber];
  kony.print("dataNews[0][1][rowNumber].lblAnswe :: " + JSON.stringify(informationRecord));

  var categorySubTitle=informationRecord.categorySubTitle;
  var categoryDescription=informationRecord.categoryDescription;
  var imgBase64=informationRecord.imagePreview.base64;
  /*if(imgBase64!="" && imgBase64!= undefined && imgBase64!= null)
  frmInformationDetailsKA.imgDetails.base64=imgBase64;
  else
   frmInformationDetailsKA.imgDetails.base64=""; */
  frmInformationDetailsKA.lblSubTitle.text=categorySubTitle;
  frmInformationDetailsKA.lblDescription.text=categoryDescription;

  frmInformationDetailsKA.show();
}

/*
More Operation in News
*/

function onClickSeeMoreNews(button){

  var segNum = button.id.substring(11,button.id.length);
  var segName = "segNews"+segNum;
  kony.print("segName :: " + segName);
  var data = [];
  var temp = [];
  var temp1 = [];

  if(button.text == geti18nkey("i18n.common.showless3")){

    for(var i in newsArr){
      for(var j in newsArr[i]){
        if(j==0){
          temp1[j] = newsArr[i][j];
        }
        else{
          temp1[j]=[].concat(newsArr[i][j]);
          temp1[j].splice(2,temp1[j].length);
        }
      }
      temp[i]=[].concat(temp1);
    }
    data.push(temp[segNum-1]);
    button.text =geti18nkey("i18n.common.shw3More");
  }
  else{
    var temp=[];
    var temp1=[];

    for(var i in newsArr){
      for(var j in newsArr[i]){
        if(j==0){
          temp1[j] = newsArr[i][j];
        }
        else{
          temp1[j]=[].concat(newsArr[i][j]);
        }
      }
      temp[i]=[].concat(temp1);
    }
    data.push(temp[segNum-1]);
    button.text = geti18nkey("i18n.common.showless3");
  }
  kony.print("newsArr show more ::" + JSON.stringify(newsArr));
  kony.print("Data show more ::" + JSON.stringify(data));

  frmInformationKA[segName].setData(data);
}
//Bank and Latest - End

/*
Fetch All FAQ.
*/

function getAllQuestions(){
  var gblNumSections = faqArr.length;

  for(var i=1;i<=gblNumSections;i++){
    var segFre = new kony.ui.SegmentedUI2({
      "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
      "data": [],
      "groupCells": false,
      "id": "segFAQ"+i,
      "isVisible": true,
      "left": "5%",
      "needPageIndicator": true,
      "pageOffDotImage": "pageoffdot.png",
      "pageOnDotImage": "pageondot.png",
      "onRowClick": onRowClickFAQ,
      "retainSelection": false,
      "rowFocusSkin": "seg0b1b665cbd23642",
      "rowSkin": "seg0b1b665cbd23642",
      "rowTemplate": flxFAQRow	,
      "sectionHeaderTemplate": Copycontainer087f3a0994b334a,
      "scrollingEvents": {},
      "sectionHeaderSkin": "CopysliPhoneSegmentHeader0e27d37c2968140",
      "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
      "separatorRequired": true,
      "separatorThickness": 5,
      "separatorColor":"052c49",
      "showScrollbars": false,
      "showProgressIndicator":false,
      "bounces": false,
      "indicator": constants.SEGUI_NONE,
      "top": "5%",
      "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
      "width": "90%",
      "zIndex": 10,
      "enableReordering": false
    }, {
      "padding": [5, 5, 5, 5],
      "paddingInPixel": false
    }, {"bounces": false});
    frmInformationKA.flxFAQ.add(segFre);
    addDataIntoSeg("segFAQ"+i);
  }
}

/*
Operation to perform Row Click of FAQ Segment
*/

function onRowClickFAQ(seguiWidget, sectionNumber, rowNumber, selectedState)
{
  kony.print("seguiWidget @ add :: " + seguiWidget.id);
  kony.print("sectionNumber @ add :: " + JSON.stringify(sectionNumber));
  kony.print("rowNumber @ add :: " + JSON.stringify(rowNumber));
  kony.print("selectedState @ add :: " + JSON.stringify(selectedState));

  var segName = seguiWidget.id;
  var segNum = segName.substring(6,segName.length);
  var dataFAQ = [];
  var dataFAQSeg= frmInformationKA[segName].data;
  dataFAQ.push(faqArr[segNum-1]);
  var checkVisibilty = dataFAQSeg[0][1][rowNumber].lblAnswer.isVisible;

  kony.print("dataFAQ :: " + JSON.stringify(dataFAQ));
  kony.print("dataFAQ[0][1][rowNumber].lblAnswe :: " + JSON.stringify(dataFAQ[0][1][rowNumber].lblQuestion));
  var data = {
    "lblQuestion":dataFAQ[0][1][rowNumber].lblQuestion ,
    "lblAnswer" : (!checkVisibilty) ? {"isVisible": true,"text":dataFAQ[0][1][rowNumber].lblAnswer+"\n"} : 
    {"isVisible": false,"text":dataFAQ[0][1][rowNumber].lblAnswer+"\n"}};
  kony.print("@@@@data = "+JSON.stringify(data));
  frmInformationKA[segName].setDataAt(data, rowNumber, sectionNumber);

  collapseAll(segNum,sectionNumber);

  if(segNum==faqArr.length)
  { 
    kony.print("in scrollToEnd ");
    //frmInformationKA.flxFAQ.scrollToEnd();
  }
}
/*
To Add FAQ Data into the segment.
*/
function addDataIntoSeg(segName){

  var temp1=[];
  var temp=[];

  var segNum = segName.substring(6,segName.length);
  kony.print("segNum :: " + segNum);

  frmInformationKA[segName].widgetDataMap = {
    lblHeaderKA:"lblSectionName",
    //imgSection : "lblSectionImage",
    lblQuestion : "lblQuestion",
    lblAnswer : "lblAnswer"
  };

  var data = [];
  if(faqArr[segNum-1][1].length <= 2){
    data.push(faqArr[segNum-1]);
  }
  else{
    kony.print("faq bef :: "+JSON.stringify(faqArr));

    for(var i in faqArr){
      for(var j in faqArr[i]){
        if(j==0){
          temp1[j] = faqArr[i][j];
        }
        else{
          temp1[j]=[].concat(faqArr[i][j]);
          temp1[j].splice(2,temp1[j].length);
        }
      }
      temp[i]=[].concat(temp1);
    }

    kony.print("temp aft :: "+JSON.stringify(temp));
    kony.print("faq aft :: "+JSON.stringify(faqArr));

    data.push(temp[segNum-1]);
    var btnScreen = new kony.ui.Button({
      "centerX": "50%",
      "focusSkin": "sknBtnBGBlueWhite105Rd10",
      "height": "32dp",
      "id": "btnShowMore"+segNum,
      "isVisible": true,
      "maxWidth": "90%",
      "onClick": onClickSeeMore,
      "skin": "SKNsHOWmORE",
      "text": geti18nkey("i18n.common.shw3More"),
      "top": "2%",
      "bottom": "2%",
      "width": "40%",
      "zIndex": 1
    }, {
      "contentAlignment": constants.CONTENT_ALIGN_CENTER,
      "displayText": true,
      "padding": [0, 0, 0, 0],
      "paddingInPixel": false
    }, {});

    frmInformationKA.flxFAQ.add(btnScreen);
  }
  kony.print("data @ add :: " + JSON.stringify(data));

  frmInformationKA[segName].setData(data);
}

/*
Function written to collapse details of other sections when other section details are viewed.
*/
function collapseAll(segNum,sectionNumber){
  if(!isEmpty(segNum)){
    for(var i=1;i<=faqArr.length;i++)
    {
      var array=[];
      var dataFAQ=[];
      kony.print("**segNum**"+segNum);
      kony.print("**i**"+i);

      if(i!==parseInt(segNum)){
        var segName = "segFAQ"+i;
        dataFAQ.push(faqArr[i-1]);
        kony.print("**dataFAQ**"+JSON.stringify(dataFAQ));

        for(var j=0;j<dataFAQ[0][1].length ;j++){

          var temp = {
            "lblQuestion": {"text" : dataFAQ[0][1][j].lblQuestion },
            "lblAnswer" : {"isVisible": false, "text":dataFAQ[0][1][j].lblAnswer+"\n" } 
          }; 
          kony.print("**TEMP**"+JSON.stringify(temp));
          frmInformationKA[segName].setDataAt(temp, j, sectionNumber);
        }
      }
    }
  }
}


/*

Function written to collapse all details of FAQ.

*/
function closeAll(){

  for(var i=1;i<=faqArr.length;i++)
  {
    var array=[];
    var dataFAQ=[];

    var segName = "segFAQ"+i;
    dataFAQ.push(faqArr[i-1]);
    kony.print("**dataFAQ**"+JSON.stringify(dataFAQ));

    for(var j=0;j<dataFAQ[0][1].length ;j++){

      var temp = {
        "lblQuestion": {"text" : dataFAQ[0][1][j].lblQuestion },
        "lblAnswer" : {"isVisible": false, "text":dataFAQ[0][1][j].lblAnswer+"\n" } 
      }; 
      kony.print("**TEMP**"+JSON.stringify(temp));
      frmInformationKA[segName].setDataAt(temp, j, 0);
    }
  }
  frmInformationKA.flxFAQInfo.setVisibility(false);
  frmInformationKA.flxFAQInfo.left = "0%";
}




/*
See More Operation in FAQ
*/
function onClickSeeMore(button){
  var segNum = button.id.substring(11,button.id.length);
  var segName = "segFAQ"+segNum;
  kony.print("segName :: " + segName);
  var data = [];
  var temp = [];
  var temp1 = [];

  if(button.text == geti18nkey("i18n.common.showless3")){

    for(var i in faqArr){
      for(var j in faqArr[i]){
        if(j==0){
          temp1[j] = faqArr[i][j];
        }
        else{
          temp1[j]=[].concat(faqArr[i][j]);
          temp1[j].splice(2,temp1[j].length);
        }
      }
      temp[i]=[].concat(temp1);
    }
    data.push(temp[segNum-1]);
    button.text = geti18nkey("i18n.common.shw3More");
  }
  else{
    var temp=[];
    var temp1=[];

    for(var i in faqArr){
      for(var j in faqArr[i]){
        if(j==0){
          temp1[j] = faqArr[i][j];
        }
        else{
          temp1[j]=[].concat(faqArr[i][j]);

        }
      }
      temp[i]=[].concat(temp1);
    }
    data.push(temp[segNum-1]);
    button.text = geti18nkey("i18n.common.showless3");
  }
  kony.print("faqArr show more ::" + JSON.stringify(faqArr));
  kony.print("Data show more ::" + JSON.stringify(data));

  frmInformationKA[segName].setData(data);
  //alert("button :: " + JSON.stringify(button));
}
/*
Function to perform Search Operation.
*/
function fetchFAQ(){
  frmInformationKA.searchInformation.setFocus(true);
  if(frmInformationKA.searchField.text===""){
    //customAlertPopup("", "Please enter the Question to be searched",popupCommonAlertDimiss,"");
    frmInformationKA.flxFAQ.setVisibility(false);
    frmInformationKA.flxFAQSearch.setVisibility(true);
    addSearchedResultsIntoSeg("segSearchedFAQ");
  }else{
    frmInformationKA.flxFAQ.setVisibility(false);
    frmInformationKA.flxFAQSearch.setVisibility(true);
    addSearchedResultsIntoSeg("segSearchedFAQ");
  }
}

/*
To Add Search data into the segment.
*/
function addSearchedResultsIntoSeg(segName){

  var searchedKey=frmInformationKA.searchField.text.toLowerCase();
  kony.print("searchedKey ::::"+searchedKey);

  frmInformationKA[segName].widgetDataMap = {
    lblQuestion : "lblQuestion",
    lblAnswer : "lblAnswer"
  };

  while(searched_data.length > 0){
    searched_data.pop();
  }
  for(var i in faqArr){

    for(var k=0;k<faqArr[i][1].length;k++){
      kony.print("**Searched Response:::**"+JSON.stringify(faqArr[i][1][k]));

      kony.print("**Question:::**"+JSON.stringify(faqArr[i][1][k].lblQuestion));
      if(searchedKey===""){
        searched_data.push(faqArr[i][1][k]);
      }else{
        var n=(faqArr[i][1][k].lblQuestion.toLowerCase()).match(searchedKey);
        if(n!==null){
          searched_data.push(faqArr[i][1][k]);
        }
      }
    }	
  }
  if(searched_data.length===0){
    customAlertPopup(geti18Value("i18n.maps.Info"), "No search Results Available",popupCommonAlertDimiss,"");
    frmInformationKA[segName].removeAll();
  }else{
    frmInformationKA[segName].setData(searched_data);  
  }
}
/*
To Perform OnClick event of FAQs retrieved from Search Result
*/

function onRowClickSearchFAQ()
{


  var dataFAQSeg= frmInformationKA["segSearchedFAQ"].data;
  kony.print("dataFAQSeg :: " + JSON.stringify(dataFAQSeg));

  var rowNumber=frmInformationKA["segSearchedFAQ"].selectedRowIndex[1];
  kony.print("**rowNumber**:::"+rowNumber);

  var selected_data=frmInformationKA["segSearchedFAQ"].selectedItems[0];

  var dataFAQ=searched_data[rowNumber];

  kony.print("dataFAQSeg :: " + JSON.stringify(dataFAQ));

  var checkVisibilty = selected_data.lblAnswer.isVisible;

  if(checkVisibilty===undefined){
    checkVisibilty=false;
  }

  kony.print("checkVisibilty :: " + checkVisibilty);
  var data = {
    "lblQuestion":dataFAQ.lblQuestion ,
    "lblAnswer" : (!checkVisibilty) ? {"isVisible": true,"text":dataFAQ.lblAnswer+"\n"} : 
    {"isVisible": false,"text":dataFAQ.lblAnswer}};
  kony.print("@@@@data ::: "+JSON.stringify(data));
  frmInformationKA["segSearchedFAQ"].setDataAt(data, rowNumber,0);
}

function CallTheNumber()
{
  try
  {
    var number=frmInformationKA.lblMobile.text;
    kony.phone.dial(number);
  } 
  catch(err)
  {
    kony.print("error in dial:: "+err);
  }
}
function sendemail()
{
  // Set the recipients.
  try
  {
    var toEmail=frmInformationKA.lblEmail.text;

    var to = [toEmail];
    // Send the email.
    kony.phone.openEmail(to, null, null, null, null, false, null);
  }
  catch(err)
  {
    kony.print("error in email:: "+err);
  }
}
function newscreen()
{ 
  popupCommonAlertDimiss();
  var weburl = "http://" + bojWEBSITE + "/";
  kony.application.openURL(weburl);
  //kony.sdk.mvvm.LogoutAction();
}
function openURLHomePage()
{
  if(isLoggedIn===true)
  {
    customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.contact.openURL"), newscreen, popupCommonAlertDimiss, geti18Value("i18n.common.Yess"), geti18Value("i18n.comon.Noo"));
  }
  else
  {
    var weburl = "http://" + bojWEBSITE + "/";
    kony.application.openURL(weburl);
  }
}

function open_SOCIALMEDIA(url){
  try{
    kony.application.openURL(url);
  }catch(e){
    kony.print("Exception_open_SOCIALMEDIA ::"+e);
  }
}