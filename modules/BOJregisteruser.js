function registeruserPreshow(){
  
  frmRegisterUser.lblCountryCodeTitle.setVisibility(false);
  if((gblFromModule == "UpdateSMSNumber" || gblFromModule == "UpdateMobileNumber") && (gblTModule !== "countryCode")){
    frmRegisterUser.lblMobileNumberTitle.setVisibility(false);
    kony.print("gblFromModule::"+gblFromModule);
    if (frmRegisterUser.txtCardNum.text !== null && frmRegisterUser.txtPIN.text !== null && frmRegisterUser.txtMobileNumber.text !== null) {
      var cardNo = getCardNum(frmRegisterUser.txtCardNum.text);
      if (cardNo.length == 16 && frmRegisterUser.txtPIN.text.length == 4) {
        frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
      }
    }else{
      frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
    }
    callProfileServiceforCustName();
    frmRegisterUser.lblTitle.text = geti18nkey("i18n.common.ChangeSMSNumber");
    frmRegisterUser.lblMobileNumberClear.setVisibility(false);
    frmRegisterUser.flxTxtMobileNukber.isVisible = true;
    frmRegisterUser.flxNext.isVisible = false;
    preShowRegisterUser();
    frmRegisterUser.lblCountryCodeTitle.setVisibility(true);
    frmRegisterUser.txtCountryCode.text = "970";
    frmRegisterUser.txtMobileNumber.text ="";
    frmRegisterUser.flxLine.skin = "sknFlxGreyLine";
    frmRegisterUser.flxLine2.skin = "sknFlxGreyLine";
    frmRegisterUser.CopyflxLine0a4214bb8a7c146.skin = "sknFlxGreyLine";
    if(gblFromModule == "UpdateSMSNumber"){
      if(!isEmpty(frmUserSettingsMyProfileKA.lblSMSNumber.text)){
        var str = frmUserSettingsMyProfileKA.lblSMSNumber.text;
        frmRegisterUser.lblHint.text = geti18Value("i18n.common.currentmobileno")+" "+str;
//         frmRegisterUser.txtMobileNumber.text = str.substring(3,str.length);
//         frmRegisterUser.txtCountryCode.text = str.substring(0,3);
      }else{
        frmRegisterUser.lblHint.text = geti18Value("i18n.common.currentmobileno")+" "+customerAccountDetails.profileDetails.smsno;
        frmRegisterUser.txtMobileNumber.text =""
      }
      frmRegisterUser.txtCountryCode.isVisible = true;
      frmRegisterUser.flxLine2.isVisible = true;
//       frmRegisterUser.txtMobileNumber.centerX = null;
//       frmRegisterUser.CopyflxLine0a4214bb8a7c146.centerX = null;
//       frmRegisterUser.txtMobileNumber.left = "25%";
//       frmRegisterUser.CopyflxLine0a4214bb8a7c146.left = "25%";
    }else{
      frmRegisterUser.lblTitle.text = geti18nkey("i18n.common.ChangeMobileNumber");
      frmRegisterUser.lblHint.text = geti18Value("i18n.common.currentmobileno")+" "+kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_mobile_number;
//       frmRegisterUser.txtMobileNumber.text = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_mobile_number;
//       frmRegisterUser.txtCountryCode.isVisible = false;
//       frmRegisterUser.flxLine2.isVisible = false;    
//       frmRegisterUser.txtMobileNumber.left = null;
//       frmRegisterUser.CopyflxLine0a4214bb8a7c146.left = null;
//       frmRegisterUser.txtMobileNumber.centerX = "50%";
//       frmRegisterUser.CopyflxLine0a4214bb8a7c146.centerX = "50%";
    }
  }else if(gblTModule === "countryCode"){
    frmRegisterUser.lblCountryCodeTitle.setVisibility(true);
    gblTModule = "";
  }else{
    frmRegisterUser.lblMobileNumberClear.setVisibility(false);
    frmRegisterUser.flxTxtMobileNukber.isVisible = false;
    frmRegisterUser.flxNext.isVisible = true;
    try {
      //#ifdef android
      CurrForm = kony.application.getCurrentForm();
      //#endif
      //#ifdef iphone
      CurrForm = kony.application.getPreviousForm();
      //#endif
      if (CurrForm.id != "frmEnrolluserLandingKA") {
        frmRegisterUser.lblTitle.text = geti18nkey("i18n.login.cardData");
      } else {
         if(gblFromModule == "RegisterUser")
        frmRegisterUser.lblTitle.text = geti18nkey("i18n.login.cardData");
        else
          frmRegisterUser.lblTitle.text = geti18nkey("i18n.common.forgotPwd");
      }
      if(gblFromModule == "RetriveUsername")
        frmRegisterUser.lblTitle.text = geti18nkey("i18n.common.RetreiveUsername");
      preShowRegisterUser();
    } catch (err) {
      exceptionLogCall("RegisterUserPreShow","UI ERROR","UI",err);
      kony.print("Error in frmRegisterUser Pre show action :: "+ err);
      frmRegisterUser.lblTitle.text = geti18nkey("i18n.login.cardData");
      preShowRegisterUser();
    }
  }
  frmRegisterUser.forceLayout();
}