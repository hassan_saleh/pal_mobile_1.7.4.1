/*
 * Model class for Transactions object under RBObjects object service group
 * This is generated file. Please do not edit.
 *
 */

kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};
kony.sdk.mvvm.ObjectServices = kony.sdk.mvvm.ObjectServices || {};
kony.sdk.mvvm.ObjectServices.RBObjects = kony.sdk.mvvm.ObjectServices.RBObjects || {};

/**
 * Creates a new Model.
 * @class TransactionsModel
 * @param {Object} applicationContext - Application Context.
 * @param {Object} entityMetaData - Entity Metadata.
 * @param {Object} configOptions - Service Name and Service Options.
 */
kony.sdk.mvvm.ObjectServices.RBObjects.TransactionsModel = Class(kony.sdk.mvvm.BaseModel, {

    constructor: function(applicationContext, entityMetaData, configOptions) {
        /**
         *  @Fields in this object
            BenAddress1
BenAddress2
BenAddress3
BenBranchCode
BenCity
BenCountry
BenificiaryName
ErrorCode
ExternalAccountNumber
P_deno
P_mobile_no
SourceBranchCode
SwiftCode
Transactions
TransferFlag
accountID
accountNumber
amount
authenticationRequired
billCategory
billDueAmount
billDueDate
billGeneratedDate
billPaidAmount
billPaidDate
billerCategoryName
billid
bulkPayString
cashWithdrawalTransactionStatus
cashlessEmail
cashlessMode
cashlessOTP
cashlessOTPValidDate
cashlessPersonName
cashlessPhone
cashlessPin
cashlessSecurityCode
category
checkImage
checkImageBack
checkNumber
cr_amt
cr_dec
cross_rate
custId
deliverBy
description
description1
eBillEnable
eBillSupport
ebillURL
errmsg
exch_comm_chrg
fees
firstDeposit
firstRecordNumber
frequencyEndDate
frequencyStartDate
frequencyType
fromAccountBalance
fromAccountName
fromAccountNumber
fromAccountType
fromCheckNumber
fromNickName
hasDepositImage
isScheduled
jomopayType
lang
lastRecordNumber
limit
numberOfRecurrences
offset
order
otp
p_AccessChannel
p_BillNo
p_BillerCode
p_BillingNo
p_Branch
p_CustInfoFlag
p_DueAmt
p_Fees_On_Biller
p_ID
p_IdType
p_MobileNo
p_Nation
p_PaymentMethod
p_SendSMSFlag
p_ServiceType
p_acc_or_cr
p_account_br
p_ben_bank_addr1
p_ben_bank_addr2
p_ben_bank_city
p_ben_bank_country
p_ben_bank_name
p_ben_branch_code
p_biller_code
p_billing_no
p_comm
p_corresp_charges
p_cr_amount
p_deal_ref_no
p_due_amt
p_err_code
p_exc_comm
p_fcdb_ref_no
p_iban
p_imb_addr1
p_imb_addr2
p_imb_city
p_imb_country
p_imb_name
p_imb_swift_code
p_lc_amount
p_national_crear_code
p_national_crear_code_type
p_no_Of_Txn
p_pay_mode
p_payment_Status
p_paymt_Type
p_pmt_details1
p_pmt_details2
p_pmt_details3
p_pmt_details4
p_remmit_purpose
p_serv_type_code
p_trans_amt_ccy
p_validation_code
pass
payPersonEmail
payPersonName
payPersonPhone
payeeAccountNumber
payeeAddressLine1
payeeId
payeeName
payeeNickName
penaltyFlag
personId
recurrenceDesc
ref_code
referenceId
scheduledDate
searchAmount
searchDateRange
searchDescription
searchEndDate
searchMaxAmount
searchMinAmount
searchStartDate
searchTransactionType
searchType
secondDeposit
sortBy
statusDescription
success
toAccountName
toAccountNumber
toAccountType
toCheckNumber
transactionComments
transactionDate
transactionId
transactionType
transactionsNotes
transferResponse
usr
validDate

        */
        this.$class.$super.call(this, applicationContext, entityMetaData, configOptions);
    },
    /**
     * This method returns requested property of column from metadata.
     * @memberof TransactionsModel#
     * @param {String} columnName - Column Name.
     * @param {String} key - property of column.
     * @returns {Object} - Value for property 
     */
    getValueForColumnProperty: function(columnName, key) {
        return this.$class.$superp.getValueForColumnProperty.call(this, columnName, key);
    },
    /**
     * This method returns list of column names for this object from metadata.
     * @memberof TransactionsModel#
     * @returns {Array} - List of columns
     */
    getColumnNames: function() {
        return this.$class.$superp.getColumnNames.call(this);
    },
    /**
     * This method returns requested property of this object from metadata.
     * @memberof TransactionsModel#
     * @param {String} propertyName - property.
     * @returns {Object} - Value for property 
     */
    getValueForProperty: function(propertyName) {
        return this.$class.$superp.getValueForProperty.call(this, propertyName);
    },
    /**
     * This method returns properties map of column from metadata.
     * @memberof TransactionsModel#
     * @param {String} columnName - Column Name.
     * @returns {Object} - Column information 
     */
    getColumnInfo: function(columnName) {
        return this.$class.$superp.getColumnInfo.call(this, columnName);
    },
    /**
     * This method returns picklist values if exists for column from metadata.
     * @memberof TransactionsModel#
     * @param {String} columnName - Column Name.
     * @returns {Array} - Pick list values for column
     */
    getFieldPickListValues: function(columnName) {
        return this.$class.$superp.getFieldPickListValues.call(this, columnName);
    },
    /**
     * This method fetches the data for requested columns of this object.
     * @memberof TransactionsModel#
     * @param {Array} columnNames - List of Columns.
     * @param {SuccessCallback} onSuccess - Success Callback.
     * @param {SuccessCallback} onError - Error Callback.
     * @param {Object} [dataModel] - DataModel, (applies filter if contains primary key value map).
     */
    fetchDataForColumns: function(columnNames, onSuccess, onError, contextData) {
        this.$class.$superp.fetchDataForColumns.call(this, columnNames, onSuccess, onError, contextData);
    },
    /**
     * This method fetches the data of this object as requested in dataObject
     * @memberof TransactionsModel#
     * @param {Object} options - includes {"dataObject": kony.sdk.dto.DataObject}
     * @param {SuccessCallback} onSuccess - Success Callback.
     * @param {SuccessCallback} onError - Error Callback.
     */
    fetch: function(options, onSuccess, onError) {
        this.$class.$superp.fetch.call(this, options, onSuccess, onError);
    },
    /**
     * This method saves the record provided in dataObject.
     * @memberof TransactionsModel#
     * @param {Object} options - includes {"dataObject": kony.sdk.dto.DataObject}
     * @param {SuccessCallback} onSuccess - Success Callback.
     * @param {SuccessCallback} onError - Error Callback.
     */
    create: function(options, onSuccess, onError) {
        this.$class.$superp.create.call(this, options, onSuccess, onError);
    },
    /**
     * This method updates the columns of record provided in dataObject.
     * @memberof TransactionsModel#
     * @param {Object} options - includes {"dataObject": kony.sdk.dto.DataObject}
     * @param {SuccessCallback} onSuccess - Success Callback.
     * @param {SuccessCallback} onError - Error Callback.
     */
    update: function(options, onSuccess, onError) {
        this.$class.$superp.update.call(this, options, onSuccess, onError);
    },
    /**
     * This method updates the columns of record provided in dataObject.
     * @memberof TransactionsModel#
     * @param {Object} options - includes {"dataObject": kony.sdk.dto.DataObject}
     * @param {SuccessCallback} onSuccess - Success Callback.
     * @param {SuccessCallback} onError - Error Callback.
     */
    partialUpdate: function(options, onSuccess, onError) {
        this.$class.$superp.partialUpdate.call(this, options, onSuccess, onError);
    },
    /**
     * This method updates(overrides) the record provided in dataObject.
     * @memberof TransactionsModel#
     * @param {Object} options - includes {"dataObject": kony.sdk.dto.DataObject}
     * @param {SuccessCallback} onSuccess - Success Callback.
     * @param {SuccessCallback} onError - Error Callback.
     */
    completeUpdate: function(options, onSuccess, onError) {
        this.$class.$superp.completeUpdate.call(this, options, onSuccess, onError);
    },
    /**
     * This method removes the record provided in dataObject.
     * @memberof TransactionsModel#
     * @param {Object} options - includes {"dataObject": kony.sdk.dto.DataObject}
     * @param {SuccessCallback} onSuccess - Success Callback.
     * @param {SuccessCallback} onError - Error Callback.
     */
    remove: function(options, onSuccess, onError) {
        this.$class.$superp.remove.call(this, options, onSuccess, onError);
    },
    /**
     * This method removes the record in this object with provided primary key values.
     * @memberof TransactionsModel#
     * @param {Object} primaryKeyValueMap - Primary Keys and values.
     * @param {SuccessCallback} onSuccess - Success Callback.
     * @param {SuccessCallback} onError - Error Callback.
     */
    removeByPrimaryKey: function(primaryKeyValueMap, onSuccess, onError) {
        this.$class.$superp.removeByPrimaryKey.call(this, primaryKeyValueMap, onSuccess, onError);
    },
    /**
     * This method fetches the complete response of fetch operation as requested in dataObject
     * @memberof TransactionsModel#
     * @param {Object} options - includes {"dataObject": kony.sdk.dto.DataObject}
     * @param {SuccessCallback} onSuccess - Success Callback.
     * @param {SuccessCallback} onError - Error Callback.
     */
    fetchResponse: function(options, onSuccess, onError) {
        this.$class.$superp.fetchResponse.call(this, options, onSuccess, onError);
    },
    /**
     * This invokes the validate method in model extension class.
     * This is called from create and update methods.
     * @memberof TransactionsModel#
     * @param {Object} dataObject - Data object.
     * @param {kony.sdk.mvvm.v2.Model.ValidationType} validationType - Create/Update.
     * @returns {Boolean} - whether data is valid
     */
    validate: function(dataObject, validationType) {
        return this.$class.$superp.validate.call(this, dataObject, validationType);
    }
});