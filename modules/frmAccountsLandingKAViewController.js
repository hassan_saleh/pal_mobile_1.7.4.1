kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.frmAccountsLandingKAViewController = function(){
  	this.adsLoaded = false;
 	this.infeedAds = null;
};

kony.rb.frmAccountsLandingKAViewController.prototype.loadInFeedAds =function(successCallback, failureCallback)
{
   var adsPC = applicationManager.getAdsPresentationController();
   adsPC.fetchInFeedAds(viewSuccessCallback, viewErrorCallback);
 
  function viewSuccessCallback(infeedAds)
  {
    if(infeedAds.length > 0)
    {
      noAdsFound = false;
      this.infeedAds = infeedAds[0];
      var navManager =  applicationManager.getNavManager();
      navManager.setCustomInfo("frmAccountsLandingKA" , infeedAds[0]);
      successCallback();
	}
    else
    {
      noAdsFound = true;
      failureCallback();
    }
  }
  
  function viewErrorCallback(err)
  {
    noAdsFound = true;
    failureCallback();
  }
};

kony.rb.frmAccountsLandingKAViewController.prototype.bindInFeedAds =function()
{
  	if(!this.adsLoaded)
  	{
  	  this.loadInFeedAds(success, error);
  	}
  	else
  	{
    	enableDisableInFeedFlexContainer();
  	}
  
   	function success()
    {
      enableDisableInFeedFlexContainer();
    }
    
    function error()
    {
      enableDisableInFeedFlexContainer(null);
    }
};
  function enableDisableInFeedFlexContainer()
  {
    if(!noAdsFound)
    {
      if(!isAdClosed)
      {
        var navManager =  applicationManager.getNavManager();
        var infeedAds = null;

        if(this.infeedAds === null)
          infeedAds = navManager.getCustomInfo("frmAccountsLandingKA");
        else
          infeedAds = this.infeedAds;
        var date = new Date();
        var param = date.getTime();
        var bannerAdImgUrl = getImageURLBasedOnDeviceType(infeedAds.adImageURL2);
        frmAccountsLandingKA.imgInFeedAd.src = bannerAdImgUrl+"?Param="+param;
        isBannerInfeedAdImageAvailable = true;
        bannerInfeedAdImageUrl = bannerAdImgUrl;
      }
    }
}

kony.rb.frmAccountsLandingKAViewController.prototype.infeedAdsOnCLick = function()
{
  ShowLoadingScreen();
  KNYMetricsService.sendCustomMetrics("frmAccountsLandingKA", {"adAction":"clickInfeedAdBanner"}); 
  var navManager =  applicationManager.getNavManager();
  this.infeedAds = navManager.getCustomInfo("frmAccountsLandingKA");
  
  if(this.infeedAds.adActionType == "form")
  {
    navManager = applicationManager.getNavManager();
    navManager.setCustomInfo("frmEnlargeAdKA" , this.infeedAds);
   	navManager.navigateTo("frmEnlargeAdKA");
  }
  else if(this.infeedAds.adActionType == "url")
  {
    if(this.infeedAds.adAction1)
      {
      kony.application.openURL(this.infeedAds.adAction1);
      }
    else
      {
        alert("Navigation URL is not there.");
      }
  }
  else
  {
    alert("Action Is not defined");
  }
};

kony.rb.frmAccountsLandingKAViewController.prototype.onDownloadComplete = function(issuccess)
{
  if(issuccess && !isAdClosed)
    {
      showInFeedAds();
      KNYMetricsService.sendCustomMetrics("frmAccountsLandingKA", {"adID":"infeedAdBanner"});   	  
    }
  else
  {
    // isAdClosed = true;
     kony.print("Infeed banner Ad not downloaded");
  }
	
};

kony.rb.frmAccountsLandingKAViewController.prototype.onSelectWithdrawCash=function(){
  ShowLoadingScreen();
  var cardlessPC = applicationManager.getCardlessWithdrawalPresentationController();
  cardlessPC.showCardlessCashLandingForm();
};
