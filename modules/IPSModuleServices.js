//Type your code here
function serv_IPS_REGISTRATION(){
//                             	 {"key":"accNum","value":kony.store.getItem("frmAccount").accountID},
	try{    
    	var requestParams = [{"key":"custId","value":custid},
                          	 {"key":"accNum","value":kony.store.getItem("frmAccount").accountID},
                             {"key":"branch","value":kony.store.getItem("frmAccount").branchNumber},
                             {"key":"aliasType","value": Alias_Type},
                             {"key":"aliasValue","value":frmIPSRegistration.lblAliasConfirmation.text},
                             {"key":"p_channel","value":"MOBILE"},
                             {"key":"p_user_id","value":"BOJMOB"}];
    	call_CUSTOM_VERB("IPSRegistration",requestParams,
                         function(res){
          						kony.print("success response ::"+JSON.stringify(res));
          						if(res.statusCode === "00000"){
                                	kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.CLIQ.registeredsuccessfully"), geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard", geti18Value("i18n.CLIQ.GoToCliqDashboard"), "CliqDashboardManage", "", "", "");
                                }else{
                                    var Message = getErrorMessage(res.statusCode);
                                  
                                	kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), Message, geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard",geti18Value("i18n.CLIQ.GoToCliqDashboard"), "CliqDashboardHome", "", "", "");
                                }
        						frmIPSRegistration.destroy();
                                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                              }, 
                         function(err){
                                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                              });
    }catch(e){
    	kony.print("Exception_serv_IPS_REGISTRATION ::"+e);
    }
}

function serv_IPS_REGISTRATION_STATUS(){
	try{
    	call_CUSTOM_VERB("checkIPSRegistrationStatus",[{"key":"custId","value":custid}], function(res){
        	kony.print("IPS Registration stauts callback ::"+JSON.stringify(res));
        	if(res.statusCode === "00000"){
            	if(res.isRegistered === "1"){
//                 	frmEPS.flxMain.setVisibility(true);
//                     frmEPS.flxConfirmEPS.setVisibility(false);
//                     frmEPS.show();
                  if (gblTModule==="getAlias")
                    {
                      kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
                      kony.print("IPS res "+JSON.stringify(res));
                      kony.print("IPS aliases "+JSON.stringify(res.aliases));
                      frmGetIPSAlias.segGetAllIPSAliases.setData([]);
                      for(var aliass in res.aliases)
                        {
                          kony.print("alias IPS"+res.aliases[aliass].aliasType);
                          if (res.aliases[aliass].aliasType==="MOBL")
                            res.aliases[aliass].aliasType=kony.i18n.getLocalizedString("i18n.jomopay.mobiletype");
                          else
                            res.aliases[aliass].aliasType=kony.i18n.getLocalizedString("i18n.jomopay.aliastype");
                        }
                       kony.print("alias IPS"+res.aliases[aliass].aliasType);
                      kony.print("IPS aliases222 "+JSON.stringify(res.aliases));
                      
                      frmGetIPSAlias.segGetAllIPSAliases.widgetDataMap = { 
                        lblAliasTypeSeg: "aliasType",
                        lblAliasSeg :"aliasValue",
                      };

                      frmGetIPSAlias.segGetAllIPSAliases.setData(res.aliases);
                      frmGetIPSAlias.show();
                      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                    }
                  else
                    
                	frmIPSManageBene.show();
                }else
                  frmIPSHome.show();
            }else{
            	customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
            }
        	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        },function(err){
        	kony.print("IPS Registration status service failure :"+JSON.stringify(err));
        	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        });
    }catch(e){
    	kony.print("Exception_serv_IPS_REGISTRATION_STATUS ::"+e);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }
}

function call_CUSTOM_VERB(customVerb_SERV, requestParams, successCallBack, errorCallBack){
	try{
    	var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Accounts");
    	
    	for(var i in requestParams)
        	dataObject.addField(requestParams[i].key, requestParams[i].value);
    	var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
    	kony.print("Service Name ::"+customVerb_SERV+"\n"+"input Parameters ::"+JSON.stringify(dataObject));
        if (kony.sdk.isNetworkAvailable()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            modelObj.customVerb(customVerb_SERV, serviceOptions, successCallBack, errorCallBack);
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    }catch(e){
    	kony.print("Exception_call_CUSTOM_VERB ::"+e);
    }
}
