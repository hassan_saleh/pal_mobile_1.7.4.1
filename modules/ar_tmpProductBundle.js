//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:41 EEST 2020
function initializetmpProductBundleAr() {
    flxMainuAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxMainu",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    flxMainuAr.setDefaultUnit(kony.flex.DP);
    var flxIconContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxIconContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "slFbox",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxIconContainer.setDefaultUnit(kony.flex.DP);
    var flxIcon1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxIcon1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxToIcon",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxIcon1.setDefaultUnit(kony.flex.DP);
    var lblInitial = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblInitial",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTick = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblTick",
        "isVisible": true,
        "skin": "sknBOJttfwhitee150",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIcon1.add(lblInitial, lblTick);
    flxIconContainer.add(flxIcon1);
    var lblLine1 = new kony.ui.Label({
        "height": "48%",
        "id": "lblLine1",
        "isVisible": true,
        "right": "22%",
        "skin": "CopylblAmountCurrency0f1534fe7031e4b",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "76%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLine2 = new kony.ui.Label({
        "height": "48%",
        "id": "lblLine2",
        "isVisible": true,
        "right": "22%",
        "skin": "sknLblAccNumBiller",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "49%",
        "width": "76%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknsegmentDivider",
        "top": "99%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    flxMainuAr.add(flxIconContainer, lblLine1, lblLine2, contactListDivider);
}
