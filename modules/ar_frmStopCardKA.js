//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:41 EEST 2020
function addWidgetsfrmStopCardKAAr() {
    frmStopCardKA.setDefaultUnit(kony.flex.DP);
    var androidTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "androidTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "s",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidTitleBar.setDefaultUnit(kony.flex.DP);
    var lblBack = new kony.ui.Label({
        "height": "50%",
        "id": "lblBack",
        "isVisible": true,
        "right": 2,
        "skin": "sknLblBoj145",
        "text": "O",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRegisteredDevicesTitle = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblRegisteredDevicesTitle",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": "Stop Card",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 20
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnCloseSCard = new kony.ui.Button({
        "centerY": "50%",
        "height": "100%",
        "id": "btnCloseSCard",
        "isVisible": true,
        "onClick": AS_Button_d0f30fc5a2244282a00975ddda8ed8b9,
        "right": 2,
        "skin": "sknBtnTrans",
        "top": "5dp",
        "width": "35dp",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    androidTitleBar.add(lblBack, lblRegisteredDevicesTitle, btnCloseSCard);
    var flxStopCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45%",
        "id": "flxStopCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxStopCard.setDefaultUnit(kony.flex.DP);
    var lblReasonDesc = new kony.ui.Label({
        "id": "lblReasonDesc",
        "isVisible": true,
        "right": "15dp",
        "skin": "sknnavBarTitle",
        "text": "Select the reason for card deactivation",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "26dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnSubmit = new kony.ui.Button({
        "bottom": "5dp",
        "centerX": "50%",
        "height": "50dp",
        "id": "btnSubmit",
        "isVisible": true,
        "right": "28dp",
        "onClick": AS_Button_h4a5fd8e0da34155bef6c5d9eefcfa45,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.NUO.Submit"),
        "width": "300dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var flxCardReason = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxCardReason",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "15dp",
        "onClick": AS_FlexContainer_cd84d7d765274d158e7ba003e13e4b3e,
        "skin": "slFbox",
        "top": "110dp",
        "width": "90%",
        "zIndex": 3
    }, {}, {});
    flxCardReason.setDefaultUnit(kony.flex.DP);
    var lblReason = new kony.ui.Label({
        "id": "lblReason",
        "isVisible": true,
        "right": "2%",
        "skin": "sknlblCairoL",
        "text": "Select a Reason",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20%",
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnDropDown = new kony.ui.Button({
        "height": "100%",
        "id": "btnDropDown",
        "isVisible": true,
        "left": "2%",
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "o",
        "top": "2%",
        "width": "90%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var lstCardReason = new kony.ui.ListBox({
        "id": "lstCardReason",
        "isVisible": false,
        "right": "4dp",
        "masterData": [["lb1", "Select a Reason"],["lb2", "Card Lost"],["lb3", "Card Damaged"]],
        "selectedKey": "lb1",
        "selectedKeyValue": ["lb1", "Select a Reason"],
        "skin": "slListBoxNew",
        "top": "1dp",
        "width": "92%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "dropDownImage": "dropdown.png",
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var flxUnderlineBillerCategory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineBillerCategory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineBillerCategory.setDefaultUnit(kony.flex.DP);
    flxUnderlineBillerCategory.add();
    flxCardReason.add(lblReason, btnDropDown, lstCardReason, flxUnderlineBillerCategory);
    var flxBorderTransferType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "flxBorderTransferType",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "18dp",
        "skin": "skntLineDiv",
        "top": 145,
        "width": "78%",
        "zIndex": 1
    }, {}, {});
    flxBorderTransferType.setDefaultUnit(kony.flex.DP);
    flxBorderTransferType.add();
    flxStopCard.add(lblReasonDesc, btnSubmit, flxCardReason, flxBorderTransferType);
    frmStopCardKA.add(androidTitleBar, flxStopCard);
};
function frmStopCardKAGlobalsAr() {
    frmStopCardKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmStopCardKAAr,
        "enabledForIdleTimeout": true,
        "id": "frmStopCardKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_hdd574f0a5954a188d7dbbb25b87943e,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
