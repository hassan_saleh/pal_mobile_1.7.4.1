//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:41 EEST 2020
function addWidgetsfrmTermsAndConditionsKAAr() {
frmTermsAndConditionsKA.setDefaultUnit(kony.flex.DP);
var titleBarWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "titleBarWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "skncontainerBkgheader",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarWrapper.setDefaultUnit(kony.flex.DP);
var androidTitleBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "androidTitleBar",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "skntitleBarGradient",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
androidTitleBar.setDefaultUnit(kony.flex.DP);
var androidTitleLabel = new kony.ui.Label({
"centerY": "50%",
"id": "androidTitleLabel",
"isVisible": true,
"left": "55dp",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.common.termsAndConditions"),
"width": "70%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var androidBack = new kony.ui.Button({
"centerY": "50%",
"height": "50dp",
"id": "androidBack",
"isVisible": true,
"left": "2%",
"onClick": AS_Button_fecbbcd7ddfb47379ac2d28eb593b3e4,
"skin": "sknandroidBackButton",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"width": "20%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var CopylblBack0gcecf2016ac54c = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "CopylblBack0gcecf2016ac54c",
"isVisible": true,
"left": "8%",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
androidTitleBar.add(androidTitleLabel, androidBack, CopylblBack0gcecf2016ac54c);
titleBarWrapper.add(androidTitleBar);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": 0,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "79%",
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkgGray",
"top": "1%",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var flHome = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flHome",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxsegSknblue",
"top": "0dp",
"width": "100%",
"zIndex": 100
}, {}, {});
flHome.setDefaultUnit(kony.flex.DP);
flHome.add();
var richTexttermsandconditions = new kony.ui.Browser({
"detectTelNumber": true,
"enableZoom": false,
"height": "96%",
"id": "richTexttermsandconditions",
"isVisible": true,
"right": "5%",
"onPageFinished": AS_Browser_f1e1fb9d4b8b46e692fe3fe24daa4fbc,
"top": "3%",
"width": "90%",
"zIndex": 1
}, {}, {});
var richTexttermsandconditionsOld = new kony.ui.RichText({
"id": "richTexttermsandconditionsOld",
"isVisible": true,
"right": "5%",
"skin": "sknRichtextTnC",
"top": "20dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
mainContent.add(flHome, richTexttermsandconditions, richTexttermsandconditionsOld);
var flxAccepteddate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "7%",
"id": "flxAccepteddate",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccepteddate.setDefaultUnit(kony.flex.DP);
var lblAcceptedText = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblAcceptedText",
"isVisible": true,
"maxNumberOfLines": 1,
"skin": "CopylblsknWhite0j2968bd8f1ed42",
"text": kony.i18n.getLocalizedString("i18n.common.TncAccepted"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAccepteddate.add(lblAcceptedText);
var FlexContainer0a0f9bf90d35346 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "7%",
"id": "FlexContainer0a0f9bf90d35346",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer0a0f9bf90d35346.setDefaultUnit(kony.flex.DP);
var noThanks = new kony.ui.Button({
"centerY": "50.26%",
"focusSkin": "slButtonBlueFocus",
"height": "94%",
"id": "noThanks",
"isVisible": true,
"right": "6.97%",
"onClick": AS_Button_fecbbcd7ddfb47379ac2d28eb593b3e4,
"skin": "sknCancleBtn",
"text": kony.i18n.getLocalizedString("i18n.common.cancelC"),
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var enableTouchID = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "slButtonGreenFocus",
"height": "94%",
"id": "enableTouchID",
"isVisible": true,
"right": "55%",
"onClick": AS_Button_jf7795884a904c3ea40d1bbb1b464901,
"skin": "sknBtnAcceptc1d52f",
"text": kony.i18n.getLocalizedString("i18n.common.btnAccept"),
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlexContainer0a0f9bf90d35346.add(noThanks, enableTouchID);
frmTermsAndConditionsKA.add(titleBarWrapper, mainContent, flxAccepteddate, FlexContainer0a0f9bf90d35346);
};
function frmTermsAndConditionsKAGlobalsAr() {
frmTermsAndConditionsKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmTermsAndConditionsKAAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmTermsAndConditionsKA",
"init": AS_Form_a239d862d69c4ddaa15e5a21fb5a6dd1,
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"preShow": AS_Form_c3652acf24784d41a4c077a59f62b0c4,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": true,
"inTransitionConfig": {
"formAnimation": 0
},
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_4effe8d7f76b4b55b3c59b38b0134b0f,
"outTransitionConfig": {
"formAnimation": 0
},
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
