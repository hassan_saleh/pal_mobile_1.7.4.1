//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:41 EEST 2020
function addWidgetsfrmSearchResultsKAAr() {
    frmSearchResultsKA.setDefaultUnit(kony.flex.DP);
    var mainSearch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "mainSearch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainSearch.setDefaultUnit(kony.flex.DP);
    var titleBarAccountDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarAccountDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarAccountDetails.setDefaultUnit(kony.flex.DP);
    var androidBackButton = new kony.ui.Button({
        "focusSkin": "sknandroidBackButtonFocus",
        "height": "50dp",
        "id": "androidBackButton",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_7d809e51e33946bf80f0e1966d14102d,
        "skin": "sknandroidBackButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var lblTitleKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblTitleKA",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.search.SearchResults"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    titleBarAccountDetails.add(androidBackButton, lblTitleKA);
    var mainContentSearch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "mainContentSearch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "skncontainerBkg",
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContentSearch.setDefaultUnit(kony.flex.DP);
    var flxSectionHeaderKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxSectionHeaderKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0",
        "skin": "skncontainerBkg",
        "top": "0",
        "width": "100%"
    }, {}, {});
    flxSectionHeaderKA.setDefaultUnit(kony.flex.DP);
    var lblHeaderKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblHeaderKA",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.common.accountName"),
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSectionHeaderKA.add(lblHeaderKA);
    var segSearchResultsKA = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "data": [{
            "lblSepKA": "Label",
            "lblTransactionAmountKA": "",
            "lblTransactionDateKA": "",
            "lblTransactionNameKA": ""
        }],
        "groupCells": false,
        "id": "segSearchResultsKA",
        "isVisible": true,
        "right": "0%",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_552bf130769c4cf5954c857c6c782b6d,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Normal",
        "rowSkin": "seg2Normal",
        "rowTemplate": CopyFlexContainer0125626bedd894d,
        "scrollingEvents": {
            "onReachingEnd": AS_Segment_9c488c1d867842b8a96e03293ba7ea7d
        },
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "40dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopyFlexContainer0125626bedd894d": "CopyFlexContainer0125626bedd894d",
            "lblSepKA": "lblSepKA",
            "lblTransactionAmountKA": "lblTransactionAmountKA",
            "lblTransactionDateKA": "lblTransactionDateKA",
            "lblTransactionNameKA": "lblTransactionNameKA"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var LabelNoRecordsKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "40%",
        "id": "LabelNoRecordsKA",
        "isVisible": false,
        "right": "10%",
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.common.norecords"),
        "top": "30dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    mainContentSearch.add(flxSectionHeaderKA, segSearchResultsKA, LabelNoRecordsKA);
    mainSearch.add(titleBarAccountDetails, mainContentSearch);
    frmSearchResultsKA.add(mainSearch);
};
function frmSearchResultsKAGlobalsAr() {
    frmSearchResultsKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmSearchResultsKAAr,
        "allowHorizontalBounce": false,
        "allowVerticalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmSearchResultsKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": true,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_5d4e7e18f49f46b79c0b03b71dd2d62a,
        "retainScrollPosition": true,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
