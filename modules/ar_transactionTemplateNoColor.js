//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:41 EEST 2020
function initializetransactionTemplateNoColorAr() {
    CopyFlexContainer0c9f1eddbc7f547Ar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "CopyFlexContainer0c9f1eddbc7f547",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    CopyFlexContainer0c9f1eddbc7f547Ar.setDefaultUnit(kony.flex.DP);
    var transactionDate = new kony.ui.Label({
        "centerY": "34%",
        "height": "20dp",
        "id": "transactionDate",
        "isVisible": true,
        "right": "5%",
        "skin": "sknRegisterMobileBank",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionName = new kony.ui.Label({
        "centerY": "63.00%",
        "height": "20dp",
        "id": "transactionName",
        "isVisible": true,
        "right": "5.03%",
        "skin": "skn383838LatoRegular107KA",
        "top": "30dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionAmount = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "transactionAmount",
        "isVisible": true,
        "left": "26dp",
        "skin": "skn383838LatoRegular107KA",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSepKA = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "lblSepKA",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLineEDEDEDKA",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "95%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var AccountTypeKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "44dp",
        "id": "AccountTypeKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "6dp",
        "zIndex": 1
    }, {}, {});
    AccountTypeKA.setDefaultUnit(kony.flex.DP);
    AccountTypeKA.add();
    CopyFlexContainer0c9f1eddbc7f547Ar.add(transactionDate, transactionName, transactionAmount, lblSepKA, AccountTypeKA);
}
