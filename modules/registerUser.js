
/*
Author : Vineeth Vishwanath
Date : 22-01-18
Desc : preShow for frmRegistration
*/
function preShowRegisterUser() {
  if(gblFromModule == "UpdateSMSNumber" || gblFromModule == "UpdateMobileNumber"){
    kony.print("Do Nothing");
  }else{
    fetchApplicationPropertiesWithoutLoading();
  }

  gblCardNo = "";
  frmRegisterUser.flxNext.setEnabled(false);
  frmRegisterUser.txtCardNum.text = "";
  frmRegisterUser.txtPIN.text = "";
  frmRegisterUser.lblCircle1.skin = "sknlblPinStar";
  frmRegisterUser.lblCircle1.text = "";
  frmRegisterUser.lblCircle2.skin = "sknlblPinStar";
  frmRegisterUser.lblCircle2.text = "";
  frmRegisterUser.lblCircle3.skin = "sknlblPinStar";
  frmRegisterUser.lblCircle3.text = "";
  frmRegisterUser.lblCircle4.skin = "sknlblPinStar";
  frmRegisterUser.lblCircle4.text = "";
  frmRegisterUser.lblNext.skin = "sknLblNextDisabled";
  for (var i = 1; i < 5; i++) {
        var lineName = "flxLinee" + i;
        frmRegisterUser[lineName].skin = sknFlxGreyLine;
    }
  frmRegisterUser.lblInvalidCredentialsKA.setVisibility(false);
  frmRegisterUser.lblClose.setVisibility(false);
}


/*
Author : Vineeth Vishwanath
Date : 22-01-18
Desc : On click of next from frmRegistration
*/
function nextFromCardDetails() {
  if (kony.sdk.isNetworkAvailable()) {
    ShowLoadingScreen();
    var cardNum = getCardNum(frmRegisterUser.txtCardNum.text);
    var PINNum = frmRegisterUser.txtPIN.text;
    var Language = kony.store.getItem("langPrefObj");
    Language = Language.toUpperCase();

    var scopeObj = this;
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";

    var modelObj = INSTANCE.getModel("NewUser", serviceName, options);

    var dataObject = new kony.sdk.dto.DataObject("NewUser");

    dataObject.addField("cardnum", cardNum);
    dataObject.addField("pin", PINNum);
    dataObject.addField("Language", Language);
    if (gblFromModule == "RegisterUser") {
      dataObject.addField("FlowFlag", "R");
    } else if (gblFromModule == "RetriveUsername") {
      dataObject.addField("FlowFlag", "R");
    } else {
      dataObject.addField("FlowFlag", "F");
    }

    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    kony.print("serviceOptions :: " + JSON.stringify(serviceOptions));
    modelObj.customVerb("UserReg_ForgotPwd", serviceOptions, successReg, errorReg);


  } else {
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    kony.application.dismissLoadingScreen();
  }

}



function successReg(res) {
  kony.print("successReg REsult JSON" + JSON.stringify(res));
  if (!isEmpty(res)) {

    kony.print("success saving record :: gblFromModule ::  "+ gblFromModule);
    if (gblFromModule == "RegisterUser" ) {
      if (res.status_code === 0 || res.status_code == "00000") {
        gblToOTPFrom = "RegisterUser";
        if (res.statusTypeCheck === "F") {
          customAlertPopup(geti18Value("i18n.Bene.Failed"),
                           res.statusMessageChek,
                           popupCommonAlertDimiss, "");
        } else {
          ShowLoadingScreen();
          serv_CustomerDetails(res.Cust_id);
          //                     callRequestOTP();
        }
        //frmNewUserOnboardVerificationKA.show();
      } else {
        kony.print("in else of fwd");
        frmRegisterUser.lblInvalidCredentialsKA.text = geti18Value("i18n.common.invalidPin");
        frmRegisterUser.lblInvalidCredentialsKA.setVisibility(true);
      }
    } 
    else if(gblFromModule == "RetriveUsername"){
      if (res.status_code === 0 || res.status_code == "00000") {
        gblToOTPFrom = "RetriveUsername";
        if (res.statusTypeCheck === "F") {
          if(res.Cust_id!== null && res.Cust_id !== "" && res.username!== null && res.username!== ""){
            custid = res.Cust_id;
            userName = res.username;
            serv_CustomerDetails(res.Cust_id);
          }else{
            kony.application.dismissLoadingScreen();
            customAlertPopup(geti18Value("i18n.maps.Info"),geti18Value("i18n.common.registerYourself"),popupCommonAlertDimiss, "");
          }
        } else {
          kony.application.dismissLoadingScreen();
          customAlertPopup(geti18Value("i18n.maps.Info"),
                           geti18Value("i18n.common.registerYourself"),
                           popupCommonAlertDimiss, "");

        }
      } else {
        frmRegisterUser.lblInvalidCredentialsKA.text = geti18Value("i18n.common.invalidPin");
        frmRegisterUser.lblInvalidCredentialsKA.setVisibility(true);
        kony.application.dismissLoadingScreen();
      }
    }else {
      kony.print("ForgotPasswordFlowSuccess:");
      if(res.cmpCustIDRes!== null && res.cmpCustIDRes!== "" && res.cmpCustIDRes == "F"){
        frmRegisterUser.lblInvalidCredentialsKA.text =geti18Value("i18n.common.invalidPin");
        frmRegisterUser.lblInvalidCredentialsKA.setVisibility(true);
      }else{
        if (res.status_code === 0 || res.status_code == "00000") {
          gblReqField = "";
          initialiseAndCallEnroll();
        } else {
          frmRegisterUser.lblInvalidCredentialsKA.text = geti18Value("i18n.common.invalidPin");
          frmRegisterUser.lblInvalidCredentialsKA.setVisibility(true);
        }
      }
    }


    kony.application.dismissLoadingScreen();
  } else {
    customAlertPopup(geti18Value("i18n.Bene.Failed"),geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
    kony.application.dismissLoadingScreen();
  }
}

function errorReg(err) {
  kony.print("REsult JSON" + "gblFromModule ::" +gblFromModule + "  ... " +  JSON.stringify(err));
  if (gblFromModule == "RegisterUser") {
    if (cmpCustIDRes == "S") {
      ShowLoadingScreen();
      callRequestOTP();
    } else if (cmpCustIDRes == "F") {
      frmRegisterUser.lblInvalidCredentialsKA.text = geti18Value("i18n.common.invalidPin");
      frmRegisterUser.lblInvalidCredentialsKA.setVisibility(true);
    } else {      
      customAlertPopup(geti18Value("i18n.Bene.Failed"),geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
      kony.application.dismissLoadingScreen();
    }
  } else {
    if (cmpCustIDRes == "S") {
      gblReqField = "";
      kony.print("errorReg:cmpCustIDRes:::"+cmpCustIDRes);
      initialiseAndCallEnroll();
    } else if (cmpCustIDRes == "F") {
      frmRegisterUser.lblInvalidCredentialsKA.text = geti18Value("i18n.common.invalidPin");
      frmRegisterUser.lblInvalidCredentialsKA.setVisibility(true);

    } else { 

      customAlertPopup(geti18Value("i18n.Bene.Failed"),geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
      kony.application.dismissLoadingScreen();
    }
  }
  cmpCustIDRes = "";

  kony.application.dismissLoadingScreen();
}

/*
Author : Vineeth Vishwanath
Date : 22-01-18
Desc : To enable or disable next button
*/
function isNextEnabled() {
  var currForm = kony.application.getCurrentForm().id;
  if (currForm == "frmRegisterUser") {
    if (frmRegisterUser.txtCardNum.text !== null && frmRegisterUser.txtPIN.text !== null) {
      var cardNo = getCardNum(frmRegisterUser.txtCardNum.text);
      if (cardNo.length == 16 && frmRegisterUser.txtPIN.text.length == 4) {
        frmRegisterUser.flxNext.setEnabled(true);
        frmRegisterUser.lblNext.skin = "sknLblNextEnabled";
        frmRegisterUser.flxProgress1.left = "20%";
      } else {
        frmRegisterUser.flxProgress1.left = "10%";
        frmRegisterUser.flxNext.setEnabled(false);
        frmRegisterUser.lblNext.skin = "sknLblNextDisabled";
      }
    }
  } else if (currForm == "frmNewUserOnboardVerificationKA") {
    if (frmNewUserOnboardVerificationKA.txtOTP.text !== null) {
      if (frmNewUserOnboardVerificationKA.txtOTP.text.length == gblMaxOTPLength) {
        frmNewUserOnboardVerificationKA.flxNext.setEnabled(true);
        frmNewUserOnboardVerificationKA.lblNext.skin = "sknLblNextEnabled";
        frmNewUserOnboardVerificationKA.flxProgress1.left = "50%";
      } else {
        frmNewUserOnboardVerificationKA.flxNext.setEnabled(false);
        frmNewUserOnboardVerificationKA.lblNext.skin = "sknLblNextDisabled";
        frmNewUserOnboardVerificationKA.flxProgress1.left = "35%";
      }
    }
  }
}

function scanCardTimerFn() //nested function
{
  kony.print("method call;ed");
   	finish();
}

function onTextChangeScanCard(data){
  //onTextChangeCardNum();
  if(frmRegisterUser.txtCardNum.text.length == 16)
  {
    frmRegisterUser.flxLine.skin = "sknFlxGreenLine";
    frmRegisterUser.txtPIN.setFocus(true);
    frmRegisterUser.flxLinee1.skin = sknBlueflxLine;
  }
  isNextEnabled();
  isClearButtonVisible(frmRegisterUser.txtCardNum,frmRegisterUser.lblClose);
  frmRegisterUser.lblInvalidCredentialsKA.setVisibility(false);
  if (frmRegisterUser.txtCardNum.text !== null && frmRegisterUser.txtPIN.text !== null && frmRegisterUser.txtMobileNumber.text !== null) {
    var cardNo = getCardNum(frmRegisterUser.txtCardNum.text);
    if (cardNo.length == 16 && frmRegisterUser.txtPIN.text.length == 4) {
      frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
    }
  }else{
    frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
  }
  if(data != undefined && data !== null && data !== ""){
    frmRegisterUser.txtCardNum.text ="";
    frmRegisterUser.txtCardNum.text = data;
  }
  frmRegisterUser.forceLayout();

}

/*
Author : Vineeth Vishwanath
Date : 22-01-18
Desc : On text change of card num
*/
function onTextChangeCardNum() {
  var len = frmRegisterUser.txtCardNum.text.length;
  var cardNo = getCardNum(frmRegisterUser.txtCardNum.text);

  if (cardNo != gblCardNo) {
    gblCardNo = cardNo;
    /*if(len == 4 || len == 9 || len == 14){
                  frmRegisterUser.txtCardNum.text = frmRegisterUser.txtCardNum.text + " ";

                }
                */
    frmRegisterUser.txtCardNum.text = "";

    if (cardNo.length >= 12) {
      frmRegisterUser.txtCardNum.text = cardNo.slice(0, 4) + " " + cardNo.slice(4, 8) + " " + cardNo.slice(8, 12) + " " + cardNo.slice(12);
    } else if (cardNo.length >= 8) {
      frmRegisterUser.txtCardNum.text = cardNo.slice(0, 4) + " " + cardNo.slice(4, 8) + " " + cardNo.slice(8);
    } else if (cardNo.length >= 4) {
      frmRegisterUser.txtCardNum.text = cardNo.slice(0, 4) + " " + cardNo.slice(4);
    } else {
      frmRegisterUser.txtCardNum.text = cardNo;
    }

    if (cardNo.length == 16) {
      frmRegisterUser.btnDummy.setFocus = true;
    }
  }
}

/*
Author : Vineeth Vishwanath
Date : 22-01-18
Desc : Removing spaces from card number
*/
function getCardNum(text) {
  return text.replace(/ /g, "");
}


/*
Author : Vineeth Vishwanath
Date : 22-01-18
Desc : Working with PIN labels
*/
function onTextChangePIN() {
  var len = frmRegisterUser.txtPIN.text.length;
  kony.print("frmRegisterUser.txtPIN.text.length :: " + frmRegisterUser.txtPIN.text.length);
  var langSelected = kony.store.getItem("langPrefObj");

  if (langSelected == "en") {

    if (len === 0) {
      frmRegisterUser.lblCircle1.text = "";
      frmRegisterUser.flxLinee1.skin = sknFlxGreyLine;
    } else if (len == 1) {
      frmRegisterUser.lblCircle1.text = "*";
      frmRegisterUser.lblCircle2.text = "";
    } else if (len == 2) {
      frmRegisterUser.lblCircle1.text = "*";
      frmRegisterUser.lblCircle2.text = "*";
      frmRegisterUser.lblCircle3.text = "";
    } else if (len == 3) {
      frmRegisterUser.lblCircle1.text = "*";
      frmRegisterUser.lblCircle2.text = "*";
      frmRegisterUser.lblCircle3.text = "*";
      frmRegisterUser.lblCircle4.text = "";
    } else if (len == 4) {
      frmRegisterUser.txtPIN.setFocus = false;
      frmRegisterUser.lblCircle1.text = "*";
      frmRegisterUser.lblCircle2.text = "*";
      frmRegisterUser.lblCircle3.text = "*";
      frmRegisterUser.lblCircle4.text = "*";
    }
     for (var i = 1; i <= 4; i++) {
      var lineName = "flxLinee" + i;
      if (i == len + 1) {
        frmRegisterUser[lineName].skin = sknBlueflxLine;
      } else if (i <= len) {
        frmRegisterUser[lineName].skin = sknFlxGreenLine;
      } else {
        frmRegisterUser[lineName].skin = sknFlxGreyLine;
      }
    }
    if (len === 0) {
       frmRegisterUser.flxLinee1.skin = sknFlxGreyLine;
    }
  } else {
    if (len === 0) {
      //frmRegisterUser.lblCircle4.skin = "sknPINEmpty";
      frmRegisterUser.flxLinee4.skin = sknFlxGreyLine;
      frmRegisterUser.lblCircle4.text = "";
    } else if (len == 1) {
      //frmRegisterUser.lblCircle4.skin = "sknPINEntered";
      frmRegisterUser.lblCircle4.text = "*";
      //frmRegisterUser.lblCircle3.skin = "sknPINEmpty";
      frmRegisterUser.lblCircle3.text = "";
    } else if (len == 2) {
      //frmRegisterUser.lblCircle4.skin = "sknPINEntered";
      frmRegisterUser.lblCircle4.text = "*";
      //frmRegisterUser.lblCircle3.skin = "sknPINEntered";
      frmRegisterUser.lblCircle3.text = "*";
      //frmRegisterUser.lblCircle2.skin = "sknPINEmpty";
      frmRegisterUser.lblCircle2.text = "";
    } else if (len == 3) {
      //frmRegisterUser.lblCircle4.skin = "sknPINEntered";
      frmRegisterUser.lblCircle4.text = "*";
     // frmRegisterUser.lblCircle3.skin = "sknPINEntered";
      frmRegisterUser.lblCircle3.text = "*";
      //frmRegisterUser.lblCircle2.skin = "sknPINEntered";
      frmRegisterUser.lblCircle2.text = "*";
      //frmRegisterUser.lblCircle1.skin = "sknPINEmpty";
      frmRegisterUser.lblCircle1.text = "";
    } else if (len == 4) {
      frmRegisterUser.txtPIN.setFocus = false;
      //frmRegisterUser.lblCircle4.skin = "sknPINEntered";
      frmRegisterUser.lblCircle4.text = "*";
      //frmRegisterUser.lblCircle3.skin = "sknPINEntered";
      frmRegisterUser.lblCircle3.text = "*";
      //frmRegisterUser.lblCircle2.skin = "sknPINEntered";
      frmRegisterUser.lblCircle2.text = "*";
      //frmRegisterUser.lblCircle1.skin = "sknPINEntered";
      frmRegisterUser.lblCircle1.text = "*";
      
    }
    for (var i = 1; i <= 4; i++) {
        var lineName = "flxLinee" + i;
        if (i == 4 - len) {
          frmRegisterUser[lineName].skin = sknBlueflxLine;
        } else if (i > 4 - len) {
          frmRegisterUser[lineName].skin = sknFlxGreenLine;
        } else {
          frmRegisterUser[lineName].skin = sknFlxGreyLine;
        }
      }
     if (len === 0) {
      frmRegisterUser.flxLinee4.skin = sknFlxGreyLine;
     }
  }
     
}


/*
Author : Vineeth Vishwanath
Date : 22-01-18
Desc : deleting character by character
*/
function backspace(widgetName) {
  var currForm = kony.application.getCurrentForm();

  if (currForm[widgetName].text !== "" || currForm[widgetName].text !== null) {
    //currForm[widgetName].text = currForm[widgetName].text.substr(0,currForm[widgetName].text.length-1);
    currForm[widgetName].text = "";
  }
}


function isClearButtonVisible(widget, clear) {
  if (widget.text != "") {
    clear.setVisibility(true);
  } else {
    clear.setVisibility(false);
  }
}

function set_SHOW_BUTTON_VISIBILITY(widget, target) {
  if (widget.text != "") {
    target.setVisibility(true);
  } else {
    target.setVisibility(false);
  }
}

function serv_CustomerDetails(customerID){
  try{
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("User", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("User");
    //         dataObject.addField("usr", "icbsserv");
    //         dataObject.addField("pass", "icbsserv_1");
    dataObject.addField("custId", customerID);
    dataObject.addField("language", (kony.store.getItem("langPrefObj")==="en"?"eng":"ara"));
    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    if (kony.sdk.isNetworkAvailable()) {
      kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      modelObj.customVerb("get", serviceOptions, function(res){kony.print("Customer Details Success ::"+JSON.stringify(res));kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                                                               if(customerAccountDetails.profileDetails !== null){
                                                                 customerAccountDetails.profileDetails = null;
                                                               }
                                                               kony.print(res.records[0].custData[0]);
                                                               if(res!==undefined &&  res.records[0] !== undefined && res.records[0].custData !== null){
                                                                 res.records[0].custData[0].maskedSMSno = mask_MobileNumber(res.records[0].custData[0].smsno);
                                                                 customerAccountDetails.profileDetails = res.records[0].custData[0];
                                                               }
                                                               if (gblFromModule == "RegisterUser" || gblFromModule == "RetriveUsername") {
                                                                 callRequestOTP();
                                                               }
                                                              },function(err){
        kony.print("Customer Details Failed ::"+JSON.stringify(err));
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customerAccountDetails.profileDetails = null;
        if (gblFromModule == "RegisterUser") {
          callRequestOTP();
        }});
    }else{
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
  }catch(e){
    kony.print("Exception_serv_CustomerDetails ::"+e);
    exceptionLogCall("serv_CustomerDetails","UI ERROR","UI",e);
  }
}