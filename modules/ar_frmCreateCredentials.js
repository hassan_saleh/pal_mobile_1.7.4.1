//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:40 EEST 2020
function addWidgetsfrmCreateCredentialsAr() {
frmCreateCredentials.setDefaultUnit(kony.flex.DP);
var FlxMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxMain",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
FlxMain.setDefaultUnit(kony.flex.DP);
var titleBarSignUp = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "titleBarSignUp",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntitleBarGradient",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarSignUp.setDefaultUnit(kony.flex.DP);
var btnCancel = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "skntitleBarTextButtonFocus",
"height": "50dp",
"id": "btnCancel",
"isVisible": true,
"right": "0dp",
"minWidth": "50dp",
"onClick": AS_Button_ec092d3d621e413cbd3305b390d4a2c0,
"skin": "skntitleBarTextButton",
"text": kony.i18n.getLocalizedString("i18n.common.cancel"),
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblSignup = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblSignup",
"isVisible": true,
"skin": "sknnavBarTitle",
"text": kony.i18n.getLocalizedString("i18n.NewUserOnboarding.SignUp"),
"width": "70%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
titleBarSignUp.add(btnCancel, lblSignup);
var MainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "92%",
"horizontalScrollIndicator": true,
"id": "MainContent",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkgFAFAFAKA",
"top": "8%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
MainContent.setDefaultUnit(kony.flex.DP);
var FlxContent = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "75%",
"id": "FlxContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "5dp",
"left": "10dp",
"skin": "skncontainerFAFAFAKA",
"top": "1%",
"width": "90%",
"zIndex": 1
}, {}, {});
FlxContent.setDefaultUnit(kony.flex.DP);
var flxNewUserID = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxNewUserID",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerFAFAFAKA",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxNewUserID.setDefaultUnit(kony.flex.DP);
var lblUserID = new kony.ui.Label({
"id": "lblUserID",
"isVisible": true,
"right": "5%",
"skin": "sknLatoRegular7C7C7CKA",
"text": kony.i18n.getLocalizedString("i18n.login.usenamePlh"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxUserID = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "flxUserID",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxUserID.setDefaultUnit(kony.flex.DP);
var txtboxUsernameKA = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "txtboxUsernameKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"onTextChange": AS_TextField_c56a41ffc2f3403d940e031de6716f71,
"placeholder": kony.i18n.getLocalizedString("i18n.NUO.Enterusernamehere"),
"left": 0,
"secureTextEntry": false,
"skin": "sknTbxNoBG",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0dp",
"width": "80%"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxUserID.add(txtboxUsernameKA);
var lblLineKA2 = new kony.ui.Label({
"bottom": "1%",
"centerX": "50%",
"height": "1dp",
"id": "lblLineKA2",
"isVisible": true,
"right": "5%",
"skin": "sknLineEDEDEDKA",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "90%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblNotAvailable = new kony.ui.Label({
"height": "32dp",
"id": "lblNotAvailable",
"isVisible": false,
"left": "5%",
"skin": "sknD0021BFLatoRegular",
"text": kony.i18n.getLocalizedString("i18n.NUO.NotAvailable"),
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAvailable = new kony.ui.Label({
"height": "32dp",
"id": "lblAvailable",
"isVisible": false,
"left": "5%",
"skin": "sknlbl2ebaee",
"text": kony.i18n.getLocalizedString("i18n.NUO.Available"),
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblUsernameErr = new kony.ui.Label({
"height": "32dp",
"id": "lblUsernameErr",
"isVisible": false,
"left": "5%",
"skin": "sknD0021BFLatoRegular",
"text": "Invalid Username",
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxNewUserID.add(lblUserID, flxUserID, lblLineKA2, lblNotAvailable, lblAvailable, lblUsernameErr);
var flxUsernameRules = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "15%",
"id": "flxUsernameRules",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxUsernameRules.setDefaultUnit(kony.flex.DP);
var flxUsernameRule1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxUsernameRule1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"left": "5%",
"skin": "sknslFbox",
"top": "1dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxUsernameRule1.setDefaultUnit(kony.flex.DP);
var ImgUserRule1 = new kony.ui.Image2({
"height": "25dp",
"id": "ImgUserRule1",
"isVisible": true,
"right": "0dp",
"skin": "slImage",
"src": "combinedshape.png",
"top": "3dp",
"width": "25dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblUsernameRule1 = new kony.ui.Label({
"id": "lblUsernameRule1",
"isVisible": true,
"right": "8%",
"skin": "sknlblD0021BregularKA",
"text": "Username should be 8-24 characters long and should not contain &, %, <, >, +, ' or |",
"top": "6dp",
"width": "90%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxUsernameRule1.add(ImgUserRule1, lblUsernameRule1);
flxUsernameRules.add(flxUsernameRule1);
var flxPassword = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxPassword",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerFAFAFAKA",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxPassword.setDefaultUnit(kony.flex.DP);
var lblPassword = new kony.ui.Label({
"id": "lblPassword",
"isVisible": true,
"right": "5%",
"skin": "sknLatoRegular7C7C7CKA",
"text": kony.i18n.getLocalizedString("i18n.login.passwordPlh"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxEnterPassword = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "flxEnterPassword",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxEnterPassword.setDefaultUnit(kony.flex.DP);
var answerFieldPassword = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "answerFieldPassword",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0dp",
"onTextChange": AS_TextField_b8853eb45aee40528409bd43cdefddd7,
"placeholder": kony.i18n.getLocalizedString("i18n.NUO.Enterpasswordhere"),
"left": 0,
"secureTextEntry": true,
"skin": "sknTbxNoBG",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0dp",
"width": "80%"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxImgEyeWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "30dp",
"id": "flxImgEyeWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "87%",
"onTouchEnd": AS_FlexContainer_fba54a2799bd4dd29921ba3ca12db86a,
"onTouchStart": AS_FlexContainer_i11171fdd0be44dcb1d6a1edf6b76ffc,
"skin": "slFbox",
"top": "1dp",
"width": "15%",
"zIndex": 1
}, {}, {});
flxImgEyeWrapper.setDefaultUnit(kony.flex.DP);
var imgEye = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"height": "100%",
"id": "imgEye",
"isVisible": true,
"skin": "slImage",
"src": "eye.png",
"width": "100%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxImgEyeWrapper.add(imgEye);
flxEnterPassword.add(answerFieldPassword, flxImgEyeWrapper);
var lblLineKA = new kony.ui.Label({
"bottom": "1%",
"centerX": "50%",
"height": "1dp",
"id": "lblLineKA",
"isVisible": true,
"right": "5%",
"skin": "sknLineEDEDEDKA",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "90%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPasswordNotMatch = new kony.ui.Label({
"height": "25dp",
"id": "lblPasswordNotMatch",
"isVisible": false,
"left": "5%",
"skin": "sknD0021BFLatoRegular",
"text": kony.i18n.getLocalizedString("i18n.NUO.Passworddonotmatch"),
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxPassword.add(lblPassword, flxEnterPassword, lblLineKA, lblPasswordNotMatch);
var flxPasswordRules = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "125dp",
"id": "flxPasswordRules",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxPasswordRules.setDefaultUnit(kony.flex.DP);
var flxRule1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxRule1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"left": "5%",
"skin": "sknslFbox",
"top": "1dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxRule1.setDefaultUnit(kony.flex.DP);
var ImgRule1 = new kony.ui.Image2({
"height": "25dp",
"id": "ImgRule1",
"isVisible": true,
"right": "0dp",
"skin": "slImage",
"src": "combinedshape.png",
"top": "3dp",
"width": "25dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblRule1 = new kony.ui.Label({
"id": "lblRule1",
"isVisible": true,
"right": "8%",
"skin": "sknlblD0021BregularKA",
"text": "Length should be between 8-24 characters.",
"top": "6dp",
"width": "90%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxRule1.add(ImgRule1, lblRule1);
var flxRule2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxRule2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"left": "5%",
"skin": "sknslFbox",
"top": "1dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxRule2.setDefaultUnit(kony.flex.DP);
var ImgRule2 = new kony.ui.Image2({
"height": "25dp",
"id": "ImgRule2",
"isVisible": true,
"right": "0dp",
"skin": "slImage",
"src": "combinedshape.png",
"top": "3dp",
"width": "25dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblRule2 = new kony.ui.Label({
"id": "lblRule2",
"isVisible": true,
"right": "8%",
"skin": "sknlblD0021BregularKA",
"text": "Should contain one number, one symbol, one upper case and one lower case letter.",
"top": "6dp",
"width": "90%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxRule2.add(ImgRule2, lblRule2);
var flxRule3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxRule3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"left": "5%",
"skin": "sknslFbox",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxRule3.setDefaultUnit(kony.flex.DP);
var imgRule3 = new kony.ui.Image2({
"height": "25dp",
"id": "imgRule3",
"isVisible": true,
"right": "0dp",
"skin": "slImage",
"src": "combinedshape.png",
"top": "3dp",
"width": "25dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblRule3 = new kony.ui.Label({
"id": "lblRule3",
"isVisible": true,
"right": "8%",
"skin": "sknlblD0021BregularKA",
"text": "Should not contain &, %, <, >, +, , ', \\, /, or | ",
"top": "4dp",
"width": "90%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxRule3.add(imgRule3, lblRule3);
flxPasswordRules.add(flxRule1, flxRule2, flxRule3);
var flxReenterPassword = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxReenterPassword",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerFAFAFAKA",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxReenterPassword.setDefaultUnit(kony.flex.DP);
var lblReEnterPassword = new kony.ui.Label({
"id": "lblReEnterPassword",
"isVisible": true,
"right": "5%",
"skin": "sknLatoRegular7C7C7CKA",
"text": kony.i18n.getLocalizedString("i18n.NUO.Re-enterpassword"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxTypePassword = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60%",
"id": "flxTypePassword",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxTypePassword.setDefaultUnit(kony.flex.DP);
var PasswordReEnter = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "PasswordReEnter",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0dp",
"onTextChange": AS_TextField_ddb7ec282d554c1bbca7d924f0ebe45f,
"placeholder": kony.i18n.getLocalizedString("i18n.NUO.Reenterpasswordhere"),
"left": 0,
"secureTextEntry": true,
"skin": "sknTbxNoBG",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0dp",
"width": "80%"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var imgTick = new kony.ui.Image2({
"height": "30dp",
"id": "imgTick",
"isVisible": true,
"right": "90%",
"skin": "slImage",
"src": "tickgray.png",
"top": "0%",
"width": "30dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxTypePassword.add(PasswordReEnter, imgTick);
var lblLineKA3 = new kony.ui.Label({
"centerX": "50%",
"height": "1dp",
"id": "lblLineKA3",
"isVisible": true,
"right": "5%",
"skin": "sknLineEDEDEDKA",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxReenterPassword.add(lblReEnterPassword, flxTypePassword, lblLineKA3);
var btnCreateandContinue = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknprimaryActionFocus",
"height": "42dp",
"id": "btnCreateandContinue",
"isVisible": true,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.NUO.SIGNUPCAPS"),
"top": "7.00%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxContent.add(flxNewUserID, flxUsernameRules, flxPassword, flxPasswordRules, flxReenterPassword, btnCreateandContinue);
var tabsWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1%",
"id": "tabsWrapper",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
tabsWrapper.setDefaultUnit(kony.flex.DP);
var tabSelectedIndicator1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "50%",
"id": "tabSelectedIndicator1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxFFA500KA",
"width": "10.15%"
}, {}, {});
tabSelectedIndicator1.setDefaultUnit(kony.flex.DP);
tabSelectedIndicator1.add();
var tabSelectedIndicator2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "50%",
"id": "tabSelectedIndicator2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2.65%",
"skin": "sknflxc1c1c1KA",
"width": "10.15%"
}, {}, {});
tabSelectedIndicator2.setDefaultUnit(kony.flex.DP);
tabSelectedIndicator2.add();
var tabSelectedIndicator3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "50%",
"id": "tabSelectedIndicator3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2.65%",
"skin": "sknflxc1c1c1KA",
"width": "10.15%"
}, {}, {});
tabSelectedIndicator3.setDefaultUnit(kony.flex.DP);
tabSelectedIndicator3.add();
var tabSelectedIndicator4 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "50%",
"id": "tabSelectedIndicator4",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2.65%",
"skin": "sknflxc1c1c1KA",
"width": "10.15%"
}, {}, {});
tabSelectedIndicator4.setDefaultUnit(kony.flex.DP);
tabSelectedIndicator4.add();
var tabSelectedIndicator5 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "50%",
"id": "tabSelectedIndicator5",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2.65%",
"skin": "sknflxc1c1c1KA",
"width": "10.15%"
}, {}, {});
tabSelectedIndicator5.setDefaultUnit(kony.flex.DP);
tabSelectedIndicator5.add();
var tabSelectedIndicator6 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "50%",
"id": "tabSelectedIndicator6",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2.65%",
"skin": "sknflxc1c1c1KA",
"width": "10.15%"
}, {}, {});
tabSelectedIndicator6.setDefaultUnit(kony.flex.DP);
tabSelectedIndicator6.add();
var tabSelectedIndicator7 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "50%",
"id": "tabSelectedIndicator7",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2.65%",
"skin": "sknflxc1c1c1KA",
"width": "10.15%"
}, {}, {});
tabSelectedIndicator7.setDefaultUnit(kony.flex.DP);
tabSelectedIndicator7.add();
var tabSelectedIndicator8 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "50%",
"id": "tabSelectedIndicator8",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2.65%",
"skin": "sknflxc1c1c1KA",
"width": "10.15%"
}, {}, {});
tabSelectedIndicator8.setDefaultUnit(kony.flex.DP);
tabSelectedIndicator8.add();
tabsWrapper.add( tabSelectedIndicator8, tabSelectedIndicator7, tabSelectedIndicator6, tabSelectedIndicator5, tabSelectedIndicator4, tabSelectedIndicator3, tabSelectedIndicator2,tabSelectedIndicator1);
MainContent.add(FlxContent, tabsWrapper);
FlxMain.add(titleBarSignUp, MainContent);
frmCreateCredentials.add(FlxMain);
};
function frmCreateCredentialsGlobalsAr() {
frmCreateCredentialsAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmCreateCredentialsAr,
"bounces": false,
"enabledForIdleTimeout": true,
"id": "frmCreateCredentials",
"init": AS_Form_j7039f3d99df4354832c2e4870089722,
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"postShow": AS_Form_ac11e2b45b0c4871bceba8b945975206,
"preShow": AS_Form_h798d20cfe8a44e68d8cb1c9d3b375f3,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_b106a78a27cd49daa7186f3b509e81ac,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
