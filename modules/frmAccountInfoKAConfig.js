var frmAccountInfoKAConfig = {
    "formid": "frmAccountInfoKA",
    "frmAccountInfoKA": {
        "entity": "Accounts",
        "objectServiceName": "RBObjects",
        "objectServiceOptions" : {"access":"online"},
    },
	"accountNicknameTextfield": {
        "fieldprops": {
            "entity": "Accounts",
            "field": "nickName",
            "widgettype": "TextField"
        }
    },
    "accountNumberLabel": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Accounts",
            "field": "accountID"
        }
    },
	"routingNumberLabel": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Accounts",
            "field": "accountID"
        }
    },
	"interestRateLabel": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Accounts",
            "field": "interestRate"
        }
    },
	"interestEarnedLabel": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Accounts",
            "field": "interestRate"
        }
    },
  	"CopyaccountNumberLabel036c71ac511b84d": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Accounts",
            "field": "accountID"
        }
    },
    "CopyroutingNumberLabel06e90c7d5900842": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Accounts",
            "field": "interestRate"
        }
    },
    "CopyinterestRateLabel09f06e1600f0e43": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Accounts",
            "field": "currentBalance"
        }
    },
    "CopyinterestRateLabel0b14fb4af446e45": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Accounts",
            "field": "currentBalance"
        }
    },
    "CopyinterestEarnedLabel00b76261e56e741": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Accounts",
            "field": "currentBalance"
        }
    },
   "CopyinterestEarnedLabel0daf983bec94d4c": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Accounts",
            "field": "currentBalance"
        }
    },
     "CopyaccountNumberLabel0b9e42672fcff47": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Accounts",
            "field": "availableBalance"
        }
    },   "CopyroutingNumberLabel07e049f0450604c": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Accounts",
            "field": "currentBalance"
        }
    },   "CopyinterestRateLabel0cf90d54edd014c": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "Accounts",
            "field": "accountID"
        }
    }
}