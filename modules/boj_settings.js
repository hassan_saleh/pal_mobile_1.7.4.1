function changeLanguage(lang) {
    if (kony.sdk.isNetworkAvailable()) {
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };

        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("User", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("User");

        dataObject.addField("custId", custid);
        dataObject.addField("language", lang);

        kony.print("dataobject - Language change :: " + JSON.stringify(dataObject));
        //alert("dataobject - Language change :: "+ JSON.stringify(dataObject));

        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        modelObj.customVerb("UpdateLanguage", serviceOptions, langChangeSuccess, langChangeFailure);
    } else { //Network not available
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        alert("" + kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"));
    }

}


function langChangeSuccess(res) {
    try {
        kony.application.dismissLoadingScreen();
        kony.print("Language res:: " + JSON.stringify(res));
        if (!isEmpty(res)) {
            if (res.statusType == "S") {

                if (kony.store.getItem("langPrefObj") === "ar") {
                    kony.print("Calling Switch to English Layout");
                    var engLocale = "en";
                    kony.i18n.setCurrentLocaleAsync(engLocale, onEngChangeSuccesscallback, onEngChangeFailurecallback);


                } else if (kony.store.getItem("langPrefObj") === "en") {
                    // switchToDefaultLayout(rightToLeftCallback);
                    kony.print("Calling Switch to Arabic Layout");
                    var arabicLocale = "ar";
                    kony.i18n.setCurrentLocaleAsync(arabicLocale, onArabicChangeSuccesscallback, onArabicChangeFailurecallback);
                }

                kony.print("Before Custom Alert Popup Language change Success Callback");



            } else {
                customAlertPopup(geti18nkey("i18n.NUO.Error"),
                    geti18Value("i18n.common.somethingwentwrong"),
                    popupCommonAlertDimiss, "");
            }
        } else {
            kony.print("lang change err found undefined");
            customAlertPopup(geti18nkey("i18n.NUO.Error"),
                geti18Value("i18n.common.somethingwentwrong"),
                popupCommonAlertDimiss, "");

        }
        //alert("Not Success recieceved the fees ::" + JSON.stringify(err));
    } catch (e) {
        kony.print("Exception in SuccessCallback of Language Change::" + e);
        exceptionLogCall("langChangeSuccess", "UI ERROR", "UI", e);
    }
}


function onEngChangeSuccesscallback() {
    try {
        kony.print(" --- Inside onEngChangeSuccesscallback ---");
        kony.store.setItem("langPrefObj", "en");
        switchToDefaultLayout(rightToLeftCallback);
        customAlertPopup(geti18nkey("i18n.common.success"),
            geti18nkey("i18n.common.langchange") + ". " + geti18nkey("i18n.common.loginagain"),
            function() {
                popupCommonAlertDimiss();
                kony.sdk.mvvm.LogoutAction();
            }, "");
    } catch (err) {
        switchToDefaultLayout(rightToLeftCallback);
        popupCommonAlertDimiss();
        kony.sdk.mvvm.LogoutAction();
        exceptionLogCall("onEngChangeSuccesscallback", "UI ERROR", "UI", err);
        kony.print(" --- Error ---" + err);
    }
    kony.print(" --- Exit of onEngChangeSuccesscallback ---");
}

function onEngChangeFailurecallback() {
    kony.print(" --- Inside onEngChangeFailurecallback ---");
    switchToDefaultLayout(rightToLeftCallback);
    popupCommonAlertDimiss();
    kony.sdk.mvvm.LogoutAction();

}




function onArabicChangeSuccesscallback() {
    try {
        kony.print(" --- Inside onArabicChangeSuccesscallback ---");
        kony.store.setItem("langPrefObj", "ar");
        switchToArabicLayout(rightToLeftCallback);
        customAlertPopup(geti18nkey("i18n.common.success"),
            geti18nkey("i18n.common.langchange") + ". " + geti18nkey("i18n.common.loginagain"),
            function() {
                popupCommonAlertDimiss();
                kony.sdk.mvvm.LogoutAction();

            }, "");
    } catch (err) {
        kony.print(" --- Error ---" + err);
        exceptionLogCall("onArabicChangeSuccesscallback", "UI ERROR", "UI", err);
        switchToArabicLayout(rightToLeftCallback);
        popupCommonAlertDimiss();
        kony.sdk.mvvm.LogoutAction();

    }
    kony.print(" --- Exit of onArabicChangeSuccesscallback ---");

}

function onArabicChangeFailurecallback() {
    kony.print(" --- Inside onArabicChangeFailurecallback ---");
    switchToArabicLayout(rightToLeftCallback);
    popupCommonAlertDimiss();
    kony.sdk.mvvm.LogoutAction();

}


function langChangeFailure(err) {
    kony.application.dismissLoadingScreen();
    if (isEmpty(err)) {
        kony.print("lang change err found undefined");
        customAlertPopup(geti18nkey("i18n.NUO.Error"),
            geti18Value("i18n.common.somethingwentwrong"),
            popupCommonAlertDimiss, "");
    } else {
        customAlertPopup(geti18nkey("i18n.NUO.Error"),
            geti18Value("i18n.common.somethingwentwrong"),
            popupCommonAlertDimiss, "");

    }
    //alert("Not Success recieceved the fees ::" + JSON.stringify(err));
}