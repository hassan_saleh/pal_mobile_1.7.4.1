var dueAmount;
var selData=[];
var selDataPostpaid=[];
var selDataPrepaid=[];
var totalAmounts = 0;
var tmpData = [];
var postpaiTotalAmount = 0;
var prepaidTotalAmount = 0;
var checkPostpaid = 0;
var tempPrepaid = { "custid":"",
                    "lang":"",
                    "p_biller_code":"",
                    "p_serv_type_code":"",
                    "p_billing_no":"",
                    "P_deno":"",
                    "amount":"",
                    "loop_seperator": ":",
                    "loop_count":""};
/*function getSelectedBills(){
  var data;
  data = frmManagePayeeKA.managepayeesegment.data;
  
  // frmBulkPaymentKA.bulkpaymentsegmant.setData([]);
  selData = [];
  
  frmBulkPaymentKA.bulkpaymentsegmant.widgetDataMap = { 
    payeename: "NickName",
    accountnumber :"BillingNumber",
    lblBillerType: "BillerCode",
    lblInitial: "initial",
    flxIcon1: "icon",
    btnBillsPayAccounts:"btnSetting",
    dueAmount:"dueamount"// hassan 
  };
  
  for(var i in data){
    tmpData = [];
    if (data[i].btnSetting.text === "p"){
        kony.boj.getBulkBillNumberDetails(data[i]);
        data[i].dueamount = dueAmount;
        selData.push(data[i]);
    }
  }
  
  frmBulkPaymentKA.bulkpaymentsegmant.setData(selData);
  kony.print("hassan data daaata "+JSON.stringify(selData));
  
  frmBulkPaymentKA.show();
}
*/


function serv_getBulkBillNumberDetails(){
	try{
        selData=[];
    	var servData = prepare_LOOPSERVICEDATA_BULK_PAYMENT();
    	kony.print("remit Serv data ::"+JSON.stringify(servData));
    	var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("LoopBulkPayment");
      	if (kony.sdk.isNetworkAvailable()) {
          kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
          appMFConfiguration.invokeOperation("LoopBulkPayment", {}, 
                                           servData, BulkBillNumberDetailsSuccess,function(err){kony.print("Error Data ::"+JSON.stringify(err));kony.application.dismissLoadingScreen();});
        }
    }catch(e){
    	kony.print("Exception_serv_getBulkBillNumberDetails ::"+e);
    }
}


function serv_getBulkPrepaidBillNumberValidation(){
	try{
    	var servData = prepare_LOOPSERVICEDATA_BULK_PAYMENT();
        var prepaidServData = tempPrepaid;
    	kony.print("remit prepaid Serv Data ::"+JSON.stringify(prepaidServData));
    	var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("LoopBulkPaymentPrePaid");
      	if (kony.sdk.isNetworkAvailable()) {
          kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
          appMFConfiguration.invokeOperation("LoopBulkPaymentPrePaid", {}, 
                                           prepaidServData, BulkPrepaidBillNumberValSuccess,function(err){kony.print("Error Data ::"+JSON.stringify(err));kony.application.dismissLoadingScreen();});
        }
    }catch(e){
    	kony.print("Exception_serv_getBulkPrepaidBillNumberValidation ::"+e);
    }
}



function prepare_LOOPSERVICEDATA_BULK_PAYMENT(){
	try{
        selDataPostpaid=[];
        selDataPrepaid=[];
      
        var bulkPaymentData = kony.boj.AllPayeeList;//frmManagePayeeKA.managepayeesegment.data;
        kony.print("bulkPaymentData ::"+JSON.stringify(bulkPaymentData));
        tempPrepaid = { "custId":"",
                        "lang":"",
                        "p_biller_code":"",
                        "p_serv_type_code":"",
                        "p_billing_no":"",
                        "P_deno":"",
                        "amount":"",
                        "loop_seperator": ":",
                        "loop_count":""};
      
    	
    	var temp = {"custId":"",
                    "lang":"",
                    "p_BillerCode":"",
                    "p_ServiceType":"",
                    "p_BillingNo":"",
                    "p_BillNo":"",
                    "p_DateFlag":"",
                    "p_StartDt":"",
                    "p_EndDt":"",
                    "p_CustInfoFlag":"",
                    "p_fcdb_ref_no":"",
                    "p_IdType":"",
                    "p_ID":"",
                    "p_Nation":"",
                    "p_IncPayments":"",
                    "p_fcdb_ref_no":"",
                    "loop_seperator": ":",
                    "loop_count":""};
    	var langObj = kony.store.getItem("langPrefObj");
    	if(langObj.toUpperCase() === "EN"){
        	langObj = "eng";
        }else{
        	langObj = "ara";
        }
        var selFields = 0;
        var selFieldsPrepaid = 0;
      
      
    	for(var i in bulkPaymentData){
          if (bulkPaymentData[i].category === "postpaid"){
               if (bulkPaymentData[i].btnSetting.text === "p"){
                  if (selFields === "0" || selFields == 0){
                    temp.custId = temp.custId+""+custid;
                    temp.lang = temp.lang+""+langObj;
                    temp.p_BillerCode = temp.p_BillerCode+""+bulkPaymentData[i].BillerCode;
                    temp.p_ServiceType = temp.p_ServiceType+""+bulkPaymentData[i].ServiceType;
                    temp.p_BillingNo = temp.p_BillingNo+""+bulkPaymentData[i].BillingNumber;
                    temp.p_BillNo = temp.p_BillNo+""+bulkPaymentData[i].BillingNumber;
                    temp.p_DateFlag = temp.p_DateFlag+""+"N"; 
                    temp.p_CustInfoFlag = temp.p_CustInfoFlag+""+"Y";
                    temp.p_IncPayments = temp.p_IncPayments+""+"N";
                  }else{
                    temp.custId = temp.custId+":"+custid;
                    temp.lang = temp.lang+":"+langObj;
                    temp.p_BillerCode = temp.p_BillerCode+":"+bulkPaymentData[i].BillerCode;
                    temp.p_ServiceType = temp.p_ServiceType+":"+bulkPaymentData[i].ServiceType;
                    temp.p_BillingNo = temp.p_BillingNo+":"+bulkPaymentData[i].BillingNumber;
                    temp.p_BillNo = temp.p_BillNo+":"+bulkPaymentData[i].BillingNumber;
                    temp.p_DateFlag = temp.p_DateFlag+":"+"N";
                    temp.p_CustInfoFlag = temp.p_CustInfoFlag+":"+"Y";
                    temp.p_IncPayments = temp.p_IncPayments+":"+"N";
                  }

                    selFields = selFields + 1;
                    //selData.push(bulkPaymentData[i]);
               		bulkPaymentData[i].initial = "";
               	  	bulkPaymentData[i].lblTick = "r";
                    selDataPostpaid.push(bulkPaymentData[i]);

               }
            }else{
               if (bulkPaymentData[i].btnSetting.text === "p"){
                  if (selFieldsPrepaid === "0" || selFieldsPrepaid == 0){
                    kony.print("data ::"+JSON.stringify(bulkPaymentData));
                    tempPrepaid.custId = tempPrepaid.custId+""+custid;
                    tempPrepaid.lang = tempPrepaid.lang+""+langObj;
                    tempPrepaid.p_biller_code = tempPrepaid.p_biller_code+""+bulkPaymentData[i].p_billercode;
                    tempPrepaid.p_serv_type_code = tempPrepaid.p_serv_type_code+""+bulkPaymentData[i].p_servicetype;
                    tempPrepaid.p_billing_no = tempPrepaid.p_billing_no+""+bulkPaymentData[i].p_billingno;
                    //tempPrepaid.P_deno = tempPrepaid.P_deno+""+(bulkPaymentData[i].p_denodesc === "")?"nill":bulkPaymentData[i].p_denodesc;
                    if(bulkPaymentData[i].p_denodesc === "")
                    	tempPrepaid.P_deno = tempPrepaid.P_deno+":nill";
                    else
                    	tempPrepaid.P_deno = tempPrepaid.P_deno+":"+bulkPaymentData[i].p_denodesc;
 
                    if(bulkPaymentData[i].p_dueamount !== undefined && bulkPaymentData[i].p_dueamount !== "")
                      tempPrepaid.amount = tempPrepaid.amount+""+bulkPaymentData[i].p_dueamount;
                    else
                      tempPrepaid.amount = tempPrepaid.amount+""+"0";
                  }else{
                    tempPrepaid.custId = tempPrepaid.custId+":"+custid;
                    tempPrepaid.lang = tempPrepaid.lang+":"+langObj;
                    tempPrepaid.p_biller_code = tempPrepaid.p_biller_code+":"+bulkPaymentData[i].p_billercode;
                    tempPrepaid.p_serv_type_code = tempPrepaid.p_serv_type_code+":"+bulkPaymentData[i].p_servicetype;
                    tempPrepaid.p_billing_no = tempPrepaid.p_billing_no+":"+bulkPaymentData[i].p_billingno;
                    if(bulkPaymentData[i].p_denodesc === "")
                    	tempPrepaid.P_deno = tempPrepaid.P_deno+":nill";
                    else
                    	tempPrepaid.P_deno = tempPrepaid.P_deno+":"+bulkPaymentData[i].p_denodesc;
                    
                    if(bulkPaymentData[i].p_dueamount !== undefined && bulkPaymentData[i].p_dueamount !== "")
                      tempPrepaid.amount = tempPrepaid.amount+":"+bulkPaymentData[i].p_dueamount;
                    else
                      tempPrepaid.amount = tempPrepaid.amount+":"+"0";
                  }
                  selFieldsPrepaid = selFieldsPrepaid + 1;
                  //selData.push(bulkPaymentData[i]);
               	  bulkPaymentData[i].initial = "";
               	  bulkPaymentData[i].lblTick = "r";
                  selDataPrepaid.push(bulkPaymentData[i]);
               }
            }
        }
                
    	temp.loop_count = selFields;
        tempPrepaid.loop_count = selFieldsPrepaid;

    	return temp;
    }catch(e){
    	kony.print("Exception_prepare_LOOPSERVICEDATA_REMIT_TRANSACTIONSTATUS ::"+e);
    }
}


function BulkBillNumberDetailsSuccess(res){
  checkPostpaid = 0;
  kony.print("Success BillNumberDetailPostpaid-->"+ JSON.stringify(res) );
  //alert("Success BillNumberDetailPostpaid-->"+ JSON.stringify(response) );
  kony.application.dismissLoadingScreen();
  var dueAmount = 0;
      postpaiTotalAmount = 0;
  var hiddueamount = 0;
  
  frmBulkPaymentKA.bulkpaymentsegmant.setData([]);
  frmBulkPaymentKA.bulkpaymentsegmant.widgetDataMap = {
    payeename: "NickName",
    accountnumber :"billingTitle",//BillingNumber",    
    lblBillerType: "BillerCode",
    lblInitial: "initial",
    flxIcon1: "icon",
//     lblTick:"lblTick",
    lblBulkSelection:"lblBulkSelection",
    dueAmount:"dueamount",
    paidAmount:"paidAmount",
    hiddenDueAmount:"hiddueamount"
    };
  
   if(res.LoopDataset !== undefined && res.LoopDataset.length > 0){
     
     for(var i in res.LoopDataset){
   //    if (selData[i].category === "postpaid"){
     	selDataPostpaid[i].billingTitle = selDataPostpaid[i].ServiceType+" / "+selDataPostpaid[i].BillingNumber;
     	dueAmount = 0;
     	hiddueamount = 0;
         if(res.LoopDataset[i].BillerDetails !== undefined && res.LoopDataset[i].BillerDetails !== ""){
           for(var j in res.LoopDataset[i].BillerDetails){
             //initial = res.LoopDataset[i].BillerDetails[j].payer_name.substring(0, 2).toUpperCase();
             dueAmount = 0;
     		 hiddueamount = 0;
             kony.print("hassan res.LoopDataset -->"+ JSON.stringify(res.LoopDataset[i].BillerDetails[j]) );
             if(res.LoopDataset[i].BillerDetails[j].dueamount !== undefined && res.LoopDataset[i].BillerDetails[j].dueamount !== "" && (parseFloat(res.LoopDataset[i].BillerDetails[j].dueamount) > 0)){
                  dueAmount = geti18Value("i18n.bills.DueAmount")+" :  "+setDecimal(res.LoopDataset[i].BillerDetails[j].dueamount, 3)+" JOD";
                  hiddueamount = setDecimal(res.LoopDataset[i].BillerDetails[j].dueamount, 3);
             }else{
                  dueAmount = geti18Value("i18n.bills.DueAmount")+" :  "+setDecimal(0, 3)+" JOD";
                  hiddueamount = setDecimal(0, 3);
            	  selDataPostpaid[i].btnSetting.text = "q";
             }
             postpaiTotalAmount = postpaiTotalAmount + parseFloat(res.LoopDataset[i].BillerDetails[j].dueamount.replace(/,/g,""));
           //  selData[i].dueamount = dueAmount;
            // selData[i].hiddueamount = hiddueamount;
			 selDataPostpaid[i].dueamount = dueAmount;
             selDataPostpaid[i].paidAmount = geti18Value("i18n.bulkpayment.paidamount")+" "+setDecimal(hiddueamount, 3)+" "+CURRENCY;
             selDataPostpaid[i].hiddueamount = hiddueamount;
             selDataPostpaid[i].feesamt = res.LoopDataset[i].BillerDetails[j].feesamt;
             selDataPostpaid[i].feesonbiller = res.LoopDataset[i].BillerDetails[j].feesonbiller;
             selData.push(selDataPostpaid[i]);
           }
         }else{
           	 selDataPostpaid[i].btnSetting.text = "q";
         	 selDataPostpaid[i].dueamount = geti18Value("i18n.bills.DueAmount")+" :  "+setDecimal(0, 3)+" JOD";
             selDataPostpaid[i].paidAmount = geti18Value("i18n.bulkpayment.paidamount")+" "+setDecimal(hiddueamount, 3)+" "+CURRENCY;
             selDataPostpaid[i].hiddueamount = setDecimal(0, 3);
             selDataPostpaid[i].feesamt = 0;
             selDataPostpaid[i].feesonbiller = 0;
             selData.push(selDataPostpaid[i]);
         }
     //  }
     }
     frmBulkPaymentKA.lblTotalAmount.text = setDecimal(postpaiTotalAmount, 3) +" JOD";  
     frmBulkPaymentKA.lblSelBillsAmt.text = setDecimal(postpaiTotalAmount, 3) +" JOD";
     frmBulkPaymentKA.lblHiddenSelBillsAmt.text = setDecimal(postpaiTotalAmount, 3)+"";
   }
  kony.print("selData postpaid-->"+ JSON.stringify(selData) );
  serv_getBulkPrepaidBillNumberValidation();
  //frmBulkPaymentKA.bulkpaymentsegmant.setData(selData);
  //frmBulkPaymentKA.show();
}




// PrePaid
function BulkPrepaidBillNumberValSuccess(res){
  kony.print("Success BulkPrepaidBillNumberValSuccess-->"+ JSON.stringify(res) );
  kony.print("selDataPrepaid ::"+JSON.stringify(selDataPrepaid));
  //alert("Success BillNumberDetailPostpaid-->"+ JSON.stringify(response) );
  kony.application.dismissLoadingScreen();
  var dueAmount = 0;
      prepaidTotalAmount = 0;
  var hiddueamount = 0;
  
  frmBulkPaymentKA.bulkpaymentsegmant.setData([]);
  frmBulkPaymentKA.bulkpaymentsegmant.widgetDataMap = {
    payeename: "NickName",
    accountnumber :"billingTitle",//BillingNumber",    
    lblBillerType: "BillerCode",
    lblInitial: "initial",
    flxIcon1: "icon",
//     lblTick:"lblTick",
    lblBulkSelection:"lblBulkSelection",
    dueAmount:"dueamount",
    paidAmount:"paidAmount",
    hiddenDueAmount:"hiddueamount"
    };
    
   prepaidTotalAmount = parseFloat(postpaiTotalAmount);  
  
   if(res.LoopDataset !== undefined && res.LoopDataset.length > 0){
     
     for(var i in res.LoopDataset){
      // if (selData[i].category === "prepaid"){
     	selDataPrepaid[i].billingTitle = selDataPrepaid[i].p_servicetype+" / "+selDataPrepaid[i].BillingNumber;
     	dueAmount = 0;
     	hiddueamount = 0;
         if(res.LoopDataset[i].FeeOutput !== undefined && res.LoopDataset[i].FeeOutput !== ""){
           for(var j in res.LoopDataset[i].FeeOutput){
             //initial = res.LoopDataset[i].BillerDetails[j].payer_name.substring(0, 2).toUpperCase();
			dueAmount = 0;
     		hiddueamount = 0;
             kony.print("hassan amount "+res.LoopDataset[i].FeeOutput[j].DueAmount);

             if(res.LoopDataset[i].FeeOutput[j].DueAmount !== undefined && res.LoopDataset[i].FeeOutput[j].DueAmount !== "" && (parseFloat(res.LoopDataset[i].FeeOutput[j].DueAmount) > 0)){
                  dueAmount = geti18Value("i18n.bills.DueAmount")+" :  "+setDecimal(res.LoopDataset[i].FeeOutput[j].DueAmount, 3)+" JOD";
                  hiddueamount = setDecimal(res.LoopDataset[i].FeeOutput[j].DueAmount, 3);
             }else{
                  dueAmount = geti18Value("i18n.bills.DueAmount")+" :  "+setDecimal(0, 3)+" JOD";
                  hiddueamount = setDecimal(0, 3);
             	  selDataPrepaid[i].btnSetting.text = "q";
             }
             prepaidTotalAmount = prepaidTotalAmount + parseFloat(res.LoopDataset[i].FeeOutput[j].DueAmount.replace(/,/g,""));
            // selData[i].dueamount = dueAmount;
             //selData[i].hiddueamount = hiddueamount;
             
			 selDataPrepaid[i].dueamount = dueAmount;
             selDataPrepaid[i].paidAmount = geti18Value("i18n.bulkpayment.paidamount")+" "+setDecimal(hiddueamount, 3)+" "+CURRENCY;
             selDataPrepaid[i].hiddueamount = hiddueamount;
             selDataPrepaid[i].validationCode=res.LoopDataset[i].FeeOutput[j].ValidationCode;
             selDataPrepaid[i].fee=res.LoopDataset[i].FeeOutput[j].fees;
             
             selData.push(selDataPrepaid[i]);
           }
         }
     //  }
     }
     frmBulkPaymentKA.lblTotalAmount.text = setDecimal(prepaidTotalAmount, 3) +" JOD";  
     frmBulkPaymentKA.lblSelBillsAmt.text = setDecimal(prepaidTotalAmount, 3) +" JOD";
     frmBulkPaymentKA.lblHiddenSelBillsAmt.text = setDecimal(prepaidTotalAmount, 3)+"";
   }
  
  for(var i in selData){
    selData[i].initial = selData[i].NickName.substring(0, 2).toUpperCase();
    selData[i].icon.backgroundColor = kony.boj.getBackGroundColour(selData[i].initial);
    if(!(parseFloat(selData[i].hiddueamount) > 0)){
      selData[i].lblBulkSelection = {skin:"sknBulkPaymentUnSelection",text:""};
    }else{
    	selData[i].lblBulkSelection = {skin:"sknBulkPaymentSelection",text:"r"};
    }
  }
  kony.print("selData prepaid final-->"+ JSON.stringify(selData));
  frmBulkPaymentKA.bulkpaymentsegmant.setData(selData);
  frmBulkPaymentKA.bulkpaymentsegmant.rowTemplate.paidAmount.setEnabled(false);
  gblTModule = "BulkPrePayPayment";
  check_numberOfAccounts();
  frmBulkPaymentKA.show();
  frmBulkPaymentKA.lblNext.skin = "sknLblNextDisabled";
  frmBulkPaymentKA.flxConversionAmt.setVisibility(false);
  if((parseFloat(frmBulkPaymentKA.lblHiddenSelBillsAmt.text) > 0) && frmBulkPaymentKA.lblPaymentMode.text !== geti18Value("i18n.billsPay.Accounts")){
    if(kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD")
  		serv_BILLSCOMISSIONCHARGE(frmBulkPaymentKA.lblHiddenSelBillsAmt.text.replace(/,/g,""));
    else if(check_SUFFICIENT_BALANCE(frmBulkPaymentKA.lblHiddenSelBillsAmt.text.replace(/,/g,""), kony.store.getItem("BillPayfromAcc").availableBalance)){
      	frmBulkPaymentKA.lblNext.skin = "sknLblNextEnabled";
    }else{
    	customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
        frmBulkPaymentKA.flxConversionAmt.setVisibility(false);
        frmBulkPaymentKA.lblNext.skin = "sknLblNextDisabled";
        frmBulkPaymentKA.flxUnderlinePaymentModeBulk.skin = "sknFlxGreyLine";
    }
  }
}





function selectBills(eventobject,context){
  try{
    if(!isEmpty(context)){
      kony.application.showLoadingScreen(null, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
      var segData = [];
      segData.push(frmBulkPaymentKA.bulkpaymentsegmant.selectedRowItems[0]);
      var tmpAmount = frmBulkPaymentKA.lblHiddenSelBillsAmt.text + "";
      var segAmount = segData[0].hiddueamount.replace(/,/g,"");
      var amount = tmpAmount.replace(/,/g,"");
      if (segData[0].btnSetting.text === "q" && (parseFloat(segAmount) > 0)){
        segData[0].btnSetting = {"text":"p"};
        segData[0].lblBulkSelection = {skin:"sknBulkPaymentSelection",text:"r"};
        /**segData[0].initial = "";
        segData[0].lblTick = "r";**/
        //     segData[0].icon.backgroundColor = "10BF00";
        frmBulkPaymentKA.lblHiddenSelBillsAmt.text = (parseFloat(amount)+parseFloat(segAmount))+"";
      }else if(parseFloat(segAmount) == 0 || parseFloat(segAmount) == 0.000){
        kony.print("Amount is Zero");
      }else if(segData[0].btnSetting.text === "p"){
        segData[0].btnSetting = {"text":"q"};
        segData[0].lblBulkSelection = {skin:"sknBulkPaymentUnSelection",text:""};
       /*8 segData[0].initial = segData[0].NickName.substring(0, 2).toUpperCase();
        segData[0].lblTick = "";**/
        //    	segData[0].icon.backgroundColor = kony.boj.getBackGroundColour(segData[0].initial);
        kony.print("amount ::"+amount+" :: segAmount ::"+segAmount);
        frmBulkPaymentKA.lblHiddenSelBillsAmt.text = setDecimal((parseFloat(amount)-parseFloat(segAmount)),3)+"";
      }
      frmBulkPaymentKA.lblNext.skin = "sknLblNextDisabled";
      frmBulkPaymentKA.flxConversionAmt.setVisibility(false);
      kony.print("lblHiddenSelBillsAmt ::"+frmBulkPaymentKA.lblHiddenSelBillsAmt.text);
      frmBulkPaymentKA.lblSelBillsAmt.text = setDecimal(frmBulkPaymentKA.lblHiddenSelBillsAmt.text.replace(/,/g,""), 3) +" JOD";
      frmBulkPaymentKA.bulkpaymentsegmant.setDataAt(segData[0],context.rowIndex);
      if(parseFloat(frmBulkPaymentKA.lblHiddenSelBillsAmt.text) > 0){
        if(frmBulkPaymentKA.lblPaymentMode.text !== geti18Value("i18n.billsPay.Accounts") && (kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD")){
          serv_BILLSCOMISSIONCHARGE(frmBulkPaymentKA.lblHiddenSelBillsAmt.text.replace(/,/g,""));
        }else if((parseFloat(frmBulkPaymentKA.lblHiddenSelBillsAmt.text) > 0) && frmBulkPaymentKA.lblPaymentMode.text !== geti18Value("i18n.billsPay.Accounts")){
          frmBulkPaymentKA.flxConversionAmt.setVisibility(false);
          if(check_SUFFICIENT_BALANCE(frmBulkPaymentKA.lblHiddenSelBillsAmt.text, kony.store.getItem("BillPayfromAcc").availableBalance)){
            frmBulkPaymentKA.lblNext.skin = "sknLblNextEnabled";
          }else{
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
            frmBulkPaymentKA.lblNext.skin = "sknLblNextDisabled";
            frmBulkPaymentKA.flxUnderlinePaymentModeBulk.skin = "sknFlxGreyLine";
          }
        }
          kony.application.dismissLoadingScreen();
      }else
        kony.application.dismissLoadingScreen();
    }
  }catch(e){
  	kony.print("Exception_selectBills ::"+e);
    kony.application.dismissLoadingScreen();
  }
}

function navigate_BULKPAYMENT_CONFIRMATON(){
	try{
    	frmBulkPaymentConfirmKA.bulkpaymentsegmant.removeAll();
    	var data = frmBulkPaymentKA.bulkpaymentsegmant.data, temp = [];
    	kony.print("data ::"+JSON.stringify(data));
    	for(var i in data){
        	if(data[i].btnSetting.text === "p"){
            	data[i].initial = {text:data[i].NickName.substring(0, 2).toUpperCase()};
            	data[i].btnSetting = {isVisible:false};
            	data[i].dueAmount = {text:data[i].paidAmount, centerY:"75%", width:"60%"};
            	data[i].icon = {backgroundColor:kony.boj.getBackGroundColour(data[i].initial.text)};
            	data[i].paidAmount = {isVisible:false};
            	data[i].NickName = {text:data[i].NickName, centerY:"20%"};
            	data[i].BillingNumber = {text:data[i].BillingNumber, centerY:"50%"};
                data[i].billingTitle = {text:data[i].billingTitle, centerY:"50%"};
            	temp.push(data[i]);
            }
        }
        frmBulkPaymentConfirmKA.bulkpaymentsegmant.widgetDataMap = {
          payeename: "NickName",
          accountnumber :"billingTitle",    
          lblBillerType: "BillerCode",
          lblInitial: "initial",
          flxIcon1: "icon",
          dueAmount:"dueAmount",
          paidAmount:"paidAmount",
          hiddenDueAmount:"hiddueamount",
          btnBillsPayAccounts:"btnSetting"
        };
    	frmBulkPaymentConfirmKA.bulkpaymentsegmant.setData(temp);
    	frmBulkPaymentConfirmKA.flxBack.setVisibility(true);
    	frmBulkPaymentConfirmKA.btnBillsScreen.text = geti18Value("i18n.cards.paynow");
    	frmBulkPaymentConfirmKA.show();
    }catch(e){
    	kony.print("Exception_navigate_BULKPAYMENT_CONFIRMATON ::"+e);
    }
}