//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:40 EEST 2020
function addWidgetsfrmChatBotsAr() {
    frmChatBots.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "flxBackGroundWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var lblTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "70%",
        "id": "lblTitle",
        "isVisible": true,
        "right": "135dp",
        "skin": "CopyslLabel0cffbff27efe742",
        "text": "Myra",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "9dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnClose = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossRed0ibf99d78c68742",
        "height": "50dp",
        "id": "btnClose",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_c8632779fb04485b9f63f37753b3adf4,
        "skin": "CopyslButtonGlossBlue0e82cd35563ec43",
        "text": "Close",
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxHeader.add(lblTitle, btnClose);
    var flexScrollChatBox = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "51dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "flexScrollChatBox",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "CopyslFSbox0ab3cd41915e54d",
        "top": "50dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexScrollChatBox.setDefaultUnit(kony.flex.DP);
    flexScrollChatBox.add();
    var flxUserInput = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxUserInput",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUserInput.setDefaultUnit(kony.flex.DP);
    var flxVoice = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "48dp",
        "id": "flxVoice",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "80%",
        "onClick": AS_FlexContainer_ee6a65968af14b18931d3817453c3c24,
        "skin": "CopyslFbox0b758b5bb00a348",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxVoice.setDefaultUnit(kony.flex.DP);
    var imgUserAction = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "imgUserAction",
        "isVisible": true,
        "right": "0dp",
        "skin": "slImage",
        "src": "mic.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxVoice.add(imgUserAction);
    var flxVerticalSeperator = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxVerticalSeperator",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "80%",
        "skin": "sknflxSeperatorBot",
        "top": "1dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    flxVerticalSeperator.setDefaultUnit(kony.flex.DP);
    flxVerticalSeperator.add();
    var flxSwipe = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxSwipe",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "80%",
        "zIndex": 2
    }, {}, {});
    flxSwipe.setDefaultUnit(kony.flex.DP);
    var txtBoxUserText = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "0dp",
        "focusSkin": "CopyslTextBox0b1ea04e59e0f4d",
        "height": "48dp",
        "id": "txtBoxUserText",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "right": "0%",
        "maxTextLength": null,
        "onTextChange": AS_TextField_ef0cf9288f4645daa30a3f257a4ca249,
        "placeholder": "Type your message",
        "secureTextEntry": false,
        "skin": "CopyslTextBox0g235b2f1393a45",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,3, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "CopyslTextBox0d2a427eb89504f",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxSwipe.add(txtBoxUserText);
    flxUserInput.add(flxVoice, flxVerticalSeperator, flxSwipe);
    var flxHeaderSeperator = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxHeaderSeperator",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknflxSeperatorBot",
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
    flxHeaderSeperator.add();
    var flxBottomSeperator = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "48dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxBottomSeperator",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknflxSeperatorBot",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomSeperator.setDefaultUnit(kony.flex.DP);
    flxBottomSeperator.add();
    var segBotUser = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "data": [{
            "TextField0e5adb2bfb61248": "TextBox2"
        }, {
            "TextField0e5adb2bfb61248": "TextBox2"
        }, {
            "TextField0e5adb2bfb61248": "TextBox2"
        }],
        "groupCells": false,
        "height": "50dp",
        "id": "segBotUser",
        "isVisible": false,
        "right": "0dp",
        "needPageIndicator": true,
        "onTouchStart": AS_Segment_c6d599ed963a456cb8415a2275a35bb8,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxBotUserMsg,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "FlexContainer0h9372881f12a46": "FlexContainer0h9372881f12a46",
            "TextField0e5adb2bfb61248": "TextField0e5adb2bfb61248",
            "flxBotUserMsg": "flxBotUserMsg",
            "flxBotUserSwipe": "flxBotUserSwipe"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    frmChatBots.add(flxHeader, flexScrollChatBox, flxUserInput, flxHeaderSeperator, flxBottomSeperator, segBotUser);
};
function frmChatBotsGlobalsAr() {
    frmChatBotsAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmChatBotsAr,
        "enabledForIdleTimeout": true,
        "id": "frmChatBots",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFrmBackGroundWhite"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_fb315b31639a49dd850979515093222b,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
