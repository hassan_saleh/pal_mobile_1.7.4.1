kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.frmConfirmCashWithdrawViewController = function(){
  
};

kony.rb.frmConfirmCashWithdrawViewController.prototype.frmConfirmCashpreshow=function()
{
    this.resetSkins();
    var navManager = applicationManager.getNavManager();
    var txnData = navManager.getCustomInfo("frmConfirmCashWithDraw");
    var withDrawDetails=txnData.withdraw.withdrawdetails;
    frmConfirmCashWithDraw.withdrawAmount.text=kony.retailBanking.util.currency_symbols[kony.retailBanking.globalData.globals.CurrencyCode]+" "+withDrawDetails.amount;
    frmConfirmCashWithDraw.collectorName.text=withDrawDetails.cashlessPersonName;
    frmConfirmCashWithDraw.withdrawFrom.text=withDrawDetails.fromAccountName;
    frmConfirmCashWithDraw.transactionNotes.text=withDrawDetails.transactionsNotes;
    if(withDrawDetails.cashlessMode!==i18n_self)
      {
        frmConfirmCashWithDraw.enableScrolling=true;
        frmConfirmCashWithDraw.FlexScrollContainerConfirmWithdraw.enableScrolling=true;
        frmConfirmCashWithDraw.flxImportantKA.setVisibility(false);
        frmConfirmCashWithDraw.flxCreateSecureCodeKA.setVisibility(true);
        frmConfirmCashWithDraw.tbxCreateSecureCodeKA.text = "";
        frmConfirmCashWithDraw.tbxReEnterSecureCodeKA.text = "";
        frmConfirmCashWithDraw.lblPersonNameKA.text = withDrawDetails.cashlessPersonName;
        var utlHandler = applicationManager.getUtilityHandler();
        var contacttype= txnData.withdraw.contactType;
        if(contacttype==="email")
          {
            frmConfirmCashWithDraw.collectorName.text=withDrawDetails.cashlessPersonName+"("+withDrawDetails.cashlessEmail+")";
          }
        else
          {
            frmConfirmCashWithDraw.collectorName.text=withDrawDetails.cashlessPersonName+"("+withDrawDetails.cashlessPhone+")";
          }
      }
     else{
         // frmConfirmCashWithDraw.flxImportantKA.setVisibility(true);
            frmConfirmCashWithDraw.flxCreateSecureCodeKA.setVisibility(false);
     }
  
};

kony.rb.frmConfirmCashWithdrawViewController.prototype.onClickConfirm=function(){
  
    var cardlessPC = applicationManager.getCardlessWithdrawalPresentationController();
    var navManager = applicationManager.getNavManager();
    var createdetails=navManager.getCustomInfo("frmConfirmCashWithDraw");
    var createTxnRecordDetails=createdetails.withdraw.withdrawdetails;
    var contactType = createdetails.withdraw.contactType;
    var secureCodeFlag=false;
    var contact;
     frmConfirmCashWithDraw.tbxCreateSecureCodeKA.skin="skngeneralTextField";
     frmConfirmCashWithDraw.tbxReEnterSecureCodeKA.skin="skngeneralTextField";
     frmConfirmCashWithDraw.tbxCreateSecureCodeKA.focusSkin="skngeneralTextField";
     frmConfirmCashWithDraw.tbxReEnterSecureCodeKA.focusSkin="skngeneralTextField";
     frmConfirmCashWithDraw.lblInvalidSecureCode.setVisibility(false);
     if(contactType==="email")
     {
          contact = createTxnRecordDetails.cashlessEmail;
     }
     else
     {
          contact = createTxnRecordDetails.cashlessPhone;
     }
                              
     if(createTxnRecordDetails.cashlessMode!==i18n_self)
      {
        var txnData = navManager.getCustomInfo("frmConfirmCashWithDraw");
    	var withDrawDetails=txnData.withdraw.withdrawdetails;
    	var nav = {};
     	nav.contact = contact;
        nav.toContactName=withDrawDetails.cashlessPersonName;
        nav.amount=kony.retailBanking.util.currency_symbols[kony.retailBanking.globalData.globals.CurrencyCode]+" "+withDrawDetails.amount;
        //navManager.setCustomInfo("frmCardlessAcknowledgeKA",nav);   
        if((frmConfirmCashWithDraw.tbxCreateSecureCodeKA.text==="")&&(frmConfirmCashWithDraw.tbxReEnterSecureCodeKA.text===""))
          {
            frmConfirmCashWithDraw.lblInvalidSecureCode.setVisibility(true);
            secureCodeFlag=false;
          }
        else
          {
             frmConfirmCashWithDraw.lblInvalidSecureCode.setVisibility(false);
            
          }
        if((frmConfirmCashWithDraw.tbxCreateSecureCodeKA.text===frmConfirmCashWithDraw.tbxReEnterSecureCodeKA.text)&&(frmConfirmCashWithDraw.tbxCreateSecureCodeKA.text!==""))
          {
            secureCodeFlag=true;
             frmConfirmCashWithDraw.tbxCreateSecureCodeKA.skin="skngeneralTextField";
             frmConfirmCashWithDraw.tbxReEnterSecureCodeKA.skin="skngeneralTextField";
             frmConfirmCashWithDraw.tbxCreateSecureCodeKA.focusSkin="skngeneralTextField";
             frmConfirmCashWithDraw.tbxReEnterSecureCodeKA.focusSkin="skngeneralTextField";
          }
        else
          {
            secureCodeFlag=false;
            frmConfirmCashWithDraw.tbxCreateSecureCodeKA.skin="sknErrorText";
            frmConfirmCashWithDraw.tbxReEnterSecureCodeKA.skin="sknErrorText";
            frmConfirmCashWithDraw.tbxCreateSecureCodeKA.focusSkin="sknErrorText";
            frmConfirmCashWithDraw.tbxReEnterSecureCodeKA.focusSkin="sknErrorText";
          }
      }
    
    
    if(createTxnRecordDetails.cashlessMode!==i18n_self)
      {
       createTxnRecordDetails.cashlessSecurityCode = frmConfirmCashWithDraw.tbxCreateSecureCodeKA.text;
        if(frmConfirmCashWithDraw.tbxCreateSecureCodeKA.text.length!==4)
          {
            secureCodeFlag=false;
            frmConfirmCashWithDraw.lblInvalidSecureCode.setVisibility(true);
          }
        if(secureCodeFlag)
          {
           //cardlessPC.showAcknowledgeForm(createTxnRecordDetails);
           ShowLoadingScreen();
          }
      }
    else
    {
       // cardlessPC.showAcknowledgeForm(createTxnRecordDetails);
        ShowLoadingScreen();
    }
};

kony.rb.frmConfirmCashWithdrawViewController.prototype.goBackToNewCashWithdraw = function(){
    var navManager = applicationManager.getNavManager();
    var txnData = navManager.getCustomInfo("frmConfirmCashWithDraw");
    var curData = {};
    curData.contactDetails = txnData.withdraw.withdrawdetails;
    curData.contactType = txnData.withdraw.contactType;
    curData.contactDetails.fromAccountBalance=kony.retailBanking.util.currency_symbols[kony.retailBanking.globalData.globals.CurrencyCode]+" "+curData.contactDetails.fromAccountBalance;
    navManager.goBack(curData);
};
kony.rb.frmConfirmCashWithdrawViewController.prototype.resetSkins=function()
{
 frmConfirmCashWithDraw.lblInvalidSecureCode.setVisibility(false);
 frmConfirmCashWithDraw.tbxCreateSecureCodeKA.skin="skngeneralTextField";
 frmConfirmCashWithDraw.tbxReEnterSecureCodeKA.skin="skngeneralTextField";
 frmConfirmCashWithDraw.tbxCreateSecureCodeKA.focusSkin="skngeneralTextField";
 frmConfirmCashWithDraw.tbxReEnterSecureCodeKA.focusSkin="skngeneralTextField";
};