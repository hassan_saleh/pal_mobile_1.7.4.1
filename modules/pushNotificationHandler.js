kony = kony || {};
kony.retailbanking = kony.retailbanking || {};


kony.retailbanking.pushNotificationsHandler = function(){
  kony.print(" in constructor of pushNotificationsHandler");
};

kony.retailbanking.pushNotificationsHandler.launchingFromNotification = false;
kony.retailbanking.pushNotificationsHandler.pushNotificationData = null;
kony.retailbanking.pushNotificationsHandler.mapOpenedFromOnlineNotification = false;
kony.retailbanking.pushNotificationsHandler.mapBeforeFormOnlineNotification = null;
kony.retailbanking.pushNotificationsHandler.isAppInBackGround = true;

kony.retailbanking.pushNotificationsHandler.prototype.setNotificationData = function(data){
  try{
    kony.print("$$start setNotificationData function of pushNotificationHandler class$$");
    kony.retailbanking.pushNotificationsHandler.pushNotificationData = data;
    kony.print("$$notification data "+data+"$$");
  }catch(err){
    kony.print("in catch of setNotificationData "+err);
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.getNotificationData = function(){
  try{
    return kony.retailbanking.pushNotificationsHandler.pushNotificationData;
  }catch(err){
    kony.print("in catch of getNotificationData "+err);
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.changeUIOfFrmLocatorKA = function(flag){
  try{
    kony.print("$$start changeUIOfFrmLocatorKA function of pushNotificationHandler class$$");
    if(flag){
      //frmLocatorKA.backButton.isVisible = true;
      frmLocatorKA.searchContainer.isVisible = false;
      //#ifdef android
      //frmLocatorKA.hamburgerButton.isVisible = false;
      //#endif
      frmLocatorKA.searchAccountFilters.isVisible = false;
      frmLocatorKA.locatorMapView.top = "0%"; 
      frmLocatorKA.MapListControllerWrapper.top = "25dp";
      frmLocatorKA.locatorMapView.height = "100%";
      frmLocatorKA.locatorListView.height = "100%";
      //frmLocatorKA.imgFilter.isVisible = false;
      frmLocatorKA.locatorSegmentedControllerContainer.isVisible = false;
    }
    kony.print("$$end changeUIOfFrmLocatorKA function of pushNotificationHandler class$$");
  }catch(err){
    kony.print(err+"  "+JSON.stringify(err));
    //customErrorCallback(err);
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.revertUIChangesOfLocatorKA = function(){
  try{
    kony.print("$$start revertUIChangesOfLocatorKA function of pushNotificationHandler class $$");
    //frmLocatorKA.backButton.isVisible = false;
    frmLocatorKA.searchContainer.isVisible = true;
    //#ifdef android
    //frmLocatorKA.hamburgerButton.isVisible = true;
    //#endif
    frmLocatorKA.searchAccountFilters.isVisible = true;
    frmLocatorKA.locatorMapView.top = "9%";
    frmLocatorKA.MapListControllerWrapper.top = "65dp";
    frmLocatorKA.locatorMapView.height = "91%";
    frmLocatorKA.locatorListView.height = "91%";
    //frmLocatorKA.imgFilter.isVisible = true;
    kony.print("$$end revertUIChangesOfLocatorKA function of pushNotificationHandler class $$");
  }catch(err){
    kony.print("in catch of revertUIChangesOfLocatorKA function "+err);
    customErrorCallback(err);
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.getAtmDetails = function(notificationData){
  try{
    kony.print("$$start getAtmDetails function of pushNotificationHandler class $$");
    if(notificationData){
      kony.print(JSON.stringify(notificationData));
      var placeID = notificationData.placeid;
      var options = {"access":"online"};
      objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
      var dataObject = new kony.sdk.dto.DataObject("Locations");
      dataObject.addField("placeID",placeID);
      var queryParams = {"type":"details","placeID":placeID};
      var serviceOptions = {"dataObject":dataObject,"queryParams":queryParams};
      objectService.customVerb('getLocationDetails',serviceOptions,this.successGetAtmDetails.bind(this),this.errorGetAtmDetails.bind(this));
    }
    kony.print("$$end getAtmDetails function of pushNotificationHandler class $$");
  }catch(err){
   kony.print("in catch of getAtmDetails function "+err);
    customErrorCallback(err);
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.successGetAtmDetails = function(res){
  try{
    kony.print("$$start successGetAtmDetails function of pushNotificationsHandler class$$");
    var locatonDATA = [];
    if(res && res.PlaceDetails){
      locationDATA = res.PlaceDetails;
    }
    gblFilterType="BOTH";
    kony.location.getCurrentPosition(function(response){
      try{
        gblIsCurrentLocation = true;
        if(response && response.coords && response.coords.latitude && response.coords.longitude){
          gblLatitude =response.coords.latitude;
          gbLlongitude =response.coords.longitude;
          gblCurrentLocator = { lat: gblLatitude, 
                               lon: gbLlongitude,
                               image:"current_location.png",
                               showcallout: false
                              };
          frmLocatorKA.locatorMap.locationData = [];
          var obj = new kony.retailbanking.pushNotificationsHandler();
          obj.setDataToMap(locationDATA);
        }
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      }catch(err){
        kony.print("something went wrong "+err);
        customErrorCallback(err);
      }
    },function(err){
      kony.print(err);
      gblIsCurrentLocation = false;
      var obj = new kony.retailbanking.pushNotificationsHandler();
      obj.setDataToMap(locationDATA);

      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }
                                    );
    kony.print("$$end successGetAtmDetails function of pushNotificationsHandler class$$");
  }catch(err){
    kony.print("error is "+err);
    customErrorCallback(err);
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.errorGetAtmDetails = function(err){
  try{
    kony.print("in error get Atm Detaisl "+JSON.stringify(err));
    customErrorCallback(err);
  }catch(error){
    
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.setDataToMap = function(locationList){
  try{
    kony.print("$$ start setDataToMap function of pushNotificationHandler class $$");
    frmLocatorKA.locatorMap.locationData = [];
    var tempLocaterData = [];
    var locatorResultSet = [];
    gblLocationData = [];gblLocationDataATM = [];gblLocationDataBranch = [];
    for(var i=0;i<locationList.length;i++){
        tempLocaterData =
                      {
                         lat: locationList[i].latitude,
                         lon:locationList[i].longitude,
                         name:locationList[i].addressLine1,
                         desc:locationList[i].addressLine2,
                         locationId : locationList[i].locationId,
                         image: getMapPinIcon(locationList[i].type),
                         city:locationList[i].city,
                         zipCode:locationList[i].zipCode,
                         phone:locationList[i].phone,
                         email:locationList[i].email,
                         services:locationList[i].services,
                         workingHours:locationList[i].workingHours,
                         type: locationList[i].type,
                         showcallout: true,
                         calloutData:{
                                       rbName: locationList[i].addressLine1,
                                       rbDesc:locationList[i].addressLine2,
                                       lblDistance: "", 
                                       lblDistanceUnit:"",
                                       lblStatus:{"text":locationList[i].status,"skin":getSknColorStatus(locationList[i].status)},
                                       imgStatus:{"src":getStatusImage(locationList[i].status)},
                                       imgNext:{"isVisible": true}
                                     },
                       rightChevron1:{"src":"map_drilldown.png"},
                       distanceLabel:"",
                       informationListIcon: {"src":getLocationImage(locationList[i].type)},
                       lblListStatus: {"text":locationList[i].status,"skin":getSknColorStatus(locationList[i].status)},
                       imgListStatusseg:{"src":getStatusImage(locationList[i].status)},
                       status : locationList[i].status
                    };
      gblLocationData.push(tempLocaterData);
      gblLocationDataATM.push(tempLocaterData);
      tempLocaterData = [];
    }
    
    this.setDataToMapHelper(gblLocationData);
    kony.print("$$ end setDataToMap function of pushNotificationHandler class $$");

  }catch(err){
    kony.print("error is "+err);
    customErrorCallback(err);
  }
};


kony.retailbanking.pushNotificationsHandler.prototype.setDataToMapHelper = function(data){
  try{
    kony.print("$$ start setDataToMapHelper function of pushNotificationHandler class$$");
    if(data && data.length >0){
      frmLocatorKA.locatorMap.calloutTemplate=flxMapATMBranchWithoutLocation;
      frmLocatorKA.locatorMap.widgetDataMapForCallout={
       lblName:"rbName",
       lblAddress:"rbDesc",
       lblDistance:"lblDistance",
       lblDistanceUnit:"lblDistanceUnit",
       lblStatus:"lblStatus",
       imgStatus:"imgStatus",
       imgNext:"imgNext",
     };
      frmLocatorKA.locatorSegmentList.widgetDataMap={
       rightChevron1:"rightChevron1",
       informationListLabel: "name",
       distanceLabel:"distanceLabel",
       informationListIcon:"informationListIcon",
       addressLine1: "desc",
       lblListStatus: "lblListStatus",
       type: "type",
       imgListStatus:"imgListStatusseg",
       imgNext:"imgNext",
       city:"city",
       zipCode:"zipCode",
       phone:"phone",
       email:"email",
       services:"services",
       workingHours:"workingHours"
     };
      
      frmLocatorKA.locatorMap.calloutWidth=55;
      frmLocatorKA.locatorMap.locationData=data;
      //alert(JSON.stringify(frmLocatorKA.locatorMap.locationData));
      frmLocatorKA.locatorSegmentList.setData(data);
      frmLocatorKA.locatorSegmentList.setVisibility(true);
      frmLocatorKA.LabelNoRecordsKA.setVisibility(false);
      frmLocatorKA.locatorSegmentList.onRowClick=onRowClickSegment;
      if (userAgent=="iPhone" || userAgent=="iPad") 
      {
        frmLocatorKA.locatorMap.calloutTemplate["onTouchEnd"] = onClickPinCalloutiPhone;
      }
      else{
      frmLocatorKA.locatorMap.onSelection=onClickPinCallout;
      }
      frmLocatorKA.locatorMap.onPinClick=onClickPin;
      frmLocatorKA.locatorMap.zoomLevel=13; 
    }
    else{
      frmLocatorKA.locatorSegmentList.setVisibility(false);
      frmLocatorKA.LabelNoRecordsKA.setVisibility(true);
      frmLocatorKA.locatorSegmentList.removeAll();
      frmLocatorKA.locatorMap.locationData=[];
      gotoCurrentLocation();
    }
    kony.application.dismissLoadingScreen();
    frmLocatorKA.noSearchResults.isVisible = false;
	frmLocatorKA.locatorSegmentList.opacity = 1;
    frmLocatorKA.locatorMap.isVisible = true;
    gotoCurrentLocation();
    kony.print("$$ end setDataToMapHelper function of pushNotificationHandler class$$");
  }catch(err){
    kony.print("error is "+err);
    customErrorCallback(err);
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.offlinePushHandlerIphone = function(response){
  try{
    if(response !== null && response !== undefined){
      var type = response.type;
      if(type === "overdraft"){
        var pushTitle = response.alert.title;
        var pushMessage = response.alert.body;
        this.askConfirmationToNavigateOverdraftForm(pushTitle,pushMessage);
      }
      if(type === "map"){
        var latitude = response.lat;
        var longitude = response.lon;
        var placeID = response.placeid;
        var title = response.alert.title;
        var message = response.alert.body;
        this.askConfirmationToNavigateToMap(latitude,longitude,placeID,message,title);
      }
    }
  }catch(err){
    //alert(err);
    customErrorCallback(err);
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.offlinePushHandlerAndroid = function(response){
  try{
    if(response!== null && response !==undefined){
      var type = response.type;
      if(type === "overdraft"){

      }
      if(type === "map"){
        var latitude = response.lat;
        var longitude = response.lon;
        var placeID = response.placeid;
        var title = response.title;
        var message = response.content;
        this.navigateToMap(latitude,longitude,placeID);
      }
    }
  }catch(err){
    customErrorCallback(err);
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.onlinePushHandlerIphone = function(response){
  try{
    if(response !== null && response !== undefined){
      var type = response.type;
      if(type === "overdraft"){
        var pushTitle = response.alert.title;
        var pushMessage = response.alert.body;
        this.askConfirmationToNavigateOverdraftForm(pushTitle,pushMessage);
      }
      if(type === "map"){
        var latitude = response.lat;
        var longitude = response.lon;
        var placeID = response.placeid;
        var title = response.alert.title;
        var message = response.alert.body;
        this.askConfirmationToNavigateToMap(latitude,longitude,placeID,message,title);
      }
    }
  }catch(err){
    customErrorCallback(err);
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.askConfirmationToNavigateToMap = function(latitude,longitude,placeID,message,title){
  try{
    function handleAlert(action){
      try{
        if(action){
          kony.retailbanking.pushNotificationsHandler.mapOpenedFromOnlineNotification = true;
          kony.retailbanking.pushNotificationsHandler.mapBeforeFormOnlineNotification = kony.application.getCurrentForm();
          var pushObj = new kony.retailbanking.pushNotificationsHandler();
          pushObj.navigateToMap(latitude,longitude,placeID);
          pushObj.changeUIOfFrmLocatorKA(true);
        }
      }catch(err){

      }
    }
    
    var basicConf = {
      "message" : message+"  "+geti18nkey("i18n.common.mapsome"),
      "alertType" : constants.ALERT_TYPE_CONFIRMATION,
      "alertTitle" : title,
      "yesLabel" : geti18nkey("i18n.settings.ok"),
      "noLabel" : geti18nkey("i18n.common.cancelC"),
      "alertHandler" : handleAlert
    };
    var pspConf = {};
    var infoAlert = kony.ui.Alert(basicConf,pspConf);
  }catch(err){
    customErrorCallback(err);
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.askConfirmationToNavigateOverdraftForm = function(){
  try{
    
  }catch(err){
    
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.onlinePushHandlerAndroid = function(response){
  try{
    kony.print("onlinePushHandlerAndroid response::"+response);
    if(response!== null && response !==undefined){
      var type = response.type;
      if(type === "overdraft"){
        //need to implement
        var pushTitle = response.title;
        var pushMessage = response.content;
        this.askConfirmationToNavigateOverdraftForm(pushTitle,pushMessage);
      }
      if(type === "map"){
        var latitude = response.lat;
        var longitude = response.lon;
        var placeID = response.placeid;
        var title = response.title;
        var message = response.content;
        this.askConfirmationToNavigateToMap(latitude,longitude,placeID,message,title);
      }
    }
  }catch(err){
    customErrorCallback(err);
  }
};

kony.retailbanking.pushNotificationsHandler.prototype.navigateToMap = function(latitude,longitude,placeID){
  try{
    var data = {};
    data.placeid = placeID;
    this.changeUIOfFrmLocatorKA();
    this.getAtmDetails(data);
    frmLocatorKA.show();
  }catch(err){
    customErrorCallback(err);
  }
};
