var iWatchcustid = "";
var iwatchServicecall = "";
var global_replay_object = null;
var iwatch_ACCOUNT_DETAILS = null;
var iwatchdictobj = "";
var iwatchCounter = 0;
var rService = "";

function pre_init() {
	kony.application.setApplicationCallbacks({onwatchrequest: watchCallback});
}

function cleanUp() {
	kony.print(" Iwatch :: cleaning up ...");
}


function watchCallback(dict, replyObj) {
  kony.print("Iwatch :: Inside watchCallback dict is :: "+ JSON.stringify(dict));
  var taskID = kony.application.beginBackgroundTask("UniqueTaskID", cleanUp);
  var Token = kony.store.getItem("soft_token");
  rService = dict.service;
  iwatch_ACCOUNT_DETAILS = null;
  iwatchServicecall = dict.service;
  iwatchdictobj = dict;
  isiWatchRequest = true;
  global_replay_object = replyObj;
	if (kony.sdk.isNetworkAvailable()) {
	if (Token !== undefined && Token !== null && Token !== "") {
	if (dict.service === "isUserLoggedIn") {
			loginCallforWatch("Login");
	}else if (dict.service === "accounts") {
			if (iWatchcustid !== undefined && iWatchcustid !== null && iWatchcustid !== "")
				{
					kony.print(" Iwatch :: Found Customer id so calling accounts :: "+ iWatchcustid);
					iWatchAccountsCall("accounts");
				}else{
					kony.print(" Iwatch :: Did not Found Customer id so calling accounts :: ");
					loginCallforWatch("accounts");
				}			
	}else if (dict.service === "loans") {
			if (iWatchcustid !== undefined && iWatchcustid !== null && iWatchcustid !== "")
				{
					kony.print(" Iwatch :: Found Customer id so calling loans :: "+ iWatchcustid);
					iWatchAccountsCall("loans");
				}else{
					kony.print(" Iwatch :: Did not Found Customer id so calling loans :: ");
					loginCallforWatch("loans");
				}
	}else if (dict.service === "deposits") {   
			if (iWatchcustid !== undefined && iWatchcustid !== null && iWatchcustid !== "")
				{
					kony.print(" Iwatch :: Found Customer id so calling deposits :: "+ iWatchcustid);
					iWatchAccountsCall("deposits");
				}else{
					kony.print(" Iwatch :: Did not Found Customer id so calling deposits :: ");
					loginCallforWatch("deposits");
				}
	}else if (dict.service === "loanInfo" || dict.service === "depositInfo") {
			if (iWatchcustid !== undefined && iWatchcustid !== null && iWatchcustid !== "")
				{
					kony.print(" Iwatch :: Found Customer id so calling deposits :: "+ iWatchcustid);
					var serv_DATA = dict.selectedInfo;
					iWatchLoanAndDepositDetails(serv_DATA,rService);
				}else{
					kony.print(" Iwatch :: Did not Found Customer id so calling deposits :: ");
					loginCallforWatch(rService);
				}
	}else if (dict.service === "transactions") {
			if (iWatchcustid !== undefined && iWatchcustid !== null && iWatchcustid !== "")
				{
					kony.print(" Iwatch :: Found Customer id so calling transactions :: "+ iWatchcustid);
					iwatch_ACCOUNT_DETAILS = dict.selectedAcc;
					iWatchAccTransactionCall(dict.selectedAcc,{"toDATE":"","fromDATE":"","no_TNX":"5"});
				}else{
					kony.print(" Iwatch :: Did not Found Customer id so calling transactions :: ");
					loginCallforWatch("transactions");
				}
	}else if (dict.service === "cards") {
			if (iWatchcustid !== undefined && iWatchcustid !== null && iWatchcustid !== "")
				{
					kony.print(" Iwatch :: Found Customer id so calling cards :: "+ iWatchcustid);
					iWatchCardsCall();  
				}else{
					kony.print(" Iwatch :: Did not Found Customer id so calling cards :: ");
					loginCallforWatch("cards");
				}
	}else if (dict.service === "cardTransactions") {
			if (iWatchcustid !== undefined && iWatchcustid !== null && iWatchcustid !== "")
				{
					kony.print(" Iwatch :: Found Customer id so calling cardTransactions :: "+ iWatchcustid);
					        var cardNo = dict.cardNo;
							var cardFlag = dict.cardFlag;
							var branch = dict.branch;
							var accountNo = dict.accountNo;
							if(cardFlag === "C"){
								iWatchserv_getCrCardTransactions({"card_num":cardNo},{"toDATE":"","fromDATE":""});
							}else if(cardFlag === "D"){
								iWatchserv_FETCHDEBITCARDTRANSACTION({"card_number":cardNo,"card_acc_num":accountNo,"card_acc_branch":branch},{"toDATE":"","fromDATE":""});
							}else if(cardFlag === "W"){
								iWatchserv_getCardTransactions({"card_number":cardNo,"card_acc_num":accountNo,"card_acc_branch":branch},{"toDATE":"","fromDATE":""});
							} 
				}else{
					kony.print(" Iwatch :: Did not Found Customer id so calling cardTransactions :: ");
					loginCallforWatch("cardTransactions");
				}
	}}else{
		replyObj.executeWithReply({rService: [], "isUserLoggedIn": false, "isNetworkAvailable": true , "isRetry": true});
	}}else{
		replyObj.executeWithReply({rService: [], "isUserLoggedIn": true, "isNetworkAvailable": false, "isRetry": true});
	}
  kony.application.endBackgroundTask(taskID);
}


function return_watchrequest(data) {
	try{
      	kony.print("Iwatch :: return_watchrequest data is :: "+ JSON.stringify(data));
		global_replay_object.executeWithReply(data);
		isiWatchRequest = false;
	}catch(err){
		kony.print(" Iwatch :: In Catch of return_watchrequest :: "+ err);
		global_replay_object.executeWithReply({iwatchServicecall: [],"isUserLoggedIn": true,"isNetworkAvailable": true,"isRetry": false});
	}
}