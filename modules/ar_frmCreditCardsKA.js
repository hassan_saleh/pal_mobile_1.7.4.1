//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:40 EEST 2020
function addWidgetsfrmCreditCardsKAAr() {
frmCreditCardsKA.setDefaultUnit(kony.flex.DP);
var overview = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "overview",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
overview.setDefaultUnit(kony.flex.DP);
var titleBarAccountInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "85dp",
"id": "titleBarAccountInfo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarAccountInfo.setDefaultUnit(kony.flex.DP);
var flxAndroidTittleBarKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "53dp",
"id": "flxAndroidTittleBarKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAndroidTittleBarKA.setDefaultUnit(kony.flex.DP);
var titleBarLabel = new kony.ui.Label({
"centerX": "50%",
"id": "titleBarLabel",
"isVisible": true,
"skin": "skniconButtonLabel",
"text": kony.i18n.getLocalizedString("i18n.common.openinganAccount"),
"top": "12dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var androidBack = new kony.ui.Button({
"focusSkin": "sknandroidBackButtonFocus",
"height": "50dp",
"id": "androidBack",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_c775a247c808453087ef0d7e72ff78e5,
"skin": "sknandroidBackButton",
"top": "0dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxAndroidTittleBarKA.add(titleBarLabel, androidBack);
var lblHeadingKA = new kony.ui.Label({
"centerX": "50.03%",
"id": "lblHeadingKA",
"isVisible": true,
"skin": "skniconButtonLabel",
"text": kony.i18n.getLocalizedString("i18n.common.creditCards"),
"top": "53dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxTransitionKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50.00%",
"clipBounds": true,
"height": "20dp",
"id": "flxTransitionKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"top": "35dp",
"width": "200dp",
"zIndex": 1
}, {}, {});
flxTransitionKA.setDefaultUnit(kony.flex.DP);
var flx1KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx1KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx1KA.setDefaultUnit(kony.flex.DP);
flx1KA.add();
var flx2KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx2KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": "0dp",
"width": "20dp"
}, {}, {});
flx2KA.setDefaultUnit(kony.flex.DP);
flx2KA.add();
var flx3KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "49.44%",
"clipBounds": true,
"height": "5dp",
"id": "flx3KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx3KA.setDefaultUnit(kony.flex.DP);
flx3KA.add();
var flx4KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx4KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "skncontainerBkgWhite",
"top": 0,
"width": "20dp"
}, {}, {});
flx4KA.setDefaultUnit(kony.flex.DP);
flx4KA.add();
var flx5KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx5KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx5KA.setDefaultUnit(kony.flex.DP);
flx5KA.add();
var flx6KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx6KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx6KA.setDefaultUnit(kony.flex.DP);
flx6KA.add();
var flx7KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx7KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx7KA.setDefaultUnit(kony.flex.DP);
flx7KA.add();
var flx8KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx8KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx8KA.setDefaultUnit(kony.flex.DP);
flx8KA.add();
flxTransitionKA.add( flx8KA, flx7KA, flx6KA, flx5KA, flx4KA, flx3KA, flx2KA,flx1KA);
titleBarAccountInfo.add(flxAndroidTittleBarKA, lblHeadingKA, flxTransitionKA);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": 0,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "87%",
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkgGray",
"top": "0dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var moreResourcesSegment = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [{
"HiddenLbl": "Label",
"imgicontick": "right_chevron_icon.png",
"lblPageNameKA": "Example"
}, {
"HiddenLbl": "Label",
"imgicontick": "right_chevron_icon.png",
"lblPageNameKA": "Example"
}, {
"HiddenLbl": "Label",
"imgicontick": "right_chevron_icon.png",
"lblPageNameKA": "Example"
}],
"groupCells": false,
"id": "moreResourcesSegment",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_b9668e445a204f98b62cbad18adf9dc0,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": container,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "f7f7f700",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"HiddenLbl": "HiddenLbl",
"container": "container",
"imgicontick": "imgicontick",
"lblPageNameKA": "lblPageNameKA"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopydeleteScheduleTransactionButton0ff2090b870b341 = new kony.ui.Button({
"centerX": "50.00%",
"focusSkin": "sknsecondaryActionFocus",
"height": "60dp",
"id": "CopydeleteScheduleTransactionButton0ff2090b870b341",
"isVisible": true,
"onClick": AS_Button_3830c7381ba6454187d9019b8ed377a0,
"skin": "sknSecondaryActionWhiteBgKA",
"text": kony.i18n.getLocalizedString("i18n.opening_account.pickOtherProduct"),
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopydeleteScheduleTransactionButton0838debf1b2f34c = new kony.ui.Button({
"centerX": "50.00%",
"focusSkin": "sknsecondaryActionFocus",
"height": "60dp",
"id": "CopydeleteScheduleTransactionButton0838debf1b2f34c",
"isVisible": true,
"onClick": AS_Button_4735dec5dc724fb2baae3ad2ece7eda7,
"skin": "sknSecondaryActionWhiteBgKA",
"text": kony.i18n.getLocalizedString("i18n.opening_account.backToMenu"),
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
mainContent.add(moreResourcesSegment, CopydeleteScheduleTransactionButton0ff2090b870b341, CopydeleteScheduleTransactionButton0838debf1b2f34c);
var footerBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "50dp",
"id": "footerBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0",
"skin": "skncontainerBkgWhite",
"top": "0",
"width": "100%"
}, {}, {});
footerBack.setDefaultUnit(kony.flex.DP);
var footerBackground = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "footerBackground",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0",
"skin": "menu",
"top": "0",
"width": "100%"
}, {}, {});
footerBackground.setDefaultUnit(kony.flex.DP);
var FlxAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxAccounts",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknfocusmenu",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxAccounts.setDefaultUnit(kony.flex.DP);
var img1 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img1",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_accounts_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label03174bff69bb54c = new kony.ui.Label({
"centerX": "50%",
"id": "Label03174bff69bb54c",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.my_money.accounts"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnAccounts = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnUser",
"height": "50dp",
"id": "btnAccounts",
"isVisible": true,
"onClick": AS_Button_b8d8989685cb4b87b79393a4bd33095b,
"skin": "btnUser",
"text": "H",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxAccounts.add(img1, Label03174bff69bb54c, btnAccounts);
var FlxTranfers = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxTranfers",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_e296ba6ea0474c1a9e5d8993bd082baf,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxTranfers.setDefaultUnit(kony.flex.DP);
var img2 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img2",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_t_and_p_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label02bec01fd5baf4c = new kony.ui.Label({
"centerX": "50%",
"id": "Label02bec01fd5baf4c",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.Payments"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopybtnAccounts0ia3b1ff37c304b = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCard",
"height": "50dp",
"id": "CopybtnAccounts0ia3b1ff37c304b",
"isVisible": true,
"skin": "btnCard",
"text": "I",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxTranfers.add(img2, Label02bec01fd5baf4c, CopybtnAccounts0ia3b1ff37c304b);
var FlxBot = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxBot",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_ib4d83e0d7b04f96b85936fd2bab0834,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 2
}, {}, {});
FlxBot.setDefaultUnit(kony.flex.DP);
var imgBot = new kony.ui.Image2({
"centerX": "50%",
"height": "40dp",
"id": "imgBot",
"isVisible": false,
"right": "13dp",
"skin": "slImage",
"src": "chaticonactive.png",
"top": "4dp",
"width": "40dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopybtnAccounts0ff48f9feb0aa42 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCard",
"height": "50dp",
"id": "CopybtnAccounts0ff48f9feb0aa42",
"isVisible": true,
"onClick": AS_Button_a4a66f3ab84043499299b3e3692c013f,
"skin": "btnCard",
"text": "i",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxBot.add(imgBot, CopybtnAccounts0ff48f9feb0aa42);
var FlxDeposits = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxDeposits",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_ae1a610229d5490bb3ec3f0e30f5486a,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxDeposits.setDefaultUnit(kony.flex.DP);
var img3 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img3",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_deposits_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label04221a71494e848 = new kony.ui.Label({
"centerX": "50%",
"id": "Label04221a71494e848",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.deposits"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopybtnAccounts0bc68a97c14fb49 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCard",
"height": "50dp",
"id": "CopybtnAccounts0bc68a97c14fb49",
"isVisible": true,
"skin": "btnCard",
"text": "J",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxDeposits.add(img3, Label04221a71494e848, CopybtnAccounts0bc68a97c14fb49);
var FlxMore = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxMore",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_g83fcb5ba6ad41a8a2c91effc6fe237c,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxMore.setDefaultUnit(kony.flex.DP);
var img4 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img4",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_more_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label0e5331028c2ef41 = new kony.ui.Label({
"centerX": "50%",
"id": "Label0e5331028c2ef41",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.more"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopybtnAccounts0ec4d23080f0146 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCard",
"height": "50dp",
"id": "CopybtnAccounts0ec4d23080f0146",
"isVisible": true,
"skin": "btnCard",
"text": "K",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxMore.add(img4, Label0e5331028c2ef41, CopybtnAccounts0ec4d23080f0146);
footerBackground.add( FlxMore, FlxDeposits, FlxBot, FlxTranfers,FlxAccounts);
footerBack.add(footerBackground);
overview.add(titleBarAccountInfo, mainContent, footerBack);
frmCreditCardsKA.add(overview);
};
function frmCreditCardsKAGlobalsAr() {
frmCreditCardsKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmCreditCardsKAAr,
"bounces": true,
"enableScrolling": true,
"enabledForIdleTimeout": true,
"id": "frmCreditCardsKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"inTransitionConfig": {
"formAnimation": 0
},
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_f260547f88824ec984b162585f87686f,
"outTransitionConfig": {
"formAnimation": 0
},
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
