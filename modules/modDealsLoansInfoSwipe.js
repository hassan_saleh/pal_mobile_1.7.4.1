function create_PageIndicatorInfo(form, centerX, totalAcc,CurrIndex) {
  kony.print("create_PageIndicatorInfo ::" + centerX + "totalAcc:: " + totalAcc+ "CurrIndex:: " + CurrIndex);
  try {
    form.flxPageIndicator.removeAll();
    if(totalAcc > 1)
      centerX = Math.round((centerX*2)/totalAcc)-10;
    kony.print("create_PageIndicator ::left " + centerX);
    frmAccountInfoKA.flxPageIndicator.padding = [0, 0, 0, 0];
    for (var i = 0; i < totalAcc; i++) {
      //#ifdef android
      centerX = centerX + 13;
      //#endif
      //#ifdef iphone
      centerX = centerX + 18;
      //#endif
      kony.print("create_PageIndicator ::left " + centerX);
      var flex = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": centerX+"%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "20%",
        "id": "flxPageIndicator" + (i),
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknPageIndicatorFlex",
        "width": "8%",
        "zIndex": 1
      }, {}, {});
      form.flxPageIndicator.add(flex);
    }
    form["flxPageIndicator"+CurrIndex].setFocus();
    form["flxPageIndicator"+CurrIndex].skin = "sknPageIndicatorFlexnoOpc";
    frmAccountInfoKA.forceLayout();
  } catch (e) {
    kony.print("Exception in create_PageIndicator ::" + e);
  }
}





function setGesture_InfoAnimation() {
  try {
    kony.print("setGesture_InfoAnimation BEGIN");
    var setupTblTap = {
      fingers: 1,
      swipedistance: 55,
      swipevelocity: 60
    };
    frmAccountInfoKA.mainContent.setGestureRecognizer(2, setupTblTap, animation_Information);

    function animation_Information(myWidget, gestureInfo) {
      kony.print("animation_Information ::" + JSON.stringify(gestureInfo));
      try {
        if (gestureInfo.swipeDirection == 1) {
          kony.print("gestureInfo.swipeDirection ::" + gestureInfo.swipeDirection);
          if(frmAccountInfoKA.lblLoanAccountInfo.text == kony.i18n.getLocalizedString("i18n.accounts.loans")){                    
            kony.print("Loans:::swipe"+DealsLoansCurrIndex);
            var segData = frmAccountsLandingKA.segAccountsKALoans.data;
            if(!isEmpty(segData)){
              if ((parseInt(DealsLoansCurrIndex) + 1) < (parseInt(segData.length))) {
                var context = {
                  "sectionIndex": 0,
                  "rowIndex": (parseInt(DealsLoansCurrIndex) + 1),
                  "widgetInfo": {
                    "id": "segAccountsKALoans"
                  }
                };
                kony.print("accountDashboardFlowFlag" + kony.retailBanking.globalData.globals.accountDashboardFlowFlag);
                onheaderDashboard(frmAccountsLandingKA.segAccountsKALoans.data, context);
              } else if ((parseInt(DealsLoansCurrIndex) + 1) == (parseInt(segData.length))) {
                return;
              }

            }


          }else if(frmAccountInfoKA.lblLoanAccountInfo.text == kony.i18n.getLocalizedString("i18n.accounts.deals")){
            kony.print("deposits:::swipe"+DealsLoansCurrIndex);
            var segData = frmAccountsLandingKA.segAccountsKADeals.data;
            if(!isEmpty(segData)){
              if ((parseInt(DealsLoansCurrIndex) + 1) < (parseInt(segData.length))) {
                var context = {
                  "sectionIndex": 0,
                  "rowIndex": (parseInt(DealsLoansCurrIndex) + 1),
                  "widgetInfo": {
                    "id": "segAccountsKADeals"
                  }
                };
                kony.print("accountDashboardFlowFlag" + kony.retailBanking.globalData.globals.accountDashboardFlowFlag);
                onheaderDashboard(frmAccountsLandingKA.segAccountsKADeals.data, context);
              } else if ((parseInt(DealsLoansCurrIndex) + 1) == (parseInt(segData.length))) {
                return;
              }
            }
          }else if(frmAccountInfoKA.lblLoanAccountInfo.text == kony.i18n.getLocalizedString("i18n.accounts.accounts")){
            kony.print("accounts:::swipe left"+customerAccountDetails.currentIndex);
            var segData = frmAccountsLandingKA.segAccountsKA.data;
            if(!isEmpty(segData)){
              if ((parseInt(customerAccountDetails.currentIndex)+1) < (parseInt(segData.length))) {
                customerAccountDetails.currentIndex = (parseInt(customerAccountDetails.currentIndex)) + 1;
                kony.print("accountDashboardFlowFlag" + kony.retailBanking.globalData.globals.accountDashboardFlowFlag);
                create_PageIndicatorInfo(frmAccountInfoKA, 50, frmAccountsLandingKA.segAccountsKA.data.length, customerAccountDetails.currentIndex);

                accountInfo("account");
              } else if ((parseInt(DealsLoansCurrIndex) + 1) == (parseInt(segData.length))) {
                return;
              }
            }
          }
        }

        else if (gestureInfo.swipeDirection == 2) {
          kony.print("gestureInfo.swipeDirection ::" + gestureInfo.swipeDirection);
          kony.print("DealsLoansCurrIndex22" + DealsLoansCurrIndex);
          if(frmAccountInfoKA.lblLoanAccountInfo.text == kony.i18n.getLocalizedString("i18n.accounts.loans")){                    
            kony.print("Loans:::swipe"+DealsLoansCurrIndex);
            var segData = frmAccountsLandingKA.segAccountsKALoans.data;
            if(!isEmpty(segData)){
              if ((parseInt(DealsLoansCurrIndex) == 0)) {
                return;
              } else {
                var context = {
                  "sectionIndex": 0,
                  "rowIndex": (parseInt(DealsLoansCurrIndex) - 1),
                  "widgetInfo": {
                    "id": "segAccountsKALoans"
                  }
                };
                kony.print("accountDashboardFlowFlag" + kony.retailBanking.globalData.globals.accountDashboardFlowFlag);
                onheaderDashboard(frmAccountsLandingKA.segAccountsKALoans.data, context);
              }

            }
          }else if(frmAccountInfoKA.lblLoanAccountInfo.text == kony.i18n.getLocalizedString("i18n.accounts.deals")){
            kony.print("deposits:::swipe"+DealsLoansCurrIndex);
            var segData = frmAccountsLandingKA.segAccountsKADeals.data;
            if(!isEmpty(segData)){
              if ((parseInt(DealsLoansCurrIndex) === 0)) {
                return;
              } else {
                var context = {
                  "sectionIndex": 0,
                  "rowIndex": (parseInt(DealsLoansCurrIndex) - 1),
                  "widgetInfo": {
                    "id": "segAccountsKADeals"
                  }
                };
                onheaderDashboard(frmAccountsLandingKA.segAccountsKADeals.data, context);
              }

            }
          }else if(frmAccountInfoKA.lblLoanAccountInfo.text == kony.i18n.getLocalizedString("i18n.accounts.accounts")){
            kony.print("accounts:::swipe left"+customerAccountDetails.currentIndex);
            var segData = frmAccountsLandingKA.segAccountsKA.data;

            if(!isEmpty(segData)){
              if ((parseInt(customerAccountDetails.currentIndex) === 0)) {
                return;
              } else {
                customerAccountDetails.currentIndex = (parseInt(customerAccountDetails.currentIndex)) -1;
                kony.print("accountDashboardFlowFlag" + kony.retailBanking.globalData.globals.accountDashboardFlowFlag);
                create_PageIndicatorInfo(frmAccountInfoKA, 50, frmAccountsLandingKA.segAccountsKA.data.length, customerAccountDetails.currentIndex);

                accountInfo("account");
              }

            }

          }
        }
      }catch (e) {
        kony.print("Animation Exception ::" + e)
      }
    }
    kony.print("gesture_AccountAnimation END");
  } catch (e) {
    kony.print("Exception_gesture_AccountAnimation ::" + e);
  }
}