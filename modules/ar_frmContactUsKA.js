//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:40 EEST 2020
function addWidgetsfrmContactUsKAAr() {
frmContactUsKA.setDefaultUnit(kony.flex.DP);
var androidTitleBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "androidTitleBar",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntitleBarGradient",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
androidTitleBar.setDefaultUnit(kony.flex.DP);
var androidTitleLabel = new kony.ui.Label({
"centerY": "50%",
"id": "androidTitleLabel",
"isVisible": false,
"right": "55dp",
"skin": "sknnavBarTitle",
"text": kony.i18n.getLocalizedString("i18n.common.contactUs"),
"width": "70%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var hamburgerButton = new kony.ui.Button({
"focusSkin": "sknhamburgerButtonFocus",
"height": "50dp",
"id": "hamburgerButton",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_b1403e0b28734e44b3300dfc1e9ff9f1,
"skin": "sknhamburgerButton",
"top": "0dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnback = new kony.ui.Button({
"focusSkin": "sknandroidBackButtonFocus",
"height": "50dp",
"id": "btnback",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_b7efb18e5b774ee9b128a12584c27fb1,
"skin": "sknandroidBackButton",
"top": "0dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnLogin = new kony.ui.Button({
"focusSkin": "sknTransBtn",
"height": "50dp",
"id": "btnLogin",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_bb1c48de43ac45aa89f4de7c0cf4483c,
"skin": "sknTransBtn",
"text": kony.i18n.getLocalizedString("i18n.login.loginBOJ"),
"top": "0dp",
"width": "58dp",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 1, 0,0, 0],
"paddingInPixel": false
}, {});
var lblBack = new kony.ui.Label({
"height": "35dp",
"id": "lblBack",
"isVisible": true,
"right": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "6dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMapHeading = new kony.ui.Label({
"centerX": "50%",
"id": "lblMapHeading",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.common.informationSupport"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "7%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 20
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var searchInformation = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "8%",
"centerX": "50%",
"clipBounds": true,
"height": "30%",
"id": "searchInformation",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "sknBOJblueBG",
"width": "88%"
}, {}, {});
searchInformation.setDefaultUnit(kony.flex.DP);
var btnATMKA = new kony.ui.Button({
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnATMKA",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_f6fbea97248c4af7a21bb033a35d4840,
"skin": "slButtonWhiteTabDisabled",
"text": "News",
"top": "0%",
"width": "33%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 6, 0,6, 0],
"paddingInPixel": false
}, {});
var btnBothKA = new kony.ui.Button({
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnBothKA",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_cc4a8686e3c1442bac7d6ff3125ee0eb,
"skin": "slButtonWhiteTab",
"text": kony.i18n.getLocalizedString("i18n.transfer.contactSegment.contacts"),
"top": "0%",
"width": "33.30%",
"zIndex": 3
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 6, 0,6, 0],
"paddingInPixel": false
}, {});
var btnBranchKA = new kony.ui.Button({
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnBranchKA",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_d85ffe90bb3e4f5eb4fd361c15225c94,
"skin": "slButtonWhiteTabDisabled",
"text": kony.i18n.getLocalizedString("i18n.opening_account.faq"),
"top": "0%",
"width": "33.30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 6, 0,6, 0],
"paddingInPixel": false
}, {});
searchInformation.add( btnBranchKA, btnBothKA,btnATMKA);
androidTitleBar.add(androidTitleLabel, hamburgerButton, btnback, btnLogin, lblBack, lblMapHeading, searchInformation);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "80%",
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknFlxBGBlue",
"top": "20%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var contactListDivider = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1%",
"id": "contactListDivider",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
contactListDivider.setDefaultUnit(kony.flex.DP);
contactListDivider.add();
var contactSegmentList = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50.00%",
"data": [{
"imgicontick": "",
"lblNameKA": ""
}],
"groupCells": false,
"id": "contactSegmentList",
"isVisible": false,
"needPageIndicator": true,
"onRowClick": AS_Segment_aae2b1aa35dc464082b4c2955a66212f,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowTemplate": CopyFlexContainer0b2b1c26ffbf74f,
"scrollingEvents": {},
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "64646400",
"separatorRequired": false,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer0b2b1c26ffbf74f": "CopyFlexContainer0b2b1c26ffbf74f",
"contactListDivider": "contactListDivider",
"imgicontick": "imgicontick",
"lblNameKA": "lblNameKA"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxCallUS = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "15%",
"id": "flxCallUS",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxBgBlueGradientRound8",
"top": "8%",
"width": "88%",
"zIndex": 1
}, {}, {});
flxCallUS.setDefaultUnit(kony.flex.DP);
var lblCallUS = new kony.ui.Label({
"height": "40%",
"id": "lblCallUS",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128CR",
"text": kony.i18n.getLocalizedString("i18n.common.callus24"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMobile = new kony.ui.Label({
"id": "lblMobile",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128C",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "50%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMobileIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblMobileIcon",
"isVisible": true,
"left": "1%",
"skin": "sknBOJttf170Yellow",
"text": "X",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "25%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCallUS.add(lblCallUS, lblMobile, lblMobileIcon);
var flxSendUS = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "15%",
"id": "flxSendUS",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxBgBlueGradientRound8",
"top": "5%",
"width": "88%",
"zIndex": 1
}, {}, {});
flxSendUS.setDefaultUnit(kony.flex.DP);
var lblSendus = new kony.ui.Label({
"height": "40%",
"id": "lblSendus",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128CR",
"text": kony.i18n.getLocalizedString("i18n.common.sendusanemail"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblEmail = new kony.ui.Label({
"id": "lblEmail",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128C",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "50%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblEmailIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblEmailIcon",
"isVisible": true,
"left": "1%",
"skin": "sknBOJttf170Yellow",
"text": "Y",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "25%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxSendUS.add(lblSendus, lblEmail, lblEmailIcon);
var flxVisitBranch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "15%",
"id": "flxVisitBranch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxBgBlueGradientRound8",
"top": "5%",
"width": "88%",
"zIndex": 1
}, {}, {});
flxVisitBranch.setDefaultUnit(kony.flex.DP);
var lblVisitBranch = new kony.ui.Label({
"centerY": "50%",
"height": "40%",
"id": "lblVisitBranch",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128CR",
"text": kony.i18n.getLocalizedString("i18n.common.visitourbranch"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBranchIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblBranchIcon",
"isVisible": true,
"left": "1%",
"skin": "sknBOJttf170Yellow",
"text": "B",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "25%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxVisitBranch.add(lblVisitBranch, lblBranchIcon);
var flxHomepage = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "15%",
"id": "flxHomepage",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxBgBlueGradientRound8",
"top": "5%",
"width": "88%",
"zIndex": 1
}, {}, {});
flxHomepage.setDefaultUnit(kony.flex.DP);
var lblHomePage = new kony.ui.Label({
"centerY": "50.00%",
"height": "40%",
"id": "lblHomePage",
"isVisible": true,
"right": "4.96%",
"skin": "sknLblWhite128CR",
"text": kony.i18n.getLocalizedString("i18n.common.gotohomepage"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblHomepageIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblHomepageIcon",
"isVisible": true,
"left": "1%",
"skin": "sknBOJttf170Yellow",
"text": "Z",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "25%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHomepage.add(lblHomePage, lblHomepageIcon);
var lblFollowUs = new kony.ui.Label({
"centerX": "50%",
"height": "33dp",
"id": "lblFollowUs",
"isVisible": false,
"right": "0dp",
"skin": "sknLblWhite128CR",
"text": "Follow Us",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "48dp",
"width": "73dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxFollowUs = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "39dp",
"clipBounds": true,
"height": "39dp",
"id": "flxFollowUs",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFollowUs.setDefaultUnit(kony.flex.DP);
var lblFb = new kony.ui.Label({
"height": "45dp",
"id": "lblFb",
"isVisible": true,
"right": "90dp",
"skin": "sknLblBoj205White",
"text": "e",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "45dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblTwitter = new kony.ui.Label({
"height": "45dp",
"id": "lblTwitter",
"isVisible": true,
"right": "30dp",
"skin": "sknLblBoj205White",
"text": "f",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "45dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLinkedIn = new kony.ui.Label({
"height": "45dp",
"id": "lblLinkedIn",
"isVisible": true,
"right": "30dp",
"skin": "sknLblBoj205White",
"text": "g",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "45dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFollowUs.add( lblLinkedIn, lblTwitter,lblFb);
mainContent.add(contactListDivider, contactSegmentList, flxCallUS, flxSendUS, flxVisitBranch, flxHomepage, lblFollowUs, flxFollowUs);
frmContactUsKA.add(androidTitleBar, mainContent);
};
function frmContactUsKAGlobalsAr() {
frmContactUsKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmContactUsKAAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmContactUsKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_d86821a21bcb4d96b49bd44254326b7c,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": true,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_4562e95b7c7f49958989d2897eba907e,
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
