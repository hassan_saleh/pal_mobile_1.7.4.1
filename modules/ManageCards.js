var action;
var selectedOperation = "";
var cardStatus ="";
function cardManagementOperations(operationMode){
  selectedOperation = operationMode;
  var cardType = cardsList[selectedCardIndex].cardType;
  cardStatus=cardsList[selectedCardIndex].cardStatus;
  if(operationMode === "changePin"){
    action = "PinChange";
    frmCardOperationsKA.lblCardOperationHeaderKA.text = i18n_RequestingPINChange;
    frmCardOperationsKA.lblCardTypeKA.setVisibility(false);
    frmCardOperationsKA.flxCustomerCareCallKA.setVisibility(false);

  }else if(operationMode === "cancel"){
    action = "Cancel";
    frmCardOperationsKA.lblCardOperationHeaderKA.text = i18n_Cancelling+" "+cardType+" "+i18n_card;
    frmCardOperationsKA.lblCardTypeKA.setVisibility(false);
    frmCardOperationsKA.flxCustomerCareCallKA.setVisibility(false);

  }else if(operationMode === "lost"){
    action = "Lost";
    frmCardOperationsKA.lblCardOperationHeaderKA.text = i18n_Stolen+" "+ cardType+" "+i18n_card;
    frmCardOperationsKA.lblCardTypeKA.setVisibility(false);
    frmCardOperationsKA.flxCustomerCareCallKA.setVisibility(true);

  }
  else if(operationMode === "replace"){
    action = "Replace";
    frmCardOperationsKA.lblCardOperationHeaderKA.text = i18n_Replacing+" "+ cardType+" "+i18n_card;
    frmCardOperationsKA.lblCardTypeKA.setVisibility(true);
    frmCardOperationsKA.flxCustomerCareCallKA.setVisibility(false);
  }
  else {
    if(cardStatus === "Active"){
      action = "Deactivate";
      frmCardOperationsKA.lblCardOperationHeaderKA.text = i18n_Deactivating+" " + cardType+" "+i18n_card;
      frmCardOperationsKA.lblCardTypeKA.setVisibility(false);
      frmCardOperationsKA.flxCustomerCareCallKA.setVisibility(false);
    }
    else if(cardStatus === "Inactive"){
      action = "Activate";
      frmCardOperationsKA.lblCardOperationHeaderKA.text = i18n_Activating+" " + cardType+" "+i18n_card;
      frmCardOperationsKA.lblCardTypeKA.setVisibility(false);
      frmCardOperationsKA.flxCustomerCareCallKA.setVisibility(false);
    }
  }
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmCardOperationsKA");
  var navigationObject = new kony.sdk.mvvm.NavigationObject();
  var datamodelflxAddressKA = new kony.sdk.mvvm.DataModel();
  navigationObject.setDataModel(null,kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
  navigationObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
  navigationObject.setCustomInfo("dataIndex",selectedCardIndex);
  navigationObject.setCustomInfo("action",action);
  controller.performAction("navigateTo",["frmCardOperationsKA",navigationObject]);
}
var phNum = "";
function showManageCardsAlert(){
  phNum = "284 234 1234";
  //var phNum = frmContactUsKA.contactSegmentList.selectedItems[0].phoneNum;
  var noLable="";
  var yesLable="";
  var alertType="";
  var alertTitle="";
  var message="";
  var imageIcon = "";
  if(selectedOperation === "lost"){
    alertType = constants.ALERT_TYPE_CONFIRMATION;
    alertTitle = i18n_Confirmation;
    yesLable = i18n_call;
    noLable = i18n_OK;
    message = i18n_InstructCardsLost;
  }else if(selectedOperation === "changePin"){
    alertType = constants.ALERT_TYPE_INFORMATION;
    alertTitle = i18n_Confirmation;
    yesLable = i18n_OK;
    noLable = "";
    message = i18n_InstructPinChange;
  }else if(selectedOperation === "activateordeactivate"){
    alertType = constants.ALERT_TYPE_INFORMATION;
    alertTitle = i18n_Confirmation;
    yesLable = i18n_OK;
    noLable = "";
    if(cardStatus === "Active"){
      message = i18n_cardDeactivated;
    }else{
      message = i18n_cardactivated;
    }
  }else if(selectedOperation === "cancel"){
    alertType = constants.ALERT_TYPE_INFORMATION;
    alertTitle = i18n_Confirmation;
    yesLable = i18n_OK;
    noLable = "";
    message = i18n_cardCancelled;
  }else if(selectedOperation === "replace"){
    alertType = constants.ALERT_TYPE_INFORMATION;
    alertTitle = i18n_Confirmation;
    yesLable = i18n_OK;
    noLable = "";
    message = i18n_replacementReq+" "+kony.retailBanking.globalData.applicationProperties.appProperties.businessDays+" " +i18n_workingDays;
  }

  kony.ui.Alert({
    "message": message,
    "alertType": alertType,
    "alertTitle": alertTitle,
    "yesLabel": yesLable,
    "noLabel": noLable,
    "alertIcon": imageIcon,
    "alertHandler": userResponse
  }, {
    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
  });
}
function userResponse(response){
  if(response){
    try
    {
      if(selectedOperation === "lost"){
        kony.phone.dial(phNum);
      }
    } 
    catch(err)
    {
      kony.print(i18n_dailError+" "+err);
    }
  }
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmManageCardsKA");
  var navigationObject = new kony.sdk.mvvm.NavigationObject();
  navigationObject.setRequestOptions("segCardsKA",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
  controller.performAction("navigateTo",["frmManageCardsKA",navigationObject]);
}




//Activating and deactivating different lables

function changeCardsLableStatus(cardStatus){
  if(cardStatus==="Active")
  {
    frmManageCardsKA.flxDeactivateKA.btnActivateKA.text=i18n_DeactivateCard;
    frmManageCardsKA.flxPinKA.isVisible=true;
    frmManageCardsKA.flxDeactivateKA.isVisible=true;
    frmManageCardsKA.flxReplaceKA.isVisible=true;
    frmManageCardsKA.flxCancelKA.isVisible=true;
    frmManageCardsKA.flxReportKA.isVisible=true;
    frmManageCardsKA.flxMessageLabelKA.isVisible = false;
  }
  else if(cardStatus==="Inactive")
  {
    frmManageCardsKA.flxDeactivateKA.btnActivateKA.text=i18n_ActivateCard;
    frmManageCardsKA.flxPinKA.isVisible=false;
    frmManageCardsKA.flxDeactivateKA.isVisible=true;
    frmManageCardsKA.flxReplaceKA.isVisible=false;
    frmManageCardsKA.flxCancelKA.isVisible=false;
    frmManageCardsKA.flxReportKA.isVisible=false;
    frmManageCardsKA.flxMessageLabelKA.isVisible = true;
    frmManageCardsKA.lblCardStatusKA.text = i18n_DeActivateCardInfo;
  }
  else if(cardStatus==="Cancelled")
  {
    frmManageCardsKA.flxDeactivateKA.btnActivateKA.text=i18n_ActivateCard;
    frmManageCardsKA.flxPinKA.isVisible=false;
//     frmManageCardsKA.flxDeactivateKA.isVisible=false;
    frmManageCardsKA.flxReplaceKA.isVisible=false;
    frmManageCardsKA.flxCancelKA.isVisible=false;
    frmManageCardsKA.flxReportKA.isVisible=false;
    frmManageCardsKA.flxMessageLabelKA.isVisible = true;
    frmManageCardsKA.lblCardStatusKA.text =i18n_CancelCardInfo;
  }
  else if(cardStatus==="Lost")
  {
    frmManageCardsKA.flxDeactivateKA.btnActivateKA.text=i18n_ActivateCard;
    frmManageCardsKA.flxPinKA.isVisible=false;
//     frmManageCardsKA.flxDeactivateKA.isVisible=false;
    frmManageCardsKA.flxReplaceKA.isVisible=false;
    frmManageCardsKA.flxCancelKA.isVisible=false;
    frmManageCardsKA.flxReportKA.isVisible=false;
    frmManageCardsKA.flxMessageLabelKA.isVisible = true;
    frmManageCardsKA.lblCardStatusKA.text = i18n_ReportCardInfo;
  }
  else if(cardStatus==="Replaced")
  {
    frmManageCardsKA.flxDeactivateKA.btnActivateKA.text=i18n_ActivateCard;
    frmManageCardsKA.flxPinKA.isVisible=true;
//     frmManageCardsKA.flxDeactivateKA.isVisible=false;
    frmManageCardsKA.flxReplaceKA.isVisible=false;
    frmManageCardsKA.flxCancelKA.isVisible=true;
    frmManageCardsKA.flxReportKA.isVisible=true;
    frmManageCardsKA.flxMessageLabelKA.isVisible = true;
    frmManageCardsKA.lblCardStatusKA.text = i18n_ReplacementCardInfo;
  }


}
function updateCardStatus(){
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmCardOperationsKA");
  var navigationObject = new kony.sdk.mvvm.NavigationObject();
  navigationObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
  controller.performAction("saveData",[navigationObject]);
}



function onClickCardsMenu(){
  kony.print("frmManageCardsKA::");
  //frmCardsLandingKA.show();
  frmManageCardsKA.flxSettings.isVisible = false;
  frmManageCardsKA.flxTransactions.isVisible = false;
  frmManageCardsKA.flxInfo.isVisible = true;
  gblCardReorder = false;
  gblCardAddNickname = false;
  is_CARDPAYMENTSUCCESS = false;
  frmManageCardsKA.btnInfo.skin = "btnCardFoc";
  frmManageCardsKA.btnTransactions.skin = "btnCardsScreen";
  frmManageCardsKA.btnSettings.skin = "btnCardsScreen";
  service_fetch_cardListAndDetils({"custID":custid,"form":"frmCardsLandingKA"});
}

function onClickCardsSegNavigateToDetails(data){
  kony.print("onClickCardsSegNavigateToDetails::"+JSON.stringify(data));
  //alert("onClickCardsSegNavigateToDetails::"+JSON.stringify(data));
  CARDLIST_DETAILS.cardType = frmCardsLandingKA.segCardsLanding.selectedRowItems[0].cardTypeFlag.text;

  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var navcontroller = INSTANCE.getFormController("frmCardsLandingKA");
  var navigationObject = navcontroller.getContextData();
  var segdata = navigationObject.getCustomInfo("segCardsLanding");
  var temp = [];
  for(var i in segdata){
    temp[i] = {"cardType":segdata[i].card_type.text,
               "cardNumber":segdata[i].card_num,
               "ValidThru":segdata[i].card_exp_date,
               "CardHolder":segdata[i].name_on_card,
               "cardImage":segdata[i].cardImage.src};
    if(kony.store.getItem("langPrefObj") == "ar"){
      temp[i].cardNumber.right = null;
      temp[i].CardHolder.right = null;
      temp[i].cardNumber.left = "26%";
      temp[i].CardHolder.left = "8%";
      temp[i].cardNumber.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
     temp[i].CardHolder.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
    }
  }
  frmManageCardsKA.segCardsKA.widgetDataMap = {"cardType":"cardType","cardNumber":"cardNumber","ValidThru":"ValidThru","CardHolder":"CardHolder","cardImage":"cardImage"};
  frmManageCardsKA.segCardsKA.removeAll();
  frmManageCardsKA.segCardsKA.setData(temp);
//   frmManageCardsKA.forceLayout();
  kony.print("Segment Data ::"+JSON.stringify(frmManageCardsKA.segCardsKA.data));
  customerAccountDetails.currentIndex = data;
  frmManageCardsKA.segCardsKA.selectedIndex = [0,customerAccountDetails.currentIndex];
  frmManageCardsKA.flxAccountdetailsSortContainer.top = "0%";
//   if(frmManageCardsKA.flxAccountdetailsSortContainer.top !== "0%"){
//     animatePopup_Sort();
//   }
  get_cardDataList();

  if(CARDLIST_DETAILS.cardType === "C")
    CARDLIST_DETAILS.cardNumber = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_num;
  else{
    kony.print("customerAccountDetails.currentIndex ::"+customerAccountDetails.currentIndex+"prCardLandinList ::"+JSON.stringify(kony.retailBanking.globalData.prCardLandinList));
    CARDLIST_DETAILS.cardNumber = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_num;
  }
  if(temp.length > 1)
    create_PageIndicator(frmManageCardsKA,50,temp.length);
  // 	if(kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].cardTypeFlag === "D" || kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].cardTypeFlag === "W")
  // 		serv_getCardTransactions(kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex]);
  // 	else{
  // 		serv_getCrCardTransactions(kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex]);
  //     }
  //service_fetch_cardListAndDetils({"custID":custid,"cardNum":data[0].card_num.text,"cardFlag":"","form":"frmManageCardsKA"});
}


function onSwipeCardsDetailsKA(rowIndex){
  kony.print("onSwipeCardsDetailsKA::"+rowIndex);
  //alert("onSwipeCardsDetailsKA::"+rowIndex);
  var segdatalength = frmManageCardsKA.segCardsKA.data.length;
  if(segdatalength > 1){
    if(rowIndex > 0)
      frmManageCardsKA["flxPageIndicator"+(rowIndex-1)].skin = "sknPageIndicatorFlex";
    frmManageCardsKA["flxPageIndicator"+rowIndex].skin = "sknPageIndicatorFlexnoOpc";
    frmManageCardsKA["flxPageIndicator"+rowIndex].setFocus(true);
    if(rowIndex < segdatalength-1)
      frmManageCardsKA["flxPageIndicator"+(rowIndex+1)].skin = "sknPageIndicatorFlex";
    customerAccountDetails.currentIndex = rowIndex;
    clearForm.filterTransactions();
    get_cardDataList();
  }
  /*******
  if(segdata.cardType==="Visa Election Web Card")
    {
      frmManageCardsKA.flxWebPay.setVisibility(true);
      frmManageCardsKA.flxCreditCardPay.setVisibility(false);
      frmManageCardsKA.flxIBAN.isVisible = false;
      frmManageCardsKA.CopyflxIBAN0hd4c5a6edeee48.isVisible = false;
      frmManageCardsKA.CopyflxOpenedIn0b6e2187bba4f41.isVisible = false;
      frmManageCardsKA.CopyLabel0e0961549274149.text = "Type";
      frmManageCardsKA.CopyinterestRateLabel0h623a08258c745 = "Debit";
      frmManageCardsKA.CopyLabel08f2d764e1add4c.text = "Status";
      frmManageCardsKA.CopyaccountNumberLabel036c71ac511b84d.text = "Active";
      frmManageCardsKA.CopyLabel01f52e7df8d504e.text = "Expiry Date";
      frmManageCardsKA.CopyinterestEarnedLabel00b76261e56e741.text = "11 Jan 2015";
      frmManageCardsKA.CopyLabel00465c266f9f248.text = "Available Balance";
      frmManageCardsKA.CopyinterestRateLabel0b14fb4af446e45.text = "123.000 JOD";
    }
  else if(segdata.cardType==="Visa Election Credit Card")
    {
      frmManageCardsKA.flxCreditCardPay.setVisibility(true);
      frmStopCardKA.lstCardReason.masterData = [["L", "Lost"], ["RN3", "Fraud"],["S", "Stolen"]];
      frmManageCardsKA.flxWebPay.setVisibility(false);
      frmManageCardsKA.CopyLabel0e0961549274149.text = "Available Credit Limit";
      frmManageCardsKA.CopyinterestRateLabel0h623a08258c745 = "1,000.000 JOD";
      frmManageCardsKA.CopyLabel08f2d764e1add4c.text = "Account Number";
      frmManageCardsKA.CopyaccountNumberLabel036c71ac511b84d.text = "90010020177801";
      frmManageCardsKA.CopyLabel01f52e7df8d504e.text = "Created On";
      frmManageCardsKA.CopyinterestEarnedLabel00b76261e56e741.text = "11 Jan 2015";
      frmManageCardsKA.CopyLabel00465c266f9f248.text = "Spend Credit Limit";
      frmManageCardsKA.CopyinterestRateLabel0b14fb4af446e45.text = "1,000.000 JOD";
      frmManageCardsKA.flxIBAN.isVisible = true;
      frmManageCardsKA.CopyflxIBAN0hd4c5a6edeee48.isVisible = true;
      frmManageCardsKA.CopyflxOpenedIn0b6e2187bba4f41.isVisible = true;
    }
  else
    {
      frmManageCardsKA.flxWebPay.setVisibility(false);
      frmStopCardKA.lstCardReason.masterData = [["FS", "Fraud Suspected"], ["L", "Lost"],["O", "Others"],["OB", "Captured in Other Bank's ATM"],["OWB", "Captured in Own Bank's ATM"],["RN3", "Fraud"],["S", "Stolen"]];
      frmManageCardsKA.flxCreditCardPay.setVisibility(false);
      frmManageCardsKA.flxIBAN.isVisible = false;
      frmManageCardsKA.CopyflxIBAN0hd4c5a6edeee48.isVisible = false;
      frmManageCardsKA.CopyflxOpenedIn0b6e2187bba4f41.isVisible = true;
      frmManageCardsKA.CopyinterestRateLabel0aaaa0c9e91c743.text = "Top Up";
      frmManageCardsKA.CopyLabel0e0961549274149.text = "Type";
      frmManageCardsKA.CopyinterestRateLabel0h623a08258c745 = "Debit";
      frmManageCardsKA.CopyLabel08f2d764e1add4c.text = "Status";
      frmManageCardsKA.CopyaccountNumberLabel036c71ac511b84d.text = "Active";
      frmManageCardsKA.CopyLabel01f52e7df8d504e.text = "Expiry Date";
      frmManageCardsKA.CopyinterestEarnedLabel00b76261e56e741.text = "11 Jan 2015";
      frmManageCardsKA.CopyLabel00465c266f9f248.text = "Connect To";
      frmManageCardsKA.CopyinterestRateLabel0b14fb4af446e45.text = "123567890123456789";
    }******/
}

function cancelconfirm()
{
  popupCommonAlertDimiss(); 
  frmManageCardsKA.show();
  //frmCreditCardPayment.destroy();
  frmCreditCardPayment.txtFieldAmount.text="";
}
function navigatetomanagecard()
{
  frmCreditCardPayment.txtFieldAmount.text="";
  popupCommonAlertDimiss();
  if(kony.boj.creditcardOption === "fromPayNow")
    {
      kony.print("from paynow"+kony.boj.creditcardOption);
      frmCardsLandingKA.show();
      frmCreditCardPayment.destroy();
    }
  else
    {
      kony.print("from creditcardpayment"+kony.boj.creditcardOption);
      frmPaymentDashboard.show();
      frmCreditCardPayment.destroy();
    }
}




function initcreditcardpayment()
{
  try{
    var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.accountsData;
    frmCreditCardPayment.lblAccountType.text=fromAccounts[0].accountName;
    var accountNumber = fromAccounts[0].accountID;
    fromAccounts[0].hiddenAccountNumber =  "***"+ accountNumber.substring(accountNumber.length-5, accountNumber.length);
    frmCreditCardPayment.lblAccountCode.text=fromAccounts[0].hiddenAccountNumber;
    frmCreditCardPayment.lblAccountType.text = fromAccounts[0].accountName+" "+fromAccounts[0].hiddenAccountNumber;
    frmCreditCardPayment.lblAccountCode.text = fromAccounts[0].hiddenAccountNumber;
    frmCreditCardPayment.hiddenLblAccountNum.text = fromAccounts[0].accountID;
    frmCreditCardPayment.lblFromCurr.text = fromAccounts[0].currencyCode;
    frmCreditCardPayment.lblBranchNum.text = fromAccounts[0].branchNumber;
    // frmCreditCardPayment.lblCardNumber.text="Please Select Card Number";
  }catch(e){
	  exceptionLogCall("initcreditcardpayment","UI ERROR","UI",e);
    kony.print("Exception_initcreditcardpayment ::"+e);
  }
}

function preshowcard()
{
//   var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.accountsData;
//   var accountNumber = fromAccounts[0].accountID;
//   fromAccounts[0].hiddenAccountNumber =  "***"+ accountNumber.substring(accountNumber.length-5, accountNumber.length);
//   set_formCreditCardPayment(fromAccounts[0]);
//   var len2 = fromAccounts.length;
//   if (len2 < 2) {
//     frmCreditCardPayment.lblAccountType.text=fromAccounts[0].accountName;
//     var accountNumber = fromAccounts[0].accountID;
//     fromAccounts[0].hiddenAccountNumber =  "***"+ accountNumber.substring(accountNumber.length-5, accountNumber.length);
//     frmCreditCardPayment.lblAccountCode.text=fromAccounts[0].hiddenAccountNumber;
//     frmCreditCardPayment.lblAccountType.text = fromAccounts[0].accountName;
//     frmCreditCardPayment.lblAccountCode.text = fromAccounts[0].hiddenAccountNumber;
//     frmCreditCardPayment.hiddenLblAccountNum.text = fromAccounts[0].accountID;
//     frmCreditCardPayment.lblBranchNum.text = fromAccounts[0].branchNumber;


//   }
  frmCreditCardPayment.lblHiddenAmount.text="";
//   frmCreditCardPayment.hiddenLblAccountNum.text = "";
//   frmCreditCardPayment.lblBranchNum.text = "";
//   frmCreditCardPayment.hiddenCardNumber.text = "";
//   frmCreditCardPayment.lblPaymentFlag.text = "";
//   frmCreditCardPayment.lblDum.text = "";
  frmCreditCardPayment.flxConfirmPopUp.setVisibility(false);
  frmCreditCardPayment.flxBody.setVisibility(true);
  frmCreditCardPayment.txtFieldAmount.text="";
}



function onnextcard()
{
  try{
  if(frmCreditCardPayment.btnNext.skin==="jomopaynextEnabled")
  {
  	var acc = kony.store.getItem("BillPayfromAcc");
    frmCreditCardPayment.hiddenLblAccountNum.text = acc.accountID;
    frmCreditCardPayment.lblBranchNum.text = acc.branchNumber;
    frmCreditCardPayment.hiddenCardNumber.text = gblCardNum;
    frmCreditCardPayment.lblHiddenAmount.text=frmCreditCardPayment.txtFieldAmount.text;
    frmCreditCardPayment.lblPaymentFlag.text = "C";
    frmCreditCardPayment.lblDum.text = "";
    amount=parseFloat(frmCreditCardPayment.txtFieldAmount.text.replace(/,/g,""));
    if(amount > 0){
      minamount=parseFloat(frmCreditCardPayment.txtFieldAmount.text.replace(/,/g,""));
//       if((amount<minamount) && frmCreditCardPayment.btnOtherCard.text !== "t")
//       {
//         customAlertPopup("",geti18Value("i18n.common.lessamountvalue"),popupCommonAlertDimiss,"");
//       }
//       else{
        serv_BILLSCOMISSIONCHARGE(frmCreditCardPayment.txtFieldAmount.text.replace(/,/g,""));
//         frmCreditCardPayment.lblAccountNumber.text=frmCreditCardPayment.lblAccountType.text;
//         if(frmCreditCardPayment.btnCard.text==="t")
//         {
//           frmCreditCardPayment.lblCard.text=frmCreditCardPayment.lblBOJCard.text;
//           frmCreditCardPayment.lblCardValue.text=frmCreditCardPayment.lblCardNumber.text;
//         }
//         else
//         {
//           frmCreditCardPayment.lblCard.text=frmCreditCardPayment.lblOtherCard.text;
//           frmCreditCardPayment.lblCardValue.text=frmCreditCardPayment.txtBoxOtherCard.text;
//         }

//         frmCreditCardPayment.lblTUAmountValue.text=frmCreditCardPayment.lblTUAmount.text + " JOD";
//         frmCreditCardPayment.lblPVValue.text=frmCreditCardPayment.lblPaymentValue.text + " JOD";
//         frmCreditCardPayment.lblAmountValue.text=frmCreditCardPayment.txtFieldAmount.text+" JOD";
//         frmCreditCardPayment.lblDueDateValue.text=frmCreditCardPayment.lblDate.text;
//         frmCreditCardPayment.flxBody.setVisibility(false);
//         frmCreditCardPayment.flxConfirmPopUp.setVisibility(true);
//       }
    }else{
    	customAlertPopup(geti18Value("i18n.maps.Info"),geti18nkey("i18n.common.zeroamount"),popupCommonAlertDimiss,"");
    }
  }
  }catch(e){
      exceptionLogCall("::onnextcard::","value assignig to UI","UI",e);
  }
}




function onclickcardbtn()
{
  frmCreditCardPayment.btnNext.skin="jomopaynextDisabled";
  frmCreditCardPayment.btnNext.focusSkin="jomopaynextDisabled";
  frmCreditCardPayment.btnCard.text="t";
  frmCreditCardPayment.btnOtherCard.text="s";
  frmCreditCardPayment.txtBoxOtherCard.text="1";
  // frmCreditCardPayment.lBoxCardList.selectedKey="";
  // frmCreditCardPayment.lBoxCardList.setVisibility(true);
  frmCreditCardPayment.lblCardNumber.text=geti18Value("i18n.cards.selectCard");
  frmCreditCardPayment.flxCardNumber.setVisibility(true);
  frmCreditCardPayment.txtBoxOtherCard.setVisibility(false);
  frmCreditCardPayment.flxTotalUsedAmount.isVisible = true;
  frmCreditCardPayment.flxPaymentValue.isVisible = true;
  frmCreditCardPayment.flxDueDate.isVisible = true;
  frmCreditCardPayment.flxAmount.top = "40%";
  frmCreditCardPayment.flxTUAmountConfirm.isVisible = true;
  frmCreditCardPayment.flxPVConfirm.isVisible = true;
  if(frmCreditCardPayment.lblCurrency.text === "JOD")
      {
        kony.print("JOD currency "+frmCreditCardPayment.lblCurrency.text);
        frmCreditCardPayment.txtFieldAmount.placeholder = "0.000";
        frmCreditCardPayment.txtFieldAmount.text = "";
      }
      else
      {
        kony.print("Other currency "+frmCreditCardPayment.lblCurrency.text);
        frmCreditCardPayment.txtFieldAmount.placeholder = "0.00";
        frmCreditCardPayment.txtFieldAmount.text = "";
      }

}

function plot_CARDSPAYMENTCONFIRMATIONSCREEN(data){
	try{
      kony.print("data * "+JSON.stringify(data));
    	frmCreditCardPayment.lblAccountNumber.text=frmCreditCardPayment.lblAccountType.text;
        if(frmCreditCardPayment.btnCard.text==="t")
        {
          frmCreditCardPayment.lblCard.text=frmCreditCardPayment.lblBOJCard.text;
          frmCreditCardPayment.lblCardValue.text=frmCreditCardPayment.lblCardNumber.text;
          frmCreditCardPayment.lblDueDateValue.text=frmCreditCardPayment.lblDate.text;
          frmCreditCardPayment.flxDueDateConfirm.isVisible =true;
        }
        else
        {
          frmCreditCardPayment.lblCard.text=frmCreditCardPayment.lblOtherCard.text;
          frmCreditCardPayment.lblCardValue.text=frmCreditCardPayment.txtBoxOtherCard.text;
          frmCreditCardPayment.flxDueDateConfirm.isVisible =false;
        }
		kony.print("frmCreditCardPayment.lblTUAmountValue.text "+ frmCreditCardPayment.lblTUAmountValue.text);
        frmCreditCardPayment.lblTUAmountValue.text=frmCreditCardPayment.lblTUAmount.text;
        frmCreditCardPayment.lblPVValue.text=frmCreditCardPayment.lblPaymentValue.text;//data.fees
        frmCreditCardPayment.lblAmountValue.text=setDecimal(parseFloat(parseFloat(data.exch_comm_chrg)+parseFloat(data.lc_amt)).toFixed(3), 3)+" JOD";//frmCreditCardPayment.txtFieldAmount.text
        frmCreditCardPayment.flxBody.setVisibility(false);
        frmCreditCardPayment.flxConfirmPopUp.setVisibility(true);
    }catch(e){
			  exceptionLogCall("plot_CARDSPAYMENTCONFIRMATIONSCREEN","UI ERROR","UI",e);
    	kony.print("Exception_plot_CARDSPAYMENTCONFIRMATIONSCREEN ::"+e);
    }
}

function onclickothercardbtn()
{
  frmCreditCardPayment.btnCard.text="s";
  frmCreditCardPayment.btnNext.skin="jomopaynextDisabled";
  frmCreditCardPayment.btnNext.focusSkin="jomopaynextDisabled";
  frmCreditCardPayment.btnOtherCard.text="t";
  frmCreditCardPayment.txtBoxOtherCard.text="";
  frmCreditCardPayment.lblCardNumber.text=geti18Value("i18n.cards.selectCard");
  // frmCreditCardPayment.lBoxCardList.selectedKey="";
  // frmCreditCardPayment.lBoxCardList.setVisibility(false);
  frmCreditCardPayment.flxCardNumber.setVisibility(false);
  frmCreditCardPayment.txtBoxOtherCard.setVisibility(true);
  frmCreditCardPayment.flxTotalUsedAmount.isVisible = false;
  frmCreditCardPayment.flxPaymentValue.isVisible = false;
  frmCreditCardPayment.flxDueDate.isVisible = false;
  frmCreditCardPayment.flxAmount.top = "0%";
  frmCreditCardPayment.flxTUAmountConfirm.isVisible = false;
  frmCreditCardPayment.flxPVConfirm.isVisible = false;
   if(frmCreditCardPayment.lblCurrency.text === "JOD")
      {
        kony.print("JOD currency "+frmCreditCardPayment.lblCurrency.text);
        frmCreditCardPayment.txtFieldAmount.placeholder = "0.000";
        frmCreditCardPayment.txtFieldAmount.text = "";
      }
      else
      {
        kony.print("Other currency "+frmCreditCardPayment.lblCurrency.text);
        frmCreditCardPayment.txtFieldAmount.placeholder = "0.00";
        frmCreditCardPayment.txtFieldAmount.text = "";
      }
}



function ontextchangeothercard()
{
  // alert("Slected Key: "+frmCreditCardPayment.lBoxCardList.selectedKey);
  // alert("TextFieldAmount Text: "+frmCreditCardPayment.txtFieldAmount.text);
  frmCreditCardPayment.hiddenCardNumber.text = frmCreditCardPayment.txtBoxOtherCard.text;
  if(frmCreditCardPayment.txtFieldAmount.text!==""&&frmCreditCardPayment.txtFieldAmount.text!==null&&frmCreditCardPayment.txtBoxOtherCard.text!==""&&frmCreditCardPayment.txtBoxOtherCard.text.length===16.0)
  {
    frmCreditCardPayment.btnNext.skin="jomopaynextEnabled";
    frmCreditCardPayment.btnNext.focusSkin="jomopaynextEnabled";
  }
  else
  {
    frmCreditCardPayment.btnNext.skin="jomopaynextDisabled";
    frmCreditCardPayment.btnNext.focusSkin="jomopaynextDisabled";
  }
}


function ontextchangeamountcard()
{
  // alert("Selected Key: "+frmCreditCardPayment.lBoxCardList.selectedKey);
  // alert("Textof Other Card: "+frmCreditCardPayment.txtBoxOtherCard.text);

  if(frmCreditCardPayment.btnCard.text==="t")
  {
    if(frmCreditCardPayment.lblCardNumber.text!=="Please Select Card Number"&&frmCreditCardPayment.txtFieldAmount.text!=="" && frmCreditCardPayment.txtFieldAmount.text !== ".")
    {
      frmCreditCardPayment.btnNext.skin="jomopaynextEnabled";
      frmCreditCardPayment.btnNext.focusSkin="jomopaynextEnabled";
    }
    else
    {
      frmCreditCardPayment.btnNext.skin="jomopaynextDisabled";
      frmCreditCardPayment.btnNext.focusSkin="jomopaynextDisabled";
    }
  }
  else
  {
    if(frmCreditCardPayment.txtBoxOtherCard.text.length===16.0&&frmCreditCardPayment.txtBoxOtherCard.text!==""&&frmCreditCardPayment.txtBoxOtherCard.text!==null&&frmCreditCardPayment.txtFieldAmount.text!==""&&frmCreditCardPayment.txtFieldAmount.text != "." && frmCreditCardPayment.txtFieldAmount.text != 0 && frmCreditCardPayment.txtFieldAmount.text != "NaN")
    {
      frmCreditCardPayment.btnNext.skin="jomopaynextEnabled";
      frmCreditCardPayment.btnNext.focusSkin="jomopaynextEnabled";
    }
    else
    {
      frmCreditCardPayment.btnNext.skin="jomopaynextDisabled";
      frmCreditCardPayment.btnNext.focusSkin="jomopaynextDisabled";
    }
  }
  if((frmCreditCardPayment.txtFieldAmount.text.replace(/,/g,"")).length > 9){
 	frmCreditCardPayment.txtFieldAmount.text = (frmCreditCardPayment.txtFieldAmount.text.replace(/,/g,"")).substring(0,9);
  }
}


function onendeditingiphone()
{
  frmCreditCardPayment.flxBorderAmount.skin="skntextFieldDividerGreen";
  if (frmCreditCardPayment.txtFieldAmount.text !== "" && frmCreditCardPayment.txtFieldAmount.text !== "0" && frmCreditCardPayment.txtFieldAmount.text !== 0) 
  {
    var amount = Number.parseFloat(frmCreditCardPayment.txtFieldAmount.text).toFixed(3);
    frmCreditCardPayment.txtFieldAmount.text = amount;
    //       	frmCreditCardPayment.btnNext.skin="jomopaynextEnabled";
    //       	frmCreditCardPayment.btnNext.focusSkin="jomopaynextEnabled";
  }
  // else
  //   {
  //     	frmCreditCardPayment.btnNext.skin="jomopaynextDisabled";
  //     	frmCreditCardPayment.btnNext.focusSkin="jomopaynextDisabled";
  //   }
}



function initweb()
{
  try{
    var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.accountsData;
    var storeData = isEmpty(fromAccounts[0]) ? "" : fromAccounts[0];
    kony.store.setItem("frmAccount",storeData);
    if(fromAccounts !== undefined && fromAccounts !== null){
      frmWebCharge.lblAccountType.text=fromAccounts[0].accountName;
      var accountNumber = fromAccounts[0].accountID;
      fromAccounts[0].hiddenAccountNumber =  accountNumber;//"***"+ accountNumber.substring(accountNumber.length-5, accountNumber.length);
      frmWebCharge.lblAccountCode.text=fromAccounts[0].hiddenAccountNumber;
      frmWebCharge.lblCurrencyCode.text=fromAccounts[0].currencyCode;
      frmWebCharge.lblhiddenFrmAcc.text=accountNumber;
      if(frmWebCharge.lblCurrencyCode.text==="JOD")
      {
        frmWebCharge.txtFieldAmount.placeholder="0.000";
      }
      else
      {
        frmWebCharge.txtFieldAmount.placeholder="0.00";
      }
    }
  }catch(e){
	  exceptionLogCall("initweb","UI ERROR","UI",e);
    kony.print("Exception_initweb ::"+e);
  }
}


function preshowweb()
{
  var segAccountListData = kony.retailBanking.globalData.accountsDashboardData.accountsData;
  var fromAccounts = [];
   for(var i in segAccountListData)
      {  
      if(segAccountListData[i]["accountType"] != "T" && segAccountListData[i]["accountType"] != "L" && segAccountListData[i]["dr_ind"] == "Y" && segAccountListData[i]["productCode"] != "320"){
          fromAccounts.push(segAccountListData[i]);
       }
      }
  if(fromAccounts !== null && fromAccounts !== undefined && fromAccounts !== ""){
    var len5 = fromAccounts.length;
    if (len5 < 2) {
      frmWebCharge.lblAccountType.text=fromAccounts[0].accountName;
      var accountNumber = fromAccounts[0].accountID;
      fromAccounts[0].hiddenAccountNumber =  accountNumber;//"***"+ accountNumber.substring(accountNumber.length-5, accountNumber.length);
      frmWebCharge.lblAccountCode.text=fromAccounts[0].hiddenAccountNumber;
      frmWebCharge.lblCurrencyCode.text=fromAccounts[0].currencyCode;
      if(frmWebCharge.lblCurrencyCode.text==="JOD")
      {
        frmWebCharge.txtFieldAmount.placeholder="0.000";
      }
      else
      {
        frmWebCharge.txtFieldAmount.placeholder="0.00";
      }
      var storeData = isEmpty(fromAccounts[0]) ? "" : fromAccounts[0];
      kony.store.setItem("frmAccount",storeData);
    }
  }
}


function onclicknextweb()
{
  if(frmWebCharge.btnNext.skin==="jomopaynextEnabled")
  {
    gblTModule = "web";
    gotoConfirmTransfer(); 
  }
}

function ontextchangeamountweb()
{
  if(frmWebCharge.txtFieldAmount!==null&&frmWebCharge.txtFieldAmount!==""&&frmWebCharge.txtFieldAmount!=="0")
  {
    frmWebCharge.btnNext.skin="jomopaynextEnabled";
    frmWebCharge.btnNext.focusSkin="jomopaynextEnabled";
  }
  else
  {
    frmWebCharge.btnNext.skin="jomopaynextDisabled";
    frmWebCharge.btnNext.focusSkin="jomopaynextDisabled";
  }
}


function onendeditingweb()
{
  frmWebCharge.flxBorderAmount.skin="skntextFieldDividerGreen";
  if (frmWebCharge.txtFieldAmount.text !== "" && frmWebCharge.txtFieldAmount.text !== "0" && frmWebCharge.txtFieldAmount.text !== 0) 
  {
    var amount = Number.parseFloat(frmWebCharge.txtFieldAmount.text).toFixed(3);
    frmWebCharge.txtFieldAmount.text = amount;
  }
}

function service_fetch_cardListAndDetils(params){
  try{
    kony.print("Inside service_fetch_cardListAndDetils");
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController(params.form);
    var navObject = new kony.sdk.mvvm.NavigationObject();
    navObject.setRequestOptions("segCardsLanding", {
      "headers": {
        "session_token": kony.retailBanking.globalData.session_token
      },
      "queryParams": {
        "custId": params.custID
      }
    });
    controller.loadDataAndShowForm(navObject);
  }catch(e){
    kony.print("Exception_service_fetch_cardListAndDetils ::"+e);
  }
}

function process_CardListDetails(data){
  try{
    var cardListSegData = [];
    kony.print("form printing ::"+JSON.stringify(frmCardsLandingKA.segCardsLanding));
    kony.print("process_CardListDetails ::"+JSON.stringify(data));
    var cardImage = "boj_card_elite_credit_platinum_v4_01.png",cards_Image = null;
    var type = "";
    var cardHolderName = "";
    var cardNumber = "",top = "",left = "",right = "";
        for(var i in data){
      cardHolderName = "";
	  cardNumber = "";
      if(!isEmpty(data[i].card_category)){
        if(data[i].card_category === "S"){
          cardImage = "creditsilvercard.png";
          type = geti18Value("i18n.cards.visasilver");//"Visa Silver";
        }else if(data[i].card_category === "G"){
          cardImage = "boj_card_affluent_credit_gold_v4_01.png";
          type = geti18Value("i18n.cards.visagold");//"Visa Gold";
        }else if(data[i].card_category === "P"){
          cardImage = "boj_card_affluent_credit_platinum_v4_01.png";
          type = geti18Value("i18n.cards.visaplatinum");//"Visa Platinum";
        }else if(data[i].card_category === "M"){
          cardImage = "creditworldcard.png";
          type = geti18Value("i18n.cards.worldmastercard");//"World MasterCard";
        }else if(data[i].card_category === "0037"){
        	cardImage = "debit_mass.png";
        	type = "Master";//geti18Value("i18n.common.master");//"Master";
        }else if(data[i].card_category === "0038"){
        	cardImage = "debit_mumaiaz.png";
        	type = "Master";//geti18Value("i18n.common.master");//"Master";
        }else if(data[i].card_category === "0039"){
        	cardImage = "debit_wisam_mumaiaz.png";
        	type = "Master";//geti18Value("i18n.common.master");//"Master";
        }else if(data[i].card_category === "0040"){
        	cardImage = "debit_webcharge.png";
        	type = "Master";//geti18Value("i18n.common.master");//"Master";
        }else if(data[i].card_category === "0041"){
        	cardImage = "debit_webcharge.png";
        	type = "Master";//geti18Value("i18n.common.master");//"Master";
        }else{
          cardImage = "bluecard.png";
          type = "Visa";//geti18Value("i18n.common.visa");//"Visa";
        }
      }else if(!isEmpty(data[i].card_type)){
        if(data[i].card_type === "V"){
        	cardImage = "bluecard.png";
        	type = "Visa";//geti18Value("i18n.common.visa");//"Visa";
        }else{
        	type = "Master";//geti18Value("i18n.common.master");//"Master";
        	cardImage = "card_affluent_debit.png";
        }
      }
      if(data[i].cardTypeFlag == "C"){
        data[i].card_type = type;//+" "+geti18Value("i18n.cards.creditcard");
      }else if(data[i].cardTypeFlag == "D"){
        data[i].card_type = type==="Master"?type+"card® debit":"VISA Debit";//+" "+geti18Value("i18n.messages.debitCard");
      }else if(data[i].cardTypeFlag == "W"){
        if(data[i].card_category === "0036"){
        	cardImage = "bluecard.png";
        	type = "Visa";//geti18Value("i18n.common.visa");//"Visa";
        }else{
          cardImage = "debit_webcharge.png";
          type = "Master";//geti18Value("i18n.common.master");//"Master";
        }
        data[i].card_type = "Web Charge";//type;//+" "+geti18Value("i18n.cards.webchargecard");
      }
      if(cardImage === "bluecard.png"){
        top = "35%";
        right = "26%";
        left = "28%";
      }else{
        top = "27%";
        right = "27%";
        left = "22%";
      }
      if(kony.store.getItem("langPrefObj") == "ar"){
        cardNumber = {"text":mask_CreditCardNumber(data[i].card_num),"isVisible":true,"top":top,"right":right};
      	cardHolderName = {"text":data[i].name_on_card,"isVisible":true,"right":"17%","contentAlignment":constants.CONTENT_ALIGN_MIDDLE_LEFT};
      }else{
        cardNumber = {"text":mask_CreditCardNumber(data[i].card_num),"isVisible":true,"top":top,"left":left};
      	cardHolderName = {"text":data[i].name_on_card,"isVisible":true};
      }
      var dateSplit = data[i].card_exp_date.split("-");
      if(!isEmpty(data[i]["nickname"])){
        data[i].card_type = data[i]["nickname"];
      }
      cardListSegData[i] = {"card_type":{"text":data[i].card_type,"isVisible":true},
                            "cardTypeFlag":{"text":data[i].cardTypeFlag,"isVisible":false},
                            "card_num":cardNumber,
                            "card_exp_date":{"text":dateSplit[1]+"/"+dateSplit[0].substring(2,4),"isVisible":true},
                            "name_on_card":cardHolderName, 
                            "cardImage":{"src":cardImage,"isVisible":true},
                            "nickname" : data[i].nickname,
                            "showHideFlag" : data[i].showHideFlag,
                            "prefNumber" : data[i].prefNumber,  
                             "card_mobile_number" : data[i].card_mobile_number,
                             "cardid" : data[i].card_code,
                         	"card_category_type":data[i].card_type};

      if(data[i].cardTypeFlag === "C"){
        data[i].issue_date = format_date(data[i].issue_date,"DD/MM/YYYY");
        data[i].payment_due_date = format_date(data[i].payment_due_date,"DD/MM/YYYY");
        data[i].card_exp_date = format_date(data[i].card_exp_date,"DD/MM/YYYY");
      }else if(data[i].cardTypeFlag === "D" || data[i].cardTypeFlag === "W"){
        data[i].card_exp_date = format_date(data[i].card_exp_date,"DD/MM/YYYY");
      }
    }
    kony.print("process_CardListDetails ::"+JSON.stringify(cardListSegData));
    return cardListSegData;
  }catch(e){
    kony.print("Exception_process_CardListDetails ::"+e);
  }
}

function get_cardDataList(){
  try{
    var tempData = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex];
    var splitDate = null;
    /***********CLEARING SORT POPUP ICON SELECTION************/
    frmManageCardsKA.imgSortDateAsc.src = "map_arrow_straight_nonselected.png";
    frmManageCardsKA.imgSortDateDesc.src = "map_arrow_straight_nonselected.png";
    frmManageCardsKA.imgSortAmountAsc.src = "map_arrow_straight_nonselected.png";
    frmManageCardsKA.imgSortAmountDesc.src = "map_arrow_straight_nonselected.png";
    /************END*********/
//     frmManageCardsKA.segCardsKA.selectedIndex = [0,customerAccountDetails.currentIndex];
//     clearForm.filterTransactions();
	//1986 fix
    //
    	set_UI_FOR_CASA_USER(frmManageCardsKA);
    //
    
    frmManageCardsKA.flxCreditCardPay.setVisibility(false);
    frmManageCardsKA.flxCardChequeBook.setVisibility(false);
    frmManageCardsKA.txtSearch.skin = "sknTxtBoxSearch";
    frmManageCardsKA.flxRedeemNow.setVisibility(false);
    frmManageCardsKA.flxRequestNewPin.setVisibility(false);
	frmManageCardsKA.flxUnblockPin.setVisibility(false);
    frmManageCardsKA.flxATMPOSLimit.setVisibility(false);
    frmManageCardsKA.flxReqStmntCreditCard.setVisibility(false);
    frmManageCardsKA.flxDebitCardLinkedAccounts.setVisibility(false);
    frmManageCardsKA.flxPinManagement.setVisibility(false);
    frmManageCardsKA.flxCardLimitUpdate.setVisibility(false);
    frmManageCardsKA.flxCreditCardStatus.isVisible = false;
    frmManageCardsKA.flxAvailableBalance.isVisible = false;
    frmManageCardsKA.flxExpiryDate.isVisible = false;
    frmManageCardsKA.flxLineInfo2.isVisible = true;
    frmManageCardsKA.flxOpeningDate.isVisible = true;
    frmManageCardsKA.flxLineInfo3.isVisible = true;
    frmManageCardsKA.flxOpenedIn.isVisible = true;
  	if(kony.application.getCurrentForm().id !== "frmFilterTransaction"){
      frmManageCardsKA.flxSettings.isVisible = false;
      frmManageCardsKA.flxTransactions.isVisible = false;
      frmManageCardsKA.flxInfo.isVisible = true;
    }
    frmManageCardsKA.btnInfo.skin = "btnCardFoc";
  	frmManageCardsKA.btnTransactions.skin = "btnCardsScreen";
  	frmManageCardsKA.btnSettings.skin = "btnCardsScreen";
    frmManageCardsKA.lblCcMobileNumber.text = "";
    frmManageCardsKA.lblCcMobileNumber.isVisible = false;
    frmManageCardsKA.lblCcMobileNumberTitle.isVisible  = false;
  	frmManageCardsKA.flxChangeMobileNumber.setVisibility(false);
    frmManageCardsKA.lblChangeMobileNumber.text = geti18Value("i18n.common.ChangeMobileNumber");
    if(tempData.cardTypeFlag == "C" || tempData.isWearablePrepaid === true){
	  //frmManageCardsKA.flxATMPOSLimit.setVisibility(true);
      //frmManageCardsKA.flxPinManagement.setVisibility(true);
      //frmManageCardsKA.flxCardChequeBook.setVisibility(true);
      //if(tempData.isWearablePrepaid === false){
      //frmManageCardsKA.flxRedeemNow.setVisibility(true);
      //}
      //frmManageCardsKA.flxCardLimitUpdate.setVisibility(true);
      //frmManageCardsKA.flxReqStmntCreditCard.setVisibility(true);
	  frmManageCardsKA.flxUnblockPin.setVisibility(true);
      frmManageCardsKA.flxWebPay.setVisibility(false);
      frmManageCardsKA.flxStatusInfo.isVisible = true;
      frmManageCardsKA.flxLineInfo2.isVisible = false;
      frmManageCardsKA.flxOpeningDate.isVisible = false;
      frmManageCardsKA.flxLineInfo3.isVisible = false;
      frmManageCardsKA.flxOpenedIn.isVisible = false;
      frmManageCardsKA.flxCreditCardPay.setVisibility(true);
      //frmManageCardsKA.flxChangeMobileNumber.setVisibility(true);
      frmManageCardsKA.lblCardStatement.isVisible = true;
      //added 
      frmManageCardsKA.lblCardStatmentDownload.isVisible=false;
      //end
      frmManageCardsKA.flxDeactivateKA.isVisible = true;
      frmManageCardsKA.flxLineInfo5.isVisible = true;
      frmManageCardsKA.flxLineInfo6.isVisible = true;
      frmManageCardsKA.flxCreditCardStatus.isVisible = true;
      frmManageCardsKA.flxAvailableBalance.isVisible = true;
      frmManageCardsKA.flxExpiryDate.isVisible = true;
      frmManageCardsKA.lblCcMobileNumber.text = tempData.card_mobile_number;
      frmManageCardsKA.lblCcMobileNumber.isVisible = true;
      frmManageCardsKA.lblCcMobileNumberTitle.isVisible  = true;
      frmManageCardsKA.lblCardLimitTilte.text = geti18Value("i18n.card.availabelcerditlimit");
      frmManageCardsKA.lblAvailableCreditLimitTitile.text = geti18Value("i18n.creditcard.totalamount");
      frmManageCardsKA.lblAvailableBalance.text = geti18Value("i18n.deposit.availableBalance");
      frmManageCardsKA.lblAccountNumberTitle.text = geti18Value("i18n.bills.IssueDate");
      frmManageCardsKA.lblExpirtDateTitle.text = geti18Value("i18n.common.expirydate");
      frmManageCardsKA.lblCreatedOnTitle.text = geti18Value("i18n.common.createdon");
      frmManageCardsKA.lblSpendCreditLimitTitle.text = geti18Value("i18n.common.spendcreditlimit");
      frmManageCardsKA.lblCardLimit.text = tempData.card_limit;
      frmManageCardsKA.lblAvailableCreditLimit.text = tempData.total_billed_amount;
      frmManageCardsKA.lblAvailableBalance.text = tempData.spending_limit;
      frmManageCardsKA.lblCreatedOn.text = tempData.issue_date || "";
      
      //1334 fix
      frmManageCardsKA.lblInfoNewEPN.text=geti18Value("i18n.cards.epn");
      frmManageCardsKA.lblInfoNewEPN.isVisible=true;
      frmManageCardsKA.lblInfoNewEPN1.text=tempData.card_code;
      //1334 end
      
      
      
      var date_splited = "";
      if(tempData.card_exp_date !== "" && tempData.card_exp_date !== undefined && tempData.card_exp_date !== null){
        	splitDate = tempData.card_exp_date.split("/");
        date_splited = splitDate[1]+"/"+splitDate[2].substring(splitDate[2].length-2,splitDate[2].length);
      }
      frmManageCardsKA.lblExpiryDate.text = date_splited;
      date_splited = "";
      splitDate = null;
      if(tempData.issue_date !== "" && tempData.issue_date !== undefined && tempData.issue_date !== null){
		if(tempData.isWearablePrepaid === true)
        	splitDate = tempData.issue_date.split("-");
        else
			splitDate = tempData.issue_date.split("/");
      	date_splited = splitDate[1]+"/"+splitDate[2].substring(splitDate[2].length-2,splitDate[2].length);
      }
      frmManageCardsKA.lblAccountNumber.text = date_splited;
      date_splited = "";
      frmManageCardsKA.lblSpendCreditLimit.text = tempData.spending_limit;
      frmManageCardsKA.lblNextDueDate.text = tempData.payment_due_date;
      frmManageCardsKA.lblMinimumDueAmount.text = tempData.total_due_amt;
      frmManageCardsKA.lblEarnedRewardPoints.text = tempData.card_point;
      frmManageCardsKA.flxIBAN.isVisible = true;
      frmManageCardsKA.CopyflxIBAN0hd4c5a6edeee48.isVisible = true;
      frmManageCardsKA.CopyflxOpenedIn0b6e2187bba4f41.isVisible = false;
      if(tempData.card_status == "Deactivated"){
        frmManageCardsKA.flxStopCardSwitchOff.isVisible = true;
        frmManageCardsKA.flxStopCardSwitchOn.isVisible = false;
        frmManageCardsKA.lblCreditCardStatus.text = geti18Value("i18n.card.notactive");
      }else{
        frmManageCardsKA.flxStopCardSwitchOff.isVisible = false;
        frmManageCardsKA.flxStopCardSwitchOn.isVisible = true;
        frmManageCardsKA.lblCreditCardStatus.text = geti18Value("i18n.card.active");
      }
      CARDLIST_DETAILS.cardNumber = tempData.card_num;
      if(kony.application.getCurrentForm().id === "frmFilterTransaction" && filter_OPTIONS.other.from !== null && filter_OPTIONS.other.to !== null)
    	serv_getCrCardTransactions({"card_num":tempData.card_num},{"toDATE":filter_OPTIONS.other.to,"fromDATE":filter_OPTIONS.other.from});
      else{
        clearForm.filterTransactions();
      	serv_getCrCardTransactions({"card_num":tempData.card_num},{"toDATE":"","fromDATE":""});
      }
    }else if(tempData.cardTypeFlag == "D"){
      frmManageCardsKA.flxWebPay.setVisibility(false);
      frmManageCardsKA.flxStatusInfo.isVisible = false;
      //frmManageCardsKA.flxRequestNewPin.setVisibility(true);
      //frmManageCardsKA.flxChangeMobileNumber.setVisibility(true);
      //frmManageCardsKA.flxDebitCardLinkedAccounts.setVisibility(true);
      frmManageCardsKA.flxIBAN.isVisible = false;
      frmManageCardsKA.CopyflxIBAN0hd4c5a6edeee48.isVisible = false;
      frmManageCardsKA.CopyflxOpenedIn0b6e2187bba4f41.isVisible = false;
      frmManageCardsKA.lblCardStatement.isVisible = false;
      //added 
      frmManageCardsKA.lblCardStatmentDownload.isVisible=false;
      //end
      frmManageCardsKA.flxLineInfo5.isVisible = false;
      frmManageCardsKA.flxLineInfo6.isVisible = false;
      frmManageCardsKA.lblAvailableCreditLimitTitile.text = geti18Value("i18n.common.type");
      frmManageCardsKA.lblAccountNumberTitle.text = geti18Value("i18n.accounts.status");
      frmManageCardsKA.lblCreatedOnTitle.text = geti18Value("i18n.common.expirydate");
      frmManageCardsKA.lblSpendCreditLimitTitle.text = geti18Value("i18n.common.connectto");
      frmManageCardsKA.lblChangeMobileNumber.text = geti18Value("i18n.common.ChangeSMSNumber");
      //1334 fix
      frmManageCardsKA.lblInfoNewEPN.text=geti18Value("i18n.cards.epn");
      frmManageCardsKA.lblInfoNewEPN.isVisible=true;
      frmManageCardsKA.lblInfoNewEPN1.text=tempData.card_code;
      //1334 end
      frmManageCardsKA.lblAvailableCreditLimit.text = tempData.card_type.indexOf("VISA")>-1?geti18Value("i18n.common.visa"):geti18Value("i18n.common.master");
      if(tempData.card_status == "2" || tempData.card_status == 2){
        frmManageCardsKA.lblAccountNumber.text = geti18Value("i18n.card.active");
        frmManageCardsKA.flxStopCardSwitchOff.isVisible = false;
        frmManageCardsKA.flxStopCardSwitchOn.isVisible = true;
        frmManageCardsKA.flxDeactivateKA.isVisible = true;
      }else if(tempData.card_status == "3" || tempData.card_status == 3){
        frmManageCardsKA.lblAccountNumber.text = geti18Value("i18n.card.notactive");
        frmManageCardsKA.flxStopCardSwitchOff.isVisible = true;
        frmManageCardsKA.flxStopCardSwitchOn.isVisible = false;
//         frmManageCardsKA.flxDeactivateKA.isVisible = false;
      }
      splitDate = tempData.card_exp_date.split("/");
      frmManageCardsKA.lblCreatedOn.text = splitDate[1]+"/"+splitDate[2].substring(splitDate[2].length-2,splitDate[2].length);
      frmManageCardsKA.lblSpendCreditLimit.text = tempData.card_acc_num;
      CARDLIST_DETAILS.cardNumber = tempData.card_num;
      if(kony.application.getCurrentForm().id === "frmFilterTransaction" && filter_OPTIONS.other.from !== null && filter_OPTIONS.other.to !== null)
      	serv_FETCHDEBITCARDTRANSACTION({"card_number":tempData.card_num,"card_acc_num":tempData.card_acc_num,"card_acc_branch":tempData.card_acc_branch},{"toDATE":filter_OPTIONS.other.to,"fromDATE":filter_OPTIONS.other.from});
      else{
        clearForm.filterTransactions();
      	serv_FETCHDEBITCARDTRANSACTION({"card_number":tempData.card_num,"card_acc_num":tempData.card_acc_num,"card_acc_branch":tempData.card_acc_branch},{"toDATE":"","fromDATE":""});
      }
    }else if(tempData.cardTypeFlag == "W"){
      frmManageCardsKA.flxWebPay.setVisibility(true);
      //frmManageCardsKA.flxRequestNewPin.setVisibility(true);
      frmManageCardsKA.flxStatusInfo.isVisible = false;
      //frmManageCardsKA.flxChangeMobileNumber.setVisibility(true);
      frmManageCardsKA.flxIBAN.isVisible = false;
      frmManageCardsKA.CopyflxIBAN0hd4c5a6edeee48.isVisible = false;
      frmManageCardsKA.CopyflxOpenedIn0b6e2187bba4f41.isVisible = false;
      frmManageCardsKA.lblCardStatement.isVisible = false;
      //added 
      frmManageCardsKA.lblCardStatmentDownload.isVisible=false;
      //end
      frmManageCardsKA.flxDeactivateKA.isVisible = true;
      frmManageCardsKA.flxLineInfo5.isVisible = false;
      frmManageCardsKA.flxLineInfo6.isVisible = false;
      frmManageCardsKA.lblAvailableCreditLimitTitile.text = geti18Value("i18n.common.type");
      frmManageCardsKA.lblAccountNumberTitle.text = geti18Value("i18n.accounts.status");
      frmManageCardsKA.lblCreatedOnTitle.text = geti18Value("i18n.common.expirydate");
      frmManageCardsKA.lblSpendCreditLimitTitle.text = geti18Value("i18n.deposit.availableBalance");
      frmManageCardsKA.lblChangeMobileNumber.text = geti18Value("i18n.common.ChangeSMSNumber");
      
      //1334 fix
      frmManageCardsKA.lblInfoNewEPN.text=geti18Value("i18n.cards.epn");
      frmManageCardsKA.lblInfoNewEPN.isVisible=true;
      frmManageCardsKA.lblInfoNewEPN1.text=tempData.card_code;
      //1334 end
      
      frmManageCardsKA.lblAvailableCreditLimit.text = tempData.card_type.indexOf("VISA")>-1?geti18Value("i18n.common.visa"):geti18Value("i18n.common.master");
      if(tempData.card_status == "2" || tempData.card_status == 2){
        frmManageCardsKA.lblAccountNumber.text = geti18Value("i18n.card.active");
        frmManageCardsKA.flxStopCardSwitchOff.isVisible = false;
        frmManageCardsKA.flxStopCardSwitchOn.isVisible = true;
      }else if(tempData.card_status == "3" || tempData.card_status == 3){
        frmManageCardsKA.lblAccountNumber.text = geti18Value("i18n.card.notactive");
        frmManageCardsKA.flxStopCardSwitchOff.isVisible = true;
        frmManageCardsKA.flxStopCardSwitchOn.isVisible = false;
      }
      splitDate = tempData.card_exp_date.split("/");
      frmManageCardsKA.lblCreatedOn.text = splitDate[1]+"/"+splitDate[2].substring(splitDate[2].length-2,splitDate[2].length);
      frmManageCardsKA.lblSpendCreditLimit.text = isEmpty(tempData.card_balance) ? "0.000 JOD" :  (formatamountwithCurrency(tempData.card_balance, "JOD") + " JOD");
      kony.print("After break:");
      frmManageCardsKA.CopyflxOpenedIn0b6e2187bba4f41.isVisible = false;
      frmManageCardsKA.CopyinterestRateLabel0aaaa0c9e91c743.text = "Top Up";
      CARDLIST_DETAILS.cardNumber = tempData.card_num;
      if(kony.application.getCurrentForm().id === "frmFilterTransaction" && filter_OPTIONS.other.from !== null && filter_OPTIONS.other.to !== null)
      	serv_FETCHDEBITCARDTRANSACTION({"card_number":tempData.card_num,"card_acc_num":tempData.card_acc_num,"card_acc_branch":tempData.card_acc_branch},{"toDATE":filter_OPTIONS.other.to,"fromDATE":filter_OPTIONS.other.from});
      else{
        clearForm.filterTransactions();
      	serv_FETCHDEBITCARDTRANSACTION({"card_number":tempData.card_num,"card_acc_num":tempData.card_acc_num,"card_acc_branch":tempData.card_acc_branch},{"toDATE":"","fromDATE":""});
      }
    }

  }catch(e){
    kony.print("Exception_get_cardDataList ::"+e);
  }
}

function validate_cardStatementFilterDone(){
  try{
    var curDate = new Date();
    var isValid = false;
    if(gblDownloadPDFFlow){
       if((frmCardStatementKA.lblDateFrom.text !== "" && frmCardStatementKA.lblDateFrom.text !== null) &&
       (frmCardStatementKA.lblDateTo.text !== "" && frmCardStatementKA.lblDateTo.text !== null) && !isEmpty(frmCardStatementKA.lblDownloadType.text)){
      if(new Date(frmCardStatementKA.lblDateFrom.text) < new Date(frmCardStatementKA.lblDateTo.text)){
//         if(curDate.getMonth()+1 >= parseInt(frmCardStatementKA.lblCardDate.text).toFixed()){
          isValid = true;
//         }else{
//            customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.common.MonthCurrMon"), popupCommonAlertDimiss, "");
         
//           }
      }else{
        kony.print("customAlertPopup elese isValid");
        isValid = true;
      }
    }
    }else{
    if((frmCardStatementKA.lblCardYear.text !== "" && frmCardStatementKA.lblCardYear.text !== null) &&
       (frmCardStatementKA.lblCardDate.text !== "" && frmCardStatementKA.lblCardDate.text !== null)){
      if(curDate.getFullYear() == parseInt(frmCardStatementKA.lblCardYear.text).toFixed()){
        if(curDate.getMonth()+1 >= parseInt(frmCardStatementKA.lblCardDate.text).toFixed()){
          isValid = true;
        }else{
          customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.common.MonthCurrMon"), popupCommonAlertDimiss, "");
        }
      }else{
        kony.print("customAlertPopup elese isValid");
        isValid = true;
      }
    }
    }
    if(isValid){
      if(gblDownloadPDFFlow){
        //#ifdef android
        getPermissionforStorage();               
      //#endif
        //#ifdef iphone
         serv_cardStatementDownLoadPDFXL({"dateFrom":frmCardStatementKA.lblDateFrom.text,"dateTo":frmCardStatementKA.lblDateTo.text,"Type":frmCardStatementKA.lblDownloadType.text,"serviceFlag":"CT"}); 
         //#endif
      }else{
      serv_cardStatement({"Year":frmCardStatementKA.lblCardYear.text,"Month":frmCardStatementKA.lblCardDate.text});
    }} else{
      kony.print("customAlertPopup elese");
      if(isEmpty(frmCardStatementKA.lblCardYear.text) || isEmpty(frmCardStatementKA.lblCardDate.text))
    	 customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.cardstatement.validation"), popupCommonAlertDimiss, "");
     else if(isEmpty(frmCardStatementKA.lblDownloadType.text))
             customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.cardstatement.validationDownloadType"), popupCommonAlertDimiss, "");}	
  }catch(e){
    kony.print("Exception_validate_cardStatementFilterDone ::"+e);
  }
}


function serv_getCardTransactions(data, dateOPTIONS){
  try{
    
    var queryParams = {"custId":custid,
                       "accountNumber":data.card_acc_num,
                       "p_debit_card_no":data.card_number,
                       "p_no_Of_Txn":"100",
                       "p_Branch":data.card_acc_branch,
                       "p_toDate":((dateOPTIONS.toDATE!=="")&&(dateOPTIONS.toDATE!==null))?dateOPTIONS.toDATE:"",
        				"p_fromDate":((dateOPTIONS.fromDATE!=="")&&(dateOPTIONS.fromDATE!==null))?dateOPTIONS.fromDATE:"",
                       "lang":"eng"};
    kony.print("card transactions parameters ::"+JSON.stringify(queryParams));
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJGetTransactionDetails");
    if (kony.sdk.isNetworkAvailable()) {
      kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      appMFConfiguration.invokeOperation("prGetTransactionsDetails", {},queryParams,serv_successgetCardTransactions,serv_errorgetCardTransactions);
    }else{
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
  }catch(e){
    kony.print("Exception_serv_getCardTransactions ::"+e);
  }

  function serv_errorgetCardTransactions(error){
    kony.print("error in card transaction ::"+JSON.stringify(error));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  }
}

function serv_successgetCardTransactions(response){
  kony.print("response for card transaction ::"+JSON.stringify(response));
  var temp = [];
  var data = "";
  var currencyCode = "";
  var skin = "";
  var category = "", toShow = false;
  kony.retailBanking.globalData.accountsTransactionList = {"transaction":temp};

  if(response.Transactions !== undefined && response.Transactions !== null){
    data = response.Transactions;
    currencyCode = "JOD";
    for(var i in kony.retailBanking.globalData.accountsDashboardData.accountsData){
        if(kony.retailBanking.globalData.accountsDashboardData.accountsData[i].accountID === CARDLIST_DETAILS.cardNumber){
        	currencyCode = kony.retailBanking.globalData.accountsDashboardData.accountsData[i].currencyCode;
        	break;
        }
    }
    for(var j in data){
    	if(data[j].txnType === "D")
        	data[j].amount = "- "+data[j].amount;
    	else if(data[j].txnType === "C")
        	data[j].amount = "+ "+data[j].amount;
    }
  }else if(response.History !== undefined && response.History !== null){
    for(var i = 0; i < response.History.length; i++){
      response.History[i].description = response.History[i].merchant_name_location;
      if(response.History[i].trn_amt.indexOf("-") > -1){
      	response.History[i].trn_amt = response.History[i].trn_amt.substring(0,1)+" "+response.History[i].trn_amt.substring(1,response.History[i].trn_amt.length);
        kony.print("amount ::"+response.History[i].trn_amt);
      	response.History[i].amount = response.History[i].trn_amt;
      }else
      	response.History[i].amount = response.History[i].trn_amt;
    }
    data = response.History;
    currencyCode = "JOD";
  }
  kony.print("Currency ::"+currencyCode);
  for(var j in data){
    var transDate = data[j].trn_date || data[j].transactionDate;
    if(!isEmpty(transDate)){
      transDate = transDate.split('-');
      transDate = transDate[2]+"/"+transDate[1]+"/"+transDate[0];     	
    }
    data[j].transactionDate = transDate;
    
  }
  if(data !== "" && data !== null){
    for(var i = 0; i < data.length; i++){
      skin = "";
//       if(i == 10 || i == 49 || i == 74){
//         j = j+1;
//       }
      toShow = false;
      if(data[i].trn_ccy !== undefined && data[i].trn_ccy !== null){
    	kony.print("Transaction Currency ::"+data[i].trn_ccy);
    	currencyCode = getISOCurrency(data[i].trn_ccy);
        if(currencyCode !== "JOD"){
        	toShow = true;
        }
      }else if(data[i].currencydesc !== undefined && data[i].currencydesc !== ""){
      	currencyCode = data[i].currencydesc;
      }
      if(data[i].atmLoc !== undefined && data[i].atmLoc !== null){
      	data[i].description = data[i].description+" - "+data[i].atmLoc;
      }
      if(data[i].amount.indexOf("-") > -1){
//      	data[i].amount = data[i].amount.substring(0,1)+" "+data[i].amount.substring(1, data[i].amount.length);
        category = "debit";
      }else{
      	category = "credit";
      }
      kony.print("Parsed Amount ::"+data[i].amount);
      temp.push({"transactionDate":{"text":data[i].transactionDate,"isVisible":true},
                    "description":{"text":data[i].description,"isVisible":true},
                    "amount":{"text":data[i].amount+" "+currencyCode,"isVisible":true},
               		"transactionAmountJOD":{"text":data[i].bill_amt+" JOD","isVisible":toShow},
                    "lblLastTransaction":{"text":"","isVisible":true},
                    "category":category});
    }
    kony.retailBanking.globalData.accountsTransactionList = {"transaction":temp};
      frmFilterTransaction.flxFilterDone.setEnabled(true);
      frmManageCardsKA.transactionSegment.removeAll();
      frmManageCardsKA.transactionSegment.widgetDataMap = {"transactionDate":"transactionDate","transactionName":"description","transactionAmount":"amount","lblLastTransaction":"lblLastTransaction","transactionAmountJOD":"transactionAmountJOD"};
      frmManageCardsKA.transactionSegment.setData(kony.retailBanking.globalData.accountsTransactionList.transaction);
      frmManageCardsKA.txtSearch.setEnabled(true);
      frmManageCardsKA.transactionSegment.isVisible = true;
      frmManageCardsKA.lblNoResult.isVisible = false;
      frmManageCardsKA.CopylblFilter0f7770a92cff247.setEnabled(true);
      frmManageCardsKA.lblExport.setEnabled(true);
      frmManageCardsKA.lblCardStatement.setEnabled(true);
    //added 
      frmManageCardsKA.lblCardStatmentDownload.setEnabled(false);
      //end
  }else{
      frmManageCardsKA.transactionSegment.isVisible = false;
      frmManageCardsKA.lblNoResult.isVisible = true;
      frmManageCardsKA.txtSearch.setEnabled(false);
      frmManageCardsKA.CopylblFilter0f7770a92cff247.setEnabled(true);
      frmManageCardsKA.lblExport.setEnabled(false);
      frmManageCardsKA.lblCardStatement.setEnabled(true);
    //added 
      frmManageCardsKA.lblCardStatmentDownload.setEnabled(false);
      //end
      frmManageCardsKA.transactionSegment.removeAll();
  }
  customerAccountDetails.cardTransactionIndex = 0;
  if(kony.application.getCurrentForm().id === "frmFilterTransaction" && validate_FILTEROPTIONS(filter_OPTIONS)){
  	filterAccountTransactions(filter_OPTIONS, kony.application.getPreviousForm().transactionSegment, kony.retailBanking.globalData.accountsTransactionList.transaction, kony.application.getPreviousForm());
    frmFilterTransaction.flxFilterDone.setEnabled(true);
  }
  frmManageCardsKA.show();
  if(is_CARDPAYMENTSUCCESS){
  	is_CARDPAYMENTSUCCESS = false;
    frmWebCharge.destroy();
    frmWebCharge.txtFieldAmount.text = "";
    frmCreditCardPayment.destroy();
    frmCreditCardPayment.txtFieldAmount.text = "";
  }
  frmManageCardsKA.segCardsKA.selectedIndex = [0,customerAccountDetails.currentIndex];
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function serv_getCrCardTransactions(data,dateOPTIONS){
  try{
    kony.print("Credit Card Transaction ::"+JSON.stringify(data));
    var month = "";
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var date1 = new Date();
    var curdate = "";
    var splitDATE = "";
    if(dateOPTIONS.toDATE !== "" && dateOPTIONS.toDATE !== null){
    	splitDATE = dateOPTIONS.toDATE.split("-");
    	curdate = splitDATE[2]+""+splitDATE[1]+""+splitDATE[0];
    }else
    	curdate = (date1.getDate()<10?"0"+date1.getDate():date1.getDate())+""+((date1.getMonth()+1)<10?"0"+(date1.getMonth()+1):(date1.getMonth()+1))+""+date1.getFullYear();
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Cards", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Cards");
//     dataObject.addField("usr", "icbsserv");
//     dataObject.addField("pass", "icbsserv_1");
    dataObject.addField("custId", custid);
    dataObject.addField("p_trn_nbr","45");
    dataObject.addField("DateTo", curdate);
    var monthval = (parseInt(date1.getMonth())-2)<0?11:(parseInt(date1.getMonth())-1);
//     date1.setDate(1);
    date1.setDate(date1.getDate()-60);
    if(dateOPTIONS.fromDATE !== "" && dateOPTIONS.fromDATE !== null){
    	splitDATE = "";
    	splitDATE = dateOPTIONS.fromDATE.split("-");
    	curdate = splitDATE[2]+""+splitDATE[1]+""+splitDATE[0];
    }else
    	curdate = "01"+((date1.getMonth()+1)<10?"0"+(date1.getMonth()+1):(date1.getMonth()+1))+date1.getFullYear();
    dataObject.addField("DateFrom", curdate);
    dataObject.addField("CardNum", data.card_num);
    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    kony.print("Data Objects ::"+JSON.stringify(dataObject));
    if (kony.sdk.isNetworkAvailable()) {
      kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      modelObj.customVerb("getCardTransHistory", serviceOptions, serv_successgetCardTransactions, fetch_CARDTRANSACTIONFAILURE);
    }else{
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
  }catch(e){
    kony.print("Exception_serv_getCrCardTransactions ::"+e);
  }
}

function init_frmManageCardsKA(){
  try{
    frmManageCardsKA.flxAccountdetailsSortContainer.setGestureRecognizer(1, {fingers:1,taps:1}, animatePopup_Sort);
  }catch(e){
    kony.print("Exception_init_frmManageCardsKA ::"+e);
  }
}

function serv_FETCHDEBITCARDTRANSACTION(data, dateOPTIONS){
	try{
    	kony.print("Card Data ::"+JSON.stringify(data));
    	var curDate = new Date();
    	var month = "",date = "";
    	var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
              "access": "online",
              "objectName": "RBObjects"
            };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Transactions", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Transactions");
        dataObject.addField("custId", custid);
    	month = ((curDate.getMonth()+1)>9)?(curDate.getMonth()+1):"0"+(curDate.getMonth()+1);
    	date = ((curDate.getDate()>9)?(curDate.getDate()):"0"+curDate.getDate());
    	if(dateOPTIONS.toDATE !== "" && dateOPTIONS.toDATE !== null){
        	dataObject.addField("p_toDate", dateOPTIONS.toDATE);
        }else
    		dataObject.addField("p_toDate", curDate.getFullYear()+"-"+month+"-"+date);
//     	curDate.setDate(1);
//     	var monthVal = (curDate.getMonth()-1)<0?11:(curDate.getMonth()-1);
//     	curDate.setMonth(monthVal);
    	curDate.setDate(curDate.getDate()-60);
    	month = ((curDate.getMonth()+1)>9)?(curDate.getMonth()+1):"0"+(curDate.getMonth()+1);
    	//date = ((curDate.getDate()>9)?(curDate.getDate()):"0"+curDate.getDate());
    	if(dateOPTIONS.fromDATE !== "" && dateOPTIONS.fromDATE !== null){
        	dataObject.addField("p_fromDate", dateOPTIONS.fromDATE);
        }else
        	dataObject.addField("p_fromDate", curDate.getFullYear()+"-"+month+"-01");
        dataObject.addField("p_debit_card_no", data.card_number);
        var serviceOptions = {
              "dataObject": dataObject,
              "headers": headers
            };
    	kony.print("Debit Card ::"+JSON.stringify(dataObject));
        if (kony.sdk.isNetworkAvailable()) {
        	kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            modelObj.customVerb("getDbCardTxnList", serviceOptions,function(res){
              												kony.print("Success ::"+JSON.stringify(res));
              												serv_successgetCardTransactions(res);
            },fetch_CARDTRANSACTIONFAILURE);
        }else{
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    }catch(e){
    	kony.print("Exception_serv_FETCHDEBITCARDTRANSACTION ::"+e);
    }
}

function fetch_CARDTRANSACTIONFAILURE(err){
	try{
    	kony.print("Failed ::"+JSON.stringify(err));
        frmManageCardsKA.transactionSegment.isVisible = false;
        frmManageCardsKA.lblNoResult.isVisible = true;
        frmManageCardsKA.txtSearch.setEnabled(false);
        frmManageCardsKA.CopylblFilter0f7770a92cff247.setEnabled(true);
        frmManageCardsKA.lblExport.setEnabled(false);
        frmManageCardsKA.lblCardStatement.setEnabled(true);
      //added 
      frmManageCardsKA.lblCardStatmentDownload.setEnabled(false);
      //end
        frmManageCardsKA.transactionSegment.removeAll();
        customerAccountDetails.cardTransactionIndex = 0;
        frmManageCardsKA.show();
        if(is_CARDPAYMENTSUCCESS){
          is_CARDPAYMENTSUCCESS = false;
          frmWebCharge.destroy();
          frmWebCharge.txtFieldAmount.text = "";
          frmCreditCardPayment.destroy();
          frmCreditCardPayment.txtFieldAmount.text = "";
        }
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }catch(e){
    	kony.print("Exception_fetch_CARDTRANSACTIONFAILURE ::"+e);
    }
}

function set_IMAGE_FOR_CARD(category, card_type){
	var cardImage = "", type = "";
	switch(category){
      case "S":
          cardImage = "creditsilvercard.png";
          type = geti18Value("i18n.cards.visasilver");
          break;
      case "G":
          cardImage = "boj_card_affluent_credit_gold_v4_01.png";
          type = geti18Value("i18n.cards.visagold");
          break;
      case "P":
          cardImage = "boj_card_affluent_credit_platinum_v4_01.png";
          type = geti18Value("i18n.cards.visaplatinum");
          break;
      case "M":
          cardImage = "creditworldcard.png";
          type = geti18Value("i18n.cards.worldmastercard");
          break;
      case "0036":
//       	  if(card_type === "V"){
              cardImage = "bluecard.png";
              type = "Visa";
//           }else{
//           	cardImage = "prepaid.png";
//           	type = "Master";
//           }
          break;
      case "0037":
          cardImage = "debit_mass.png";
          type = "Master";
          break;
      case "0038":
          cardImage = "debit_mumaiaz.png";
          type = "Master";
          break;
      case "0039":
          cardImage = "debit_wisam_mumaiaz.png";
          type = "Master";
          break;
      case "0040":
           if(card_type === "V"){
               cardImage = "bluecard.png";
               type = "Visa";
           }else{
              cardImage = "prepaid.png";
              type = "Master";
           }
          break;
      case "0041":
//           if(card_type === "V"){
//               cardImage = "bluecard.png";
//               type = "Visa";
//           }else{
              cardImage = "prepaid.png";
              type = "Master";
//           }
          break;
      case "0043":
      		cardImage = "sticker.png";
      		type = "Master";
      	  break;
      default:
          cardImage = "bluecard.png";
          type = "Visa";
          break;
	}
  return [cardImage, type];
}