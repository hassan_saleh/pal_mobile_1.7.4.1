kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.adsPresentationController = function(){
 
};

kony.rb.adsPresentationController.prototype.fetchInFeedAds = function(viewControllerSuccessCallback, viewControllerErrorCallback)
{
   ShowLoadingScreen();
   var adsBC = applicationManager.getAdsBusinessController();
  
   adsBC.fetchAdsByType("infeed", succsessCallback, errorCallback);

  function succsessCallback(infeedAds)
  { 
    viewControllerSuccessCallback(infeedAds);
  }
  
  function errorCallback(err)
  {
    viewControllerErrorCallback(err);
  }
  
};

kony.rb.adsPresentationController.prototype.ShowPostLoginAdvertisementform =function()
{
    ShowLoadingScreen();
    var adsBC = applicationManager.getAdsBusinessController();
    adsBC.fetchAdsByType("nonInfeed",presentationSuccessCallback,presentationErrorCallback);

  function presentationSuccessCallback(adsData){

    if(adsData && adsData.length!==0){
      var navManager = applicationManager.getNavManager();
      navManager.setCustomInfo("frmPostLoginAdvertisementKA",adsData);
      navManager.navigateTo("frmPostLoginAdvertisementKA");
    }
    else
    {
      kony.print("No ad's available for this user");
      showFormOrderList();
    }
  }
  
  function presentationErrorCallback(err){
    kony.print("Failure while fetching Ads in adsBusinessController.js : "+err);
    showFormOrderList();
  }
};

