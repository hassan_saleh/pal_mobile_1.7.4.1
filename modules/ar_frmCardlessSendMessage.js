//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:40 EEST 2020
function addWidgetsfrmCardlessSendMessageAr() {
    frmCardlessSendMessage.setDefaultUnit(kony.flex.DP);
    var flxTitleBarWrapperKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTitleBarWrapperKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTitleBarWrapperKA.setDefaultUnit(kony.flex.DP);
    var flxAndroidTitleBarKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxAndroidTitleBarKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAndroidTitleBarKA.setDefaultUnit(kony.flex.DP);
    var flxAndroidTitleLabelKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "flxAndroidTitleLabelKA",
        "isVisible": true,
        "right": "55dp",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.cardless.sendMessage"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCardlessAndroidSendMail = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCardlessAndroidSendMail",
        "isVisible": false,
        "right": "55dp",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.cardless.sendMail"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnCancelAndroid = new kony.ui.Button({
        "focusSkin": "sknbtn",
        "height": "50dp",
        "id": "btnCancelAndroid",
        "isVisible": true,
        "right": "5dp",
        "skin": "sknbtn",
        "text": "Cancel",
        "top": "0dp",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxAndroidTitleBarKA.add(flxAndroidTitleLabelKA, lblCardlessAndroidSendMail, btnCancelAndroid);
    var btnEditKA = new kony.ui.Button({
        "focusSkin": "sknbtn",
        "height": "50dp",
        "id": "btnEditKA",
        "isVisible": true,
        "onClick": AS_Button_d73085c8c7b44bcd93923a167e1d186a,
        "left": "10dp",
        "skin": "sknbtn",
        "text": "Send",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxTitleBarWrapperKA.add(flxAndroidTitleBarKA, btnEditKA);
    var flxMainContentKA = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxMainContentKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "top": 50,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContentKA.setDefaultUnit(kony.flex.DP);
    var flxNameDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxNameDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxNameDetails.setDefaultUnit(kony.flex.DP);
    var lblName = new kony.ui.Label({
        "id": "lblName",
        "isVisible": true,
        "right": "6%",
        "skin": "sknSendMessageLabels",
        "text": "Name",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var divider1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "divider1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    divider1.setDefaultUnit(kony.flex.DP);
    divider1.add();
    var txtBoxName = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "5%",
        "focusSkin": "sknlogin",
        "height": "40dp",
        "id": "txtBoxName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "right": "6%",
        "placeholder": "Enter full name",
        "secureTextEntry": false,
        "skin": "sknlogin",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "90%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknMessagePlaceHolder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxNameDetails.add(lblName, divider1, txtBoxName);
    var flxPhoneBook = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxPhoneBook",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_f1ab0ca4d4a649afad9168b7352b9a4f,
        "left": "0dp",
        "skin": "skncontainerBkg",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxPhoneBook.setDefaultUnit(kony.flex.DP);
    var imgPhoneBook = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "50dp",
        "id": "imgPhoneBook",
        "isVisible": true,
        "right": "0dp",
        "skin": "slImage",
        "src": "phonebook.png",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var divider3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "divider3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    divider3.setDefaultUnit(kony.flex.DP);
    divider3.add();
    flxPhoneBook.add(imgPhoneBook, divider3);
    var flxPhoneNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxPhoneNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "15%",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxPhoneNumber.setDefaultUnit(kony.flex.DP);
    var lblPhoneNumber = new kony.ui.Label({
        "id": "lblPhoneNumber",
        "isVisible": true,
        "right": "6%",
        "skin": "sknSendMessageLabels",
        "text": "Phone Number",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtBoxPhnNum = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "5%",
        "focusSkin": "sknlogin",
        "height": "40dp",
        "id": "txtBoxPhnNum",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "right": "6%",
        "placeholder": "Enter phone number",
        "secureTextEntry": false,
        "skin": "sknlogin",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "90%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknMessagePlaceHolder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var divider2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "divider2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    divider2.setDefaultUnit(kony.flex.DP);
    divider2.add();
    flxPhoneNumber.add(lblPhoneNumber, txtBoxPhnNum, divider2);
    var flxEmail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxEmail",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "15%",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxEmail.setDefaultUnit(kony.flex.DP);
    var lblEmail = new kony.ui.Label({
        "id": "lblEmail",
        "isVisible": true,
        "right": "6%",
        "skin": "sknSendMessageLabels",
        "text": "Email",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopytxtBoxPhnNum0c94cce1a2e2a4a = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "5%",
        "focusSkin": "sknlogin",
        "height": "40dp",
        "id": "CopytxtBoxPhnNum0c94cce1a2e2a4a",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "right": "6%",
        "placeholder": "Enter email",
        "secureTextEntry": false,
        "skin": "sknlogin",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "90%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknMessagePlaceHolder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var Copydivider0d6e4f248562a42 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0d6e4f248562a42",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    Copydivider0d6e4f248562a42.setDefaultUnit(kony.flex.DP);
    Copydivider0d6e4f248562a42.add();
    flxEmail.add(lblEmail, CopytxtBoxPhnNum0c94cce1a2e2a4a, Copydivider0d6e4f248562a42);
    var flxMessage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMessage",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "30%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMessage.setDefaultUnit(kony.flex.DP);
    var rchTxtMessage = new kony.ui.RichText({
        "height": "100%",
        "id": "rchTxtMessage",
        "isVisible": true,
        "right": "6%",
        "left": "6%",
        "skin": "rctTxtMessage",
        "text": "Hi, I've sent $1500 to a XYZ bank ATM for you to collect.<br><br>\nYour cash code for collection is: 0988. Use this code to collect cash within 30 minutes.\n",
        "top": "20dp",
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxMessage.add(rchTxtMessage);
    flxMainContentKA.add(flxNameDetails, flxPhoneBook, flxPhoneNumber, flxEmail, flxMessage);
    frmCardlessSendMessage.add(flxTitleBarWrapperKA, flxMainContentKA);
};
function frmCardlessSendMessageGlobalsAr() {
    frmCardlessSendMessageAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCardlessSendMessageAr,
        "enabledForIdleTimeout": true,
        "id": "frmCardlessSendMessage",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": true,
        "inTransitionConfig": {
            "formAnimation": 2
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
