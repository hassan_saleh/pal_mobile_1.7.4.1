//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:41 EEST 2020
function initializetmpAccountTypeAr() {
    CopyyourAccount01dfa1aa140424bAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknyourAccountCardFocus",
        "height": "62dp",
        "id": "CopyyourAccount01dfa1aa140424b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknyourAccountCard",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyyourAccount01dfa1aa140424bAr.setDefaultUnit(kony.flex.DP);
    var nameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "nameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    nameContainer.setDefaultUnit(kony.flex.DP);
    var nameAccount1 = new kony.ui.Label({
        "centerY": "50%",
        "id": "nameAccount1",
        "isVisible": true,
        "right": "15dp",
        "maxWidth": "90%",
        "skin": "sknaccountName",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblColorKA = new kony.ui.Label({
        "height": "100%",
        "id": "lblColorKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "6dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    nameContainer.add(nameAccount1, lblColorKA);
    var typeKA = new kony.ui.Label({
        "bottom": "2dp",
        "centerX": "50%",
        "id": "typeKA",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknaccountAvailableBalanceLabel",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyyourAccount01dfa1aa140424bAr.add(nameContainer, typeKA);
}
