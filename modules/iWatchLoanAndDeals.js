function iWatchLoanAndDepositDetails(serv_DATA, retType) {
  kony.print("Iwatch :: Inside iWatchLoanAndDepositDetails for :: "+ JSON.stringify(serv_DATA));
  if (kony.sdk.isNetworkAvailable()) {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
    var dataObject = new kony.sdk.dto.DataObject("Accounts");
    dataObject.addField("custId", iWatchcustid);
    dataObject.addField("p_acc_No", serv_DATA.accountID);
    dataObject.addField("p_lan", "ENG");
    if (retType === "loanInfo") {
      dataObject.addField("p_loan_ref_no", serv_DATA.reference_no);
      dataObject.addField("p_acct_Type", "217");
      dataObject.addField("AccountFlag", "L");
      dataObject.addField("p_Branch", 11);
    } else {
      dataObject.addField("p_td_ref_no", serv_DATA.reference_no);
      dataObject.addField("AccountFlag", "D");
      dataObject.addField("p_Branch", 1);
    }
    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    kony.print("Iwatch ::iWatchLoanAndDepositDetails input params :: " + JSON.stringify(serviceOptions));

    if (retType === "loanInfo") {
      objectService.customVerb("iWatchgetLoanAccDetails", serviceOptions,function(response) {
        iWatchLoanAndDepositDetailssuccesscallback(response, retType);},function(response) {
        iWatchLoanAndDepositDetailserrorcallback(response, retType);});
    }else{
      objectService.customVerb("iWatchGetTdDetails", serviceOptions,function(response) {
        iWatchLoanAndDepositDetailssuccesscallback(response, retType);},function(response) {
        iWatchLoanAndDepositDetailserrorcallback(response, retType);});
    } 
  }else {
    kony.print("Iwatch ::iWatchLoanAndDepositDetails iWatchLoanAndDepositDetails :: No internet");
    return_watchrequest({retType: [],"isUserLoggedIn": true, "isNetworkAvailable": false,"isRetry": true});
  }
}

function iWatchLoanAndDepositDetailssuccesscallback(response, retType) {
  kony.print("Iwatch ::retType :::   "+ retType + "  :::::::iWatchLoanAndDepositDetailssuccesscallback Response :: " + JSON.stringify(response));    
  if(retType == "depositInfo" && !isEmpty(response.DepositeDetail)) {
    return_watchrequest({"depositInfo": response.DepositeDetail,"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": true});
  } 
  else if(retType == "loanInfo" && !isEmpty(response.LoanDetails)) {
    return_watchrequest({"loanInfo": response.LoanDetails,"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": true});
  } else {
    return_watchrequest({retType: [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": true});
  }
}


function iWatchLoanAndDepositDetailserrorcallback(response, retType) {
  kony.print("Iwatch :: iWatchLoanAndDepositDetailserrorcallback :: " + JSON.stringify(response));
  if (retType == "loanInfo")
    return_watchrequest({"loanInfo": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
  else
    return_watchrequest({"depositInfo": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
}