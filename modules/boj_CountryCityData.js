//Created and edited by Arpan

kony = kony || {};
kony.boj = kony.boj || {};

kony.boj.detailsForBene = {
  "lblToUpdate": "",
  "flag": false,

  "Country":[
    {
      "CTRY_CODE":"1",
      "CTRY_ISO":"JO",
      "CTRY_S_DESC":"JORDAN",
      "CTRY_B_DESC":"الأردن"
    },
    {
      "CTRY_CODE":"2",
      "CTRY_ISO":"PS",
      "CTRY_S_DESC":"PALESTINE",
      "CTRY_B_DESC":"فلسطين"
    },	
    {
      "CTRY_CODE":"3",
      "CTRY_ISO":"IQ",
      "CTRY_S_DESC":"IRAQ",
      "CTRY_B_DESC":"العراق"
    },
    {
      "CTRY_CODE":"4",
      "CTRY_ISO":"SY",
      "CTRY_S_DESC":"SYRIA  ",
      "CTRY_B_DESC":"سوريا"
    },
    {
      "CTRY_CODE":"5",
      "CTRY_ISO":"SA",
      "CTRY_S_DESC":"SAUDI ARABIA  ",
      "CTRY_B_DESC":"السعودية"
    },
    {
      "CTRY_CODE":"6",
      "CTRY_ISO":"KW",
      "CTRY_S_DESC":"KUWAIT",
      "CTRY_B_DESC":"الكويت"
    },
    {
      "CTRY_CODE":"7",
      "CTRY_ISO":"LB",
      "CTRY_S_DESC":"LIBANON",
      "CTRY_B_DESC":"لبنان"
    },
    {
      "CTRY_CODE":"8",
      "CTRY_ISO":"EG",
      "CTRY_S_DESC":"EGYPT",
      "CTRY_B_DESC":"مصر"
    },
    {
      "CTRY_CODE":"9",
      "CTRY_ISO":"US",
      "CTRY_S_DESC":"USA",
      "CTRY_B_DESC":"الولايات المتحدة الامريكية"
    },
    {
      "CTRY_CODE":"10",
      "CTRY_ISO":"FR",
      "CTRY_S_DESC":"FRANCE",
      "CTRY_B_DESC":"فرنسا"
    },
    {
      "CTRY_CODE":"11",
      "CTRY_ISO":"IT",
      "CTRY_S_DESC":"ITALY   ",
      "CTRY_B_DESC":"ايطاليا"
    },
    {
      "CTRY_CODE":"12",
      "CTRY_ISO":"CA",
      "CTRY_S_DESC":"CANADA",
      "CTRY_B_DESC":"كندا"
    },
    {
      "CTRY_CODE":"13",
      "CTRY_ISO":"TR",
      "CTRY_S_DESC":"TURKEY",
      "CTRY_B_DESC":"تركيا"
    },
    {
      "CTRY_CODE":"14",
      "CTRY_ISO":"GB",
      "CTRY_S_DESC":"UNITED   KINGDOM",
      "CTRY_B_DESC":"بريطانيا"
    },
    {
      "CTRY_CODE":"18",
      "CTRY_ISO":"OM",
      "CTRY_S_DESC":"OMAN",
      "CTRY_B_DESC":"سلطنة عمان"
    },
    {
      "CTRY_CODE":"19",
      "CTRY_ISO":"JP",
      "CTRY_S_DESC":"JAPAN",
      "CTRY_B_DESC":"اليابان"
    },
    {
      "CTRY_CODE":"20",
      "CTRY_ISO":"NG",
      "CTRY_S_DESC":"NIGERIA  ",
      "CTRY_B_DESC":"نيجيريا"
    },
    {
      "CTRY_CODE":"21",
      "CTRY_ISO":"DK",
      "CTRY_S_DESC":"DANISH",
      "CTRY_B_DESC":"دنمارك"
    },
    {
      "CTRY_CODE":"22",
      "CTRY_ISO":"VE",
      "CTRY_S_DESC":"VENZWELA",
      "CTRY_B_DESC":"فنزويلا"
    },
    {
      "CTRY_CODE":"23",
      "CTRY_ISO":"CN",
      "CTRY_S_DESC":"CHINA",
      "CTRY_B_DESC":"الصين"
    },
    {
      "CTRY_CODE":"24",
      "CTRY_ISO":"AE",
      "CTRY_S_DESC":"UAE",
      "CTRY_B_DESC":"الإمارات العربية المتحدة"
    },
    {
      "CTRY_CODE":"25",
      "CTRY_ISO":"QA",
      "CTRY_S_DESC":"QATAR",
      "CTRY_B_DESC":"قطر"
    },
    {
      "CTRY_CODE":"26",
      "CTRY_ISO":"BH",
      "CTRY_S_DESC":"BAHREEN",
      "CTRY_B_DESC":"البحرين"
    },
    {
      "CTRY_CODE":"27",
      "CTRY_ISO":"TN",
      "CTRY_S_DESC":"TUNIS",
      "CTRY_B_DESC":"تونس"
    },
    {
      "CTRY_CODE":"28",
      "CTRY_ISO":"DZ",
      "CTRY_S_DESC":"ALGYRIA",
      "CTRY_B_DESC":"الجزائر"
    },
    {
      "CTRY_CODE":"29",
      "CTRY_ISO":"SD",
      "CTRY_S_DESC":"SUDAN",
      "CTRY_B_DESC":"السودان"
    },
    {
      "CTRY_CODE":"30",
      "CTRY_ISO":"LY",
      "CTRY_S_DESC":"LIBIA",
      "CTRY_B_DESC":"ليبيا"
    },
    {
      "CTRY_CODE":"31",
      "CTRY_ISO":"MA",
      "CTRY_S_DESC":"MOROCO",
      "CTRY_B_DESC":"المغرب"
    },
    {
      "CTRY_CODE":"32",
      "CTRY_ISO":"SO",
      "CTRY_S_DESC":"AL SOMAL",
      "CTRY_B_DESC":"الصومال"
    },
    {
      "CTRY_CODE":"33",
      "CTRY_ISO":"AU",
      "CTRY_S_DESC":"AUSTRALIA",
      "CTRY_B_DESC":"استراليا"
    },
    {
      "CTRY_CODE":"34",
      "CTRY_ISO":"MX",
      "CTRY_S_DESC":"MEXICO",
      "CTRY_B_DESC":"المكسيك"
    },
    {
      "CTRY_CODE":"35",
      "CTRY_ISO":"BR",
      "CTRY_S_DESC":"BRAZIL",
      "CTRY_B_DESC":"البرازيل"
    },
    {
      "CTRY_CODE":"36",
      "CTRY_ISO":"AR",
      "CTRY_S_DESC":"ARGENTINA",
      "CTRY_B_DESC":"الارجنتين"
    },
    {
      "CTRY_CODE":"37",
      "CTRY_ISO":"PY",
      "CTRY_S_DESC":"PARAGUAY",
      "CTRY_B_DESC":"برغواي"
    },
    {
      "CTRY_CODE":"38",
      "CTRY_ISO":"CO",
      "CTRY_S_DESC":"COLUMBIA",
      "CTRY_B_DESC":"كولومبيا"
    },
    {
      "CTRY_CODE":"39",
      "CTRY_ISO":"MR",
      "CTRY_S_DESC":"MAURITANIA",
      "CTRY_B_DESC":"موريتانيا"
    },
    {
      "CTRY_CODE":"40",
      "CTRY_ISO":"DJ",
      "CTRY_S_DESC":"DJIBOUTI",
      "CTRY_B_DESC":"جيبوتي"
    },
    {
      "CTRY_CODE":"41",
      "CTRY_ISO":"IE",
      "CTRY_S_DESC":"IRELAND",
      "CTRY_B_DESC":"ايرلندا"
    },
    {
      "CTRY_CODE":"42",
      "CTRY_ISO":"GR",
      "CTRY_S_DESC":"GREECE",
      "CTRY_B_DESC":"اليونان"
    },
    {
      "CTRY_CODE":"43",
      "CTRY_ISO":"CY",
      "CTRY_S_DESC":"SYPRIC",
      "CTRY_B_DESC":"قبرص"
    },
    {
      "CTRY_CODE":"44",
      "CTRY_ISO":"PT",
      "CTRY_S_DESC":"PORTUGAL",
      "CTRY_B_DESC":"البرتغال"
    },
    {
      "CTRY_CODE":"45",
      "CTRY_ISO":"ES",
      "CTRY_S_DESC":"SPAIN",
      "CTRY_B_DESC":"اسبانيا"
    },
    {
      "CTRY_CODE":"46",
      "CTRY_ISO":"YE",
      "CTRY_S_DESC":"YAMAN",
      "CTRY_B_DESC":"اليمن"
    },
    {
      "CTRY_CODE":"47",
      "CTRY_ISO":"BE",
      "CTRY_S_DESC":"BELGIUM",
      "CTRY_B_DESC":"بلجيكا"
    },
    {
      "CTRY_CODE":"48",
      "CTRY_ISO":"JM",
      "CTRY_S_DESC":"GAMAYCA",
      "CTRY_B_DESC":"جامايكا"
    },
    {
      "CTRY_CODE":"49",
      "CTRY_ISO":"DE",
      "CTRY_S_DESC":"GERMANY",
      "CTRY_B_DESC":"المانيا"
    },
    {
      "CTRY_CODE":"50",
      "CTRY_ISO":"LU",
      "CTRY_S_DESC":"LUXEMBOURG",
      "CTRY_B_DESC":"لكسمبورغ"
    },
    {
      "CTRY_CODE":"51",
      "CTRY_ISO":"NL",
      "CTRY_S_DESC":"NETHER LANDS",
      "CTRY_B_DESC":"هولندا"
    },
    {
      "CTRY_CODE":"52",
      "CTRY_ISO":"SE",
      "CTRY_S_DESC":"SWEDEN",
      "CTRY_B_DESC":"السويد"
    },
    {
      "CTRY_CODE":"53",
      "CTRY_ISO":"AT",
      "CTRY_S_DESC":"AUSTRIA",
      "CTRY_B_DESC":"النمسا"
    },
    {
      "CTRY_CODE":"54",
      "CTRY_ISO":"FI",
      "CTRY_S_DESC":"FINLAND",
      "CTRY_B_DESC":"فنلندا"
    },
    {
      "CTRY_CODE":"55",
      "CTRY_ISO":"CH",
      "CTRY_S_DESC":"SWEZIERLAND",
      "CTRY_B_DESC":"سويسرا"
    },
    {
      "CTRY_CODE":"61",
      "CTRY_ISO":"AM",
      "CTRY_S_DESC":"ARMENIA",
      "CTRY_B_DESC":"ارمينيا"
    },
    //BMA 1949 fix
    /*{
      "CTRY_CODE":"62",
      "CTRY_ISO":"KP",
      "CTRY_S_DESC":"NORTH KOREA",
      "CTRY_B_DESC":"كوريا الشمالية"
    },*/
    {
      "CTRY_CODE":"63",
      "CTRY_ISO":"KR",
      "CTRY_S_DESC":"SOUTH KOREA",
      "CTRY_B_DESC":"كوريا الجنوبية"
    },
    {
      "CTRY_CODE":"64",
      "CTRY_ISO":"UZ",
      "CTRY_S_DESC":"UZBEKISTAN",
      "CTRY_B_DESC":"اوزبكستان"
    },
    {
      "CTRY_CODE":"65",
      "CTRY_ISO":"IN",
      "CTRY_S_DESC":"INDIA",
      "CTRY_B_DESC":"الهند"
    },
    {
      "CTRY_CODE":"66",
      "CTRY_ISO":"PK",
      "CTRY_S_DESC":"PAKISTAN",
      "CTRY_B_DESC":"الباكستان"
    },
    {
      "CTRY_CODE":"67",
      "CTRY_ISO":"PH",
      "CTRY_S_DESC":"PHILIPPINES",
      "CTRY_B_DESC":"الفلبين"
    },
    {
      "CTRY_CODE":"68",
      "CTRY_ISO":"LK",
      "CTRY_S_DESC":"SRILANKA",
      "CTRY_B_DESC":"سيريلانكا"
    },
    {
      "CTRY_CODE":"69",
      "CTRY_ISO":"TW",
      "CTRY_S_DESC":"TAIWAN",
      "CTRY_B_DESC":"تايوان"
    },
    {
      "CTRY_CODE":"70",
      "CTRY_ISO":"HK",
      "CTRY_S_DESC":"HONG KONG",
      "CTRY_B_DESC":"هونغ كونغ"
    },
    {
      "CTRY_CODE":"71",
      "CTRY_ISO":"KZ",
      "CTRY_S_DESC":"KAZAKHSTAN",
      "CTRY_B_DESC":"كازاخستان"
    },
    {
      "CTRY_CODE":"72",
      "CTRY_ISO":"BG",
      "CTRY_S_DESC":"BULGARIA",
      "CTRY_B_DESC":"بلغاريا"
    },
    {
      "CTRY_CODE":"73",
      "CTRY_ISO":"RU",
      "CTRY_S_DESC":"RUSSIA",
      "CTRY_B_DESC":"روسيا"
    },
    {
      "CTRY_CODE":"74",
      "CTRY_ISO":"UA",
      "CTRY_S_DESC":"UKRAINIAN",
      "CTRY_B_DESC":"اوكرانيا"
    },
    {
      "CTRY_CODE":"75",
      "CTRY_ISO":"RO",
      "CTRY_S_DESC":"ROMANIA",
      "CTRY_B_DESC":"رومانيا"
    },
    {
      "CTRY_CODE":"76",
      "CTRY_ISO":"YU",
      "CTRY_S_DESC":"SERBIA",
      "CTRY_B_DESC":"الصربية"
    },
    {
      "CTRY_CODE":"77",
      "CTRY_ISO":"HR",
      "CTRY_S_DESC":"CORWATIA",
      "CTRY_B_DESC":"كرواتيا"
    },
    {
      "CTRY_CODE":"78",
      "CTRY_ISO":"BA",
      "CTRY_S_DESC":"BOSNIA",
      "CTRY_B_DESC":"البوسنية"
    },
    {
      "CTRY_CODE":"79",
      "CTRY_ISO":"CL",
      "CTRY_S_DESC":"CHILE",
      "CTRY_B_DESC":"تشيلي"
    },
    {
      "CTRY_CODE":"80",
      "CTRY_ISO":"TD",
      "CTRY_S_DESC":"CHAD",
      "CTRY_B_DESC":"تشاد"
    },
    {
      "CTRY_CODE":"81",
      "CTRY_ISO":"NE",
      "CTRY_S_DESC":"NIGERIA",
      "CTRY_B_DESC":"النيجر"
    },
    {
      "CTRY_CODE":"82",
      "CTRY_ISO":"CG",
      "CTRY_S_DESC":"CONGO",
      "CTRY_B_DESC":"الكونغو"
    },
    {
      "CTRY_CODE":"83",
      "CTRY_ISO":"TZ",
      "CTRY_S_DESC":"TANZANIA",
      "CTRY_B_DESC":"تنزانيا"
    },
    {
      "CTRY_CODE":"84",
      "CTRY_ISO":"ZA",
      "CTRY_S_DESC":"SOUTH AFRICA",
      "CTRY_B_DESC":"جنوب افريقيا"
    },
    {
      "CTRY_CODE":"85",
      "CTRY_ISO":"UG",
      "CTRY_S_DESC":"AUGANDA",
      "CTRY_B_DESC":"اوغندا"
    },
    {
      "CTRY_CODE":"86",
      "CTRY_ISO":"GH",
      "CTRY_S_DESC":"GHANA",
      "CTRY_B_DESC":"غانا"
    },
    {
      "CTRY_CODE":"87",
      "CTRY_ISO":"CM",
      "CTRY_S_DESC":"KAMIRON",
      "CTRY_B_DESC":"كاميرون"
    },
    {
      "CTRY_CODE":"88",
      "CTRY_ISO":"ZM",
      "CTRY_S_DESC":"ZAMBIA",
      "CTRY_B_DESC":"زامبيا"
    },
    {
      "CTRY_CODE":"89",
      "CTRY_ISO":"KM",
      "CTRY_S_DESC":"COMOROS",
      "CTRY_B_DESC":"جزر القمر"
    },
    {
      "CTRY_CODE":"90",
      "CTRY_ISO":"SN",
      "CTRY_S_DESC":"SENEGAL",
      "CTRY_B_DESC":"السنغال"
    },
    {
      "CTRY_CODE":"91",
      "CTRY_ISO":"KE",
      "CTRY_S_DESC":"KENYA",
      "CTRY_B_DESC":"كينيا"
    },
    {
      "CTRY_CODE":"92",
      "CTRY_ISO":"UY",
      "CTRY_S_DESC":"URUGUAY",
      "CTRY_B_DESC":"اورغواي"
    },
    {
      "CTRY_CODE":"93",
      "CTRY_ISO":"MY",
      "CTRY_S_DESC":"MALAYSIA",
      "CTRY_B_DESC":"ماليزيا"
    },
    {
      "CTRY_CODE":"94",
      "CTRY_ISO":"SG",
      "CTRY_S_DESC":"SINGAPORE",
      "CTRY_B_DESC":"سنغافورة"
    },
    {
      "CTRY_CODE":"95",
      "CTRY_ISO":"ID",
      "CTRY_S_DESC":"INDONESIA",
      "CTRY_B_DESC":"اندونيسيا"
    },
    {
      "CTRY_CODE":"96",
      "CTRY_ISO":"TH",
      "CTRY_S_DESC":"TILAND",
      "CTRY_B_DESC":"تايلاند"
    },
    {
      "CTRY_CODE":"97",
      "CTRY_ISO":"NZ",
      "CTRY_S_DESC":"NEWZEALAND",
      "CTRY_B_DESC":"نيوزلنده"
    },
    {
      "CTRY_CODE":"98",
      "CTRY_ISO":"CZ",
      "CTRY_S_DESC":"CZECH",
      "CTRY_B_DESC":"جمهورية التشيك"
    },
    {
      "CTRY_CODE":"99",
      "CTRY_ISO":"",
      "CTRY_S_DESC":"OTHERS",
      "CTRY_B_DESC":"اخرى"
    },
    {
      "CTRY_CODE":"102",
      "CTRY_ISO":"IL",
      "CTRY_S_DESC":"Israel",
      "CTRY_B_DESC":"اسرائيل"
    },
    {
      "CTRY_CODE":"103",
      "CTRY_ISO":"ET",
      "CTRY_S_DESC":"ETHIOPIA",
      "CTRY_B_DESC":"أثيوبيا"
    },
    {
      "CTRY_CODE":"104",
      "CTRY_ISO":"HU",
      "CTRY_S_DESC":"HUNGARY",
      "CTRY_B_DESC":"هنغاريا"
    },
    {
      "CTRY_CODE":"105",
      "CTRY_ISO":"IS",
      "CTRY_S_DESC":"ICELAND",
      "CTRY_B_DESC":"ايسلاندة"
    },
    {
      "CTRY_CODE":"106",
      "CTRY_ISO":"NO",
      "CTRY_S_DESC":"NORWAY",
      "CTRY_B_DESC":"النروج"
    },
    {
      "CTRY_CODE":"107",
      "CTRY_ISO":"PL",
      "CTRY_S_DESC":"POLAND",
      "CTRY_B_DESC":"بولند"
    },
    
    //BMA 1949 fix
   /* {
      "CTRY_CODE":"109",
      "CTRY_ISO":"IR",
      "CTRY_S_DESC":"Iran",
      "CTRY_B_DESC":"ايران"
    },*/
    {
      "CTRY_CODE":"110",
      "CTRY_ISO":"BD",
      "CTRY_S_DESC":"BANGLADESH",
      "CTRY_B_DESC":"بنغلادش"
    },
//     {
//       "CTRY_CODE":"111",
//       "CTRY_ISO":"JO",
//       "CTRY_S_DESC":"JORDANIAN WITHOUT NATIONAL ID",
//       "CTRY_B_DESC":"اردني بدون رقم وطني"
//     },
    {
      "CTRY_CODE":"112",
      "CTRY_ISO":"XK",
      "CTRY_S_DESC":"KOSOVO",
      "CTRY_B_DESC":"كوسوفو"
    },
    {
      "CTRY_CODE":"113",
      "CTRY_ISO":"MDA",
      "CTRY_S_DESC":"MOLDOVA",
      "CTRY_B_DESC":"مولدوفا"
    },
    {
      "CTRY_CODE":"114",
      "CTRY_ISO":"ALB",
      "CTRY_S_DESC":"ALBANIA",
      "CTRY_B_DESC":"البانيا"
    },
    {
      "CTRY_CODE":"115",
      "CTRY_ISO":"MKD",
      "CTRY_S_DESC":"MACEDONIA",
      "CTRY_B_DESC":"مقدونيا"
    },
    {
      "CTRY_CODE":"117",
      "CTRY_ISO":"MU",
      "CTRY_S_DESC":"MAURITIUS",
      "CTRY_B_DESC":"موريشوس"
    },
    {
      "CTRY_CODE":"118",
      "CTRY_ISO":"EC",
      "CTRY_S_DESC":"ECUADOR",
      "CTRY_B_DESC":"الاكوادور"
    },
    {
      "CTRY_CODE":"119",
      "CTRY_ISO":"CU",
      "CTRY_S_DESC":"COBA",
      "CTRY_B_DESC":"كوبا"
    },
    {
      "CTRY_CODE":"120",
      "CTRY_ISO":"MM",
      "CTRY_S_DESC":"BURMA",
      "CTRY_B_DESC":"بورما او ماينمار"
    },
    {
      "CTRY_CODE":"121",
      "CTRY_ISO":"STP",
      "CTRY_S_DESC":"SAO TOME AND PRINCIPE",
      "CTRY_B_DESC":"ساو تومي اند برنسب"
    },
    {
      "CTRY_CODE":"122",
      "CTRY_ISO":"VN",
      "CTRY_S_DESC":"VETNAM",
      "CTRY_B_DESC":"الفيتنام"
    },
    {
      "CTRY_CODE":"123",
      "CTRY_ISO":"SCG",
      "CTRY_S_DESC":"YUGOSLAVIA",
      "CTRY_B_DESC":"يوغوسلافيا"
    },
    {
      "CTRY_CODE":"124",
      "CTRY_ISO":"PER",
      "CTRY_S_DESC":"PERU",
      "CTRY_B_DESC":"البيرو"
    },
    {
      "CTRY_CODE":"125",
      "CTRY_ISO":"HND",
      "CTRY_S_DESC":"Honduras",
      "CTRY_B_DESC":"الهندوراس"
    },
    {
      "CTRY_CODE":"126",
      "CTRY_ISO":"PAN",
      "CTRY_S_DESC":"PANAMA ",
      "CTRY_B_DESC":"بنما"
    },
    {
      "CTRY_CODE":"127",
      "CTRY_ISO":"AO",
      "CTRY_S_DESC":"Angola",
      "CTRY_B_DESC":"أنغولا"
    },
    {
      "CTRY_CODE":"128",
      "CTRY_ISO":"KG",
      "CTRY_S_DESC":"Kyrgyzstan",
      "CTRY_B_DESC":"قيرغيزستان"
    },
    {
      "CTRY_CODE":"129",
      "CTRY_ISO":"",
      "CTRY_S_DESC":"Lao People's Democratic Republic",
      "CTRY_B_DESC":"جمهورية لاو الديمقراطية الشعبيه"
    },
    {
      "CTRY_CODE":"130",
      "CTRY_ISO":"MNG",
      "CTRY_S_DESC":"Mongolia",
      "CTRY_B_DESC":"منغوليا"
    },
    {
      "CTRY_CODE":"131",
      "CTRY_ISO":"NA",
      "CTRY_S_DESC":"Namibia",
      "CTRY_B_DESC":"ناميبيا"
    },
    {
      "CTRY_CODE":"132",
      "CTRY_ISO":"NPL",
      "CTRY_S_DESC":"Nepal",
      "CTRY_B_DESC":"نيبال"
    },
    {
      "CTRY_CODE":"133",
      "CTRY_ISO":"NIC",
      "CTRY_S_DESC":"Nicaragua",
      "CTRY_B_DESC":"نيكاراجوا"
    },
    {
      "CTRY_CODE":"134",
      "CTRY_ISO":"PNG",
      "CTRY_S_DESC":"Papua New Guinea",
      "CTRY_B_DESC":"بابوا غينا الجديدة"
    },
    {
      "CTRY_CODE":"135",
      "CTRY_ISO":"TJK",
      "CTRY_S_DESC":"Tajikistan",
      "CTRY_B_DESC":"طاجيكستان"
    },
    {
      "CTRY_CODE":"136",
      "CTRY_ISO":"ZWE",
      "CTRY_S_DESC":"Zimbabwe",
      "CTRY_B_DESC":"زيمبابوي"
    },
    {
      "CTRY_CODE":"137",
      "CTRY_ISO":"AF",
      "CTRY_S_DESC":"Afghanistan",
      "CTRY_B_DESC":"أفغانستان"
    },
    {
      "CTRY_CODE":"138",
      "CTRY_ISO":"KHM",
      "CTRY_S_DESC":"Cambodia",
      "CTRY_B_DESC":"كمبوديا"
    },
    {
      "CTRY_CODE":"139",
      "CTRY_ISO":"GUY",
      "CTRY_S_DESC":"Guyana",
      "CTRY_B_DESC":"غويانا"
    },
    {
      "CTRY_CODE":"140",
      "CTRY_ISO":"CAF",
      "CTRY_S_DESC":"Central African Republic",
      "CTRY_B_DESC":"جمهورية افريقيا الوسطى"
    },
    {
      "CTRY_CODE":"142",
      "CTRY_ISO":"LBR",
      "CTRY_S_DESC":"Liberia",
      "CTRY_B_DESC":"ليبيريا "
    },
    {
      "CTRY_CODE":"143",
      "CTRY_ISO":"BLR",
      "CTRY_S_DESC":"Belarus ",
      "CTRY_B_DESC":"بيلاروسيا "
    },
    {
      "CTRY_CODE":"144",
      "CTRY_ISO":"HTI",
      "CTRY_S_DESC":"Haiti",
      "CTRY_B_DESC":"هايتي "
    },
    {
      "CTRY_CODE":"145",
      "CTRY_ISO":"ERI",
      "CTRY_S_DESC":"Eritrea",
      "CTRY_B_DESC":"ارتيريا "
    },
    {
      "CTRY_CODE":"146",
      "CTRY_ISO":"KNA",
      "CTRY_S_DESC":"Saint Kitts and Nevis",
      "CTRY_B_DESC":"سانت كيتس ونيفيس"
    },
    {
      "CTRY_CODE":"147",
      "CTRY_ISO":"AZE",
      "CTRY_S_DESC":"Azerbaijan",
      "CTRY_B_DESC":"اذربيجان"
    },
    {
      "CTRY_CODE":"148",
      "CTRY_ISO":"CIV",
      "CTRY_S_DESC":"Côte d’Ivoire",
      "CTRY_B_DESC":"ساح العاج"
    },
    {
      "CTRY_CODE":"149",
      "CTRY_ISO":"DO",
      "CTRY_S_DESC":"DOMINICANA",
      "CTRY_B_DESC":"دومينيكانا"
    },
    {
      "CTRY_CODE":"150",
      "CTRY_ISO":"KY",
      "CTRY_S_DESC":"Cayman Islands",
      "CTRY_B_DESC":"جزر الكايمن"
    },
    {
      "CTRY_CODE":"151",
      "CTRY_ISO":"VG",
      "CTRY_S_DESC":"British Virgin Islands",
      "CTRY_B_DESC":"جزر العذراء البريطانية"
    },
    {
      "CTRY_CODE":"152",
      "CTRY_ISO":"MT",
      "CTRY_S_DESC":"Malta",
      "CTRY_B_DESC":"جمهورية مالطا"
    },
    {
      "CTRY_CODE":"153",
      "CTRY_ISO":"CD",
      "CTRY_S_DESC":"Democratic Republic of the Congo",
      "CTRY_B_DESC":"جمهورية الكونغو الديمقراطية"
    },
    {
      "CTRY_CODE":"154",
      "CTRY_ISO":"VU",
      "CTRY_S_DESC":"Vanuatu",
      "CTRY_B_DESC":"فانواتو"
    },
    {
      "CTRY_CODE":"155",
      "CTRY_ISO":"LT",
      "CTRY_S_DESC":"Lithuania",
      "CTRY_B_DESC":"ليتوانيا"
    },
    {
      "CTRY_CODE":"156",
      "CTRY_ISO":"LV",
      "CTRY_S_DESC":"Latvia",
      "CTRY_B_DESC":"لاتفيا"
    },
    {
      "CTRY_CODE":"157",
      "CTRY_ISO":"SK",
      "CTRY_S_DESC":"Slovakia",
      "CTRY_B_DESC":"سلوفاكيا"
    },
    {
      "CTRY_CODE":"158",
      "CTRY_ISO":"KN",
      "CTRY_S_DESC":"Federation of Saint Kitts and Nevis",
      "CTRY_B_DESC":"اتحاد القديس كريستوفر"
    },
    {
      "CTRY_CODE":"159",
      "CTRY_ISO":"SL",
      "CTRY_S_DESC":"SLOVENIA",
      "CTRY_B_DESC":"جمهورية سلوفينيا"
    },
    {
      "CTRY_CODE":"160",
      "CTRY_ISO":"LI",
      "CTRY_S_DESC":"Liechtenstein",
      "CTRY_B_DESC":"ليخنتنشتاين"
    },
    {
      "CTRY_CODE":"170",
      "CTRY_ISO":"EE",
      "CTRY_S_DESC":"Estonia",
      "CTRY_B_DESC":"استونيا"
    },
    {
      "CTRY_CODE":"171",
      "CTRY_ISO":"BS",
      "CTRY_S_DESC":"Bahamas ",
      "CTRY_B_DESC":"جزر الباهاما"
    },
    {
      "CTRY_CODE":"172",
      "CTRY_ISO":"GA",
      "CTRY_S_DESC":"Gabon",
      "CTRY_B_DESC":"الجابون"
    },
    {
      "CTRY_CODE":"173",
      "CTRY_ISO":"GE",
      "CTRY_S_DESC":"Georgia",
      "CTRY_B_DESC":"جورجيا"
    },
    {
      "CTRY_CODE":"174",
      "CTRY_ISO":"ME",
      "CTRY_S_DESC":"Montenegro",
      "CTRY_B_DESC":"مونتنيغرو"
    },
    {
      "CTRY_CODE":"175",
      "CTRY_ISO":"LC",
      "CTRY_S_DESC":"Saint Lucia",
      "CTRY_B_DESC":"سانت لوشيا"
    },
    {
      "CTRY_CODE":"176",
      "CTRY_ISO":"MV",
      "CTRY_S_DESC":"Maldives",
      "CTRY_B_DESC":"المالديف"
    },
    {
      "CTRY_CODE":"177",
      "CTRY_ISO":"AW",
      "CTRY_S_DESC":"Europe",
      "CTRY_B_DESC":"اروبا"
    },
    {
      "CTRY_CODE":"179",
      "CTRY_ISO":"MF",
      "CTRY_S_DESC":"Saint Martin (Island)",
      "CTRY_B_DESC":"سانت مارتن"
    },
    {
      "CTRY_CODE":"180",
      "CTRY_ISO":"PR",
      "CTRY_S_DESC":"Puerto Rico",
      "CTRY_B_DESC":"بورتيريكو"
    }
  ],

  "City":{
      "1": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "AMMAN",
          "CITY_B_DESC": "عمان"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "AMMAN2",
          "CITY_B_DESC": "عمان 2"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "IRBID",
          "CITY_B_DESC": "اربد"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "AZ ZARQA",
		  "CITY_B_DESC": "الزرقاء"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "JARASH",
		  "CITY_B_DESC": "جرش"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "AL KARAK",
		  "CITY_B_DESC": "الكرك"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "MA'AN",
		  "CITY_B_DESC": "معان"
        },
        {
          "CITY_CODE": "8",
          "CITY_S_DESC": "AL AQABAH",
		  "CITY_B_DESC": "العقبة"
        },
        {
          "CITY_CODE": "9",
          "CITY_S_DESC": "AJLUN",
		  "CITY_B_DESC": "عجلون"
        },
        {
          "CITY_CODE": "10",
          "CITY_S_DESC": "AL MAFRAQ",
		  "CITY_B_DESC": "المفرق"
        },
        {
          "CITY_CODE": "11",
          "CITY_S_DESC": "AL TAFILAH",
		  "CITY_B_DESC": "الطفيلة"
        },
        {
          "CITY_CODE": "12",
          "CITY_S_DESC": "MADABA",
		  "CITY_B_DESC": "مادبا"
        },
        {
          "CITY_CODE": "13",
          "CITY_S_DESC": "ALRAMTHA",
		  "CITY_B_DESC": "الرمثا"
        },
        {
          "CITY_CODE": "14",
          "CITY_S_DESC": "SALT",
		  "CITY_B_DESC": "السلط"
        },
        {
          "CITY_CODE": "101",
          "CITY_S_DESC": "SHMAISANI",
		  "CITY_B_DESC": "الشميساني"
        },
        {
          "CITY_CODE": "102",
          "CITY_S_DESC": "AMMAN",
		  "CITY_B_DESC": "عمان"
        },
        {
          "CITY_CODE": "104",
          "CITY_S_DESC": "KARAK",
		  "CITY_B_DESC": "الكرك"
        },
        {
          "CITY_CODE": "105",
          "CITY_S_DESC": "TAREEK AL- HIZAM OFFIICE",
		  "CITY_B_DESC": "مكتب طارق الحزام"
        },
        {
          "CITY_CODE": "106",
          "CITY_S_DESC": "AL-RAMTHA/AL-TURAH OFF.",
		  "CITY_B_DESC": "الرمثا / الطرة"
        },
        {
          "CITY_CODE": "107",
          "CITY_S_DESC": "IRBID",
		  "CITY_B_DESC": "اربد"
        },
        {
          "CITY_CODE": "108",
          "CITY_S_DESC": "MAAN",
		  "CITY_B_DESC": "معان"
        },
        {
          "CITY_CODE": "109",
          "CITY_S_DESC": "AL-RAMTHA",
		  "CITY_B_DESC": "الرمثا"
        },
        {
          "CITY_CODE": "110",
          "CITY_S_DESC": "AL-SALT",
		  "CITY_B_DESC": "السلط"
        },
        {
          "CITY_CODE": "111",
          "CITY_S_DESC": "JERASH",
		  "CITY_B_DESC": "جرش"
        },
        {
          "CITY_CODE": "112",
          "CITY_S_DESC": "AL-MAHATAH",
		  "CITY_B_DESC": "المحطة"
        },
        {
          "CITY_CODE": "113",
          "CITY_S_DESC": "MARKA",
		  "CITY_B_DESC": "ماركا"
        },
        {
          "CITY_CODE": "114",
          "CITY_S_DESC": "AJLOUN",
		  "CITY_B_DESC": "عجلون"
        },
        {
          "CITY_CODE": "115",
          "CITY_S_DESC": "AL-HUSSIEN",
		  "CITY_B_DESC": "الحسين"
        },
        {
          "CITY_CODE": "116",
          "CITY_S_DESC": "AL-SOUQ AL-TEGARY",
		  "CITY_B_DESC": "السوق التجاري"
        },
        {
          "CITY_CODE": "117",
          "CITY_S_DESC": "WADI AL-SSIER",
		  "CITY_B_DESC": "وادي السير"
        },
        {
          "CITY_CODE": "118",
          "CITY_S_DESC": "DER ABE SAEED",
		  "CITY_B_DESC": "دير أبي سعيد"
        },
        {
          "CITY_CODE": "119",
          "CITY_S_DESC": "SWELIH",
		  "CITY_B_DESC": "صويلح"
        },
        {
          "CITY_CODE": "120",
          "CITY_S_DESC": "MAADABA",
		  "CITY_B_DESC": "مادبا"
        },
        {
          "CITY_CODE": "121",
          "CITY_S_DESC": "JABAL AMMAN-FIRST CIRCLE",
		  "CITY_B_DESC": "جبل عمان - الدوار الاول"
        },
        {
          "CITY_CODE": "122",
          "CITY_S_DESC": "AIRPORT BRANCH",
		  "CITY_B_DESC": "فرع المطار"
        },
        {
          "CITY_CODE": "123",
          "CITY_S_DESC": "IRBID/HAKAM STREET",
		  "CITY_B_DESC": "اربد/ شارع حكما"
        },
        {
          "CITY_CODE": "124",
          "CITY_S_DESC": "ZARQA",
		  "CITY_B_DESC": "الزرقاء"
        },
        {
          "CITY_CODE": "125",
          "CITY_S_DESC": "AL-SHOUNEH AL-SHAMALIAH",
		  "CITY_B_DESC": "الشونة الشمالية"
        },
        {
          "CITY_CODE": "126",
          "CITY_S_DESC": "KOFRANJAH",
		  "CITY_B_DESC": "كفرنجة"
        },
        {
          "CITY_CODE": "127",
          "CITY_S_DESC": "IRBID/SOUK AL-BOKHARIAH",
		  "CITY_B_DESC": "اربد / سوق البخارية"
        },
        {
          "CITY_CODE": "128",
          "CITY_S_DESC": "JABAL AMAN-THIRD CIRCLE",
		  "CITY_B_DESC": "جبل عمان الدوار الثالث"
        },
        {
          "CITY_CODE": "129",
          "CITY_S_DESC": "AL-QUWESMAH",
		  "CITY_B_DESC": "القويسمة"
        },
        {
          "CITY_CODE": "130",
          "CITY_S_DESC": "AL-NUZHA",
		  "CITY_B_DESC": "النزهة"
        },
        {
          "CITY_CODE": "131",
          "CITY_S_DESC": "AL-GARDEENS",
		  "CITY_B_DESC": "جاردنز"
        },
        {
          "CITY_CODE": "132",
          "CITY_S_DESC": "AQABA",
		  "CITY_B_DESC": "العقبة"
        },
        {
          "CITY_CODE": "133",
          "CITY_S_DESC": "BAYADER",
		  "CITY_B_DESC": "البيادر"
        },
        {
          "CITY_CODE": "134",
          "CITY_S_DESC": "AL-MAFRAQ",
		  "CITY_B_DESC": "المفرق"
        },
        {
          "CITY_CODE": "135",
          "CITY_S_DESC": "IRBID/AL-HUSSUN  ST.OFF.",
		  "CITY_B_DESC": "اربد / شارع الحصن"
        },
        {
          "CITY_CODE": "136",
          "CITY_S_DESC": "AL-AZRAQ AL-SHAMALY",
		  "CITY_B_DESC": "الأزرق الشمالي"
        },
        {
          "CITY_CODE": "137",
          "CITY_S_DESC": "JABAL AL-WIEBDAH",
		  "CITY_B_DESC": "جبل اللويبدة"
        },
        {
          "CITY_CODE": "138",
          "CITY_S_DESC": "AL-FHEES OFF.",
		  "CITY_B_DESC": "الفحيص"
        },
        {
          "CITY_CODE": "139",
          "CITY_S_DESC": "WADI SAQRAH OFF.",
		  "CITY_B_DESC": "وادي صقرة "
        },
        {
          "CITY_CODE": "140",
          "CITY_S_DESC": "TAREQ",
		  "CITY_B_DESC": "طارق"
        },
        {
          "CITY_CODE": "141",
          "CITY_S_DESC": "ZARQA/MANTEKA HURRAH",
		  "CITY_B_DESC": "الزرقاء / المنطقة الحرة"
        },
        {
          "CITY_CODE": "142",
          "CITY_S_DESC": "MARJ AL-HAMAM",
		  "CITY_B_DESC": "مرج الحمام"
        },
        {
          "CITY_CODE": "143",
          "CITY_S_DESC": "JABAL AL-NASER/YARMOUK ST. OFF.",
		  "CITY_B_DESC": "مكتب جبل النصر / شارع اليرموك"
        },
        {
          "CITY_CODE": "144",
          "CITY_S_DESC": "AL-BAYADER/INDUST. AREA OFF.",
		  "CITY_B_DESC": "البيادر المنطقة الصناعية"
        },
        {
          "CITY_CODE": "145",
          "CITY_S_DESC": "IRBID/INDUS. CITY OFF.",
		  "CITY_B_DESC": "اربد / المدينة الصناعية"
        },
        {
          "CITY_CODE": "146",
          "CITY_S_DESC": "AL-JEZAH/AL-TALBIAH CAMP",
		  "CITY_B_DESC": "الجيزة"
        },
        {
          "CITY_CODE": "147",
          "CITY_S_DESC": "IRBID/IEDOON ST. OFF.",
		  "CITY_B_DESC": "اربدا / شارع ايدون"
        },
        {
          "CITY_CODE": "148",
          "CITY_S_DESC": "ABU N SAIER OFF.",
		  "CITY_B_DESC": "ابو نصير"
        },
        {
          "CITY_CODE": "149",
          "CITY_S_DESC": "HAY NAZAL OFF.",
		  "CITY_B_DESC": "حي نزال"
        },
        {
          "CITY_CODE": "150",
          "CITY_S_DESC": "RAS AL-AIN",
		  "CITY_B_DESC": "رأس العين"
        },
        {
          "CITY_CODE": "151",
          "CITY_S_DESC": "ZARQA/FAISAL ST. OFF.",
		  "CITY_B_DESC": "الزرقاء / شارع فيصل"
        },
        {
          "CITY_CODE": "152",
          "CITY_S_DESC": "AL-SWEFIAH",
		  "CITY_B_DESC": "الصويفية"
        },
        {
          "CITY_CODE": "153",
          "CITY_S_DESC": "AL-WEHDAT",
		  "CITY_B_DESC": "الوحدات"
        },
        {
          "CITY_CODE": "154",
          "CITY_S_DESC": "Rosyfeh",
		  "CITY_B_DESC": "الرصيفة"
        },
        {
          "CITY_CODE": "156",
          "CITY_S_DESC": "Mecca Street",
		  "CITY_B_DESC": "شارع مكة"
        },
        {
          "CITY_CODE": "157",
          "CITY_S_DESC": "IRBID/SOUK AL-BOKHARIAH",
		  "CITY_B_DESC": "اربد / سوق البخارية"
        },
        {
          "CITY_CODE": "158",
          "CITY_S_DESC": "New Zarqa",
		  "CITY_B_DESC": "الزرقاء الجديدة"
        },
        {
          "CITY_CODE": "159",
          "CITY_S_DESC": "Le Rolyal Hotel",
		  "CITY_B_DESC": "فندق لو روليال"
        },
        {
          "CITY_CODE": "160",
          "CITY_S_DESC": "Jbaiha",
		  "CITY_B_DESC": "الجبيهة"
        },
        {
          "CITY_CODE": "162",
          "CITY_S_DESC": "J. University Branch",
		  "CITY_B_DESC": "فرع الجامعة الاردنية"
        },
        {
          "CITY_CODE": "195",
          "CITY_S_DESC": "Invest. Dept.",
		  "CITY_B_DESC": "قسم الاستثمار."
        },
        {
          "CITY_CODE": "196",
          "CITY_S_DESC": "card departments",
		  "CITY_B_DESC": "إداراة البطاقات"
        },
        {
          "CITY_CODE": "197",
          "CITY_S_DESC": "HEAD OFFICE",
		  "CITY_B_DESC": "المكتب الرئيسي"
        },
        {
          "CITY_CODE": "198",
          "CITY_S_DESC": "SHAREHOLDERS",
		  "CITY_B_DESC": "المساهمين"
        },
        {
          "CITY_CODE": "199",
          "CITY_S_DESC": "TREASSURY",
		  "CITY_B_DESC": "الخزينة"
        },
        {
          "CITY_CODE": "15",
          "CITY_S_DESC": "AMMAN3",
		  "CITY_B_DESC": "عمان 3"
        },
        {
          "CITY_CODE": "652",
          "CITY_S_DESC": "REGIONAL OFFICE",
		  "CITY_B_DESC": "المكتب الإقليمي"
        },
        {
          "CITY_CODE": "16",
          "CITY_S_DESC": "AMMAN4",
		  "CITY_B_DESC": "عمان 4"
        },

      ],
      "2": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "al quds",
		  "CITY_B_DESC": "القدس"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Ramallah",
		  "CITY_B_DESC": "رام الله"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Nablus",
		  "CITY_B_DESC": "نابلس"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Qaza",
		  "CITY_B_DESC": "غزة"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Jenin",
		  "CITY_B_DESC": "جنين"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "Alkhaleel",
		  "CITY_B_DESC": "الخليل"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "Alram",
		  "CITY_B_DESC": "الرم"
        },
        {
          "CITY_CODE": "8",
          "CITY_S_DESC": "AlEizariya",
		  "CITY_B_DESC": "العيزرية"
        },
        {
          "CITY_CODE": "10",
          "CITY_S_DESC": "Ramallah2",
		  "CITY_B_DESC": "رام الله 2"
        },

      ],
      "3": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "BAGHDAD",
		  "CITY_B_DESC": "بغداد"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "MOSEUL",
		  "CITY_B_DESC": "الموصل"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "BASRAH",
		  "CITY_B_DESC": "البصرة"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "IRBIL",
		  "CITY_B_DESC": "اربيل"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "KIRKUK",
		  "CITY_B_DESC": "كركوك"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "SULAYMANIYAH",
		  "CITY_B_DESC": "السليمانية"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "NAJAF",
		  "CITY_B_DESC": "النجف"
        },
        {
          "CITY_CODE": "8",
          "CITY_S_DESC": "KUT",
		  "CITY_B_DESC": "الكوت"
        },
        {
          "CITY_CODE": "9",
          "CITY_S_DESC": "BA'QUBAH",
		  "CITY_B_DESC": "بعقوبة"
        },
        {
          "CITY_CODE": "10",
          "CITY_S_DESC": "FALLUJAH",
		  "CITY_B_DESC": "الفلوجة"
        },
        {
          "CITY_CODE": "11",
          "CITY_S_DESC": "TIKRIT",
		  "CITY_B_DESC": "تكريت"
        },

      ],
      "4": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Damascus",
		  "CITY_B_DESC": "دمشق"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Aleppo",
		  "CITY_B_DESC": "حلب"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Homs",
		  "CITY_B_DESC": "حمص"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Hama",
		  "CITY_B_DESC": "حماة"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Al Ladhiqiyah",
		  "CITY_B_DESC": "اللاذقية"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "Tartus",
		  "CITY_B_DESC": "طرطوس"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "Tartus",
		  "CITY_B_DESC": "طرطوس"
        },

      ],
      "5": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Riyadh",
		  "CITY_B_DESC": "الرياض"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Mecca",
		  "CITY_B_DESC": "مكة المكرمة"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Medina",
		  "CITY_B_DESC": "المدينة المنورة"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Dhahran",
		  "CITY_B_DESC": "الظهران"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Dammam",
		  "CITY_B_DESC": "الدمام"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "Jeddah",
		  "CITY_B_DESC": "جدة"
        },

      ],
      "6": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Kuwait",
		  "CITY_B_DESC": "الكويت"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Salmiya",
		  "CITY_B_DESC": "السالمية"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Ahmadi",
		  "CITY_B_DESC": "أحمدي"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Fahaheel",
		  "CITY_B_DESC": "الفحيحيل"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Kazimah",
		  "CITY_B_DESC": "كاظمة"
        },

      ],
      "7": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Beirut",
		  "CITY_B_DESC": "بيروت"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Tripoli",
		  "CITY_B_DESC": "طرابلس"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Zahlé",
		  "CITY_B_DESC": "زحلة"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Chtoura",
		  "CITY_B_DESC": "شتورا"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Naqoura",
		  "CITY_B_DESC": "الناقورة"
        },

      ],
      "8": [
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Alexandria",
		  "CITY_B_DESC": "الإسكندرية"
        },
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Cairo",
		  "CITY_B_DESC": "القاهرة"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Port Said",
		  "CITY_B_DESC": "بورسعيد"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Tanta",
		  "CITY_B_DESC": "طنطا"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Faiyum",
		  "CITY_B_DESC": "الفيوم"
        },

      ],
      "9": [
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Washington",
		  "CITY_B_DESC": "واشنطن"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Alabama",
		  "CITY_B_DESC": "ألاباما"
        },
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "New York",
		  "CITY_B_DESC": "نيويورك"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Alaska",
		  "CITY_B_DESC": "ألاسكا"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Arizona",
		  "CITY_B_DESC": "أريزونا"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "Arkansas",
		  "CITY_B_DESC": "أركنساس"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "California",
		  "CITY_B_DESC": "كاليفورنيا"
        },
        {
          "CITY_CODE": "8",
          "CITY_S_DESC": "Colorado",
		  "CITY_B_DESC": "كولورادو"
        },
        {
          "CITY_CODE": "9",
          "CITY_S_DESC": "Connecticut",
		  "CITY_B_DESC": "كونيكتيكت"
        },
        {
          "CITY_CODE": "10",
          "CITY_S_DESC": "Delaware",
		  "CITY_B_DESC": "ديلاوير"
        },
        {
          "CITY_CODE": "11",
          "CITY_S_DESC": "District of Columbia",
		  "CITY_B_DESC": "مقاطعة كولومبيا"
        },
        {
          "CITY_CODE": "12",
          "CITY_S_DESC": "Florida",
		  "CITY_B_DESC": "فلوريدا"
        },
        {
          "CITY_CODE": "13",
          "CITY_S_DESC": "Georgia",
		  "CITY_B_DESC": "جورجيا"
        },
        {
          "CITY_CODE": "14",
          "CITY_S_DESC": "Hawaii",
		  "CITY_B_DESC": "هاواي"
        },
        {
          "CITY_CODE": "15",
          "CITY_S_DESC": "Idaho",
		  "CITY_B_DESC": "أيداهو"
        },
        {
          "CITY_CODE": "16",
          "CITY_S_DESC": "Illinois",
		  "CITY_B_DESC": "إلينوي"
        },
        {
          "CITY_CODE": "17",
          "CITY_S_DESC": "Indiana",
		  "CITY_B_DESC": "إنديانا"
        },
        {
          "CITY_CODE": "18",
          "CITY_S_DESC": "Iowa",
		  "CITY_B_DESC": "أيوا"
        },
        {
          "CITY_CODE": "19",
          "CITY_S_DESC": "Kansas",
		  "CITY_B_DESC": "كانساس"
        },
        {
          "CITY_CODE": "20",
          "CITY_S_DESC": "Kentucky",
		  "CITY_B_DESC": "كنتاكي"
        },
        {
          "CITY_CODE": "21",
          "CITY_S_DESC": "Louisiana",
		  "CITY_B_DESC": "لويزيانا"
        },
        {
          "CITY_CODE": "22",
          "CITY_S_DESC": "Maine",
		  "CITY_B_DESC": "ماين"
        },
        {
          "CITY_CODE": "23",
          "CITY_S_DESC": "Maryland",
		  "CITY_B_DESC": "ماريلاند"
        },
        {
          "CITY_CODE": "24",
          "CITY_S_DESC": "Massachusetts",
		  "CITY_B_DESC": "ماساتشوستس"
        },
        {
          "CITY_CODE": "25",
          "CITY_S_DESC": "Michigan",
		  "CITY_B_DESC": "ميشيغان"
        },
        {
          "CITY_CODE": "26",
          "CITY_S_DESC": "Minnesota",
		  "CITY_B_DESC": "مينيسوتا"
        },
        {
          "CITY_CODE": "27",
          "CITY_S_DESC": "Mississippi",
		  "CITY_B_DESC": "ميسيسيبي"
        },
        {
          "CITY_CODE": "28",
          "CITY_S_DESC": "Missouri",
		  "CITY_B_DESC": "ميسوري"
        },
        {
          "CITY_CODE": "29",
          "CITY_S_DESC": "Montana",
		  "CITY_B_DESC": "مونتانا"
        },
        {
          "CITY_CODE": "30",
          "CITY_S_DESC": "Nebraska",
		  "CITY_B_DESC": "نبراسكا"
        },
        {
          "CITY_CODE": "31",
          "CITY_S_DESC": "Nevada",
		  "CITY_B_DESC": "نيفادا"
        },
        {
          "CITY_CODE": "32",
          "CITY_S_DESC": "New Hampshire",
		  "CITY_B_DESC": "نيو هامبشاير"
        },
        {
          "CITY_CODE": "33",
          "CITY_S_DESC": "New Jersey",
		  "CITY_B_DESC": "نيو جيرسي"
        },
        {
          "CITY_CODE": "34",
          "CITY_S_DESC": "New Mexico",
		  "CITY_B_DESC": "المكسيك"
        },
        {
          "CITY_CODE": "35",
          "CITY_S_DESC": "North Carolina",
		  "CITY_B_DESC": "شمال كارولينا"
        },
        {
          "CITY_CODE": "36",
          "CITY_S_DESC": "North Dakota",
		  "CITY_B_DESC": "أوهايو"
        },
        {
          "CITY_CODE": "37",
          "CITY_S_DESC": "Ohio",
		  "CITY_B_DESC": "أوهايو"
        },
        {
          "CITY_CODE": "38",
          "CITY_S_DESC": "Oklahoma",
		  "CITY_B_DESC": "أوكلاهوما"
        },
        {
          "CITY_CODE": "39",
          "CITY_S_DESC": "Oregon",
		  "CITY_B_DESC": "أوريغون"
        },
        {
          "CITY_CODE": "40",
          "CITY_S_DESC": "Pennsylvania",
		  "CITY_B_DESC": "بنسلفانيا"
        },
        {
          "CITY_CODE": "41",
          "CITY_S_DESC": "Rhode Island",
		  "CITY_B_DESC": "جزيرة رود"
        },
        {
          "CITY_CODE": "42",
          "CITY_S_DESC": "South Carolina",
		  "CITY_B_DESC": "كارولينا الجنوبية"
        },
        {
          "CITY_CODE": "43",
          "CITY_S_DESC": "South Dakota",
		  "CITY_B_DESC": "جنوب داكوتا"
        },
        {
          "CITY_CODE": "44",
          "CITY_S_DESC": "Tennessee",
		  "CITY_B_DESC": "تينيسي"
        },
        {
          "CITY_CODE": "45",
          "CITY_S_DESC": "Texas",
		  "CITY_B_DESC": "تكساس"
        },
        {
          "CITY_CODE": "46",
          "CITY_S_DESC": "Utah",
		  "CITY_B_DESC": "يوتا"
        },
        {
          "CITY_CODE": "47",
          "CITY_S_DESC": "Vermont",
		  "CITY_B_DESC": "فيرمونت"
        },
        {
          "CITY_CODE": "48",
          "CITY_S_DESC": "Virginia",
		  "CITY_B_DESC": "فرجينيا"
        },
        {
          "CITY_CODE": "49",
          "CITY_S_DESC": "West Virginia",
		  "CITY_B_DESC": "فرجينيا الغربية"
        },
        {
          "CITY_CODE": "50",
          "CITY_S_DESC": "Wisconsin",
		  "CITY_B_DESC": "ويسكونسن"
        },
        {
          "CITY_CODE": "51",
          "CITY_S_DESC": "Wyoming",
		  "CITY_B_DESC": "وايومنغ"
        },

      ],
      "10": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Paris",
		  "CITY_B_DESC": "باريس"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Bordeaux",
		  "CITY_B_DESC": "بوردو"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Cannes",
		  "CITY_B_DESC": "كان"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Lourdes",
		  "CITY_B_DESC": "لوردز"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Lyon",
		  "CITY_B_DESC": "ليون"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "Marseille",
		  "CITY_B_DESC": "مرسيليا"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "Toulouse",
		  "CITY_B_DESC": "تولوز"
        },

      ],
      "11": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "ROME",
		  "CITY_B_DESC": "روما"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Milan",
		  "CITY_B_DESC": "ميلان"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Naples",
		  "CITY_B_DESC": "نابولي"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Turin",
		  "CITY_B_DESC": "تورينو"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Palermo",
		  "CITY_B_DESC": "باليرمو"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "Genoa",
		  "CITY_B_DESC": "جنوة"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "Bologna",
		  "CITY_B_DESC": "بولونيا"
        },
        {
          "CITY_CODE": "8",
          "CITY_S_DESC": "Florence",
		  "CITY_B_DESC": "فلورنسا"
        },
        {
          "CITY_CODE": "9",
          "CITY_S_DESC": "Catania",
		  "CITY_B_DESC": "كاتانيا"
        },
        {
          "CITY_CODE": "10",
          "CITY_S_DESC": "Bari",
		  "CITY_B_DESC": "باري"
        },
        {
          "CITY_CODE": "11",
          "CITY_S_DESC": "Venice",
		  "CITY_B_DESC": "البندقية"
        },
        {
          "CITY_CODE": "12",
          "CITY_S_DESC": "Taranto",
		  "CITY_B_DESC": "تارانتو"
        },
        {
          "CITY_CODE": "13",
          "CITY_S_DESC": "Trento",
		  "CITY_B_DESC": "ترينتو"
        },

      ],
      "12": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "OTTAWA",
		  "CITY_B_DESC": "اوتاوا"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "EDMONTON",
		  "CITY_B_DESC": "ادمونتون"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "TORONTO",
		  "CITY_B_DESC": "تورونتو"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "VANCOUVER",
		  "CITY_B_DESC": "فانكوفر"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "VICTORIA",
		  "CITY_B_DESC": "قكتوريا"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "WINNIPEG",
		  "CITY_B_DESC": "وينيبغ"
        },

      ],
      "13": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "ANKARA",
		  "CITY_B_DESC": "أنقرة"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "ISTANBUL",
		  "CITY_B_DESC": "اسطنبول"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "IZMIR",
		  "CITY_B_DESC": "ازمير"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "BURSA",
		  "CITY_B_DESC": "بورصة"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "ADANA",
		  "CITY_B_DESC": "اضنة"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "GAZIANTEP",
		  "CITY_B_DESC": "غازينتب"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "KONYA",
		  "CITY_B_DESC": "قونية"
        },
        {
          "CITY_CODE": "8",
          "CITY_S_DESC": "ANTALYA",
		  "CITY_B_DESC": "انطاليا"
        },
        {
          "CITY_CODE": "9",
          "CITY_S_DESC": "DIYARBAKIR",
		  "CITY_B_DESC": "ديار بكر"
        },
        {
          "CITY_CODE": "10",
          "CITY_S_DESC": "MERSIN",
		  "CITY_B_DESC": "مرسين"
        },

      ],
      "14": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "LONDON",
		  "CITY_B_DESC": "لندن"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Birmingham",
		  "CITY_B_DESC": "برمنغهام"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Glasgow",
		  "CITY_B_DESC": "غلاسكو"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Liverpool",
		  "CITY_B_DESC": "ليفربول"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Sheffield",
		  "CITY_B_DESC": "شيفيلد"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "Leeds",
		  "CITY_B_DESC": "ليدز"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "Bristol",
		  "CITY_B_DESC": "بريستول"
        },
        {
          "CITY_CODE": "8",
          "CITY_S_DESC": "Manchester",
		  "CITY_B_DESC": "مانشستر"
        },
        {
          "CITY_CODE": "9",
          "CITY_S_DESC": "Edinburgh",
		  "CITY_B_DESC": "أدنبرة"
        },
        {
          "CITY_CODE": "10",
          "CITY_S_DESC": "Leicester",
		  "CITY_B_DESC": "ليستر"
        },
        {
          "CITY_CODE": "11",
          "CITY_S_DESC": "Coventry",
		  "CITY_B_DESC": "كوفنتري"
        },
        {
          "CITY_CODE": "12",
          "CITY_S_DESC": "Bradford",
		  "CITY_B_DESC": "برادفورد"
        },
        {
          "CITY_CODE": "13",
          "CITY_S_DESC": "Cardiff",
		  "CITY_B_DESC": "كارديف"
        },
        {
          "CITY_CODE": "14",
          "CITY_S_DESC": "Nottingham",
		  "CITY_B_DESC": "نوتنغهام"
        },
        {
          "CITY_CODE": "15",
          "CITY_S_DESC": "Belfast",
		  "CITY_B_DESC": "بلفاست"
        },
        {
          "CITY_CODE": "16",
          "CITY_S_DESC": "Southampton",
		  "CITY_B_DESC": "ساوثامبتون"
        },
        {
          "CITY_CODE": "25",
          "CITY_S_DESC": "ALL U.K OFFICES",
		  "CITY_B_DESC": "جميع مكاتب المملكة المتحدة"
        },

      ],
      "18": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Muscat",
		  "CITY_B_DESC": "مسقط"
        },

      ],
      "19": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "TOKYO",
		  "CITY_B_DESC": "طوكيو"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "YOKOHAMA",
		  "CITY_B_DESC": "يوكوهاما"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "OSAKA",
		  "CITY_B_DESC": "اوساكا"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "NAGOYA",
		  "CITY_B_DESC": "ناغويا"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "SAPPORO",
		  "CITY_B_DESC": "سابورو"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "KOBE",
		  "CITY_B_DESC": "كوبي"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "KYOTO",
		  "CITY_B_DESC": "كيوتو"
        },
        {
          "CITY_CODE": "8",
          "CITY_S_DESC": "FUKUOKA",
		  "CITY_B_DESC": "فوكوكا"
        },
        {
          "CITY_CODE": "9",
          "CITY_S_DESC": "KAWASAKI",
		  "CITY_B_DESC": "كاواساكي"
        },
        {
          "CITY_CODE": "10",
          "CITY_S_DESC": "HIROSHIMA",
		  "CITY_B_DESC": "هيروشيما"
        },
        {
          "CITY_CODE": "11",
          "CITY_S_DESC": "SAITAMA",
		  "CITY_B_DESC": "سايتاما"
        },
        {
          "CITY_CODE": "12",
          "CITY_S_DESC": "KITAKYUSHU",
		  "CITY_B_DESC": "كيتاكيوشو"
        },
        {
          "CITY_CODE": "13",
          "CITY_S_DESC": "SENDAI",
		  "CITY_B_DESC": "سانداي"
        },

      ],
      "20": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Abuja",
		  "CITY_B_DESC": "أبوجا"
        },

      ],
      "21": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "KOPNHAGEN",
		  "CITY_B_DESC": "كونبناهاغن"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "ABENRA",
		  "CITY_B_DESC": "أبنرا"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "ALBORG",
		  "CITY_B_DESC": "فايل"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "ARHUS",
		  "CITY_B_DESC": "آرهوس"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "ESBJERG",
		  "CITY_B_DESC": "اسبرغ"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "FREDERICIA",
		  "CITY_B_DESC": "فردريسا"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "KOLDING",
		  "CITY_B_DESC": "كولدنغ"
        },
        {
          "CITY_CODE": "8",
          "CITY_S_DESC": "SKAGEN",
		  "CITY_B_DESC": "سكاغن"
        },
        {
          "CITY_CODE": "9",
          "CITY_S_DESC": "VEJLE",
		  "CITY_B_DESC": "فايلي"
        },
        {
          "CITY_CODE": "10",
          "CITY_S_DESC": "DANFOSS",
		  "CITY_B_DESC": "دانفوس"
        },

      ],
      "22": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "CARACAS",
		  "CITY_B_DESC": "كراكاس"
        },

      ],
      "23": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "BEKEEN",
		  "CITY_B_DESC": "بكين"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "ANJI",
		  "CITY_B_DESC": "انجي"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "ANSHAN",
		  "CITY_B_DESC": "أنشان"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "BAODING",
		  "CITY_B_DESC": "باودنغ"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "BEIJING",
		  "CITY_B_DESC": "بكين"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "CANGZHOU",
		  "CITY_B_DESC": "كانجزهو"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "CHANGCHUN",
		  "CITY_B_DESC": "تشانغتشون"
        },
        {
          "CITY_CODE": "8",
          "CITY_S_DESC": "DALIAN",
		  "CITY_B_DESC": "داليان"
        },
        {
          "CITY_CODE": "9",
          "CITY_S_DESC": "FOSHAN",
		  "CITY_B_DESC": "فوشان"
        },
        {
          "CITY_CODE": "10",
          "CITY_S_DESC": "GUANGZHOU",
		  "CITY_B_DESC": "قوانغتشو"
        },
        {
          "CITY_CODE": "11",
          "CITY_S_DESC": "HAICHENG",
		  "CITY_B_DESC": "هايتشنغ"
        },
        {
          "CITY_CODE": "12",
          "CITY_S_DESC": "JIANGDU",
		  "CITY_B_DESC": "جيانجو"
        },
        {
          "CITY_CODE": "13",
          "CITY_S_DESC": "KAIFENG",
		  "CITY_B_DESC": "كايفنغ"
        },
        {
          "CITY_CODE": "14",
          "CITY_S_DESC": "LANXI",
		  "CITY_B_DESC": "انكسي"
        },
        {
          "CITY_CODE": "15",
          "CITY_S_DESC": "MACAU",
		  "CITY_B_DESC": "ماكاو"
        },
        {
          "CITY_CODE": "16",
          "CITY_S_DESC": "NANHAI",
		  "CITY_B_DESC": "نانهاي"
        },
        {
          "CITY_CODE": "17",
          "CITY_S_DESC": "PANJIN",
		  "CITY_B_DESC": "بانجين"
        },
        {
          "CITY_CODE": "18",
          "CITY_S_DESC": "QIANJIANG",
		  "CITY_B_DESC": "تشيانجيانغ"
        },
        {
          "CITY_CODE": "19",
          "CITY_S_DESC": "RIZHAO",
		  "CITY_B_DESC": "ريتشاو"
        },
        {
          "CITY_CODE": "20",
          "CITY_S_DESC": "SANMING",
		  "CITY_B_DESC": "سانمينغ"
        },
        {
          "CITY_CODE": "21",
          "CITY_S_DESC": "TAIAN",
		  "CITY_B_DESC": "تايا"
        },
        {
          "CITY_CODE": "22",
          "CITY_S_DESC": "URUMQI",
		  "CITY_B_DESC": "أورومتشي"
        },
        {
          "CITY_CODE": "23",
          "CITY_S_DESC": "WEIFANG",
		  "CITY_B_DESC": "ويفانج"
        },
        {
          "CITY_CODE": "24",
          "CITY_S_DESC": "XIAMEN",
		  "CITY_B_DESC": "شيامن"
        },
        {
          "CITY_CODE": "25",
          "CITY_S_DESC": "YANGCHEN",
		  "CITY_B_DESC": "يانغشن"
        },
        {
          "CITY_CODE": "26",
          "CITY_S_DESC": "ZHUHAI",
		  "CITY_B_DESC": "تشوهاى"
        },
        {
          "CITY_CODE": "30",
          "CITY_S_DESC": "ALL OVER CHINA",
		  "CITY_B_DESC": "في جميع أنحاء الصين"
        },

      ],
      "24": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Abu Dhabi",
		  "CITY_B_DESC": "أبو ظبي"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Dubai",
		  "CITY_B_DESC": "دبي"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Turaif",
		  "CITY_B_DESC": "طريف"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Sharjah",
		  "CITY_B_DESC": "الشارقة"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Ajman",
		  "CITY_B_DESC": "عجمان"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "Ras al Khaimah",
		  "CITY_B_DESC": "رأس الخيمة"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "Al Ain",
		  "CITY_B_DESC": "العين"
        },

      ],
      "25": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "DOHA",
		  "CITY_B_DESC": "الدوحة"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "AL WAKRAH",
		  "CITY_B_DESC": "الوكرة"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "GHARIYAT ADKHAN",
		  "CITY_B_DESC": "غريات أدخان"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "AL JUMAYLIYAH",
		  "CITY_B_DESC": "الجميلية"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "AL GHUWAYRIYAH",
		  "CITY_B_DESC": "الغويرية"
        },

      ],
      "26": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Manama",
		  "CITY_B_DESC": "المنامة"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "AlMahraq",
		  "CITY_B_DESC": "المحرق"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "AlJahra",
		  "CITY_B_DESC": "الجهرة"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Alzalaq",
		  "CITY_B_DESC": "الزلاق"
        },

      ],
      "27": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Tunis",
		  "CITY_B_DESC": "تونس"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Alqairawan",
		  "CITY_B_DESC": "القيروان"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Bizerte",
		  "CITY_B_DESC": "بنزرت"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Sussa",
		  "CITY_B_DESC": "سوسة"
        },

      ],
      "28": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "ALGYRIA",
		  "CITY_B_DESC": "الجزائر"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Wahran",
		  "CITY_B_DESC": "وهران"
        },

      ],
      "29": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "AlKhartoum",
		  "CITY_B_DESC": "الخرطوم"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Omdurman",
		  "CITY_B_DESC": "أم درمان"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Atbara",
		  "CITY_B_DESC": "عطبرة"
        },

      ],
      "30": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Tripoli",
		  "CITY_B_DESC": "طرابلس"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Benghazi",
		  "CITY_B_DESC": "بنغازي"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Tobruk",
		  "CITY_B_DESC": "طبرق"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Derna",
		  "CITY_B_DESC": "درنة"
        },

      ],
      "31": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Rabat",
		  "CITY_B_DESC": "الرباط"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Marrakesh",
		  "CITY_B_DESC": "مراكش"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Tangier",
		  "CITY_B_DESC": "طنجة"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Fes",
		  "CITY_B_DESC": "فاس"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Casablanca",
		  "CITY_B_DESC": "الدار البيضاء"
        },

      ],
      "32": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Mogadishu",
		  "CITY_B_DESC": "مقديشو"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Jarwa",
		  "CITY_B_DESC": "الجروة"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Margarita",
		  "CITY_B_DESC": "مارغريتا"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Berbera",
		  "CITY_B_DESC": "بربرة"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Ayl",
		  "CITY_B_DESC": "ايل"
        },

      ],
      "33": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "SYDNEY",
		  "CITY_B_DESC": "سيدني"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "VICTORIA",
		  "CITY_B_DESC": "قكتوريا"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "PERTH",
		  "CITY_B_DESC": "بيرث"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "ADELAIDE",
		  "CITY_B_DESC": "أديليد"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "MELBOURNE",
		  "CITY_B_DESC": "ملبورن"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "HOBART",
		  "CITY_B_DESC": "هوبارت"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "ALBURY-WODONGA",
		  "CITY_B_DESC": "البوري-دونغا"
        },
        {
          "CITY_CODE": "8",
          "CITY_S_DESC": "CANBERRA",
		  "CITY_B_DESC": "كانبيرا"
        },
        {
          "CITY_CODE": "9",
          "CITY_S_DESC": "WOLLONGONG",
		  "CITY_B_DESC": "ولونغونغ"
        },
        {
          "CITY_CODE": "10",
          "CITY_S_DESC": "BROOME",
		  "CITY_B_DESC": "بروم"
        },
        {
          "CITY_CODE": "11",
          "CITY_S_DESC": "NEWCASTLE",
		  "CITY_B_DESC": "نيوكاسل"
        },
        {
          "CITY_CODE": "12",
          "CITY_S_DESC": "BRISBANE",
		  "CITY_B_DESC": "بريسبان"
        },
        {
          "CITY_CODE": "13",
          "CITY_S_DESC": "TOWNSVILLE",
		  "CITY_B_DESC": "تونفيل"
        },
        {
          "CITY_CODE": "14",
          "CITY_S_DESC": "CAIRNS",
		  "CITY_B_DESC": "كيرنز"
        },
        {
          "CITY_CODE": "15",
          "CITY_S_DESC": "DARWIN",
		  "CITY_B_DESC": "داروين"
        },
        {
          "CITY_CODE": "16",
          "CITY_S_DESC": "ALICE SPRINGS",
		  "CITY_B_DESC": "أليس الينابيع"
        },

      ],
      "34": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "MEXICO CITY",
		  "CITY_B_DESC": "مدينة مكسيكو"
        },

      ],
      "35": [
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "SAO PAULO",
		  "CITY_B_DESC": "ساو باولو"
        },
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "BARAZILIA",
		  "CITY_B_DESC": "برازيليا"
        },

      ],
      "36": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "BUENOS AIRES",
		  "CITY_B_DESC": "بوينوس آيرز"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "CORDOBA",
		  "CITY_B_DESC": "كوردوبا"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "PARANA",
		  "CITY_B_DESC": "بارانا"
        },

      ],
      "37": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Asuncion",
		  "CITY_B_DESC": "أسونسيون"
        },

      ],
      "38": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "ARMENIA",
		  "CITY_B_DESC": "أرمينيا"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "BOGOTA",
		  "CITY_B_DESC": "بوجوتا"
        },

      ],
      "39": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "NOUAKCHOTT",
		  "CITY_B_DESC": "نواكشوط"
        },

      ],
      "40": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Djibouti",
		  "CITY_B_DESC": "جيبوتي"
        },

      ],
      "41": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Dublin",
		  "CITY_B_DESC": "دبلن"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Cork",
		  "CITY_B_DESC": "كورك"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Galway",
		  "CITY_B_DESC": "غالواي"
        },

      ],
      "42": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "ATHENS",
		  "CITY_B_DESC": "أثينا"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "SPARTA",
		  "CITY_B_DESC": "سبارتا"
        },

      ],
      "43": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Nicosia",
		  "CITY_B_DESC": "نيقوسيا"
        },

      ],
      "44": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "LISBON",
		  "CITY_B_DESC": "لشبونة"
        },

      ],
      "45": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "MADRID",
		  "CITY_B_DESC": "مدريد"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "VALENCIA",
		  "CITY_B_DESC": "فالنسيا"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "PALMA",
		  "CITY_B_DESC": "بالما"
        },

      ],
      "46": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Sana'a",
		  "CITY_B_DESC": "صنعاء"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Taizz",
		  "CITY_B_DESC": "تعز"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Al Hudaydah",
		  "CITY_B_DESC": "الحديدة"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Aden",
		  "CITY_B_DESC": "عدن"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Ibb",
		  "CITY_B_DESC": "إب"
        },

      ],
      "47": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "BRUSSELS",
		  "CITY_B_DESC": "بروكسل"
        },

      ],
      "49": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "BERLIN",
		  "CITY_B_DESC": "برلين"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "BREMEN",
		  "CITY_B_DESC": "بريمن"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "COLOGNE",
		  "CITY_B_DESC": "كولونيا"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "DRESDEN",
		  "CITY_B_DESC": "دريسدن"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "DUESSELDORF",
		  "CITY_B_DESC": "دوسيلدورف"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "FRANKFURT",
		  "CITY_B_DESC": "فرانكفورت"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "HAMBURG",
		  "CITY_B_DESC": "هامبورغ"
        },
        {
          "CITY_CODE": "8",
          "CITY_S_DESC": "HEIDELBERG",
		  "CITY_B_DESC": "هايدلبرغ"
        },
        {
          "CITY_CODE": "9",
          "CITY_S_DESC": "KOBLENZ",
		  "CITY_B_DESC": "كوبلينز"
        },
        {
          "CITY_CODE": "10",
          "CITY_S_DESC": "NUREMBERG",
		  "CITY_B_DESC": "نورمبرج"
        },
        {
          "CITY_CODE": "11",
          "CITY_S_DESC": "MUNICH",
		  "CITY_B_DESC": "ميونيخ"
        },
        {
          "CITY_CODE": "12",
          "CITY_S_DESC": "OSNABRUECK",
		  "CITY_B_DESC": "أوسنابروك"
        },
        {
          "CITY_CODE": "13",
          "CITY_S_DESC": "TECKLENBURG",
		  "CITY_B_DESC": "تكلنبورغ"
        },

      ],
      "51": [
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "ROTTERDAM",
		  "CITY_B_DESC": "روتردام"
        },
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "AMSTERDAM",
		  "CITY_B_DESC": "امستردام"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "THE HAGUE",
		  "CITY_B_DESC": "لاهاي"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "UTRECHT",
		  "CITY_B_DESC": "اوترخت"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "EINDHOVEN",
		  "CITY_B_DESC": "ايندهوفن"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "TILBURG",
		  "CITY_B_DESC": "تيلبورغ"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "GRONINGEN",
		  "CITY_B_DESC": "جرونينجن"
        },
        {
          "CITY_CODE": "8",
          "CITY_S_DESC": "BREDA",
		  "CITY_B_DESC": "بريدا"
        },
        {
          "CITY_CODE": "9",
          "CITY_S_DESC": "APELDOORN",
		  "CITY_B_DESC": "أبلدورن"
        },
        {
          "CITY_CODE": "10",
          "CITY_S_DESC": "ENSCHEDE",
		  "CITY_B_DESC": "انشيد"
        },
        {
          "CITY_CODE": "11",
          "CITY_S_DESC": "HAARLEM",
		  "CITY_B_DESC": "هارلم "
        },
        {
          "CITY_CODE": "12",
          "CITY_S_DESC": "ARNHEIM",
		  "CITY_B_DESC": "أرنهيم"
        },
        {
          "CITY_CODE": "13",
          "CITY_S_DESC": "ZAANSTAD",
		  "CITY_B_DESC": "زانستاد"
        },
        {
          "CITY_CODE": "14",
          "CITY_S_DESC": "AMERSFOORT",
		  "CITY_B_DESC": "امرسفورت"
        },
        {
          "CITY_CODE": "15",
          "CITY_S_DESC": "LEIDEN",
		  "CITY_B_DESC": "إيدن"
        },
        {
          "CITY_CODE": "16",
          "CITY_S_DESC": "ZOETERMEER",
		  "CITY_B_DESC": "زويتيرمير"
        },

      ],
      "52": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Stockholm",
		  "CITY_B_DESC": "ستوكهولم"
        },

      ],
      "53": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "VIENNA",
		  "CITY_B_DESC": "فيينا"
        },

      ],
      "54": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Helsink",
		  "CITY_B_DESC": "هلسنكي"
        },

      ],
      "55": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "ZURICH",
		  "CITY_B_DESC": "زوريخ"
        },

      ],
      "61": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Yerevan",
		  "CITY_B_DESC": "يريفان"
        },

      ],
      "62": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "CHENGIJN",
		  "CITY_B_DESC": "تشنجين"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "NAMPO",
		  "CITY_B_DESC": "نامبو"
        },

      ],
      "63": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Seoul",
		  "CITY_B_DESC": "سيول"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Pusan",
		  "CITY_B_DESC": "بوسان"
        },

      ],
      "65": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "NEWDELHI",
		  "CITY_B_DESC": "نيو دلهي"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Kashmir",
		  "CITY_B_DESC": "كشمير"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Jammu",
		  "CITY_B_DESC": "جامو"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Uttar Pradesh",
		  "CITY_B_DESC": "ولاية اوتار براديش"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Poompuhar",
		  "CITY_B_DESC": "بومبوهار"
        },

      ],
      "66": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "ISLAMABAD",
		  "CITY_B_DESC": "اسلام اباد"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "KARACHI",
		  "CITY_B_DESC": "كراتشي"
        },

      ],
      "67": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "ANGELES",
		  "CITY_B_DESC": "انجليس"
        },

      ],
      "68": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "COLOMBO",
		  "CITY_B_DESC": "كولومبو"
        },

      ],
      "69": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "TAPEI",
		  "CITY_B_DESC": "تايبي"
        },

      ],
      "70": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "HONG KONG",
		  "CITY_B_DESC": "هونج كونج"
        },

      ],
      "71": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Astana",
		  "CITY_B_DESC": "أستانا"
        },

      ],
      "72": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "SOFIA",
		  "CITY_B_DESC": "صوفيا"
        },

      ],
      "73": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Moscow",
		  "CITY_B_DESC": "موسكو"
        },

      ],
      "74": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "KIEV",
		  "CITY_B_DESC": "كييف"
        },

      ],
      "75": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "BUCHAREST",
		  "CITY_B_DESC": "بوخارست"
        },

      ],
      "76": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Belgrade",
		  "CITY_B_DESC": "بلغراد"
        },

      ],
      "77": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Zagreb",
		  "CITY_B_DESC": "زغرب"
        },

      ],
      "78": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Sarajevo",
		  "CITY_B_DESC": "سراييفو"
        },

      ],
      "79": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Santiago",
		  "CITY_B_DESC": "سانتياغو"
        },

      ],
      "82": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Kinshasa",
		  "CITY_B_DESC": "كينشاسا"
        },

      ],
      "83": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Dodoma",
		  "CITY_B_DESC": "دودوما"
        },

      ],
      "84": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "PRETORIA",
		  "CITY_B_DESC": "بريتوريا"
        },

      ],
      "85": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Kampala",
		  "CITY_B_DESC": "كمبالا"
        },

      ],
      "87": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Yaounde",
		  "CITY_B_DESC": "ياوندي"
        },

      ],
      "90": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Kuki",
		  "CITY_B_DESC": "كوكي"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Tashreen Jam",
		  "CITY_B_DESC": "طاشرين مربى"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Toon",
		  "CITY_B_DESC": "تون"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Kaolack",
		  "CITY_B_DESC": "كاولاك"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Lucca",
		  "CITY_B_DESC": "لوكا"
        },

      ],
      "91": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Nairobi",
		  "CITY_B_DESC": "نيروبي"
        },

      ],
      "93": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Kuala Lumpur",
		  "CITY_B_DESC": "كوالا لامبور"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Pahang",
		  "CITY_B_DESC": "باهانج"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Kuala Terengganu",
		  "CITY_B_DESC": "كوالا تيرينجانو"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Alor Setar",
		  "CITY_B_DESC": "الور ستار"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Bmalkah",
		  "CITY_B_DESC": "بمالكة"
        },
        {
          "CITY_CODE": "6",
          "CITY_S_DESC": "Penang",
		  "CITY_B_DESC": "بينانغ"
        },
        {
          "CITY_CODE": "7",
          "CITY_S_DESC": "Kuching",
		  "CITY_B_DESC": "كوتشينغ"
        },

      ],
      "94": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "SINGAPORE",
		  "CITY_B_DESC": "سنغافورة"
        },

      ],
      "95": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Jakarta",
		  "CITY_B_DESC": "جاكرتا"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Demak",
		  "CITY_B_DESC": "ديماك"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Banjarmas",
		  "CITY_B_DESC": "بانجارما"
        },

        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "Dumai",
		  "CITY_B_DESC": "دوماي"
        },

      ],
      "96": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Bankok",
		  "CITY_B_DESC": "بانكوك"
        },

      ],
      "97": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "WELLINGTON",
		  "CITY_B_DESC": "ولنجتون"
        },
        {
          "CITY_CODE": "2",
          "CITY_S_DESC": "Auckland",
		  "CITY_B_DESC": "أوكلاند"
        },
        {
          "CITY_CODE": "3",
          "CITY_S_DESC": "Christchurch",
		  "CITY_B_DESC": "كرايست تشيرش"
        },
        {
          "CITY_CODE": "4",
          "CITY_S_DESC": "Manukau",
		  "CITY_B_DESC": "مانوكاو"
        },
        {
          "CITY_CODE": "5",
          "CITY_S_DESC": "North Shore",
		  "CITY_B_DESC": "الشاطئ الشمالي"
        },

      ],
      "98": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Prague",
		  "CITY_B_DESC": "براغ"
        },

      ],
      "102": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Jerusalem",
		  "CITY_B_DESC": "بيت المقدس"
        },

      ],
      "103": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Addis Ababa",
		  "CITY_B_DESC": "اديس ابابا"
        },

      ],
      "104": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Budapest",
		  "CITY_B_DESC": "بودابست"
        },

      ],
      "106": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Oslo",
		  "CITY_B_DESC": "أوسلو"
        },

      ],
      "107": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Warsaw",
		  "CITY_B_DESC": "وارسو"
        },

      ],
      "110": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Dhaka",
		  "CITY_B_DESC": "دكا"
        },

      ],
      "112": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Prishtina",
		  "CITY_B_DESC": "بريشتينا"
        },

      ],
      "117": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Port Louis",
		  "CITY_B_DESC": "بورت لويس"
        },

      ],
      "118": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Quito",
		  "CITY_B_DESC": "كيتو"
        },

      ],
      "119": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Havana",
		  "CITY_B_DESC": "هافانا"
        },

      ],
      "120": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Naypyitaw",
		  "CITY_B_DESC": "نايبيتاو"
        },

      ],
      "122": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Hanoi",
		  "CITY_B_DESC": "هانوي"
        },

      ],
      "124": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Lima",
		  "CITY_B_DESC": "ليما"
        },

      ],
      "126": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Panama",
		  "CITY_B_DESC": "بنما"
        },

      ],
      "127": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Luanda",
		  "CITY_B_DESC": "لواندا"
        },

      ],
      "128": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Bishkek",
		  "CITY_B_DESC": "بيشكيك"
        },

      ],
      "131": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Windhoek",
		  "CITY_B_DESC": "ويندهوك"
        },

      ],
      "133": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Managua",
		  "CITY_B_DESC": "ماناغوا"
        },

      ],
      "143": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Minsk",
		  "CITY_B_DESC": "مينسك"
        },

      ],
      "146": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Louis Pasteur",
		  "CITY_B_DESC": "لويس باستور"
        },

      ],
      "147": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Baku",
		  "CITY_B_DESC": "باكو"
        },

      ],
      "148": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Yamoussoukro",
		  "CITY_B_DESC": "ياموسوكرو"
        },

      ],
      "149": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Roseau",
		  "CITY_B_DESC": "روسو"
        },

      ],
      "150": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Georgetown",
		  "CITY_B_DESC": "جورج تاون"
        },

      ],
      "151": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Road Town",
		  "CITY_B_DESC": "رود تاون"
        },

      ],
      "152": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Valletta",
		  "CITY_B_DESC": "فاليتا"
        },

      ],
      "155": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Vilnius",
		  "CITY_B_DESC": "فيلنيوس"
        },

      ],
      "156": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Riga",
		  "CITY_B_DESC": "ريغا"
        },

      ],
      "157": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Bratislava",
		  "CITY_B_DESC": "براتيسلافا"
        },

      ],
      "158": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Saint Kitts",
		  "CITY_B_DESC": "سانت كيتس"
        },

      ],
      "159": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Ljubljana",
		  "CITY_B_DESC": "ليوبليانا"
        },

      ],
      "160": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Vaduz",
		  "CITY_B_DESC": "فادوز"
        },

      ],
      "170": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Tallinn",
		  "CITY_B_DESC": "تالين"
        },

      ],
      "171": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Nassau",
          "CITY_B_DESC": "ناسو"
        },

      ],
      "172": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Libreville",
          "CITY_B_DESC": "ليبرفيل"
        },

      ],
      "173": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Tbilisi",
          "CITY_B_DESC": "تبليسي"
        },

      ],
      "174": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Podgorica",
		  "CITY_B_DESC": "بودغوريتشا"
        },

      ],
      "175": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Castries",
		  "CITY_B_DESC": "كاستريس"
        },

      ],
      "176": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Malé",
		  "CITY_B_DESC": "ميل"
        },

      ],
      "177": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Oranjestad",
		  "CITY_B_DESC": "أورانجيستاد"
        },

      ],
      "178": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Aruba",
		  "CITY_B_DESC": "أروبا"
        },

      ],
      "179": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "Saint Martin",
		  "CITY_B_DESC": "سانت مارتن"
        },

      ],
      "180": [
        {
          "CITY_CODE": "1",
          "CITY_S_DESC": "San Juan",
		  "CITY_B_DESC": "سان خوان"
        },

      ]
	},

  "Branch":[
    {
      "BRANCH_CODE":"1",
      "BRANCH_NAME":"SHMIESANI",
      "INITIAL":"SH"
    },
    {
      "BRANCH_CODE":"2",
      "BRANCH_NAME":"AMMAN",
      "INITIAL":"AM"
    },
    {
      "BRANCH_CODE":"4",
      "BRANCH_NAME":"KARAK",
      "INITIAL":"KA"
    },
    {
      "BRANCH_CODE":"7",
      "BRANCH_NAME":"IRBID",
      "INITIAL":"IR"
    },
    {
      "BRANCH_CODE":"8",
      "BRANCH_NAME":"MA'AN",
      "INITIAL":"MA"
    },
    {
      "BRANCH_CODE":"9",
      "BRANCH_NAME":"RAMTHA",
      "INITIAL":"RA"
    },
    {
      "BRANCH_CODE":"10",
      "BRANCH_NAME":"SALT",
      "INITIAL":"SA"
    },
    {
      "BRANCH_CODE":"11",
      "BRANCH_NAME":"JERASH",
      "INITIAL":"JE"
    },
    {
      "BRANCH_CODE":"12",
      "BRANCH_NAME":"MAHATA",
      "INITIAL":"MA"
    },
    {
      "BRANCH_CODE":"13",
      "BRANCH_NAME":"MARKA",
      "INITIAL":"MA"
    },
    {
      "BRANCH_CODE":"14",
      "BRANCH_NAME":"AJLOUN",
      "INITIAL":"AJ"
    },
    {
      "BRANCH_CODE":"15",
      "BRANCH_NAME":"JABAL AL-HUSSIEN",
      "INITIAL":"JA"
    },
    {
      "BRANCH_CODE":"16",
      "BRANCH_NAME":"COMMERCIAL MARKET",
      "INITIAL":"CM"
    },
    {
      "BRANCH_CODE":"17",
      "BRANCH_NAME":"WADI AL-SIR",
      "INITIAL":"WA"
    },
    {
      "BRANCH_CODE":"18",
      "BRANCH_NAME":"DIR ABI SAI'ED",
      "INITIAL":"DI"
    },
    {
      "BRANCH_CODE":"19",
      "BRANCH_NAME":"SWAILEH",
      "INITIAL":"SW"
    },
    {
      "BRANCH_CODE":"20",
      "BRANCH_NAME":"MADABA",
      "INITIAL":"MA"
    },
    {
      "BRANCH_CODE":"21",
      "BRANCH_NAME":"FIRST CIRCLE",
      "INITIAL":"FC"
    },
    {
      "BRANCH_CODE":"22",
      "BRANCH_NAME":"AIRPORT",
      "INITIAL":"AI"
    },
    {
      "BRANCH_CODE":"23",
      "BRANCH_NAME":"HAKAMA ST.",
      "INITIAL":"HS"
    },
    {
      "BRANCH_CODE":"24",
      "BRANCH_NAME":"ZARQA'A",
      "INITIAL":"ZA"
    },
    {
      "BRANCH_CODE":"25",
      "BRANCH_NAME":"AL-SHOUNA AL-SHMALYA",
      "INITIAL":"AL"
    },
    {
      "BRANCH_CODE":"26",
      "BRANCH_NAME":"KUFRANJA",
      "INITIAL":"KU"
    },
    {
      "BRANCH_CODE":"28",
      "BRANCH_NAME":"3RD CIRCLE",
      "INITIAL":"3C"
    },
    {
      "BRANCH_CODE":"29",
      "BRANCH_NAME":"QWAISMEH",
      "INITIAL":"QW"
    },
    {
      "BRANCH_CODE":"30",
      "BRANCH_NAME":"NUZHA",
      "INITIAL":"NU"
    },
    {
      "BRANCH_CODE":"31",
      "BRANCH_NAME":"GARDENS",
      "INITIAL":"GA"
    },
    {
      "BRANCH_CODE":"32",
      "BRANCH_NAME":"AQABA",
      "INITIAL":"AQ"
    },
    {
      "BRANCH_CODE":"33",
      "BRANCH_NAME":"BAYADER",
      "INITIAL":"BA"
    },
    {
      "BRANCH_CODE":"34",
      "BRANCH_NAME":"MAFRAQ",
      "INITIAL":"MA"
    },
    {
      "BRANCH_CODE":"36",
      "BRANCH_NAME":"AL-AZRAQ AL-SHAMALI",
      "INITIAL":"AL"
    },
    {
      "BRANCH_CODE":"37",
      "BRANCH_NAME":"ALLWAIBDEH",
      "INITIAL":"AL"
    },
    {
      "BRANCH_CODE":"40",
      "BRANCH_NAME":"TARIQ",
      "INITIAL":"TA"
    },
    {
      "BRANCH_CODE":"41",
      "BRANCH_NAME":"ZARQA'A FREE ZONE",
      "INITIAL":"ZA"
    },
    {
      "BRANCH_CODE":"42",
      "BRANCH_NAME":"MARJ AL-HAMAM",
      "INITIAL":"MA"
    },
    {
      "BRANCH_CODE":"46",
      "BRANCH_NAME":"AL-JEEZA",
      "INITIAL":"AJ"
    },
    {
      "BRANCH_CODE":"50",
      "BRANCH_NAME":"RAS ALAIN",
      "INITIAL":"RA"
    },
    {
      "BRANCH_CODE":"52",
      "BRANCH_NAME":"SWIEFYEH",
      "INITIAL":"SW"
    },
    {
      "BRANCH_CODE":"53",
      "BRANCH_NAME":"WEHDAT",
      "INITIAL":"WE"
    },
    {
      "BRANCH_CODE":"54",
      "BRANCH_NAME":"RUSAIFA",
      "INITIAL":"RU"
    },
    {
      "BRANCH_CODE":"56",
      "BRANCH_NAME":"MECCA ST.",
      "INITIAL":"MS"
    },
    {
      "BRANCH_CODE":"57",
      "BRANCH_NAME":"30 ST.",
      "INITIAL":"3S"
    },
    {
      "BRANCH_CODE":"60",
      "BRANCH_NAME":"JUBAIHA",
      "INITIAL":"JU"
    },
    {
      "BRANCH_CODE":"62",
      "BRANCH_NAME":"JORDANIAN UNI",
      "INITIAL":"JU"
    },
    {
      "BRANCH_CODE":"63",
      "BRANCH_NAME":"KHALDA",
      "INITIAL":"KH"
    },
    {
      "BRANCH_CODE":"64",
      "BRANCH_NAME":"ABU ALANDA",
      "INITIAL":"AA"
    },
    {
      "BRANCH_CODE":"65",
      "BRANCH_NAME":"CITY MALL",
      "INITIAL":"CM"
    },
    {
      "BRANCH_CODE":"66",
      "BRANCH_NAME":"AL-MADINA ST.(1)",
      "INITIAL":"AM"
    },
    {
      "BRANCH_CODE":"67",
      "BRANCH_NAME":"AL-NASER",
      "INITIAL":"AN"
    },
    {
      "BRANCH_CODE":"68",
      "BRANCH_NAME":"AL-TURRA",
      "INITIAL":"AT"
    },
    {
      "BRANCH_CODE":"69",
      "BRANCH_NAME":"FUHAIS",
      "INITIAL":"FU"
    },
    {
      "BRANCH_CODE":"70",
      "BRANCH_NAME":"AL-HUSUN",
      "INITIAL":"AH"
    },
    {
      "BRANCH_CODE":"72",
      "BRANCH_NAME":"AL-BYADER INDUSTRY ZONE",
      "INITIAL":"AB"
    },
    {
      "BRANCH_CODE":"74",
      "BRANCH_NAME":"AIDOON",
      "INITIAL":"AI"
    },
    {
      "BRANCH_CODE":"75",
      "BRANCH_NAME":"ABU NSAIR",
      "INITIAL":"AN"
    },
    {
      "BRANCH_CODE":"76",
      "BRANCH_NAME":"DAHYAT AL-YASMEEN",
      "INITIAL":"DA"
    },
    {
      "BRANCH_CODE":"77",
      "BRANCH_NAME":"FAISAL ST.",
      "INITIAL":"FS"
    },
    {
      "BRANCH_CODE":"78",
      "BRANCH_NAME":"NEW ZARQA",
      "INITIAL":"NZ"
    },
    {
      "BRANCH_CODE":"79",
      "BRANCH_NAME":"AL-KHALDI",
      "INITIAL":"AK"
    },
    {
      "BRANCH_CODE":"80",
      "BRANCH_NAME":"AL-RABIA",
      "INITIAL":"AR"
    },
    {
      "BRANCH_CODE":"81",
      "BRANCH_NAME":"ABDOUN",
      "INITIAL":"AB"
    },
    {
      "BRANCH_CODE":"82",
      "BRANCH_NAME":"AL-RAWNAQ",
      "INITIAL":"AR"
    },
    {
      "BRANCH_CODE":"83",
      "BRANCH_NAME":"AL-HURRYA ST.",
      "INITIAL":"AH"
    },
    {
      "BRANCH_CODE":"85",
      "BRANCH_NAME":"SPORT CITY",
      "INITIAL":"SC"
    },
    {
      "BRANCH_CODE":"86",
      "BRANCH_NAME":"TAJ MALL",
      "INITIAL":"TM"
    },
    {
      "BRANCH_CODE":"87",
      "BRANCH_NAME":"AL-HASHMI AL-SHAMALI",
      "INITIAL":"AH"
    },
    {
      "BRANCH_CODE":"88",
      "BRANCH_NAME":"AL-JABAL AL-SHAMALI",
      "INITIAL":"AJ"
    },
    {
      "BRANCH_CODE":"89",
      "BRANCH_NAME":"DORRAT KHALDA",
      "INITIAL":"DK"
    },
    {
      "BRANCH_CODE":"101",
      "BRANCH_NAME":"AL-MADINA ST.(2)/TILA AL-ALI",
      "INITIAL":"AM"
    },
    {
      "BRANCH_CODE":"102",
      "BRANCH_NAME":"SAHAB",
      "INITIAL":"SA"
    },
    {
      "BRANCH_CODE":"103",
      "BRANCH_NAME":"ABDALI MALL",
      "INITIAL":"AM"
    },
    {
      "BRANCH_CODE":"104",
      "BRANCH_NAME":"AL HASSAN INDUSTRIAL CITY/ IRBID",
      "INITIAL":"AH"
    },
    {
      "BRANCH_CODE":"105",
      "BRANCH_NAME":"UM UTHAINA",
      "INITIAL":"UU"
    }
  ],

  "BankDetail":[
        {
            "SwiftCode": "9960001",
            "BankName": "Central Bank of Jordan",
            "BankAdd1": "AMMAN - JORDAN",
            "BankAdd2": "AMMAN - JORDAN",
            "BankAdd3": "",
            "INITIAL":"CB"
        }, {
            "SwiftCode": "9970311",
            "BankName": "Arab Bank Ltd",
            "BankAdd1": "AMMAN",
            "BankAdd2": "AMMAN",
            "BankAdd3": "",
            "INITIAL":"AB"
        }, {
            "SwiftCode": "9970136",
            "BankName": "CITIBANK",
            "BankAdd1": "CITIBANK JORDAN",
            "BankAdd2": "CITIBANK JORDAN",
            "BankAdd3": "",
            "INITIAL":"CJ"
        }, {
            "SwiftCode": "9970312",
            "BankName": "Jordan Kuwait Bank",
            "BankAdd1": "AMMAN",
            "BankAdd2": "AMMAN",
            "BankAdd3": "",
            "INITIAL":"JK"
        }, {
            "SwiftCode": "9970313",
            "BankName": "Jordan Ahli  Bank",
            "BankAdd1": "AMMAN",
            "BankAdd2": "AMMAN",
            "BankAdd3": "",
            "INITIAL":"JA"
        }, {
            "SwiftCode": "9970316",
            "BankName": "Egyptian Arab Land Bank",
            "BankAdd1": "AMMAN",
            "BankAdd2": "AMMAN",
            "BankAdd3": "",
            "INITIAL": "EA"
        }, {
            "SwiftCode": "9970317",
            "BankName": "CAAB BANK",
            "BankAdd1": "AMMAN",
            "BankAdd2": "AMMAN",
            "BankAdd3": "",
            "INITIAL":"CB"
        }, {
            "SwiftCode": "9970751",
            "BankName": "National Bank of Kuwait - Jordan",
            "BankAdd1": "",
            "BankAdd2": "",
            "BankAdd3": "",
            "INITIAL":"NB"
        }, {
            "SwiftCode": "9970713",
            "BankName": "Islamic Intl Arab Bank",
            "BankAdd1": "",
            "BankAdd2": "",
            "BankAdd3": "",
            "INITIAL":"II"
        }, {
            "SwiftCode": "9970352",
            "BankName": "Jordan Islamic Bank",
            "BankAdd1": "AMMAN/SHMESSINI/",
            "BankAdd2": "AMMAN/SHMESSINI/",
            "BankAdd3": "",
            "INITIAL":"JI"
        }, {
            "SwiftCode": "9970325",
            "BankName": "Arab Banking Corporation (Jordan)",
            "BankAdd1": "AMMAN",
            "BankAdd2": "AMMAN",
            "BankAdd3": "",
            "INITIAL":"AB"
        }, {
            "SwiftCode": "9970347",
            "BankName": "Capital Bank of Jordan",
            "BankAdd1": "AMMAN",
            "BankAdd2": "AMMAN",
            "BankAdd3": "",
            "INITIAL":"CB"
        }, {
            "SwiftCode": "9970476",
            "BankName": "Jordan Investment and Finance Bank",
            "BankAdd1": "AMMAN",
            "BankAdd2": "AMMAN",
            "BankAdd3": "",
            "INITIAL":"JI"
        }, {
            "SwiftCode": "9970318",
            "BankName": "Standard Chartered Bank",
            "BankAdd1": "AMMAN",
            "BankAdd2": "AMMAN",
            "BankAdd3": "",
            "INITIAL":"SC"
        }, {
            "SwiftCode": "9970320",
            "BankName": "Rafidain Bank (Jordan)",
            "BankAdd1": "AMMAN- P.O BOX 1194",
            "BankAdd2": "AMMAN- P.O BOX 1194",
            "BankAdd3": "",
            "INITIAL":"RB"
        }, {
            "SwiftCode": "9970322",
            "BankName": "Arab Jordan Investment Bank",
            "BankAdd1": "AMMAN",
            "BankAdd2": "AMMAN",
            "BankAdd3": "",
            "INITIAL":"AJ"
        }, {
            "SwiftCode": "9970753",
            "BankName": "BANK AUDI S.A.L",
            "BankAdd1": "AUDI BANK",
            "BankAdd2": "AUDI BANK",
            "BankAdd3": "",
            "INITIAL":"BA"
        }, {
            "SwiftCode": "9970767",
            "BankName": "BLOM BANK",
            "BankAdd1": "",
            "BankAdd2": "",
            "BankAdd3": "",
            "INITIAL":"BB"
        }, {
            "SwiftCode": "9970314",
            "BankName": "The Housing Bank for Trade and Finance",
            "BankAdd1": "",
            "BankAdd2": "",
            "BankAdd3": "",
            "INITIAL":"TH"
        }, {
            "SwiftCode": "9970903",
            "BankName": "Safwa Islamic Bank",
            "BankAdd1": "",
            "BankAdd2": "",
            "BankAdd3": "",
            "INITIAL":"SI"
        },
/*		{
            "SwiftCode": "9970912",
            "BankName": "National Bank of Abu Dhabi",
            "BankAdd1": "",
            "BankAdd2": "",
            "BankAdd3": "",
            "INITIAL":"NB"
        },*/
		{
            "SwiftCode": "9971028",
            "BankName": "AL RAJHI BANK",
            "BankAdd1": "",
            "BankAdd2": "",
            "BankAdd3": "",
            "INITIAL":"AR"
        }, {
            "SwiftCode": "9950097",
            "BankName": "Bank of Jordan",
            "BankAdd1": "",
            "BankAdd2": "",
            "BankAdd3": "",
            "INITIAL":"BO"
        }, {
            "SwiftCode": "9970350",
            "BankName": "Jordan Commercial Bank",
            "BankAdd1": "JORDAN COMMIRCALL BANK",
            "BankAdd2": "JORDAN COMMIRCALL BANK",
            "BankAdd3": "",
            "INITIAL":"JC"
        }, {
            "SwiftCode": "9970354",
            "BankName": "Etihad Bank",
            "BankAdd1": "BANK ALETIHAD",
            "BankAdd2": "BANK ALETIHAD",
            "BankAdd3": "",
            "INITIAL":"UB"
        }, {
            "SwiftCode": "9970328",
            "BankName": "Societe Generale de Banque-Jordanie",
            "BankAdd1": "AMMAN",
            "BankAdd2": "AMMAN",
            "BankAdd3": "",
            "INITIAL":"SG"
        }
    ]
};

function get_BRANCH_DETAILS(branch_CODE, branch_Data){
  try{
    kony.print("branch_CODE ::"+branch_CODE+" :: branch_Data :: "+JSON.stringify(branch_Data));
    for(var i in branch_Data){
    	if(branch_Data[i].BRANCH_CODE === branch_CODE){
          return branch_Data[i];
        }
    }
  }catch(e){
  	kony.print("Exception_get_BRANCH_DETAILS ::"+e);
  }
}

function get_COUNTRY_DETAILS(country_CODE){
  try{
 	for(var i in kony.boj.detailsForBene.Country){
    	if(kony.boj.detailsForBene.Country[i].CTRY_CODE === country_CODE){
          return kony.boj.detailsForBene.Country[i];
        }
    }
  }catch(e){
    kony.print("Exception_get_COUNTRY_DETAILS ::"+e);
  }
}

function get_COUNTRY_LIST_WITHOUT_SPECIFIC_COUNTRY(list){
	try{
    	var temp = [];
        for(var j in kony.boj.detailsForBene.Country){
        	for(var i in list){
              kony.print("hassan country::"+kony.boj.detailsForBene.Country[j].CTRY_S_DESC);
              kony.print("hassan country222 ::"+list[i].countryName.toUpperCase());
            	if(kony.boj.detailsForBene.Country[j].CTRY_S_DESC !== list[i].countryName.toUpperCase())
                  temp.push(kony.boj.detailsForBene.Country[j]);
            }
        }
    	return temp;
    }catch(e){
    	kony.print("Exception_get_COUNTRY_LIST_WITHOUT_SPECIFIC_COUNTRY ::"+e);
    }
}