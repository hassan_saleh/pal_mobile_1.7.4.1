glbPeriod="";
glbPeriodCode="";
var interestPay="";
var matInst="";
var glbDepSelects={};
var gblopenTermDepositeMI;
var selectedBalAccount="";
function callRateService()
{
  var actpType="";
  var effDate="";
  if (!isEmpty(frmOpenTermDeposit.lblDepositStartDate.text) && frmOpenTermDeposit.txtDepositAmount.text !== 0 &&
      frmOpenTermDeposit.txtDepositAmount.text!== null && !isEmpty(frmOpenTermDeposit.txtDepositAmount.text))
  {  
      var date=frmOpenTermDeposit.lblDepositStartDate.text.split("/");
      //effDate = date[2]+"-"+ (parseInt(date[1])<10?"0"+date[1]:date[1]) +"-"+ (parseInt(date[0])<10?"0"+date[0]:date[0]);
      effDate = date[2]+"-"+ date[1] +"-"+ date[0];

      if (frmOpenTermDeposit.lblDepositCurrency.text === "JOD")
        actpType=317;
      else
        actpType=318;
   
      var queryParams = 
          {
            "currCode":frmOpenTermDeposit.lblCurrencyCode.text,
            "valueDate": effDate,
            "accountTYpe":actpType,
            "amount":frmOpenTermDeposit.txtDepositAmount.text,
            "periodCode":glbDepSelects.PeriodCode,
            "period":glbDepSelects.Period
          };
 kony.print("rate queryParams ::"+JSON.stringify(queryParams));
      var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprGetFtdIntRate");

        appMFConfiguration.invokeOperation("prGetFtdIntRate", {},queryParams,
                                           function(res){
          kony.print("success response ::"+JSON.stringify(res));
          if(res.intRate !== undefined && res.intRate !== null && !isEmpty(res.intRate) && (res.errorCode === "00000")){
              frmOpenTermDeposit.lblInterestRate.text = res.intRate;
          }else{
            frmOpenTermDeposit.lblInterestRate.text =0;
          }
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();},function(err){kony.print("error in service ::"+JSON.stringify(err));kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();});
  }
}

function funSelectTener(tener)
{
  resetTenerButtons();
  var depDate = new Date();
  if (tener==="1Month")
    {
      frmOpenTermDeposit.btnTener1Month.skin="sknOrangeBGRNDBOJ";
      //glbPeriodCode=1;
      //glbPeriod=2;
      glbDepSelects.PeriodCode=1;
      glbDepSelects.Period=2;
      glbDepSelects.tenerDesc=geti18Value("i18n.termDepodit.tener1Month");
     // depDate.setMonth(parseInt(depDate.getMonth())+2);
    }
  else if (tener==="3Months")
    {
      frmOpenTermDeposit.btnTener3Months.skin="sknOrangeBGRNDBOJ";
      //glbPeriodCode=3;
      //glbPeriod=2;
      glbDepSelects.PeriodCode=3;
      glbDepSelects.Period=2;
      glbDepSelects.tenerDesc=geti18Value("i18n.termDepodit.tener3Month");
    //  depDate.setMonth(parseInt(depDate.getMonth())+4);
    }
  else if (tener==="6Months")
    {
      frmOpenTermDeposit.btnTener6Months.skin="sknOrangeBGRNDBOJ";
      //glbPeriodCode=6;
      //glbPeriod=2;
      glbDepSelects.PeriodCode=6;
      glbDepSelects.Period=2;
      glbDepSelects.tenerDesc=geti18Value("i18n.termDepodit.tener6Month");
    //  depDate.setMonth(parseInt(depDate.getMonth())+7);

    }
  else if (tener==="1Year")
    {
      frmOpenTermDeposit.btnTener1Year.skin="sknOrangeBGRNDBOJ";
      //glbPeriodCode=1;
      //glbPeriod=3;
      glbDepSelects.PeriodCode=1;
      glbDepSelects.Period=3;
      glbDepSelects.tenerDesc=geti18Value("i18n.termDepodit.tener1Year");
    //  depDate.setMonth(parseInt(depDate.getMonth())+12);
  //    depDate.setYear(parseInt(depDate.getFullYear())+1);
    }
  
  //frmOpenTermDeposit.lblDepositStartDate.text = depDate.getDate() + "/" + (parseInt(depDate.getMonth())+1) + "/" + depDate.getFullYear();
  
  
  callRateService();
  checkNextOpenDeposite();
}

function funMatInstructions(instruction)
{
  resetMatInstButtons();
  if (instruction===geti18Value("i18n.termDeposit.renWithInterest"))
    {
      glbDepSelects.matInstDesc=geti18Value("i18n.termDeposit.renWithInterest");
    }
  else if (instruction===geti18Value("i18n.termDeposit.renWithOutInterest"))
    {
      glbDepSelects.matInstDesc=geti18Value("i18n.termDeposit.renWithOutInterest");
    }
  else if (instruction===geti18Value("i18n.termDeposit.close"))
    {
      glbDepSelects.matInstDesc=geti18Value("i18n.termDeposit.close");
    }
  //matInst=instruction;
  
  checkNextOpenDeposite();
}


function funInterestPayable(type)
{
  resetInterestPayButtons();
  if (type==="Monthly")
    {
  	  frmOpenTermDeposit.btnMonthly.skin="sknOrangeBGRNDBOJ";
      glbDepSelects.interestPayDesc=geti18Value("i18n.termDeposit.intMonthly");
    }
  else if (type==="OnMaturity")
    {
      frmOpenTermDeposit.btnOnMaturity.skin="sknOrangeBGRNDBOJ";
      glbDepSelects.interestPayDesc=geti18Value("i18n.termDeposit.inOnMaturity");
    }
  //interestPay=type;
  //glbDepSelects.interestPayDesc=type;
  checkNextOpenDeposite();
}


function checkNextOpenDeposite()
{
  kony.print("glbDepSelects.period "+glbDepSelects.Period);
  var balanceAmount=parseFloat(selectedBalAccount)-parseFloat(frmOpenTermDeposit.txtDepositAmount.text)
  if (frmOpenTermDeposit.flxBorderDepositAmount.skin==="skntextFieldDividerGreen" &&
     frmOpenTermDeposit.lblDepositCurrency.text !==null && frmOpenTermDeposit.lblInterestRate.text !== 0.000 &&
     !isEmpty(frmOpenTermDeposit.lblInterestRate.text) &&
     (glbDepSelects.Period!== null && glbDepSelects.Period!==undefined && !isEmpty(glbDepSelects.Period)) && 
     (glbDepSelects.interestPayDesc!== null && glbDepSelects.interestPayDesc!==undefined && !isEmpty(glbDepSelects.interestPayDesc))&&
     (glbDepSelects.matInstDesc!== null && glbDepSelects.matInstDesc!==undefined && !isEmpty(glbDepSelects.matInstDesc))&&
     balanceAmount>0)
  {
     frmOpenTermDeposit.btnNextDeposite.setEnabled(true);
     frmOpenTermDeposit.btnNextDeposite.skin="jomopaynextEnabled";
  }
  else
  {
     frmOpenTermDeposit.btnNextDeposite.setEnabled(false);
     frmOpenTermDeposit.btnNextDeposite.skin="jomopaynextDisabled";
  }
}

function resetTenerButtons()
{
  frmOpenTermDeposit.btnTener1Month.skin="slButtonBlueFocus";
  frmOpenTermDeposit.btnTener3Months.skin="slButtonBlueFocus";
  frmOpenTermDeposit.btnTener6Months.skin="slButtonBlueFocus";
  frmOpenTermDeposit.btnTener1Year.skin="slButtonBlueFocus";
}

function resetMatInstButtons()
{
  glbDepSelects.matInstDesc="";
  frmOpenTermDeposit.btnWithInterest.skin="slButtonBlueFocus";
  frmOpenTermDeposit.btnWithoutInterest.skin="slButtonBlueFocus";
  frmOpenTermDeposit.btnClose.skin="slButtonBlueFocus";
}

function resetInterestPayButtons()
{
  glbDepSelects.interestPayDesc="";
  frmOpenTermDeposit.btnMonthly.skin="slButtonBlueFocus";
  frmOpenTermDeposit.btnOnMaturity.skin="slButtonBlueFocus";
}



function goToConfirmDeposite()
{
  frmOpenTermDeposit.lblConfirmAccount.text=frmOpenTermDeposit.lblFundDeductionAccountNumber.text;
  frmOpenTermDeposit.lblConfirmDepositeAmount.text=formatAmountwithcomma(frmOpenTermDeposit.txtDepositAmount.text, 3);
  frmOpenTermDeposit.lblConfirmDepositeAmountCurr.text=frmOpenTermDeposit.lblDepositCurrency.text;
  frmOpenTermDeposit.lblConfirmTener.text=glbDepSelects.tenerDesc;
  frmOpenTermDeposit.lblConfirmStartDepositeDate.text=frmOpenTermDeposit.lblDepositStartDate.text;
  frmOpenTermDeposit.lblConfirmInterestRate.text=frmOpenTermDeposit.lblInterestRate.text+"%";
  frmOpenTermDeposit.lblConfirmMaturityInstruction.text=glbDepSelects.matInstDesc;
  frmOpenTermDeposit.lblConfirmInterestPayable.text=glbDepSelects.interestPayDesc;
  frmOpenTermDeposit.flxConfirmDeposite.setVisibility(true);
  frmOpenTermDeposit.flxDepositSelection.setVisibility(false);
  frmOpenTermDeposit.btnNextDeposite.setVisibility(false);
  
}



function serv_createDeposite()
{
  var actpType=""
  var currDate= new Date();
  var valueDate;
  var valueDate1;
  if (frmOpenTermDeposit.lblDepositCurrency.text === "JOD")
    actpType=317;
  else
    actpType=318;
  
  currDat1 = currDate.getFullYear() + "-" +((currDate.getMonth()+1)<10?"0"+(currDate.getMonth()+1):(currDate.getMonth()+1))+"-"+(currDate.getDate()<10?"0"+currDate.getDate():currDate.getDate());
  
  valueDate=frmOpenTermDeposit.lblDepositStartDate.text.split("/");
  valueDate1 = valueDate[2]+"-"+ valueDate[1] +"-"+ valueDate[0];
  
  var queryParams = 
      {
        "brchCode":1,
        "oprDate": currDat1,
        "valueDate":valueDate1,
        "currCode":frmOpenTermDeposit.lblCurrencyCode.text,
        "actpType":actpType,
        "custID":custid,
        "amount":frmOpenTermDeposit.txtDepositAmount.text,
        "perdCode":glbDepSelects.Period,
        "period":glbDepSelects.PeriodCode,
        "intPeriodCode":2,
        "intPeriod":1,
        "intRate":frmOpenTermDeposit.lblInterestRate.text,
        "intMrg":0,
        "fundBrch":1,
        "fundCacc":frmOpenTermDeposit.lblFundDeductionAccountNumber.text,
        "profBrch":1,
        "profCacc":frmOpenTermDeposit.lblFundDeductionAccountNumber.text,
       /* "parentBrch":1,
        "parentBrchRef":1,*/
        "renewalInd":1,
        "p_channel":"MOBILE",
        "channelUser":"BOJMOB"
      };
kony.print("hassan queryParams-->"+ JSON.stringify(queryParams) );
      var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprCreateAddFtd");
      appMFConfiguration.invokeOperation("prCreateAddFtd", {},queryParams,serv_createDeposite_success,function(err){kony.print("error in service ::"+JSON.stringify(err));kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();});
}


function serv_createDeposite_success(res)
{
    kony.print("hassan success ::"+JSON.stringify(res));
  
  if(res.errorCode === "00000" && (res.dealBrchRef !== null && !isEmpty(res.dealBrchRef)))
    {
       kony.boj.populateSuccessScreen("success.png", 
                                      geti18nkey("i18n.termDeposit.createSuccess"), 
                                      "", 
                                      geti18nkey("i18n.common.CgtAD"), 
                                      geti18nkey ("i18n.common.AccountDashboard"), geti18Value("i18n.termDeposit.toDepositeScreen"), "openDeposite");
    }
  else
    {
       var Message = getErrorMessage(response.errorCode);
       customAlertPopup(geti18Value("i18n.Bene.Failed"), Message,popupCommonAlertDimiss,"");
    }
}



function setDataMaturityInstruction(data){
  frmOpenTermDeposit.lblMaturityInstruction.text = data.Maturity;
  frmOpenTermDeposit.lblMaturityInstructionTitle.setVisibility(true);
  frmOpenTermDeposit.lblMaturityInstruction.skin="lblAmountCurrency";
  funMatInstructions(data.Maturity);
  gblopenTermDepositeMI=false;
  frmOpenTermDeposit.show();
}



function callAccountsDeposite(val){
  
  //1960 fix
  var fromAccounts="";
  var toAccounts="";
  var accountsData="";
  kony.print("accountsData"+kony.retailBanking.globalData.accountsDashboardData.accountsData);
  if(kony.boj.siri === "fromSiri")
  {
     accountsData=kony.retailBanking.globalData.accountsDashboardData.accountsData;
  }
  else
  {
     fromAccounts = kony.retailBanking.globalData.accountsDashboardData.fromAccounts;
     toAccounts	= kony.retailBanking.globalData.accountsDashboardData.toAccounts.slice(0);
  }
  kony.print("fromAccounts :: "+fromAccounts);
  kony.print("toAccounts :: "+toAccounts);

  if(val === 1){
    if(kony.newCARD.applyCardsOption === true || kony.application.getCurrentForm().id === "frmCardLinkedAccounts" || kony.boj.selectedBillerType === "PrePaid"  || kony.boj.siri ==="fromSiri"){
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.common.accountNumber");
    }else{
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyFrom");
    }
    kony.store.setItem("toval",1);
    if((kony.application.getCurrentForm().id === "frmNewBillKA" && frmNewBillKA.btnBillsPayCards.text == "t") ||
       (kony.application.getCurrentForm().id === "frmBills" && frmBills.btnBillsPayCards.text == "t") ||
       (kony.application.getCurrentForm().id === "frmNewBillDetails" && frmNewBillDetails.btnBillsPayCards.text == "t") ){
      if(kony.retailBanking.globalData.prCreditCardPayList.length > 0){
        var tempdata = [];
        var cardData = kony.retailBanking.globalData.prCreditCardPayList;
        var cc = false;
        for(var i=0;i<cardData.length;i++){
          kony.print("cardTypeFlag ::"+cardData[i].cardTypeFlag);
          if(cardData[i].cardTypeFlag === "C"){
            cc = true;
            tempdata.push(cardData[i]);
          }
        }
        if(cc){
          accountsScreenPreshow(tempdata);
          frmAccountDetailsScreen.show();
        }
        else
          customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.nocards"),popupCommonAlertDimiss,"");
      }else
        customVerb_CARDSDETAILS();
    }else{
      if(kony.application.getCurrentForm().id === "frmCardLinkedAccounts"){
        var data = frmCardLinkedAccounts.segCardLinkedAccounts.data;
        var temp = [];
        for(var i in data){
          if(data[i].flxLinkedAccountsEnable.isVisible === true){
            for(var j in fromAccounts){
              if(fromAccounts[j].accountID === data[i].lblAccountNumber.text){
                temp.push(fromAccounts[j]);
                break;
              }
            }
          }
        }
        accountsScreenPreshow(temp);
      }
      //added for 1960
       else{ 
        if(kony.boj.siri ==="fromSiri")
          {
            kony.print("loding accounts for siri");
            accountsScreenPreshow(accountsData);
          }
        else
          {
            accountsScreenPreshow(fromAccounts);
          }
       }

      frmAccountDetailsScreen.show();
    }
    //frmNewTransferKA.flxAcc2.setEnabled(true);

  }
  if(val==2){
    kony.store.setItem("toval",0);

    if(gblTModule!=="send")
    {
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyTo");
      var i =  kony.store.getItem("toAccount");
      for(var j in toAccounts){
        if(toAccounts[j].accountID === i.accountID)
        {
          toAccounts.splice(j,1);
          break;
        }
      }
      accountsScreenPreshow(toAccounts);      
      frmAccountDetailsScreen.show();
    }
    else{
      var data = [];
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyFrom");
      accountsScreenPreshow(fromAccounts);
      frmAccountDetailsScreen.show();
    }
    //alert(JSON.stringify("todata "+i+"index "+j+"data "+data));

  }
}


function onClickAccountDetSegDeposite(){
  try{
    var AccDetailsSegdata = frmAccountDetailsScreen.segFre.data;
    var dataSelected =  frmAccountDetailsScreen.segFre.selectedRowItems[0];
    kony.print(dataSelected.availableBalance);
    var text;
    var isNoFund = false;
    kony.print("Selected Account Data ::"+JSON.stringify(dataSelected));
    toval =  kony.store.getItem("toval");
    if(toval ===  1){
      if(dataSelected.availableBalance<=0){
        isNoFund = true;
      }
      //     }else if(parseInt(dataSelected.availableBalance).toFixed() === 0 || parseInt(dataSelected.availableBalance).toFixed() === "0"){
      //     	isNoFund = true;

      if(isNoFund && kony.newCARD.applyCardsOption === false && kony.application.getPreviousForm().id !== "frmCardLinkedAccounts"){
        customAlertPopup(geti18Value("i18n.common.Information"),geti18Value("i18n.errorCode.54"), popupCommonAlertDimiss, "", geti18Value("i18n.NUO.OKay"), "");
        isNoFund = false;
        return;
      }
    }
    kony.print("gblTModule ::"+gblTModule);

    if (gblTModule ==="OpenTermDeposite"){
       set_formOpenTermDeposite(dataSelected);
       frmOpenTermDeposit.show();
      var balanceAmount=parseFloat(selectedBalAccount)-parseFloat(frmOpenTermDeposit.txtDepositAmount.text)
     if (balanceAmount<0)
     {
       kony.print("balanceeee ::"+selectedBalAccount);
       var Message=geti18Value("i18n.termDeposit.checkAmount");
       frmOpenTermDeposit.btnNextDeposite.setEnabled(false);
       frmOpenTermDeposit.btnNextDeposite.skin="jomopaynextDisabled";
       customAlertPopup(geti18Value("i18n.common.alert"), Message,popupCommonAlertDimiss,"", geti18nkey("i18n.settings.ok"), "");
     }
    }// hassan OpenTermDeposite
    else{
      set_formCreditCardPayment(dataSelected);
      frmCreditCardPayment.show();
    }
  }catch(e){
    exceptionLogCall("::onClickAccountDetailsSegment::","Exception while assigning values, on click of segment","UI",e);
  }
}

/* Hassan OpenTermDeposite*/
function set_formOpenTermDeposite(dataSelected){
  try{
    kony.print("Data Selected set_formOpenTermDeposite  ::"+JSON.stringify(dataSelected));
    var name = "";
    if(dataSelected.accountName !== null && dataSelected.accountName !== undefined){
      if(dataSelected.AccNickName !== "" && dataSelected.AccNickName !== null)
        name = dataSelected.AccNickName;
      else
      	name = dataSelected.accountName;
    }else{
      name = dataSelected.name_on_card;
    }
    
    if(gblTModule === "OpenTermDeposite"){
      //frmCardlessTransaction.lblPaymentMode.text = name;
      selectedBalAccount=dataSelected.currentBalance;
      frmOpenTermDeposit.lblFundDeductionAccountNumber.text =dataSelected.accountID;
      frmOpenTermDeposit.lblDepositCurrency.text = dataSelected.currencyCode;
      frmOpenTermDeposit.lblFundDeductionAccountNumberTitle.setVisibility(true);
      frmOpenTermDeposit.txtDepositAmount.setEnabled(true);
      frmOpenTermDeposit.calOpenDeposite.setEnabled(true);
      for(var j in gblCurrList){
        if(frmOpenTermDeposit.lblDepositCurrency.text === gblCurrList[j].CURR_ISO){
          frmOpenTermDeposit.lblCurrencyCode.text=gblCurrList[j].CURR_CODE;
          break;
        }
      }
      //frmCardlessTransaction.lblPaymentMode.skin = "sknLblBack";
      //frmOpenTermDeposit.flxUnderlinePaymentModeBulk.skin = "sknFlxGreenLine";
      //frmCardlessTransaction.lblBranchCode.text = dataSelected.branchNumber;
      kony.store.setItem("BillPayfromAcc",dataSelected);
    }
    checkNextOpenDeposite();
    //checkNextCardless();
  }catch(e){
    kony.print("Exception_set_formOpenTermDeposite ::"+e);
	exceptionLogCall("set_formOpenTermDeposite","UI ERROR","UI",e);
  }
}
/* Hassan OpenTermDeposite*/