var frmAccountsLandingKAConfig = {
    "formid": "frmAccountsLandingKA",
    "frmAccountsLandingKA": {
        "entity": "Accounts",
        "objectServiceName": "RBObjects",
        "objectServiceOptions" : {"access":"online"},
    },
    "segAccountsKA": {
        "fieldprops": {
            "widgettype": "Segment",
            "entity": "Accounts",
            "additionalFields": ["availablePoints","currencyCode","dueDate","openingDate","paymentTerm","maturityDate","lastStatementBalance","creditCardNumber","accountID","bankName","currentBalance","interestRate","minimumDue","principalValue","supportBillPay","supportTransferFrom","supportTransferTo","transactionLimit","transferLimit","errmsg","nickName","success","dr_ind","cr_ind","reference_no","branchName","iban","productCode","branchNumber","isjomopayacc","codBank","QBHideShowFlag","accountNumber","accountName","AccNickName","AccHideShowFlag","PrefNumber"],
            "field": {
                "nameAccount1": {
                    "widgettype": "Label",
                    "field" : "nickName"
                },
              "dummyAccountNumber": {
                "widgettype": "Label",
                "field" : "accountID"
              },
              "dummyAccountName" : {
                 "widgettype": "Label",
                 "field" : "accountName"
              },
              "amountAccount1":{
                	"widgettype": "Label",
                    "field": "availableBalance"
              },
              "amountcurrBal":{
                	"widgettype": "Label",
                    "field": "currentBalance"
              },
              "amtOutsatndingBal":{
                	"widgettype": "Label",
                    "field": "outstandingBalance"
              },
              "typeAccount":{
                	"widgettype": "Label",
                    "field": "accountType"
              },
              "lblColorKA":{
                	"widgettype": "Label",
                    "field": "accountType",
                    "alias":"sknColor"
              },
              "lblAccountID": {
               "widgettype": "Label",
              "field": "accountID"
              },
              "lblCurrency": {
               "widgettype": "Label",
              "field": "currencyCode"
            }
              
            }
        }
    } ,
    "segAccountsKADeals": {
        "fieldprops": {
            "widgettype": "Segment",
          "constrained":true,
            "entity": "Accounts",
            "additionalFields": ["availablePoints","currencyCode","dueDate","openingDate","paymentTerm","maturityDate","lastStatementBalance","creditCardNumber","accountID","bankName","currentBalance","interestRate","minimumDue","principalValue","supportBillPay","supportTransferFrom","supportTransferTo","transactionLimit","transferLimit","errmsg","nickName","success","dr_ind","cr_ind","reference_no","branchName","iban","productCode","branchNumber","isjomopayacc","codBank","QBHideShowFlag","accountNumber","accountName","AccNickName","AccHideShowFlag","PrefNumber"],
            "field": {
                "nameAccount1": {
                    "widgettype": "Label",
                    "field" : "nickName"
                },
              "dummyAccountNumber": {
                "widgettype": "Label",
                "field" : "accountID"
              },
              "dummyAccountName" : {
                 "widgettype": "Label",
                 "field" : "accountName"
              },
              "amountAccount1":{
                	"widgettype": "Label",
                    "field": "availableBalance"
              },
              "amountcurrBal":{
                	"widgettype": "Label",
                    "field": "currentBalance"
              },
              "amtOutsatndingBal":{
                	"widgettype": "Label",
                    "field": "outstandingBalance"
              },
              "typeAccount":{
                	"widgettype": "Label",
                    "field": "accountType"
              },
              "lblColorKA":{
                	"widgettype": "Label",
                    "field": "accountType",
                    "alias":"sknColor"
              },
              "lblAccountID": {
               "widgettype": "Label",
              "field": "accountID"
              },
              "lblCurrency": {
               "widgettype": "Label",
              "field": "currencyCode"
            }
              
            }
        }
    }
  
 ,
    "segAccountsKALoans": {
        "fieldprops": {
            "widgettype": "Segment",
          "constrained":true,
            "entity": "Accounts",
            "additionalFields": ["availablePoints","currencyCode","dueDate","openingDate","paymentTerm","maturityDate","lastStatementBalance","creditCardNumber","accountID","bankName","currentBalance","interestRate","minimumDue","principalValue","supportBillPay","supportTransferFrom","supportTransferTo","transactionLimit","transferLimit","errmsg","nickName","success","dr_ind","cr_ind","reference_no","branchName","iban","productCode","branchNumber","isjomopayacc","codBank","QBHideShowFlag","accountNumber","accountName","AccNickName","AccHideShowFlag","PrefNumber"],
            "field": {
                "nameAccount1": {
                    "widgettype": "Label",
                    "field" : "nickName"
                },
              "dummyAccountNumber": {
                "widgettype": "Label",
                "field" : "accountID"
              },
              "dummyAccountName" : {
                 "widgettype": "Label",
                 "field" : "accountName"
              },
              "amountAccount1":{
                	"widgettype": "Label",
                    "field": "availableBalance"
              },
              "amountcurrBal":{
                	"widgettype": "Label",
                    "field": "currentBalance"
              },
              "amtOutsatndingBal":{
                	"widgettype": "Label",
                    "field": "outstandingBalance"
              },
              "typeAccount":{
                	"widgettype": "Label",
                    "field": "accountType"
              },
              "lblColorKA":{
                	"widgettype": "Label",
                    "field": "accountType",
                    "alias":"sknColor"
              },
              "lblAccountID": {
               "widgettype": "Label",
              "field": "accountID"
              },
              "lblCurrency": {
               "widgettype": "Label",
              "field": "currencyCode"
            }
              
            }
        }
    }
};