function actionOnSpendingSelectedKA(eventobject) {
    return AS_Button_c263b57f9fb3479a955fbddbac988f97(eventobject);
}

function AS_Button_c263b57f9fb3479a955fbddbac988f97(eventobject) {
    onSpendingSelected();
    if (kony.retailBanking.globalData.globals.PFM_ACCOUNTS_PRESENT && !kony.retailBanking.columnChartGenereted) {
        frmMyMoneyListKA.flxSpendingOverviewKAinner.isVisible = true;
        frmMyMoneyListKA.LabelNoRecordsSpendingKA.isVisible = false;
        ColumnChart = column_createChartWidget();
        frmMyMoneyListKA.flxSpendingOverviewKAinner.add(ColumnChart);
        kony.retailBanking.columnChartGenereted = true;
    }
    if (kony.retailBanking.globalData.globals.PFM_ACCOUNTS_PRESENT) {
        frmMyMoneyListKA.flxSpendingOverviewKAinner.isVisible = true;
        frmMyMoneyListKA.LabelNoRecordsSpendingKA.isVisible = false;
    } else {
        frmMyMoneyListKA.flxSpendingOverviewKAinner.isVisible = false;
        frmMyMoneyListKA.LabelNoRecordsSpendingKA.isVisible = true;
    }
}