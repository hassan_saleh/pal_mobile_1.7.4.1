function actionLogoutPersonalInfoName(eventobject) {
    return AS_Button_fb363f9235294d4581ea25f21b120aa3(eventobject);
}

function AS_Button_fb363f9235294d4581ea25f21b120aa3(eventobject) {
    function SHOW_ALERT__a003f1397b2442b38aebb92cb4e1773b_True() {
        kony.print("deleted");
    }

    function SHOW_ALERT__a003f1397b2442b38aebb92cb4e1773b_False() {}

    function SHOW_ALERT__a003f1397b2442b38aebb92cb4e1773b_Callback(response) {
        if (response == true) {
            SHOW_ALERT__a003f1397b2442b38aebb92cb4e1773b_True()
        } else {
            SHOW_ALERT__a003f1397b2442b38aebb92cb4e1773b_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT__a003f1397b2442b38aebb92cb4e1773b_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}