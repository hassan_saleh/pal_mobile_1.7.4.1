function AS_FlexContainer_ebc4485fae454f7984b20fc84c1d4ab4(eventobject) {
    if (gblPayAppFlow && !isEmpty(gblLaunchModeOBJ.appFlow)) {
        openBOJPAYAppLink();
    } else if (gblforceflag) {
        kony.sdk.mvvm.LogoutAction();
    } else {
        if (gblFromModule == "RegisterUser") {
            frmRegisterUser.show();
        } else if (gblFromModule == "ChangeUsername") {
            frmSettingsKA.show();
        } else if (gblFromModule == "ChangePassword") {
            frmSettingsKA.show();
        } else {
            frmEnrolluserLandingKA.lblFlag.text = "F";
            if (gblReqField == "Username") {
                frmLoginKA.show();
            } else {
                frmRegisterUser.show();
            }
        }
        frmNewUserOnboardVerificationKA.destroy();
        //frmEnrolluserLandingKA.destroy();
    }
}