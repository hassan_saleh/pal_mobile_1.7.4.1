function onClickBackOpenTermDeposite(eventobject) {
    return AS_FlexContainer_b1c6acb2850a480b9f8c627242ee334d(eventobject);
}

function AS_FlexContainer_b1c6acb2850a480b9f8c627242ee334d(eventobject) {
    if (frmOpenTermDeposit.flxConfirmDeposite.isVisible === true) {
        frmOpenTermDeposit.flxDepositSelection.setVisibility(true);
        frmOpenTermDeposit.flxConfirmDeposite.setVisibility(false);
        frmOpenTermDeposit.btnNextDeposite.setVisibility(true);
    } else {
        frmAccountsLandingKA.show();
        frmOpenTermDeposit.destroy();
    }
}