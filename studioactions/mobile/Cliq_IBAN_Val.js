function Cliq_IBAN_Val(eventobject, changedtext) {
    return AS_TextField_a34cd033ff6e4437b2b1f8d6c7437e63(eventobject, changedtext);
}

function AS_TextField_a34cd033ff6e4437b2b1f8d6c7437e63(eventobject, changedtext) {
    if (frmEPS.txtIBANAlias.text.match(/^JO/g) === null || frmEPS.txtIBANAlias.text.match(/^JO/g) === "") {
        frmEPS.flxUnderlineIBAN.skin = "sknFlxOrangeLine";
        frmEPS.lblNext.skin = "sknLblNextDisabled";
        frmEPS.lblHintIBAN.setVisibility(true);
    } else {
        if (frmEPS.txtIBANAlias.text.length == 30) {
            frmEPS.flxUnderlineIBAN.skin = "sknFlxGreyLine";
            frmEPS.lblHintIBAN.setVisibility(false);
        } else {
            frmEPS.flxUnderlineIBAN.skin = "sknFlxOrangeLine";
            frmEPS.lblNext.skin = "sknLblNextDisabled";
            frmEPS.lblHintIBAN.setVisibility(true);
        }
    }
    validateLblNext();
}