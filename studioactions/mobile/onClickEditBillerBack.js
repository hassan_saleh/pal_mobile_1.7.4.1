function onClickEditBillerBack(eventobject) {
    return AS_FlexContainer_f585b16e711d41c192208eac41647afd(eventobject);
}

function AS_FlexContainer_f585b16e711d41c192208eac41647afd(eventobject) {
    //frmManagePayeeKA.show();
    if (kony.sdk.isNetworkAvailable()) {
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var controller = INSTANCE.getFormController("frmManagePayeeKA");
        var navObject = new kony.sdk.mvvm.NavigationObject();
        navObject.setRequestOptions("managepayeesegment", {
            "headers": {},
            "queryParams": {
                "custId": custid,
                "P_BENE_TYPE": "PRE"
            }
        });
        controller.loadDataAndShowForm(navObject);
    } else {
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}