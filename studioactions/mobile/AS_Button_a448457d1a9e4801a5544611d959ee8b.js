function AS_Button_a448457d1a9e4801a5544611d959ee8b(eventobject) {
    if (kony.sdk.isNetworkAvailable()) {
        frmJoMoPay.show();
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}