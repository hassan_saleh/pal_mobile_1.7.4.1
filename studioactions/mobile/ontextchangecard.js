function ontextchangecard(eventobject, changedtext) {
    return AS_TextField_a38625f8a0e247b98d05d406f50bb2cc(eventobject, changedtext);
}

function AS_TextField_a38625f8a0e247b98d05d406f50bb2cc(eventobject, changedtext) {
    ontextchangeamountcard();
    frmCreditCardPayment.lblHiddenAmount.text = frmCreditCardPayment.txtFieldAmount.text;
    if (frmCreditCardPayment.lblCurrency.text === "JOD") {
        kony.print("JOD currency " + frmCreditCardPayment.lblCurrency.text);
        frmCreditCardPayment.txtFieldAmount.text = fixDecimal(frmCreditCardPayment.txtFieldAmount.text, 3);
    } else {
        kony.print("Other currency " + frmCreditCardPayment.lblCurrency.text);
        frmCreditCardPayment.txtFieldAmount.text = fixDecimal(frmCreditCardPayment.txtFieldAmount.text, 2);
    }
}