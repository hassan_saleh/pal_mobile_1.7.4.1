function AS_Switch_ceb0971d73e247b08022d130f285f5d7(eventobject) {
    if (frmAuthorizationAlternatives.flxQuickBalance.height === "8%") {
        frmAuthorizationAlternatives.flxQuickBalance.height = "40%";
        addDatatoSegQuickBalance(1);
    } else {
        frmAuthorizationAlternatives.flxQuickBalance.height = "8%";
        addDatatoSegQuickBalance(0);
    }
}