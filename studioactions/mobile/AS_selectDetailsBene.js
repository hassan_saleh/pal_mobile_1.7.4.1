function AS_selectDetailsBene(eventobject) {
    return AS_Form_ecbe304a7baf4ad1af43fb202ad14253(eventobject);
}

function AS_Form_ecbe304a7baf4ad1af43fb202ad14253(eventobject) {
    if (kony.boj.detailsForBene.lblToUpdate === "lblBranchName") frmSelectDetailsBene.lblTitle.text = geti18Value("i18n.Map.Branchname");
    else if (kony.boj.detailsForBene.lblToUpdate === "lblCountryNameKA") frmSelectDetailsBene.lblTitle.text = geti18Value("i18n.NUO.Country");
    else if (kony.boj.detailsForBene.lblToUpdate === "lblCityName" || kony.boj.detailsForBene.lblToUpdate === "lblCityBank") frmSelectDetailsBene.lblTitle.text = geti18Value("i18n.manage_payee.cityPlh");
    else if (kony.boj.detailsForBene.lblToUpdate === "lblBankBranch") frmSelectDetailsBene.lblTitle.text = geti18Value("i18n.Bene.BankBranch");
    else if (kony.boj.detailsForBene.lblToUpdate === "lblBankName") frmSelectDetailsBene.lblTitle.text = geti18Value("i18n.Bene.Bankname");
    frmSelectDetailsBene.tbxSearch.text = "";
}