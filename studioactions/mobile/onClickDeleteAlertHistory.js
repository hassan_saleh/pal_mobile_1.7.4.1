function onClickDeleteAlertHistory(eventobject) {
    return AS_Button_ed3a4a7981f94c63ac67ab2cf256168e(eventobject);
}

function AS_Button_ed3a4a7981f94c63ac67ab2cf256168e(eventobject) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.alert.deletehistoryhint"), serv_DELETEALERTHISTORY, popupCommonAlertDimiss, geti18Value("i18n.common.Yess"), geti18Value("i18n.comon.Noo"));
}