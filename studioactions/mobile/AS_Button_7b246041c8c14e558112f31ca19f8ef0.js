function AS_Button_7b246041c8c14e558112f31ca19f8ef0(eventobject) {
    //getTransferPayLandingForm("frmNewTransferKA");
    frmNewBillKA.transferPayTitleLabel.text = "Edit PayBill";
    frmNewBillKA.fromCard.setVisibility(true);
    frmNewBillKA.toCard.setVisibility(false);
    frmNewBillKA.amountCard.setVisibility(true);
    frmNewBillKA.dateCard.setVisibility(true);
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmNewBillKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    navObject.setRequestOptions("segInternalFromAccountsPayKA", {
        "headers": {
            "session_token": kony.retailBanking.globalData.session_token
        }
    });
    listController.performAction("navigateTo", ["frmNewBillKA", navObject]);
}