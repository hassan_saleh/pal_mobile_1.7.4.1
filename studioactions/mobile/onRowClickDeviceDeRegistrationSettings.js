function onRowClickDeviceDeRegistrationSettings(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_af4054e07e9149c780af5df1c11d9b23(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_af4054e07e9149c780af5df1c11d9b23(eventobject, sectionNumber, rowNumber) {
    devlistSelectedObj = "";
    devlistSelectedObj = frmDeviceRegistration.segRegisteredDevices.selectedItems[0];
    if (kony.sdk.isNetworkAvailable()) {
        changeRegistrationStatus(frmDeviceRegistration.segRegisteredDevices.selectedItems[0], frmDeviceRegistration.segRegisteredDevices.selectedIndex);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}