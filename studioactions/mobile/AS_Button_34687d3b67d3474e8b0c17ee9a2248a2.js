function AS_Button_34687d3b67d3474e8b0c17ee9a2248a2(eventobject) {
    function SHOW_ALERT__22caf961c7a2426a8de3f07b4463cf40_True() {
        frmAccountInfoKA.flxAccountInfoNavOptKA.isVisible = false;
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT__22caf961c7a2426a8de3f07b4463cf40_False() {}

    function SHOW_ALERT__22caf961c7a2426a8de3f07b4463cf40_Callback(response) {
        if (response == true) {
            SHOW_ALERT__22caf961c7a2426a8de3f07b4463cf40_True()
        } else {
            SHOW_ALERT__22caf961c7a2426a8de3f07b4463cf40_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT__22caf961c7a2426a8de3f07b4463cf40_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}