function AS_Switch_78888f72f8b4491b83c532ba275321c0(eventobject) {
    if (frmAccountAlertsKA.SwitchAlert.selectedIndex === 1) {
        frmAccountAlertsKA.TextFieldFlex.setVisibility(false);
        frmAccountAlertsKA.androidAlerts.setVisibility(false);
        // deregisterPush();
        //	 updateFlags("alerts",false);
    } else {
        frmAccountAlertsKA.TextFieldFlex.setVisibility(true);
        frmAccountAlertsKA.androidAlerts.setVisibility(true);
        //registerPush();
        //updateFlags("alerts",true);
    }
}