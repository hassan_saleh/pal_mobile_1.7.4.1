function AS_FlexContainer_bd93cf2fb8e74b4ca4ac10bad0d22359(eventobject) {
    if (frmAuthorizationAlternatives.flxSwitchOff.isVisible === true) {
        frmAuthorizationAlternatives.flxSwitchOn.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchOff.isVisible = false;
        //frmAuthorizationAlternatives.flxTransactionPreview.isVisible = true;
    } else {
        frmAuthorizationAlternatives.flxSwitchOff.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchOn.isVisible = false;
        var segLength = frmAuthorizationAlternatives.segAccounts.data.length;
        var segArray = [];
        kony.print("da---:" + JSON.stringify(frmAuthorizationAlternatives.segAccounts.data));
        for (var i = 0; i < segLength; i++) {
            var segData = frmAuthorizationAlternatives.segAccounts.data[i];
            segData.lblIncommingTick.isVisible = false;
            segArray.push(segData);
        }
        frmAuthorizationAlternatives.segAccounts.setData(segArray);
        frmAuthorizationAlternatives.flxSwitchTransactionOff.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchTransactionsOn.isVisible = false;
    }
}