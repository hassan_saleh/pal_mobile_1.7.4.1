function AS_onBackSelectDetailsBene(eventobject) {
    return AS_FlexContainer_ca225436b3e949ae8cbab6f142a8fd95(eventobject);
}

function AS_FlexContainer_ca225436b3e949ae8cbab6f142a8fd95(eventobject) {
    if (kony.newCARD.applyCardsOption === true) {
        frmApplyNewCardsKA.show();
    } else {
        if (gblsubaccBranchList) {
            frmOrderCheckBook.show();
        } else if (gblrequestStatementBranch) {
            frmRequestStatementAccounts.show();
        } else if (gblRequestNewPinBranchList) {
            frmRequestPinCard.show();
        } else if (gblWorkflowType === "IPS") {
            frmEPS.show();
        } else {
            frmAddExternalAccountKA.show();
            kony.boj.addBeneNextStatus();
        }
    }
}