function AS_onRowClickSelectDetail(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_ab0e4baf7fc742c0b6906a3adcaf8ba4(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_ab0e4baf7fc742c0b6906a3adcaf8ba4(eventobject, sectionNumber, rowNumber) {
    //alert(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
    if (kony.newCARD.applyCardsOption === true) {
        selected_OPTIONS_APPLYNEWCARDS(frmSelectDetailsBene.segDetails.selectedItems[0], kony.boj.selectedDetailsType);
    } else if (gblWorkflowType === "IPS") {
        setupBanknameIPS(frmSelectDetailsBene.segDetails.selectedItems[0]);
    } else {
        if (gblsubaccBranchList) {
            setDatatoCheckOrderBranchList(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
        } else if (gblrequestStatementBranch) {
            setDatatoRequestStatementBranchList(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
        } else if (gblRequestNewPinBranchList) {
            setDatatoRequestNewPinBranchList(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
        } else if (gblRequestStatementCrediCards) {
            setDatatoRequestStatementBranchList(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
        } else if (gblopenTermDepositeMI) { // hassan open deposite
            setDataMaturityInstruction(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
        } else {
            kony.boj.updateLabel(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
        }
    }
}