function onClickFilterBack(eventobject) {
    return AS_FlexContainer_dfbacc341e6a472a86e64ea72fa7408a(eventobject);
}

function AS_FlexContainer_dfbacc341e6a472a86e64ea72fa7408a(eventobject) {
    if ((frmFilterTransaction.flxDatePicker.isVisible == true)) {
        clearForm.filterTransactions();
        frmFilterTransaction.flxDatePicker.isVisible = false;
        frmFilterTransaction.flxFilterTransaction.isVisible = true;
        frmFilterTransaction.lblFilterHeader.text = kony.i18n.getLocalizedString("i18n.FilterTransaction");
    } else if ((frmFilterTransaction.flxFilterTransaction.isVisible == true || frmFilterTransaction.flxCardsFilterScreen.isVisible == true)) {
        kony.application.getPreviousForm().show();
        clearForm.filterTransactions();
    }
}