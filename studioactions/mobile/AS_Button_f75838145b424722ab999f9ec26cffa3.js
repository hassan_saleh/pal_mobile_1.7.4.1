function AS_Button_f75838145b424722ab999f9ec26cffa3(eventobject) {
    function SHOW_ALERT__93cd77986f994d02b733dfa9554ee21b_True() {
        frmDepositPayLandingKA.flxCheckDepositNavOptKA.isVisible = false;
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT__93cd77986f994d02b733dfa9554ee21b_False() {}

    function SHOW_ALERT__93cd77986f994d02b733dfa9554ee21b_Callback(response) {
        if (response == true) {
            SHOW_ALERT__93cd77986f994d02b733dfa9554ee21b_True()
        } else {
            SHOW_ALERT__93cd77986f994d02b733dfa9554ee21b_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT__93cd77986f994d02b733dfa9554ee21b_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}