function AS_Button_j8bbf8d401af469c9aa004fc4fbe3a4e(eventobject) {
    function SHOW_ALERT__e4a255e93adf42e3b5cca50a2046a756_True() {
        frmAccountInfoEditKA.copyflxAccountInfoNavOptKA.isVisible = false;
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT__e4a255e93adf42e3b5cca50a2046a756_False() {}

    function SHOW_ALERT__e4a255e93adf42e3b5cca50a2046a756_Callback(response) {
        if (response == true) {
            SHOW_ALERT__e4a255e93adf42e3b5cca50a2046a756_True()
        } else {
            SHOW_ALERT__e4a255e93adf42e3b5cca50a2046a756_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT__e4a255e93adf42e3b5cca50a2046a756_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}