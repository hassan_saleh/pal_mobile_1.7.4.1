function AS_TextField_e9c586fa1e6a477a92f66b9d7f4fb480(eventobject, changedtext) {
    try {
        frmEstatementLandingKA.txtEmail2.text = frmEstatementLandingKA.txtEmail2.text.trim();
        if (frmEstatementLandingKA.txtEmail2.text !== null && frmEstatementLandingKA.txtEmail2.text !== "" && isValidEmaill(frmEstatementLandingKA.txtEmail2.text)) {
            frmEstatementLandingKA.borderBottom2.skin = "skntextFieldDividerGreen";
            frmEstatementLandingKA.lblInvalidEmail2.setVisibility(false);
        } else {
            frmEstatementLandingKA.borderBottom2.skin = "skntextFieldDivider";
            frmEstatementLandingKA.lblInvalidEmail2.text = geti18Value("i18n.Estmt.invalidEmail");
            frmEstatementLandingKA.lblInvalidEmail2.setVisibility(true);
        }
        frmEstatementLandingKA.btn.setFocus(true);
    } catch (err) {
        kony.print("err in pwd::" + err);
        frmEstatementLandingKA.btn.setFocus(true);
    }
}