function AS_onEndEditingEmailiOS(eventobject, changedtext) {
    return AS_TextField_ha91c7e1ac194af285623dbccaa1bbbb(eventobject, changedtext);
}

function AS_TextField_ha91c7e1ac194af285623dbccaa1bbbb(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxEmail.text !== "") {
        frmAddExternalAccountKA.flxUnderlineEmail.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineEmail.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblEmail", frmAddExternalAccountKA.tbxEmail.text);
}