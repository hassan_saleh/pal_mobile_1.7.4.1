function AS_OnClickCardsReOrderAction(eventobject) {
    return AS_FlexContainer_cc4a9159a08746359fba9d334737c759(eventobject);
}

function AS_FlexContainer_cc4a9159a08746359fba9d334737c759(eventobject) {
    //getDataforAccountsReOrder();
    frmAccountsReorderKA.lblSettingsTitle.text = geti18nkey("i18n.appsettings.reordercardlist");
    frmAccountsReorderKA.lblReorderDesc.text = geti18nkey("i18n.reorderDescCards");
    gblCardReorder = true;
    gblCardAddNickname = false;
    getDataforCardsReOrder();
}