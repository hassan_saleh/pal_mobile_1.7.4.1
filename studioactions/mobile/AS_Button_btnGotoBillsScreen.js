function AS_Button_btnGotoBillsScreen(eventobject) {
    return AS_Button_c105e4a46e8a4125817967f83e644928(eventobject);
}

function AS_Button_c105e4a46e8a4125817967f83e644928(eventobject) {
    if (frmBulkPaymentConfirmKA.btnBillsScreen.text === geti18Value("i18n.cards.paynow")) {
        serv_BULK_PAYMENT();
    } else {
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var controller = INSTANCE.getFormController("frmManagePayeeKA");
        var navObject = new kony.sdk.mvvm.NavigationObject();
        navObject.setRequestOptions("managepayeesegment", {
            "headers": {},
            "queryParams": {
                "custId": custid,
                "P_BENE_TYPE": "PRE",
                "lang": (kony.store.getItem("langPrefObj") === "en") ? "eng" : "ara"
            }
        });
        controller.loadDataAndShowForm(navObject);
    }
}