function AS_onDoneFilterSI(eventobject) {
    return AS_FlexContainer_g025c7184c0e44e0aeef33c5e519337c(eventobject);
}

function AS_FlexContainer_g025c7184c0e44e0aeef33c5e519337c(eventobject) {
    if (frmFilterSI.txtAmountRangeFrom.text === "" && frmFilterSI.txtAmountRangeTo.text === "") amountRangeDataSI(-1, -1);
    else if (frmFilterSI.txtAmountRangeFrom.text !== "" && frmFilterSI.txtAmountRangeTo.text === "") amountRangeDataSI(frmFilterSI.txtAmountRangeFrom.text, -1);
    else if (frmFilterSI.txtAmountRangeFrom.text === "" && frmFilterSI.txtAmountRangeTo.text !== "") amountRangeDataSI(-1, frmFilterSI.txtAmountRangeTo.text);
    else if (frmFilterSI.txtAmountRangeFrom.text <= frmFilterSI.txtAmountRangeTo.text) amountRangeDataSI(frmFilterSI.txtAmountRangeFrom.text, frmFilterSI.txtAmountRangeTo.text);
    else {
        frmFilterSI.flxAmountMaxLine.skin = "sknFlxOrangeLine";
        frmFilterSI.flxAmountMinLine.skin = "sknFlxOrangeLine";
    }
}