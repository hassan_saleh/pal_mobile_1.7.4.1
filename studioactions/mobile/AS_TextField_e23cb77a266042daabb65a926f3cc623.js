function AS_TextField_e23cb77a266042daabb65a926f3cc623(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidUsername(frmEnrolluserLandingKA.tbxUsernameKA.text)) {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxGreenLine";
        usernameFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxOrangeLine";
        usernameFlag = false;
    }
    if (usernameFlag === true && passwordFlag === true && confrmPasswordFlag === true) {
        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextEnabled";
        frmEnrolluserLandingKA.flxProgress1.left = "80%";
    } else {
        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextDisabled";
        frmEnrolluserLandingKA.flxProgress1.left = "65%";
    }
}