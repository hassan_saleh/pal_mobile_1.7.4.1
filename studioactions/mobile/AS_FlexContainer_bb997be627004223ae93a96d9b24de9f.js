function AS_FlexContainer_bb997be627004223ae93a96d9b24de9f(eventobject) {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController("frmAccountsLandingKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    navObject.setRequestOptions("segAccountsKA", {
        "headers": {
            "session_token": kony.retailBanking.globalData.session_token
        }
    });
    controller.loadDataAndShowForm(navObject);
}