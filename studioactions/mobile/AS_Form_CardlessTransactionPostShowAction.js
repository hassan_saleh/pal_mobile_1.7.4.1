function AS_Form_CardlessTransactionPostShowAction(eventobject) {
    return AS_Form_hc6edfb735aa452885967b5931319343(eventobject);
}

function AS_Form_hc6edfb735aa452885967b5931319343(eventobject) {
    //frmCardlessTransaction.richtxtTermsandConditions.text = "<U>"+geti18Value("i18n.Cardless.TNC")+"</U>";
    frmCardlessTransaction.flxConfirm.isVisible = false;
    frmCardlessTransaction.flxBody.isVisible = true;
    var value = frmCardlessTransaction.txtFieldAmount.text;
    if (value === "" || isEmpty(value) || value === null) {
        frmCardlessTransaction.flxMinusContainer.skin = "CopyslButtonPaymentDashFocus0fc27e0876b3b4d";
        frmCardlessTransaction.flxMinusContainer.setEnabled(false);
    }
    //frmCardlessTransaction.btnSubmit.setEnabled(false);
    var langObj = kony.store.getItem("langPrefObj");
    if (langObj.toUpperCase() === "EN") {
        frmCardlessTransaction.BrowserTCCardless.requestURLConfig = {
            "URL": "http://bojm01jo2020.dev.dot.jo/rewards/tccdle",
            "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
        };
    } else {
        frmCardlessTransaction.BrowserTCCardless.requestURLConfig = {
            "URL": "http://bojm01jo2020.dev.dot.jo/ar/rewards/tccdla",
            "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
        };
    }
}