function jomopayparsefloatiphone(eventobject, changedtext) {
    return AS_TextField_a83ed96d37ac47abb8863f4e885f546f(eventobject, changedtext);
}

function AS_TextField_a83ed96d37ac47abb8863f4e885f546f(eventobject, changedtext) {
    if (frmJoMoPay.txtFieldAmount.text !== "" && frmJoMoPay.txtFieldAmount.text !== "0" && frmJoMoPay.txtFieldAmount.text !== 0) {
        var amount = Number.parseFloat(frmJoMoPay.txtFieldAmount.text).toFixed(3);
        frmJoMoPay.txtFieldAmount.text = amount;
    }
}