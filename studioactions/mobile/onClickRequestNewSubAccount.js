function onClickRequestNewSubAccount(eventobject) {
    return AS_Button_c0d38695780f4d85be603425d748fd93(eventobject);
}

function AS_Button_c0d38695780f4d85be603425d748fd93(eventobject) {
    frmNewSubAccountLandingNew.lblNext.skin = "sknLblNextDisabled";
    frmNewSubAccountLandingNew.lblFre.text = geti18Value("i18n.common.accountType");
    frmNewSubAccountLandingNew.lblCurr.text = geti18Value("i18n.common.currencyType");
    frmNewSubAccountConfirm.lblCheck1.text = "q";
    frmNewSubAccountLandingNew.lblFrequency.setVisibility(false);
    frmNewSubAccountLandingNew.lblCurrHead.setVisibility(false);
    frmNewSubAccountLandingNew.lblLine4.skin = "sknFlxGreyLine"
    frmNewSubAccountLandingNew.CopylblLine0ib1721fa6b7140.skin = "sknFlxGreyLine";
    frmNewSubAccountLandingNew.show();
}