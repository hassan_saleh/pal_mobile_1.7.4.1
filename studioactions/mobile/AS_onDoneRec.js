function AS_onDoneRec(eventobject, changedtext) {
    return AS_TextField_c5a7dadb1ff643d4b6cfcfd6a1cb1b24(eventobject, changedtext);
}

function AS_TextField_c5a7dadb1ff643d4b6cfcfd6a1cb1b24(eventobject, changedtext) {
    animateLabel("DOWN", "lblNumRecurr", frmNewTransferKA.txtNumRecurrences.text);
    var Text = frmNewTransferKA.txtDesc.text;
    animateLabel("UP", "lblDescription", Text);
    onTextChangeRecurrence();
}