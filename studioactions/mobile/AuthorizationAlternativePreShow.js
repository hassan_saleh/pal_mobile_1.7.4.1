function AuthorizationAlternativePreShow(eventobject) {
    return AS_Form_cf6b6d5d09544b57a668bc3773fce7a2(eventobject);
}

function AS_Form_cf6b6d5d09544b57a668bc3773fce7a2(eventobject) {
    if (frmAuthorizationAlternatives.btnSaveQuickBal.isVisible) {
        fnauthorizationAlternativePreShow("frmSettingsKA");
    } else {
        fnauthorizationAlternativePreShow("frmAlternative");
    }
}