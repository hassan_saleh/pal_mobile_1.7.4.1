function AS_Button_70caf02b9b8e4885a95ec4b12612cbae(eventobject) {
    function SHOW_ALERT__8cbe9ca39de54709a45bdcf92a358d83_True() {
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var listController = INSTANCE.getFormController("frmPayeeDetailsKA");
        //listController.performAction("deleteData");
        ShowLoadingScreen();
        deleteBillerCustomVerbCall();
    }

    function SHOW_ALERT__8cbe9ca39de54709a45bdcf92a358d83_False() {}

    function SHOW_ALERT__8cbe9ca39de54709a45bdcf92a358d83_Callback(response) {
        if (response == true) {
            SHOW_ALERT__8cbe9ca39de54709a45bdcf92a358d83_True()
        } else {
            SHOW_ALERT__8cbe9ca39de54709a45bdcf92a358d83_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "Delete Payee",
        "yesLabel": "Yes",
        "noLabel": "No",
        "message": "Do you want to delete the payee?",
        "alertHandler": SHOW_ALERT__8cbe9ca39de54709a45bdcf92a358d83_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}