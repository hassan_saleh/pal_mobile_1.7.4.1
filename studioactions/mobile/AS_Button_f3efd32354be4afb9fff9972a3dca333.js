function AS_Button_f3efd32354be4afb9fff9972a3dca333(eventobject) {
    function SHOW_ALERT__79a983363eca4624adbf8b5bf2e435d4_True() {
        frmAccountsLandingKA.flxAccntLandingNavOptKA.isVisible = false;
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT__79a983363eca4624adbf8b5bf2e435d4_False() {}

    function SHOW_ALERT__79a983363eca4624adbf8b5bf2e435d4_Callback(response) {
        if (response == true) {
            SHOW_ALERT__79a983363eca4624adbf8b5bf2e435d4_True()
        } else {
            SHOW_ALERT__79a983363eca4624adbf8b5bf2e435d4_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT__79a983363eca4624adbf8b5bf2e435d4_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}