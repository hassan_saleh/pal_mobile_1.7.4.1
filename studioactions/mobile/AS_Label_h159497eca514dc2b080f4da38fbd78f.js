function AS_Label_h159497eca514dc2b080f4da38fbd78f(eventobject, x, y) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.Transfer.cancelTra"), onClickYesClose, onClickNoClose, geti18Value("i18n.login.continue"), geti18Value("i18n.common.cancel"));

    function onClickYesClose() {
        popupCommonAlertDimiss();
        removeDatafrmTransfersform();
        makeInlineErrorsTransfersInvisible();
        frmPaymentDashboard.show();;
    }

    function onClickNoClose() {
        popupCommonAlertDimiss();
    }
}