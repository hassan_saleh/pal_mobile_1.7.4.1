function actionoNLogoutClickDOB(eventobject) {
    return AS_Button_ba72e46497b440d6bd991dcd8cf0e52f(eventobject);
}

function AS_Button_ba72e46497b440d6bd991dcd8cf0e52f(eventobject) {
    function SHOW_ALERT__j81d06c9c7094b4b8b8d9402fec5926b_True() {
        kony.print("Deleted");
    }

    function SHOW_ALERT__j81d06c9c7094b4b8b8d9402fec5926b_False() {}

    function SHOW_ALERT__j81d06c9c7094b4b8b8d9402fec5926b_Callback(response) {
        if (response == true) {
            SHOW_ALERT__j81d06c9c7094b4b8b8d9402fec5926b_True()
        } else {
            SHOW_ALERT__j81d06c9c7094b4b8b8d9402fec5926b_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT__j81d06c9c7094b4b8b8d9402fec5926b_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}