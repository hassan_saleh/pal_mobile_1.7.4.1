function AS_TextField_onDoneDepositeAmountText(eventobject, changedtext) {
    return AS_TextField_e80f545b8ff04ac480dfd005e443789b(eventobject, changedtext);
}

function AS_TextField_e80f545b8ff04ac480dfd005e443789b(eventobject, changedtext) {
    kony.print("selectedBalAccount " + selectedBalAccount);
    kony.print("balance " + frmOpenTermDeposit.txtDepositAmount.text);
    var balanceAmount = parseFloat(selectedBalAccount) - parseFloat(frmOpenTermDeposit.txtDepositAmount.text)
    if (balanceAmount < 0) {
        kony.print("balance55 " + frmOpenTermDeposit.txtDepositAmount.text);
        var Message = geti18Value("i18n.termDeposit.checkAmount");
        customAlertPopup(geti18Value("i18n.common.alert"), Message, popupCommonAlertDimiss, "", geti18nkey("i18n.settings.ok"), "");
    }
    if (frmOpenTermDeposit.txtDepositAmount.text < 5000 && frmOpenTermDeposit.lblDepositCurrency.text === "JOD") {
        frmOpenTermDeposit.lblMinUSD.setVisibility(false);
        frmOpenTermDeposit.lblMinJOD.setVisibility(true);
        frmOpenTermDeposit.flxBorderDepositAmount.skin = "skntextFieldDividerOrange";
    } else if (frmOpenTermDeposit.txtDepositAmount.text < 3000 && frmOpenTermDeposit.lblDepositCurrency.text !== "JOD") {
        frmOpenTermDeposit.lblMinJOD.setVisibility(false);
        frmOpenTermDeposit.lblMinUSD.setVisibility(true);
        frmOpenTermDeposit.flxBorderDepositAmount.skin = "skntextFieldDividerOrange";
    } else {
        frmOpenTermDeposit.lblMinJOD.setVisibility(false);
        frmOpenTermDeposit.lblMinUSD.setVisibility(false);
        frmOpenTermDeposit.flxBorderDepositAmount.skin = "skntextFieldDividerGreen";
    }
    callRateService();
    checkNextOpenDeposite();
}