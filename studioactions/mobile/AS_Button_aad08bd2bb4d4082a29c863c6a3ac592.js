function AS_Button_aad08bd2bb4d4082a29c863c6a3ac592(eventobject) {
    function SHOW_ALERT_ide_onClick_bfd8761b23d048898169e38735dad260_True() {
        //closeAndroidNav();
        //deviceRegFrom = "logout";
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT_ide_onClick_bfd8761b23d048898169e38735dad260_False() {}

    function SHOW_ALERT_ide_onClick_bfd8761b23d048898169e38735dad260_Callback(response) {
        if (response == true) {
            SHOW_ALERT_ide_onClick_bfd8761b23d048898169e38735dad260_True()
        } else {
            SHOW_ALERT_ide_onClick_bfd8761b23d048898169e38735dad260_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_bfd8761b23d048898169e38735dad260_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}