function AndroidAlertsRequiredOnClick(eventobject) {
    return AS_FlexContainer_ec9ee3e54948414387f8ef76faf11038(eventobject);
}

function AS_FlexContainer_ec9ee3e54948414387f8ef76faf11038(eventobject) {
    if (frmAccountAlertsKA.infoImg.src == "checkbox_off.png") {
        frmAccountAlertsKA.infoImg.src = "checkbox_on.png";
        frmAccountAlertsKA.androidAlerts.setVisibility(true);
        frmAccountAlertsKA.HiddenAlertsReq.text = "true";
    } else {
        frmAccountAlertsKA.infoImg.src = "checkbox_off.png";
        frmAccountAlertsKA.androidAlerts.setVisibility(false);
        frmAccountAlertsKA.HiddenAlertsReq.text = "false";
    }
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController("frmAccountAlertsKA");
    controller.performAction("saveData");
}