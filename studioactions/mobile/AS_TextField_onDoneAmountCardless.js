function AS_TextField_onDoneAmountCardless(eventobject, changedtext) {
    return AS_TextField_aab002d3b3ff4972814c0b74ff660db2(eventobject, changedtext);
}

function AS_TextField_aab002d3b3ff4972814c0b74ff660db2(eventobject, changedtext) {
    frmCardlessTransaction.flxBorderAmount.skin = "skntextFieldDividerGreen";
    frmCardlessTransaction.lblHiddenAmount.text = frmCardlessTransaction.txtFieldAmount.text;
    frmCardlessTransaction.txtFieldAmount.maxTextLength = 20;
    frmCardlessTransaction.txtFieldAmount.text = frmCardlessTransaction.lblHiddenAmount.text;
    checkNextCardless();
    // if (frmCardlessTransaction.txtFieldAmount > 500){
    //   frmCardlessTransaction.flxBorderAmount.skin = "skntextFieldDividerOrange";
    // }else{
    //   frmCardlessTransaction.flxBorderAmount.skin = "skntextFieldDivider";
    // }
}