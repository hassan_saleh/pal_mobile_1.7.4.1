function AS_Button_d9c609034c2d4ba0a774effe0c824cb8(eventobject) {
    function SHOW_ALERT_ide_onClick_j1f3112c7c0a4e838c9092d89041f64d_True() {
        frmAccountsLandingKA.flxAccntLandingNavOptKA.isVisible = false;
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT_ide_onClick_j1f3112c7c0a4e838c9092d89041f64d_False() {}

    function SHOW_ALERT_ide_onClick_j1f3112c7c0a4e838c9092d89041f64d_Callback(response) {
        if (response == true) {
            SHOW_ALERT_ide_onClick_j1f3112c7c0a4e838c9092d89041f64d_True()
        } else {
            SHOW_ALERT_ide_onClick_j1f3112c7c0a4e838c9092d89041f64d_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_j1f3112c7c0a4e838c9092d89041f64d_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}