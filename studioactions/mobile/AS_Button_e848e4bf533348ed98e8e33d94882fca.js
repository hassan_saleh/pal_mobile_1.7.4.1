function AS_Button_e848e4bf533348ed98e8e33d94882fca(eventobject) {
    function SHOW_ALERT_ide_onClick_805740d7a4914ce08b22d6ab0c5262e0_True() {
        deviceRegFrom = "logout";
        removeSwipe();
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT_ide_onClick_805740d7a4914ce08b22d6ab0c5262e0_False() {}

    function SHOW_ALERT_ide_onClick_805740d7a4914ce08b22d6ab0c5262e0_Callback(response) {
        if (response == true) {
            SHOW_ALERT_ide_onClick_805740d7a4914ce08b22d6ab0c5262e0_True()
        } else {
            SHOW_ALERT_ide_onClick_805740d7a4914ce08b22d6ab0c5262e0_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_805740d7a4914ce08b22d6ab0c5262e0_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}