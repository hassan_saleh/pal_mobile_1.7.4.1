function initializeCopyFBox0c9be9d6518174e() {
    CopyFBox0c9be9d6518174e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyFBox0c9be9d6518174e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    CopyFBox0c9be9d6518174e.setDefaultUnit(kony.flex.DP);
    var lblCurrencyValKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCurrencyValKA",
        "isVisible": true,
        "left": "5%",
        "skin": "skn",
        "text": "Label",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblUSDExRateKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblUSDExRateKA",
        "isVisible": true,
        "right": "5%",
        "skin": "skn",
        "text": "Label",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyFBox0c9be9d6518174e.add(lblCurrencyValKA, lblUSDExRateKA);
}