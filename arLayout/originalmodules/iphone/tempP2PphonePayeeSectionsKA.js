function initializetempP2PphonePayeeSectionsKA() {
    CopyFlexContainer0d4f57045e51b42 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35dp",
        "id": "CopyFlexContainer0d4f57045e51b42",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    CopyFlexContainer0d4f57045e51b42.setDefaultUnit(kony.flex.DP);
    var flxP2PphonePayeeNameKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "41.97%",
        "id": "flxP2PphonePayeeNameKA",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxP2PphonePayeeNameKA.setDefaultUnit(kony.flex.DP);
    var transactionName = new kony.ui.Label({
        "centerY": "34%",
        "height": "20dp",
        "id": "transactionName",
        "isVisible": true,
        "left": "5%",
        "skin": "skn383838LatoRegular107KA",
        "text": "Payment to City of Austin",
        "top": "30dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var transactionLastName = new kony.ui.Label({
        "centerY": "34%",
        "height": "20dp",
        "id": "transactionLastName",
        "isVisible": true,
        "left": "2%",
        "skin": "skn383838LatoRegular107KA",
        "text": "Payment to City of Austin",
        "top": "10dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxP2PphonePayeeNameKA.add(transactionName, transactionLastName);
    var transactionDate = new kony.ui.Label({
        "height": "20dp",
        "id": "transactionDate",
        "isVisible": true,
        "left": "5%",
        "skin": "sknRegisterMobileBank",
        "top": "15%",
        "width": "95.83%",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var transactionEmail = new kony.ui.Label({
        "height": "20dp",
        "id": "transactionEmail",
        "isVisible": true,
        "left": "5%",
        "skin": "sknRegisterMobileBank",
        "top": "15%",
        "width": "95.95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblSepKA = new kony.ui.Label({
        "height": "1dp",
        "id": "lblSepKA",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknLineEDEDEDKA",
        "text": "Label",
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyFlexContainer0d4f57045e51b42.add(flxP2PphonePayeeNameKA, transactionDate, transactionEmail, lblSepKA);
}