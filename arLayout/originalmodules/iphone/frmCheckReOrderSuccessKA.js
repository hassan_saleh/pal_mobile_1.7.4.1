function addWidgetsfrmCheckReOrderSuccessKA() {
    frmCheckReOrderSuccessKA.setDefaultUnit(kony.flex.DP);
    var touchFeature = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "touchFeature",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    touchFeature.setDefaultUnit(kony.flex.DP);
    var FlexContainer00a3c07fadcbf4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainer00a3c07fadcbf4b",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "60dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer00a3c07fadcbf4b.setDefaultUnit(kony.flex.DP);
    var Label07b268452752c4c = new kony.ui.Label({
        "id": "Label07b268452752c4c",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknonboardingHeader",
        "text": kony.i18n.getLocalizedString("i18n.checkReOrder.CheckReOrder"),
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopysuccessIcon0f42e785ae2344c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "30dp",
        "id": "CopysuccessIcon0f42e785ae2344c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknsuccessIcon",
        "top": "40dp",
        "width": "30dp",
        "zIndex": 1
    }, {}, {});
    CopysuccessIcon0f42e785ae2344c.setDefaultUnit(kony.flex.DP);
    var CopyImage031bb4461f4b644 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "50%",
        "id": "CopyImage031bb4461f4b644",
        "isVisible": true,
        "skin": "sknslImage",
        "src": "check.png",
        "width": "50%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopysuccessIcon0f42e785ae2344c.add(CopyImage031bb4461f4b644);
    var CopyLabel04e32e5c0a78845 = new kony.ui.Label({
        "id": "CopyLabel04e32e5c0a78845",
        "isVisible": true,
        "left": "8dp",
        "skin": "sknonboardingHeader",
        "text": kony.i18n.getLocalizedString("i18n.checkReOrder.OrderSubmittedSuccessfully"),
        "top": "7dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyLabel0d00f815bcc7c41 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel0d00f815bcc7c41",
        "isVisible": true,
        "left": "20%",
        "skin": "sknonboardingText",
        "text": kony.i18n.getLocalizedString("i18n.checkReOrder.Thanks"),
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyLabel016560c53601949 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel016560c53601949",
        "isVisible": true,
        "left": "20%",
        "skin": "sknonboardingText",
        "text": kony.i18n.getLocalizedString("i18n.checkReOder.CheckStatusInList"),
        "top": "0dp",
        "width": "84.54%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnReLoginKA = new kony.ui.Button({
        "centerX": "49.97%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "btnReLoginKA",
        "isVisible": true,
        "onClick": AS_Button_1bf59c04e4224b73ada4e944dd2b0ac9,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.checkReOrder.Continue"),
        "top": "60dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    FlexContainer00a3c07fadcbf4b.add(Label07b268452752c4c, CopysuccessIcon0f42e785ae2344c, CopyLabel04e32e5c0a78845, CopyLabel0d00f815bcc7c41, CopyLabel016560c53601949, btnReLoginKA);
    touchFeature.add(FlexContainer00a3c07fadcbf4b);
    frmCheckReOrderSuccessKA.add(touchFeature);
};

function frmCheckReOrderSuccessKAGlobals() {
    frmCheckReOrderSuccessKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCheckReOrderSuccessKA,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmCheckReOrderSuccessKA",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "pagingEnabled": false,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "transitionFade"
        },
        "retainScrollPosition": false,
        "titleBar": false
    });
};