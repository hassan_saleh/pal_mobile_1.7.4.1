function addWidgetsfrmSplash() {
    frmSplash.setDefaultUnit(kony.flex.DP);
    var vdoSplash = new kony.ui.Video({
        "height": "100%",
        "id": "vdoSplash",
        "isVisible": true,
        "left": "0dp",
        "onPrepared": AS_Video_ic2321187e0a4a568abd2adf0adced66,
        "onCompletion": AS_Video_id036a0eb569434e976af47e55a02f8f,
        "skin": "slVideo",
        "source": {
            "mp4": "boj_splash.mp4"
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "controls": false,
        "poster": "tran.png",
        "volume": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    frmSplash.add(vdoSplash);
};

function frmSplashGlobals() {
    frmSplash = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmSplash,
        "enabledForIdleTimeout": false,
        "id": "frmSplash",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_i1337e43920841539f5574488c8b73cc,
        "skin": "sknBackground",
        "statusBarHidden": true
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};