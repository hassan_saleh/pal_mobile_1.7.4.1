function initializetmpBudgetListKA() {
    flxBudgetListKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknyourAccountCardFocus",
        "height": "80dp",
        "id": "flxBudgetListKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknyourAccountCard",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBudgetListKA.setDefaultUnit(kony.flex.DP);
    var flxTopLevelKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70%",
        "id": "flxTopLevelKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopLevelKA.setDefaultUnit(kony.flex.DP);
    var ImgCategoryKA = new kony.ui.Image2({
        "height": "95%",
        "id": "ImgCategoryKA",
        "isVisible": true,
        "left": "6.35%",
        "skin": "slImage",
        "src": "budget_furnish.png",
        "top": "5%",
        "width": "10%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var fullColorKA = new kony.ui.Label({
        "bottom": 12,
        "height": "70%",
        "id": "fullColorKA",
        "isVisible": true,
        "left": "2%",
        "skin": "sknBlueDark64A1D6",
        "top": "15%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var restofColorKA = new kony.ui.Label({
        "bottom": 12,
        "height": "70%",
        "id": "restofColorKA",
        "isVisible": true,
        "left": "0%",
        "skin": "sknBlueLightB1D0EA",
        "top": "15%",
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblPercentageValKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblPercentageValKA",
        "isVisible": true,
        "left": "2%",
        "skin": "sknPercentageBoldKA",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxTopLevelKA.add(ImgCategoryKA, fullColorKA, restofColorKA, lblPercentageValKA);
    var flxBottomLevelKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxBottomLevelKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "52dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomLevelKA.setDefaultUnit(kony.flex.DP);
    var lblBelowLabelKA = new kony.ui.Label({
        "id": "lblBelowLabelKA",
        "isVisible": true,
        "left": "20%",
        "skin": "sknBottomLabelKA",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBottomLevelKA.add(lblBelowLabelKA);
    flxBudgetListKA.add(flxTopLevelKA, flxBottomLevelKA);
}