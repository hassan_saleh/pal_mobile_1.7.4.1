function addWidgetsfrmLoanPostpone() {
    frmLoanPostpone.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "s",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_ic0499bf27094e3a94ac7af77d1be747,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblBack0h2fe661f120d45 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblBack0h2fe661f120d45",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, CopylblBack0h2fe661f120d45);
    var lblLoanPostponeTitle = new kony.ui.Label({
        "height": "90%",
        "id": "lblLoanPostponeTitle",
        "isVisible": true,
        "left": "20%",
        "minHeight": "90%",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.loanpostpone.title"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnNext = new kony.ui.Button({
        "focusSkin": "jomopaynextDisabled",
        "height": "90%",
        "id": "btnNext",
        "isVisible": true,
        "onClick": AS_Button_e8923b24d7a9462591827bab82cc8a27,
        "right": "0%",
        "skin": "jomopaynextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeader.add(flxBack, lblLoanPostponeTitle, btnNext);
    var flxLoanPostpone = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxLoanPostpone",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLoanPostpone.setDefaultUnit(kony.flex.DP);
    var flxLoanAccountNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxLoanAccountNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxLoanAccountNumber.setDefaultUnit(kony.flex.DP);
    var lblLoanAccountNumberTitle = new kony.ui.Label({
        "id": "lblLoanAccountNumberTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.common.loanaccno"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLoanAccountNumber = new kony.ui.Label({
        "id": "lblLoanAccountNumber",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxLoanAccountNumber.add(lblLoanAccountNumberTitle, lblLoanAccountNumber);
    var flxRemainingBalance = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxRemainingBalance",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "11%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRemainingBalance.setDefaultUnit(kony.flex.DP);
    var lblRemainingBalanceTitle = new kony.ui.Label({
        "id": "lblRemainingBalanceTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.loans.remainingBalance"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRemainingBalance = new kony.ui.Label({
        "id": "lblRemainingBalance",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRemainingBalance.add(lblRemainingBalanceTitle, lblRemainingBalance);
    var flxNextPaymentDate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxNextPaymentDate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "22%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNextPaymentDate.setDefaultUnit(kony.flex.DP);
    var lblNextPaymentDateTitle = new kony.ui.Label({
        "id": "lblNextPaymentDateTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.loanpostpone.nextpaymentdate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNextPaymentDate = new kony.ui.Label({
        "id": "lblNextPaymentDate",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNextPaymentDate.add(lblNextPaymentDateTitle, lblNextPaymentDate);
    var flxPaymentAmount0aac26225df1f42 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxPaymentAmount0aac26225df1f42",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "33%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxPaymentAmount0aac26225df1f42.setDefaultUnit(kony.flex.DP);
    var lblPayAmounttt = new kony.ui.Label({
        "id": "lblPayAmounttt",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.loans.paymentAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPaymentAmount1 = new kony.ui.Label({
        "id": "lblPaymentAmount1",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPaymentAmount0aac26225df1f42.add(lblPayAmounttt, lblPaymentAmount1);
    var flxMonths = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxMonths",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "57%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxMonths.setDefaultUnit(kony.flex.DP);
    var lblMonthsTitle = new kony.ui.Label({
        "id": "lblMonthsTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.loanpostpone.noofinspos"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMonths = new kony.ui.Label({
        "id": "lblMonths",
        "isVisible": true,
        "left": "0%",
        "skin": "sknTransferType",
        "text": "1",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnMonthsList = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "height": "60%",
        "id": "btnMonthsList",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_c7b6b465849d4afe8ef1b6b15b4f492f,
        "skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
        "text": "d",
        "top": "35%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBorderMobileNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxBorderMobileNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "skntextFieldDividerJomoPay",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxBorderMobileNumber.setDefaultUnit(kony.flex.DP);
    flxBorderMobileNumber.add();
    flxMonths.add(lblMonthsTitle, lblMonths, btnMonthsList, flxBorderMobileNumber);
    var flxInterestPayment = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxInterestPayment",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "45%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxInterestPayment.setDefaultUnit(kony.flex.DP);
    var lblInterestPaymentTitle = new kony.ui.Label({
        "id": "lblInterestPaymentTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.loanpostpone.feesandinterest"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxEndOfLoan = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "25%",
        "id": "flxEndOfLoan",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "24%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxEndOfLoan.setDefaultUnit(kony.flex.DP);
    var btnEndOfLoan = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknRDOWhiteBRDBOJFont",
        "height": "60%",
        "id": "btnEndOfLoan",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_i2c92883041d44448c155f46ca5079cd,
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "t",
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblEndOfLoan = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblEndOfLoan",
        "isVisible": true,
        "left": "17%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.loanpostpone.endofloan"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "73%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEndOfLoan.add(btnEndOfLoan, lblEndOfLoan);
    var flxCurLoanInst = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxCurLoanInst",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "40%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCurLoanInst.setDefaultUnit(kony.flex.DP);
    var btnCurLoanInst = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknRDOWhiteBRDBOJFont",
        "height": "60%",
        "id": "btnCurLoanInst",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_i2c92883041d44448c155f46ca5079cd,
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "s",
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblCurLoanInst = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCurLoanInst",
        "isVisible": true,
        "left": "17%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.loanpostpone.nextloanins"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "73%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCurLoanInst.add(btnCurLoanInst, lblCurLoanInst);
    flxInterestPayment.add(lblInterestPaymentTitle, flxEndOfLoan, flxCurLoanInst);
    flxLoanPostpone.add(flxLoanAccountNumber, flxRemainingBalance, flxNextPaymentDate, flxPaymentAmount0aac26225df1f42, flxMonths, flxInterestPayment);
    var flxLoanPostponeConfirmation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxLoanPostponeConfirmation",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLoanPostponeConfirmation.setDefaultUnit(kony.flex.DP);
    var flxLoanAccountNumberConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxLoanAccountNumberConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxLoanAccountNumberConfirm.setDefaultUnit(kony.flex.DP);
    var lblLoanAccountNumberTitleConfirm = new kony.ui.Label({
        "id": "lblLoanAccountNumberTitleConfirm",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.common.loanaccno"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLoanAccNum = new kony.ui.Label({
        "id": "lblLoanAccNum",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxLoanAccountNumberConfirm.add(lblLoanAccountNumberTitleConfirm, lblLoanAccNum);
    var flxRemainingBalanceConfirmation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxRemainingBalanceConfirmation",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "13%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRemainingBalanceConfirmation.setDefaultUnit(kony.flex.DP);
    var lblRemainingBalanceTitleConfirmation = new kony.ui.Label({
        "id": "lblRemainingBalanceTitleConfirmation",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.loans.remainingBalance"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRemainingBal = new kony.ui.Label({
        "id": "lblRemainingBal",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRemainingBalanceConfirmation.add(lblRemainingBalanceTitleConfirmation, lblRemainingBal);
    var flxNextPaymentDateConfirmation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxNextPaymentDateConfirmation",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "24%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNextPaymentDateConfirmation.setDefaultUnit(kony.flex.DP);
    var lblNextPaymentDateTitleConfirmation = new kony.ui.Label({
        "id": "lblNextPaymentDateTitleConfirmation",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.loanpostpone.nextpayment"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNxtPaymntDate = new kony.ui.Label({
        "id": "lblNxtPaymntDate",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNextPaymentDateConfirmation.add(lblNextPaymentDateTitleConfirmation, lblNxtPaymntDate);
    var flxNextPaymentAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxNextPaymentAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "35%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNextPaymentAmount.setDefaultUnit(kony.flex.DP);
    var lblPayAmount = new kony.ui.Label({
        "id": "lblPayAmount",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.loans.paymentAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPaymentAmount = new kony.ui.Label({
        "id": "lblPaymentAmount",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNextPaymentAmount.add(lblPayAmount, lblPaymentAmount);
    var flxNumofInstallmentPostpone = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxNumofInstallmentPostpone",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "46%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNumofInstallmentPostpone.setDefaultUnit(kony.flex.DP);
    var lblNumofInsPostponeTitle = new kony.ui.Label({
        "id": "lblNumofInsPostponeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.loanpostpone.noofinspos"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNumofInsPostpone = new kony.ui.Label({
        "id": "lblNumofInsPostpone",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNumofInstallmentPostpone.add(lblNumofInsPostponeTitle, lblNumofInsPostpone);
    var flxInterestPaymentConfirmation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11%",
        "id": "flxInterestPaymentConfirmation",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "46%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxInterestPaymentConfirmation.setDefaultUnit(kony.flex.DP);
    var lblInterestPaymentTitleConfirmation = new kony.ui.Label({
        "id": "lblInterestPaymentTitleConfirmation",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.loanpostpone.feesandinterest"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIntPayment = new kony.ui.Label({
        "id": "lblIntPayment",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxInterestPaymentConfirmation.add(lblInterestPaymentTitleConfirmation, lblIntPayment);
    var flxTermsandConditionCheck = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxTermsandConditionCheck",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "slFbox",
        "top": "60%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxTermsandConditionCheck.setDefaultUnit(kony.flex.DP);
    var lblTermsandConditionsCheckBox = new kony.ui.Label({
        "id": "lblTermsandConditionsCheckBox",
        "isVisible": true,
        "left": "0%",
        "onTouchEnd": AS_Label_ef8f60d0181644608e9aa14338643fb6,
        "skin": "sknBOJttfwhitee150",
        "text": "q",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxTncBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxTncBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "10%",
        "onClick": AS_RichText_acf298b1cb214dd287d13087ab61560c,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTncBody.setDefaultUnit(kony.flex.DP);
    var richtxtTermsandCondition = new kony.ui.RichText({
        "id": "richtxtTermsandCondition",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknrichTxtWhite100",
        "text": "<U>RichText</U>",
        "top": "0dp",
        "width": "85%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxTncBody.add(richtxtTermsandCondition);
    flxTermsandConditionCheck.add(lblTermsandConditionsCheckBox, flxTncBody);
    var btnConfirm = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "94%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "8%",
        "id": "btnConfirm",
        "isVisible": true,
        "onClick": AS_Button_bc2f34feeb23416599f2830f49e9d548,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxLoanPostponeConfirmation.add(flxLoanAccountNumberConfirm, flxRemainingBalanceConfirmation, flxNextPaymentDateConfirmation, flxNextPaymentAmount, flxNumofInstallmentPostpone, flxInterestPaymentConfirmation, flxTermsandConditionCheck, btnConfirm);
    frmLoanPostpone.add(flxHeader, flxLoanPostpone, flxLoanPostponeConfirmation);
};

function frmLoanPostponeGlobals() {
    frmLoanPostpone = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmLoanPostpone,
        "bounces": false,
        "enabledForIdleTimeout": false,
        "id": "frmLoanPostpone",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "slFormCommon",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};