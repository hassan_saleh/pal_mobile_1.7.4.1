function addWidgetsfrmFxRate() {
    frmFxRate.setDefaultUnit(kony.flex.DP);
    var flxHeaderFxRate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeaderFxRate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "CopysknslFbox0b60a9222667f44",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeaderFxRate.setDefaultUnit(kony.flex.DP);
    var lblNumberKA = new kony.ui.Label({
        "height": "20dp",
        "id": "lblNumberKA",
        "isVisible": false,
        "right": "5dp",
        "skin": "CopyslLabel0f7ca8453c24243",
        "text": "18",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "13dp",
        "width": "20dp",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxForTappingKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxForTappingKA",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "onTouchEnd": AS_FlexContainer_d5f773d70b564d338bb5d0b1b63aaef7,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxForTappingKA.setDefaultUnit(kony.flex.DP);
    flxForTappingKA.add();
    var lblTitleLabel1 = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "45%",
        "height": "90%",
        "id": "lblTitleLabel1",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.fxRate"),
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_c6e8dfd3096f4d2cb54c51c0d3ca1b53,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 10
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblBack0c15b1f41de0b41 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblBack0c15b1f41de0b41",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, CopylblBack0c15b1f41de0b41);
    flxHeaderFxRate.add(lblNumberKA, flxForTappingKA, lblTitleLabel1, flxBack);
    var flxTab = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxTab",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTab.setDefaultUnit(kony.flex.DP);
    var flxContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "65%",
        "id": "flxContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFboxOuterRing",
        "top": "0dp",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxContent.setDefaultUnit(kony.flex.DP);
    var btnExchangeRates = new kony.ui.Button({
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnExchangeRates",
        "isVisible": true,
        "left": "0",
        "onClick": AS_Button_c47b55d940254da4b8397657d787ee7d,
        "skin": "slButtonWhiteTab",
        "text": kony.i18n.getLocalizedString("i18n.fxRate"),
        "top": "0dp",
        "width": "50%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnCalculateFx = new kony.ui.Button({
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnCalculateFx",
        "isVisible": true,
        "left": "0",
        "onClick": AS_Button_e8dcd797a6a948838ee05dde8ea0dfec,
        "skin": "slButtonWhiteTabDisabled",
        "text": kony.i18n.getLocalizedString("i18n.fxCalculate"),
        "top": "0dp",
        "width": "50%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxContent.add(btnExchangeRates, btnCalculateFx);
    flxTab.add(flxContent);
    var flxScrlExchangeRates = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0%",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "flxScrlExchangeRates",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "22%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxScrlExchangeRates.setDefaultUnit(kony.flex.DP);
    var flxListbox = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "17%",
        "id": "flxListbox",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "onClick": AS_FlexContainer_ab22773207764ce5b705fb12213781ae,
        "skin": "sknflxwhiteBorder1",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxListbox.setDefaultUnit(kony.flex.DP);
    var flxFlag = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxFlag",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknflxroundflagBorder",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxFlag.setDefaultUnit(kony.flex.DP);
    var imgFlag = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "imgFlag",
        "isVisible": true,
        "skin": "slImage",
        "src": "bahrainflag.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxFlag.add(imgFlag);
    var lblCountryName = new kony.ui.Label({
        "id": "lblCountryName",
        "isVisible": true,
        "left": "23%",
        "skin": "sknlblWhitecariolight135",
        "text": "Jordan",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "14%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCurrency = new kony.ui.Label({
        "id": "lblCurrency",
        "isVisible": true,
        "left": "23%",
        "skin": "sknlblWhitecarioRegular135",
        "text": "JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "47%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSelectList = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSelectList",
        "isVisible": true,
        "right": "5%",
        "skin": "CopysknLblBoj150",
        "text": "d",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxListbox.add(flxFlag, lblCountryName, lblCurrency, lblSelectList);
    var flxSubTitle = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxSubTitle",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "CopysknslFbox0b60a9222667f44",
        "top": "22%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSubTitle.setDefaultUnit(kony.flex.DP);
    var CopylblConvertedValue0j4485f10241249 = new kony.ui.Label({
        "centerY": "48%",
        "id": "CopylblConvertedValue0j4485f10241249",
        "isVisible": true,
        "left": "4%",
        "skin": "sknlblWhitecarioRegular",
        "text": kony.i18n.getLocalizedString("i18n.NUO.Country"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblConvertedValue0e0645b306ff94d = new kony.ui.Label({
        "centerY": "48%",
        "id": "CopylblConvertedValue0e0645b306ff94d",
        "isVisible": true,
        "left": "22%",
        "skin": "sknlblWhitecarioRegular",
        "text": kony.i18n.getLocalizedString("i18n.more.currency"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblConvertedValue0de6ded6c2cfb4f = new kony.ui.Label({
        "centerY": "48%",
        "id": "CopylblConvertedValue0de6ded6c2cfb4f",
        "isVisible": false,
        "left": "55%",
        "skin": "sknlblWhitecarioRegular",
        "text": kony.i18n.getLocalizedString("i18n.BuyRate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "22%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblConvertedValue0da07f7b1132f4e = new kony.ui.Label({
        "centerY": "48%",
        "id": "CopylblConvertedValue0da07f7b1132f4e",
        "isVisible": true,
        "left": "73%",
        "skin": "sknlblWhitecarioRegular",
        "text": kony.i18n.getLocalizedString("i18n.SellRate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "24%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Label0d3c83e907d114a = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "Label0d3c83e907d114a",
        "isVisible": true,
        "left": "43%",
        "skin": "sknlblWhitecarioRegular",
        "text": kony.i18n.getLocalizedString("i18n.BuyRate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "22%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSubTitle.add(CopylblConvertedValue0j4485f10241249, CopylblConvertedValue0e0645b306ff94d, CopylblConvertedValue0de6ded6c2cfb4f, CopylblConvertedValue0da07f7b1132f4e, Label0d3c83e907d114a);
    var segExchangeRates = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "imgFlag": "",
            "lblBuyRate": "",
            "lblCountry": "",
            "lblCurrency": "",
            "lblSellRate": ""
        }],
        "groupCells": false,
        "height": "53%",
        "id": "segExchangeRates",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "segManageCardsKA",
        "rowTemplate": flxExchangeRatestmp,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "ffffff50",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "31%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxExchangeRatestmp": "flxExchangeRatestmp",
            "flxFlag": "flxFlag",
            "flxMain": "flxMain",
            "imgFlag": "imgFlag",
            "lblBuyRate": "lblBuyRate",
            "lblCountry": "lblCountry",
            "lblCurrency": "lblCurrency",
            "lblSellRate": "lblSellRate"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopylblNotes0b244525f55a042 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopylblNotes0b244525f55a042",
        "isVisible": true,
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.fxmsgg"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "87%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxScrlExchangeRates.add(flxListbox, flxSubTitle, segExchangeRates, CopylblNotes0b244525f55a042);
    var flxScrlCalculateFx = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0%",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "flxScrlCalculateFx",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "24%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxScrlCalculateFx.setDefaultUnit(kony.flex.DP);
    var CopyflxListbox0fc89f8830c884e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "17%",
        "id": "CopyflxListbox0fc89f8830c884e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    CopyflxListbox0fc89f8830c884e.setDefaultUnit(kony.flex.DP);
    var CopylblSelectList0j6a93302e7e240 = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "50%",
        "id": "CopylblSelectList0j6a93302e7e240",
        "isVisible": true,
        "onTouchStart": AS_Label_dfe84995e25a4258a5fb32164ee7fcb9,
        "right": "5%",
        "skin": "sknbojFont2White80New",
        "text": "V",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxLeftList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxLeftList",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_g4c50135d01747e7bfdfc2f22012eba1,
        "skin": "slFbox",
        "top": "0dp",
        "width": "41%",
        "zIndex": 1
    }, {}, {});
    flxLeftList.setDefaultUnit(kony.flex.DP);
    var CopyflxFlag0cfe0ad27a19042 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "CopyflxFlag0cfe0ad27a19042",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "1%",
        "skin": "sknflxroundflagBorder",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    CopyflxFlag0cfe0ad27a19042.setDefaultUnit(kony.flex.DP);
    var imgLeftFlag = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "imgLeftFlag",
        "isVisible": true,
        "skin": "slImage",
        "src": "bahrainflag.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyflxFlag0cfe0ad27a19042.add(imgLeftFlag);
    var lblLeftValue = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblLeftValue",
        "isVisible": true,
        "left": "45%",
        "skin": "sknlblWhitecariolight135",
        "text": "Jordan",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxLeftList.add(CopyflxFlag0cfe0ad27a19042, lblLeftValue);
    var flxRightList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxRightList",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_g8199e16696345ba9c065801b30cc5b4,
        "right": "0%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "41%",
        "zIndex": 1
    }, {}, {});
    flxRightList.setDefaultUnit(kony.flex.DP);
    var CopyflxFlag0bcd475c737524c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "CopyflxFlag0bcd475c737524c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "1%",
        "skin": "sknflxroundflagBorder",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    CopyflxFlag0bcd475c737524c.setDefaultUnit(kony.flex.DP);
    var imgRightFlag = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "imgRightFlag",
        "isVisible": true,
        "skin": "slImage",
        "src": "jordanflag.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyflxFlag0bcd475c737524c.add(imgRightFlag);
    var lblRightValue = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblRightValue",
        "isVisible": true,
        "right": "45%",
        "skin": "sknlblWhitecariolight135",
        "text": "Jordan",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRightList.add(CopyflxFlag0bcd475c737524c, lblRightValue);
    CopyflxListbox0fc89f8830c884e.add(CopylblSelectList0j6a93302e7e240, flxLeftList, flxRightList);
    var flxLeft = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "22%",
        "id": "flxLeft",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "skin": "slFbox",
        "top": "20%",
        "width": "40%",
        "zIndex": 2
    }, {}, {});
    flxLeft.setDefaultUnit(kony.flex.DP);
    var flxMobContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45dp",
        "id": "flxMobContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMobContainer.setDefaultUnit(kony.flex.DP);
    var txtEnteredValue = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknDebitCardNum",
        "height": "40dp",
        "id": "txtEnteredValue",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "0%",
        "maxTextLength": 13,
        "onDone": AS_TextField_e8b2b12e89f249a28ccf4d1dd44b6623,
        "onTextChange": AS_TextField_gd1f306c71054d59963ff87b57c8ded0,
        "placeholder": kony.i18n.getLocalizedString("i18.FxRateValue"),
        "secureTextEntry": false,
        "skin": "CopysknDebitCardNum0e03fa095ab3a47",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTxtplacehodler",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyflxLine0a4214bb8a7c146 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2dp",
        "id": "CopyflxLine0a4214bb8a7c146",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxGreyLine",
        "top": "40dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxLine0a4214bb8a7c146.setDefaultUnit(kony.flex.DP);
    CopyflxLine0a4214bb8a7c146.add();
    flxMobContainer.add(txtEnteredValue, CopyflxLine0a4214bb8a7c146);
    var lblActualValue = new kony.ui.Label({
        "id": "lblActualValue",
        "isVisible": true,
        "right": "0%",
        "skin": "sknlblWhitecarioRegular100",
        "text": "1 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "3%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxLeft.add(flxMobContainer, lblActualValue);
    var flxRight = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "22%",
        "id": "flxRight",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "5%",
        "skin": "slFbox",
        "top": "20%",
        "width": "40%",
        "zIndex": 2
    }, {}, {});
    flxRight.setDefaultUnit(kony.flex.DP);
    var CopyflxMobContainer0b316a4da99464b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45dp",
        "id": "CopyflxMobContainer0b316a4da99464b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxMobContainer0b316a4da99464b.setDefaultUnit(kony.flex.DP);
    var txtConvertedValue = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknDebitCardNum",
        "height": "40dp",
        "id": "txtConvertedValue",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "maxTextLength": 13,
        "onTextChange": AS_TextField_a0f9222e9df14d87b3970236ae3b8d66,
        "placeholder": kony.i18n.getLocalizedString("i18n.ConvertedValue"),
        "secureTextEntry": false,
        "skin": "CopysknDebitCardNum0e03fa095ab3a47",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTxtplacehodler",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyflxLine0fa3d061d922f45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2dp",
        "id": "CopyflxLine0fa3d061d922f45",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxGreyLine",
        "top": "40dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxLine0fa3d061d922f45.setDefaultUnit(kony.flex.DP);
    CopyflxLine0fa3d061d922f45.add();
    CopyflxMobContainer0b316a4da99464b.add(txtConvertedValue, CopyflxLine0fa3d061d922f45);
    var lblEqualentValue = new kony.ui.Label({
        "id": "lblEqualentValue",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblWhitecarioRegular100",
        "text": "1 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "3%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRight.add(CopyflxMobContainer0b316a4da99464b, lblEqualentValue);
    var segCalculateFX = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "imgFlag": "bahrainflag.png",
            "lblActualValue": "1 JOD",
            "lblConvertedValue": "2.08",
            "lblCurrency": "British Pound",
            "lblEqual": "=",
            "lblEqualentValue": "0.202 USA"
        }, {
            "imgFlag": "bahrainflag.png",
            "lblActualValue": "1 JOD",
            "lblConvertedValue": "2.08",
            "lblCurrency": "British Pound",
            "lblEqual": "=",
            "lblEqualentValue": "0.202 USA"
        }, {
            "imgFlag": "bahrainflag.png",
            "lblActualValue": "1 JOD",
            "lblConvertedValue": "2.08",
            "lblCurrency": "British Pound",
            "lblEqual": "=",
            "lblEqualentValue": "0.202 USA"
        }],
        "groupCells": false,
        "height": "58%",
        "id": "segCalculateFX",
        "isVisible": false,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowSkin": "sknSegHeaderColor",
        "rowTemplate": flxTmpCalculateFx,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "ffffff50",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "42%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxFlag": "flxFlag",
            "flxTmpCalculateFx": "flxTmpCalculateFx",
            "imgFlag": "imgFlag",
            "lblActualValue": "lblActualValue",
            "lblConvertedValue": "lblConvertedValue",
            "lblCurrency": "lblCurrency",
            "lblEqual": "lblEqual",
            "lblEqualentValue": "lblEqualentValue"
        },
        "width": "100%",
        "zIndex": 2
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblNotes = new kony.ui.Label({
        "bottom": "10%",
        "centerX": "50%",
        "id": "lblNotes",
        "isVisible": true,
        "skin": "sknLblWhite100",
        "text": kony.i18n.getLocalizedString("i18n.fxmsgg"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxScrlCalculateFx.add(CopyflxListbox0fc89f8830c884e, flxLeft, flxRight, segCalculateFX, lblNotes);
    frmFxRate.add(flxHeaderFxRate, flxTab, flxScrlExchangeRates, flxScrlCalculateFx);
};

function frmFxRateGlobals() {
    frmFxRate = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmFxRate,
        "enabledForIdleTimeout": false,
        "id": "frmFxRate",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": AS_Form_e5359e1ac2a04ec3a1aaa731dec71c4d,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_bf14ef6fec4e4876b2fefb84c0b03a37,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};