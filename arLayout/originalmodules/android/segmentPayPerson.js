function initializesegmentPayPerson() {
    Copycontainer06818659abbaf4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "Copycontainer06818659abbaf4b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    Copycontainer06818659abbaf4b.setDefaultUnit(kony.flex.DP);
    var lblContact = new kony.ui.Label({
        "id": "lblContact",
        "isVisible": true,
        "left": "5%",
        "skin": "skn",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel07d831b99b64f45 = new kony.ui.Label({
        "id": "CopyLabel07d831b99b64f45",
        "isVisible": true,
        "left": "5%",
        "top": 32,
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    Copycontainer06818659abbaf4b.add(lblContact, CopyLabel07d831b99b64f45, contactListDivider);
}