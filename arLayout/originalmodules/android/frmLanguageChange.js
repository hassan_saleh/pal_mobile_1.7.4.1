function addWidgetsfrmLanguageChange() {
    frmLanguageChange.setDefaultUnit(kony.flex.DP);
    var flxMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var flxLogo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxLogo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLogo.setDefaultUnit(kony.flex.DP);
    var imgLogo = new kony.ui.Image2({
        "height": "50%",
        "id": "imgLogo",
        "isVisible": true,
        "left": "2%",
        "skin": "slImage",
        "src": "logo.png",
        "top": "8%",
        "width": "50%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxLogo.add(imgLogo);
    var flxLangSelect = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70%",
        "id": "flxLangSelect",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "30%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLangSelect.setDefaultUnit(kony.flex.DP);
    var btnEnglish = new kony.ui.Button({
        "focusSkin": "slButtonWhite",
        "height": "13%",
        "id": "btnEnglish",
        "isVisible": true,
        "left": "6%",
        "onClick": AS_Button_b618a4a1a3f74290bcb406ef92919fd9,
        "skin": "slButtonWhite",
        "text": "English",
        "top": "11%",
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnArabic = new kony.ui.Button({
        "focusSkin": "slButtonWhite",
        "height": "13%",
        "id": "btnArabic",
        "isVisible": true,
        "left": "6.00%",
        "onClick": AS_Button_j805f01b6a654ec3af09f89febed4776,
        "skin": "slButtonWhite",
        "text": "عربي",
        "top": "6.00%",
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxRememberLang = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0%",
        "clipBounds": true,
        "height": "55%",
        "id": "flxRememberLang",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRememberLang.setDefaultUnit(kony.flex.DP);
    var imgRememMeTick = new kony.ui.Image2({
        "height": "30%",
        "id": "imgRememMeTick",
        "isVisible": false,
        "left": "10%",
        "skin": "slImage",
        "src": "tickboj.png",
        "top": "5%",
        "width": "10%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgRememMeUntick = new kony.ui.Image2({
        "height": "30%",
        "id": "imgRememMeUntick",
        "isVisible": false,
        "left": "10%",
        "skin": "slImage",
        "src": "untickboj.png",
        "top": "5%",
        "width": "10%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblRememChoice = new kony.ui.Label({
        "id": "lblRememChoice",
        "isVisible": true,
        "left": "25%",
        "skin": "sknLblRemTxt052c49",
        "text": "Remember My Choice",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRememCheckBox = new kony.ui.Label({
        "height": "20%",
        "id": "lblRememCheckBox",
        "isVisible": true,
        "left": "10%",
        "onTouchEnd": AS_Label_ca341f970fa14291827679b590236225,
        "skin": "sknLblRemCheckBox",
        "text": "q",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "6%",
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLangChangeTxt = new kony.ui.Label({
        "id": "lblLangChangeTxt",
        "isVisible": true,
        "left": "25%",
        "skin": "sknLblRemChoiceffffff",
        "text": "You can change language under app settings",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "21%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnDone = new kony.ui.Button({
        "bottom": "0%",
        "centerX": "50%",
        "focusSkin": "sknBtnDonec0f56afocus",
        "height": "20%",
        "id": "btnDone",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_b512ecc4dbf64ed7a40485194cc63f4b,
        "skin": "sknBtnDonec0f56afocus",
        "text": "Done",
        "top": "80%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxRememberLang.add(imgRememMeTick, imgRememMeUntick, lblRememChoice, lblRememCheckBox, lblLangChangeTxt, btnDone);
    var chckBoxRememberMe = new kony.ui.CheckBoxGroup({
        "height": "120dp",
        "id": "chckBoxRememberMe",
        "isVisible": false,
        "left": "10dp",
        "masterData": [
            ["cbg1", "Checkbox One"],
            ["cbg2", "Checkbox Two"],
            ["cbg3", "Checkbox Three"]
        ],
        "top": "227dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxLangSelect.add(btnEnglish, btnArabic, flxRememberLang, chckBoxRememberMe);
    flxMain.add(flxLogo, flxLangSelect);
    var flxLogin = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": true,
        "allowVerticalBounce": false,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": false,
        "id": "flxLogin",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
        "skin": "sknTransperentcommon",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLogin.setDefaultUnit(kony.flex.DP);
    var loginMainScreen = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "100%",
        "id": "loginMainScreen",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    loginMainScreen.setDefaultUnit(kony.flex.DP);
    var logoHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "15%",
        "id": "logoHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    logoHeader.setDefaultUnit(kony.flex.DP);
    var CopyimgLogo0dd32bdae6f4147 = new kony.ui.Image2({
        "centerX": "25%",
        "centerY": "50%",
        "height": "80%",
        "id": "CopyimgLogo0dd32bdae6f4147",
        "isVisible": true,
        "maxWidth": "40%",
        "skin": "sknslImage",
        "src": "logo.png",
        "top": "10%",
        "width": "50%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var logoContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "100dp",
        "id": "logoContainer",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    logoContainer.setDefaultUnit(kony.flex.DP);
    logoContainer.add();
    var lblVersion = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Register"
        },
        "centerX": "90%",
        "id": "lblVersion",
        "isVisible": false,
        "maxNumberOfLines": 1,
        "skin": "CopysknRegisterMobileBank0h92a6e4066af49",
        "text": "V 3.1",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "2%",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    logoHeader.add(CopyimgLogo0dd32bdae6f4147, logoContainer, lblVersion);
    var loginCardWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "54%",
        "id": "loginCardWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "skin": "sknslFbox",
        "top": "20%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 200
    }, {}, {});
    loginCardWrapper.setDefaultUnit(kony.flex.DP);
    var loginCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "100%",
        "id": "loginCard",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "skngenericCard",
        "top": "0%",
        "width": "100%",
        "zIndex": 99
    }, {}, {});
    loginCard.setDefaultUnit(kony.flex.DP);
    var usernameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "18%",
        "id": "usernameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknslFbox",
        "top": "0%",
        "width": "75%",
        "zIndex": 1
    }, {}, {});
    usernameContainer.setDefaultUnit(kony.flex.PERCENTAGE);
    var usernameTextField = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "0%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "usernameTextField",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "maxTextLength": 20,
        "onDone": AS_TextField_a8301298b8854888a9904d7230d62111,
        "onTextChange": AS_TextField_c9325b33f925429fa31c9b993584200c,
        "onTouchEnd": AS_TextField_gde2330ca7cd4c42b5eb5dda0d1bc028,
        "onTouchStart": AS_TextField_aa9aca97808947a396553184c773c344,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "95%",
        "zIndex": 20
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
        "onBeginEditing": AS_TextField_c0d84383f6894df280a312a783fd0d9e,
        "onEndEditing": AS_TextField_e13fd592e51e4ebcbb00fe5c665c148e,
        "placeholderSkin": "sknPlaceholderKA",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var tbxusernameTextField = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "0%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxusernameTextField",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "maxTextLength": 20,
        "onDone": AS_TextField_h968e35fdbe645158069a76df967ad26,
        "onTextChange": AS_TextField_b95beac9d45a4544b30c20055dcdd796,
        "onTouchEnd": AS_TextField_gde2330ca7cd4c42b5eb5dda0d1bc028,
        "onTouchStart": AS_TextField_aa9aca97808947a396553184c773c344,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "95%",
        "zIndex": 20
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
        "onBeginEditing": AS_TextField_dd3dec3321d64e5994b78f5fe77925d6,
        "onEndEditing": AS_TextField_e1c3e34a26a14b7dbc6bb761c83cb6cf,
        "placeholderSkin": "sknPlaceholderKA",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var borderBottom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "3%",
        "id": "borderBottom",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "skntextFieldDivider",
        "top": "90%",
        "width": "95%",
        "zIndex": 2
    }, {}, {});
    borderBottom.setDefaultUnit(kony.flex.DP);
    borderBottom.add();
    var lblChangeUserName = new kony.ui.Label({
        "id": "lblChangeUserName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknNumber",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "7dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 99
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblUserName = new kony.ui.Label({
        "id": "lblUserName",
        "isVisible": true,
        "left": "2%",
        "skin": "sknCaiRegWhite50Op",
        "text": kony.i18n.getLocalizedString("i18n.login.username"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblClose = new kony.ui.Label({
        "height": "100%",
        "id": "lblClose",
        "isVisible": false,
        "onTouchEnd": AS_Label_ac7dbf71f91a4306b4e64bbbd005d013,
        "right": "2%",
        "skin": "sknClose",
        "text": "O",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "10%",
        "zIndex": 20
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    usernameContainer.add(usernameTextField, tbxusernameTextField, borderBottom, lblChangeUserName, lblUserName, lblClose);
    var passwordContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "18%",
        "id": "passwordContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknslFbox",
        "top": "0%",
        "width": "75%",
        "zIndex": 1
    }, {}, {});
    passwordContainer.setDefaultUnit(kony.flex.DP);
    var lblPassword = new kony.ui.Label({
        "id": "lblPassword",
        "isVisible": true,
        "left": "2%",
        "skin": "sknCaiRegWhite50Op",
        "text": kony.i18n.getLocalizedString("i18n.login.password"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var passwordTextField = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "0%",
        "focusSkin": "skntxtMasked",
        "height": "60%",
        "id": "passwordTextField",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "maxTextLength": 20,
        "onDone": AS_TextField_d830873bcba34ae5a68388d7ef9a98e9,
        "onTextChange": AS_TextField_ad3ac5441c5b45668afa7d277e4f7415,
        "onTouchEnd": AS_TextField_j3223466667d4767a05c94e8307e7f3b,
        "onTouchStart": AS_TextField_a97a99d890794fdbae84264ec2f33229,
        "secureTextEntry": true,
        "skin": "skntxtMasked",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "95%",
        "zIndex": 20
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_a3ad0b8557574fa9b94d3f9638824c90,
        "onEndEditing": AS_TextField_h1734a6ca5e245b3b3275a10b135f4f2,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblClosePass = new kony.ui.Label({
        "height": "100%",
        "id": "lblClosePass",
        "isVisible": false,
        "onTouchEnd": AS_Label_ida4b61186bc4cc1b3007b80924f7d27,
        "right": "2%",
        "skin": "sknClose",
        "text": "O",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "10%",
        "zIndex": 20
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxbdrpwd = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "3%",
        "id": "flxbdrpwd",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "skntextFieldDivider",
        "top": "90%",
        "width": "95%",
        "zIndex": 2
    }, {}, {});
    flxbdrpwd.setDefaultUnit(kony.flex.DP);
    flxbdrpwd.add();
    passwordContainer.add(lblPassword, passwordTextField, lblClosePass, flxbdrpwd);
    var lblInvalidCredentialsKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblInvalidCredentialsKA",
        "isVisible": false,
        "skin": "sknInvalidCredKA",
        "text": kony.i18n.getLocalizedString("i18n.login.invalidCredentials"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "3%",
        "width": "86%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var signInButton = new kony.ui.Button({
        "centerX": "50.03%",
        "focusSkin": "sknprimaryAction",
        "height": "16%",
        "id": "signInButton",
        "isVisible": true,
        "onClick": AS_Button_a44e81171c1d4f15b20b16206e5c6943,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.login.loginToMobileBank"),
        "top": "8%",
        "width": "75%",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyflxRegisterMobileBanking0j972c4f144ed4e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "flexTransparent",
        "height": "10%",
        "id": "CopyflxRegisterMobileBanking0j972c4f144ed4e",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "onClick": AS_FlexContainer_aeadd47f83c74db3b15316eb54df20cf,
        "skin": "flexTransparent",
        "top": "2%",
        "width": "100%",
        "zIndex": 50
    }, {}, {});
    CopyflxRegisterMobileBanking0j972c4f144ed4e.setDefaultUnit(kony.flex.DP);
    var CopylblRegisterMobileBanking0d62e1dadcbca4d = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Register"
        },
        "centerX": "50%",
        "centerY": "50%",
        "id": "CopylblRegisterMobileBanking0d62e1dadcbca4d",
        "isVisible": true,
        "left": "0%",
        "skin": "lblsknWhite50transparent",
        "text": kony.i18n.getLocalizedString("i18n.login.registerForMobileBank"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxGreyLine",
        "top": "0dp",
        "width": "75%",
        "zIndex": 1
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    CopyflxRegisterMobileBanking0j972c4f144ed4e.add(CopylblRegisterMobileBanking0d62e1dadcbca4d, flxLine);
    var flxForgotP = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0.80%",
        "clipBounds": true,
        "height": "10%",
        "id": "flxForgotP",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "skin": "slFbox",
        "width": "95%",
        "zIndex": 201
    }, {}, {});
    flxForgotP.setDefaultUnit(kony.flex.DP);
    var forgotpassword = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "sknsecondaryActionFocus424242KA",
        "id": "forgotpassword",
        "isVisible": true,
        "onClick": AS_Button_c4e176f7b28c47c6852e95d3b2983021,
        "skin": "sknsecaction1",
        "text": kony.i18n.getLocalizedString("i18n.common.ForUnamPwd"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 202
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxForgotP.add(forgotpassword);
    var flxSwitchLanguage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxSwitchLanguage",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "skin": "slFbox",
        "top": "-1%",
        "width": "95%",
        "zIndex": 201
    }, {}, {});
    flxSwitchLanguage.setDefaultUnit(kony.flex.DP);
    var btnSwitchLanguage = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "sknsecondaryActionFocus424242KA",
        "id": "btnSwitchLanguage",
        "isVisible": true,
        "onClick": AS_Button_a7000baf54c74e8d9742c2568dd37703,
        "skin": "sknsecaction1",
        "text": kony.i18n.getLocalizedString("i18n.common.switchlanguage"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 202
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSwitchLanguage.add(btnSwitchLanguage);
    loginCard.add(usernameContainer, passwordContainer, lblInvalidCredentialsKA, signInButton, CopyflxRegisterMobileBanking0j972c4f144ed4e, flxForgotP, flxSwitchLanguage);
    loginCardWrapper.add(loginCard);
    var utilityLinks = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "15%",
        "id": "utilityLinks",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "40%",
        "skin": "sknslFbox",
        "top": "80%",
        "width": "100%",
        "zIndex": 100
    }, {}, {});
    utilityLinks.setDefaultUnit(kony.flex.DP);
    var contactLink = new kony.ui.Button({
        "centerY": "49.52%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "60dp",
        "id": "contactLink",
        "isVisible": true,
        "left": "3%",
        "onClick": AS_Button_328c114136424552afae15f35f5f3837,
        "skin": "sknsecondaryAction",
        "text": "E",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var atmLink = new kony.ui.Button({
        "centerY": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "60dp",
        "id": "atmLink",
        "isVisible": true,
        "onClick": AS_Button_99012586285d4090870332cef101ce50,
        "right": "55%",
        "skin": "sknsecondaryAction",
        "text": "B",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnPinLogin = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "60dp",
        "id": "btnPinLogin",
        "isVisible": true,
        "left": "55%",
        "onClick": AS_Button_ie729bbbcca24784a8c1aa45dfff67f7,
        "skin": "sknsecondaryAction",
        "text": "3",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnTouchID = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "60dp",
        "id": "btnTouchID",
        "isVisible": false,
        "onClick": AS_Button_bcc85b20219b45e8be7e1a01dbe48752,
        "right": "3%",
        "skin": "sknsecondaryAction",
        "text": "2",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnFaceID = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknsecondaryActionphase2Focus",
        "height": "60dp",
        "id": "btnFaceID",
        "isVisible": true,
        "onClick": AS_Button_f26f9f1edd6640328296c8c9b772ff84,
        "right": "3%",
        "skin": "sknsecondaryActionphase2",
        "text": "a",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    utilityLinks.add(contactLink, atmLink, btnPinLogin, btnTouchID, btnFaceID);
    var flxRegisterMobileBanking = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "focusSkin": "sknRegisterMobileBankingFocused",
        "height": "44dp",
        "id": "flxRegisterMobileBanking",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "onClick": AS_FlexContainer_bfc59840e10d49d195c637031a38e41a,
        "skin": "sknRegisterMobileBanking",
        "width": "100%",
        "zIndex": 50
    }, {}, {});
    flxRegisterMobileBanking.setDefaultUnit(kony.flex.DP);
    var lblRegisterMobileBanking = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Register"
        },
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblRegisterMobileBanking",
        "isVisible": true,
        "skin": "sknRegisterMobileBank",
        "text": kony.i18n.getLocalizedString("i18n.login.registerForMobileBank"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRegisterMobileBanking.add(lblRegisterMobileBanking);
    loginMainScreen.add(logoHeader, loginCardWrapper, utilityLinks, flxRegisterMobileBanking);
    flxLogin.add(loginMainScreen);
    frmLanguageChange.add(flxMain, flxLogin);
};

function frmLanguageChangeGlobals() {
    frmLanguageChange = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmLanguageChange,
        "enabledForIdleTimeout": false,
        "id": "frmLanguageChange",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_f5aec9db43a34c5fb6d58064f8999ebb,
        "preShow": AS_Form_hd17e154e6c14ed38742899a15ea9a11,
        "skin": "sknback"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_hdf96a8fa9e746ecbf48592f435d5e7c,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};