function addWidgetsfrmMore() {
    frmMore.setDefaultUnit(kony.flex.DP);
    var flxMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "93%",
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slPaymentSKin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var flxHeaderDashboard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeaderDashboard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1000
    }, {}, {});
    flxHeaderDashboard.setDefaultUnit(kony.flex.DP);
    var flxSide = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxSide",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "right": 0,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100dp",
        "zIndex": 1
    }, {}, {});
    flxSide.setDefaultUnit(kony.flex.DP);
    var btnMessage = new kony.ui.Button({
        "focusSkin": "BtnNotificationMail",
        "height": "50dp",
        "id": "btnMessage",
        "isVisible": false,
        "left": "10dp",
        "skin": "BtnNotificationMail",
        "text": "G",
        "top": "0dp",
        "width": "42dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnAccountInfo = new kony.ui.Button({
        "focusSkin": "btnUser",
        "height": "50dp",
        "id": "btnAccountInfo",
        "isVisible": true,
        "left": "54dp",
        "onClick": AS_Button_ab7db35499dc4ffeaf6347067848b147,
        "skin": "btnUser",
        "text": "F",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSide.add(btnMessage, btnAccountInfo);
    var imgLogo = new kony.ui.Image2({
        "height": "30dp",
        "id": "imgLogo",
        "isVisible": false,
        "left": "15dp",
        "skin": "slImage",
        "src": "logo03.png",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_c416ccb05fc048c4bc9c9a0b3c863483,
        "skin": "slFbox",
        "top": "0%",
        "width": "26%",
        "zIndex": 10
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.accounts.accounts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    flxHeaderDashboard.add(flxSide, imgLogo, flxBack);
    var flxOptions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "91%",
        "id": "flxOptions",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1000
    }, {}, {});
    flxOptions.setDefaultUnit(kony.flex.DP);
    var flxRow1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "28%",
        "clipBounds": true,
        "height": "28%",
        "id": "flxRow1",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "15%",
        "width": "40%",
        "zIndex": 1
    }, {}, {});
    flxRow1.setDefaultUnit(kony.flex.DP);
    var flxRight1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxRight1",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRight1.setDefaultUnit(kony.flex.DP);
    var btnStatistics = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonPaymentDashFocus",
        "height": "80dp",
        "id": "btnStatistics",
        "isVisible": true,
        "onClick": AS_Button_g0679e698a654b2785250dfb4bc8447e,
        "skin": "slButtonPaymentDash",
        "text": "D",
        "top": "0",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblStatistics = new kony.ui.Label({
        "centerX": "50%",
        "height": "14%",
        "id": "lblStatistics",
        "isVisible": true,
        "skin": "sknLblPayment",
        "text": kony.i18n.getLocalizedString("i18n.common.statistics"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "1%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRight1.add(btnStatistics, lblStatistics);
    flxRow1.add(flxRight1);
    var flxRow4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "20%",
        "clipBounds": true,
        "height": "28%",
        "id": "flxRow4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10%",
        "skin": "slFbox",
        "width": "40%",
        "zIndex": 1
    }, {}, {});
    flxRow4.setDefaultUnit(kony.flex.DP);
    var CopyflxRight0c014e541a81e4a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "CopyflxRight0c014e541a81e4a",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "50%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxRight0c014e541a81e4a.setDefaultUnit(kony.flex.DP);
    var btnExchangeRates = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonPaymentDashFocus",
        "height": "80dp",
        "id": "btnExchangeRates",
        "isVisible": true,
        "onClick": AS_Button_i26aba8b03004750a941af8bd92fc7d0,
        "skin": "slButtonPaymentDash",
        "text": "P",
        "top": "0",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblExchangeRates = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblExchangeRates",
        "isVisible": true,
        "skin": "sknLblPayment",
        "text": kony.i18n.getLocalizedString("i18n.fxRate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "1%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxRight0c014e541a81e4a.add(btnExchangeRates, lblExchangeRates);
    flxRow4.add(CopyflxRight0c014e541a81e4a);
    var flxRow2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "28%",
        "id": "flxRow2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "10%",
        "skin": "slFbox",
        "width": "40%",
        "zIndex": 1
    }, {}, {});
    flxRow2.setDefaultUnit(kony.flex.DP);
    var flxRight2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxRight2",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "50%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRight2.setDefaultUnit(kony.flex.DP);
    var btnInfo = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonPaymentDashFocus",
        "height": "80dp",
        "id": "btnInfo",
        "isVisible": true,
        "onClick": AS_Button_i333e36c4fe34c39a51291d8102e10e3,
        "skin": "slButtonPaymentDash",
        "text": "E",
        "top": "0",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lbleFawateer = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbleFawateer",
        "isVisible": true,
        "skin": "sknLblPayment",
        "text": kony.i18n.getLocalizedString("i18n.common.infonsupp"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "1%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRight2.add(btnInfo, lbleFawateer);
    flxRow2.add(flxRight2);
    var flxRow3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "85%",
        "clipBounds": true,
        "height": "28%",
        "id": "flxRow3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10%",
        "skin": "slFbox",
        "width": "40%",
        "zIndex": 1
    }, {}, {});
    flxRow3.setDefaultUnit(kony.flex.DP);
    var flxRight3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxRight3",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "50%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRight3.setDefaultUnit(kony.flex.DP);
    var btnPaymentH = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonPaymentDashFocus",
        "height": "80dp",
        "id": "btnPaymentH",
        "isVisible": true,
        "onClick": AS_Button_jf1cb6913c844c1c85e28cd983bd52a4,
        "skin": "slButtonPaymentDash",
        "text": "B",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblPaymentH = new kony.ui.Label({
        "centerX": "50%",
        "height": "14%",
        "id": "lblPaymentH",
        "isVisible": true,
        "skin": "sknLblPayment",
        "text": kony.i18n.getLocalizedString("i18n.common.branchATM"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "1%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRight3.add(btnPaymentH, lblPaymentH);
    flxRow3.add(flxRight3);
    var flxRow5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "75%",
        "centerY": "65%",
        "clipBounds": true,
        "height": "28%",
        "id": "flxRow5",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "width": "40%",
        "zIndex": 1
    }, {}, {});
    flxRow5.setDefaultUnit(kony.flex.DP);
    var flxRight5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxRight5",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "50%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRight5.setDefaultUnit(kony.flex.DP);
    var btnPayment5 = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBOJNewIconsttf",
        "height": "80dp",
        "id": "btnPayment5",
        "isVisible": true,
        "onClick": AS_Button_fcd46f0a769644f4ba5aa40f366d33c7,
        "skin": "sknBOJNewIconsttf",
        "text": "D",
        "top": "0",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblPayment5 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPayment5",
        "isVisible": true,
        "skin": "sknLblPayment",
        "text": kony.i18n.getLocalizedString("i18n.Cardless.NewCardless"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "1%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRight5.add(btnPayment5, lblPayment5);
    flxRow5.add(flxRight5);
    flxOptions.add(flxRow1, flxRow4, flxRow2, flxRow3, flxRow5);
    flxMain.add(flxHeaderDashboard, flxOptions);
    var flxFooter = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "7%",
        "id": "flxFooter",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0",
        "skin": "menu",
        "top": "93%",
        "width": "100%"
    }, {}, {});
    flxFooter.setDefaultUnit(kony.flex.DP);
    var FlxAccounts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxAccounts",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknfocusmenu",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    FlxAccounts.setDefaultUnit(kony.flex.DP);
    var img1 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img1",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_accounts_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label03174bff69bb54c = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label03174bff69bb54c",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": kony.i18n.getLocalizedString("i18n.my_money.accounts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnAccounts = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnCardFoc",
        "height": "50dp",
        "id": "btnAccounts",
        "isVisible": true,
        "onClick": AS_Button_ifec2c0acf3545b48d5d565d17b66aa0,
        "skin": "btnCard",
        "text": "H",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 14],
        "paddingInPixel": false
    }, {});
    FlxAccounts.add(img1, Label03174bff69bb54c, btnAccounts);
    var FlxTranfers = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxTranfers",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    FlxTranfers.setDefaultUnit(kony.flex.DP);
    var img2 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img2",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_t_and_p_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label02bec01fd5baf4c = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label02bec01fd5baf4c",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnTransfers = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnFooterDisablefocusskin",
        "height": "50dp",
        "id": "btnTransfers",
        "isVisible": true,
        "onClick": AS_Button_i81253531226436aa8a40fcf4c06bbda,
        "skin": "btnFooterDisable",
        "text": "G",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 14],
        "paddingInPixel": false
    }, {});
    FlxTranfers.add(img2, Label02bec01fd5baf4c, btnTransfers);
    var FlxPayment = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxPayment",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    FlxPayment.setDefaultUnit(kony.flex.DP);
    var imgBot = new kony.ui.Image2({
        "centerX": "50%",
        "height": "40dp",
        "id": "imgBot",
        "isVisible": false,
        "left": "13dp",
        "skin": "slImage",
        "src": "chaticonactive.png",
        "top": "4dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnPayment = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnCardFoc",
        "height": "50dp",
        "id": "btnPayment",
        "isVisible": true,
        "onClick": AS_Button_cec06ca4a5414687a01e9ae34bd152a9,
        "skin": "btnCard",
        "text": "i",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 14],
        "paddingInPixel": false
    }, {});
    var CopyLabel0i584dcaf2e8546 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel0i584dcaf2e8546",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": kony.i18n.getLocalizedString("i18n.alert.transfers"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlxPayment.add(imgBot, btnPayment, CopyLabel0i584dcaf2e8546);
    var FlxDeposits = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxDeposits",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    FlxDeposits.setDefaultUnit(kony.flex.DP);
    var img3 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img3",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_deposits_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label04221a71494e848 = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label04221a71494e848",
        "isVisible": false,
        "skin": "sknlblmenu",
        "text": kony.i18n.getLocalizedString("i18n.common.deposits"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "34dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnDeposit = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnCardFoc",
        "height": "50dp",
        "id": "btnDeposit",
        "isVisible": true,
        "skin": "btnCard",
        "text": "J",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlxDeposits.add(img3, Label04221a71494e848, btnDeposit);
    var flxEfawatercoom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxEfawatercoom",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "23%",
        "zIndex": 1
    }, {}, {});
    flxEfawatercoom.setDefaultUnit(kony.flex.DP);
    var btnEfawatercoom = new kony.ui.Button({
        "centerY": "46%",
        "focusSkin": "btnCardFoc",
        "height": "50dp",
        "id": "btnEfawatercoom",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_c26e1ef1b6e8474d94d8d0f73504cce7,
        "skin": "btnCard",
        "text": "x",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 14],
        "paddingInPixel": false
    }, {});
    var lblEfawatercoom = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblEfawatercoom",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": kony.i18n.getLocalizedString("i18n.footerBill.BillPayment"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEfawatercoom.add(btnEfawatercoom, lblEfawatercoom);
    var FlxMore = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxMore",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    FlxMore.setDefaultUnit(kony.flex.DP);
    var img4 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img4",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_more_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label0e5331028c2ef41 = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label0e5331028c2ef41",
        "isVisible": true,
        "skin": "sknlblFootertitleFocus",
        "text": kony.i18n.getLocalizedString("i18n.common.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnMore = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnFooterDisableaccount",
        "height": "50dp",
        "id": "btnMore",
        "isVisible": true,
        "skin": "btnFooterDisableaccount",
        "text": "K",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 14],
        "paddingInPixel": false
    }, {});
    FlxMore.add(img4, Label0e5331028c2ef41, btnMore);
    flxFooter.add(FlxAccounts, FlxTranfers, FlxPayment, FlxDeposits, flxEfawatercoom, FlxMore);
    frmMore.add(flxMain, flxFooter);
};

function frmMoreGlobals() {
    frmMore = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmMore,
        "enabledForIdleTimeout": false,
        "id": "frmMore",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_f106cfb3e9424eb697659cecb9b86068,
        "skin": "sknBackground"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_f76dfcf0a22d4be0a984caabb97d3d0a,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};