function addWidgetsfrmAddNewPrePayeeKA() {
    frmAddNewPrePayeeKA.setDefaultUnit(kony.flex.DP);
    var flxFormMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxFormMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxFormMain.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "18%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxTop = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45%",
        "id": "flxTop",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTop.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_f4f2d1566bb24d6abbb8b3304e913cf1,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "70%",
        "id": "lblTitle",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.AddNewBill"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNext = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNext",
        "isVisible": true,
        "left": "86%",
        "onTouchEnd": AS_Label_c87a0ddb7d624f4e84875c9b1b921309,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTop.add(flxBack, lblTitle, lblNext);
    var flxTab = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40%",
        "id": "flxTab",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "5%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTab.setDefaultUnit(kony.flex.DP);
    var flxContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "65%",
        "id": "flxContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFboxOuterRing",
        "top": "0dp",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxContent.setDefaultUnit(kony.flex.DP);
    var btnPostPaid = new kony.ui.Button({
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnPostPaid",
        "isVisible": true,
        "left": "0",
        "onClick": AS_Button_j641e271b9e54b35a2919f5a5c9ea52a,
        "skin": "slButtonWhiteTabDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.PostPaid"),
        "top": "0dp",
        "width": "50%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnPrePaid = new kony.ui.Button({
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnPrePaid",
        "isVisible": true,
        "left": "0",
        "onClick": AS_Button_c45596530c304e2680fc8218b8c16c11,
        "skin": "slButtonWhiteTab",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.PrePaid"),
        "top": "0dp",
        "width": "50%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxContent.add(btnPostPaid, btnPrePaid);
    flxTab.add(flxContent);
    flxHeader.add(flxTop, flxTab);
    var flxMain = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "82%",
        "horizontalScrollIndicator": true,
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var flxNickName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxNickName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "2.00%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNickName.setDefaultUnit(kony.flex.DP);
    var tbxNickName = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxNickName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "maxTextLength": 35,
        "onDone": AS_TextField_ff50e45cf78043e8bbd3f105662253ff,
        "onTextChange": AS_TextField_e78e7f5651054c8986f4459fc040e786,
        "onTouchEnd": AS_TextField_a2bd29e7d5944c77b59772db8d5e038b,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onEndEditing": AS_TextField_ibb30407d05042578bbd91bd92cc1727,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlineNickName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineNickName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "skin": "sknFlxGreyLine",
        "top": "93%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineNickName.setDefaultUnit(kony.flex.DP);
    flxUnderlineNickName.add();
    var lblNickName = new kony.ui.Label({
        "id": "lblNickName",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Bene.NickName"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNickName.add(tbxNickName, flxUnderlineNickName, lblNickName);
    var flxBillerCategory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxBillerCategory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxBillerCategory.setDefaultUnit(kony.flex.DP);
    var tbxBillerCategory = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxBillerCategory",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "maxTextLength": null,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlineBillerCategory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineBillerCategory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "skin": "sknFlxGreyLine",
        "top": "93%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineBillerCategory.setDefaultUnit(kony.flex.DP);
    flxUnderlineBillerCategory.add();
    var flxBillerCategoryHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxBillerCategoryHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "onClick": AS_FlexContainer_d0fb37bb5e4c415d9790a4e96e75c169,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxBillerCategoryHolder.setDefaultUnit(kony.flex.DP);
    var lblBillerCategory = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblBillerCategory",
        "isVisible": true,
        "left": "0%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.BillerCategory"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "88%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowBillerCategory = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowBillerCategory",
        "isVisible": true,
        "left": "91%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBillerCategoryHolder.add(lblBillerCategory, lblArrowBillerCategory);
    flxBillerCategory.add(tbxBillerCategory, flxUnderlineBillerCategory, flxBillerCategoryHolder);
    var flxBillerName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxBillerName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxBillerName.setDefaultUnit(kony.flex.DP);
    var tbxBillerName = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxBillerName",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "maxTextLength": null,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlineBillerName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineBillerName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "skin": "sknFlxGreyLine",
        "top": "93%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineBillerName.setDefaultUnit(kony.flex.DP);
    flxUnderlineBillerName.add();
    var flxBillerNameHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxBillerNameHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "onClick": AS_FlexContainer_b53ce2a33a57478eaf3af5c5a8ffbcfd,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxBillerNameHolder.setDefaultUnit(kony.flex.DP);
    var lblBillerName = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblBillerName",
        "isVisible": true,
        "left": "0%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.billerName"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "88%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowBillerName = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowBillerName",
        "isVisible": true,
        "left": "91%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEditBillerNameTitile = new kony.ui.Label({
        "id": "lblEditBillerNameTitile",
        "isVisible": false,
        "left": "0%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled80",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.billerName"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "-10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBillerNameHolder.add(lblBillerName, lblArrowBillerName, lblEditBillerNameTitile);
    flxBillerName.add(tbxBillerName, flxUnderlineBillerName, flxBillerNameHolder);
    var flxServiceType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxServiceType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxServiceType.setDefaultUnit(kony.flex.DP);
    var tbxServiceType = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxServiceType",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "maxTextLength": null,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlineServiceType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineServiceType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "skin": "sknFlxGreyLine",
        "top": "93%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineServiceType.setDefaultUnit(kony.flex.DP);
    flxUnderlineServiceType.add();
    var flxServiceTypeHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxServiceTypeHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "onClick": AS_FlexContainer_dea82a277ad249e3a30a1ac947dd2ae3,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxServiceTypeHolder.setDefaultUnit(kony.flex.DP);
    var lblServiceType = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblServiceType",
        "isVisible": true,
        "left": "0%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "88%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowServiceType = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowServiceType",
        "isVisible": true,
        "left": "91%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEditBillerServiceTypeTitle = new kony.ui.Label({
        "id": "lblEditBillerServiceTypeTitle",
        "isVisible": false,
        "left": "0%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled80",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "-10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxServiceTypeHolder.add(lblServiceType, lblArrowServiceType, lblEditBillerServiceTypeTitle);
    flxServiceType.add(tbxServiceType, flxUnderlineServiceType, flxServiceTypeHolder);
    var flxDenomination = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxDenomination",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0.00%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDenomination.setDefaultUnit(kony.flex.DP);
    var tbxDenomination = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxDenomination",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "maxTextLength": null,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlineDenomination = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineDenomination",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "skin": "sknFlxGreyLine",
        "top": "93%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineDenomination.setDefaultUnit(kony.flex.DP);
    flxUnderlineDenomination.add();
    var flxDenominationHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxDenominationHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "onClick": AS_FlexContainer_g2f0be3323644491a4c33711294986b9,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxDenominationHolder.setDefaultUnit(kony.flex.DP);
    var lblDenomination = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblDenomination",
        "isVisible": true,
        "left": "0%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "88%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowDenomination = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowDenomination",
        "isVisible": true,
        "left": "91%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEditBillerDenominationTitle = new kony.ui.Label({
        "id": "lblEditBillerDenominationTitle",
        "isVisible": false,
        "left": "0%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled80",
        "text": kony.i18n.getLocalizedString("i18n.prepaid.denomination"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "-10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDenominationHolder.add(lblDenomination, lblArrowDenomination, lblEditBillerDenominationTitle);
    flxDenomination.add(tbxDenomination, flxUnderlineDenomination, flxDenominationHolder);
    var flxBillerNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "80dp",
        "id": "flxBillerNumber",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxBillerNumber.setDefaultUnit(kony.flex.DP);
    var tbxBillerNumber = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "21%",
        "focusSkin": "sknTxtBox",
        "height": "54%",
        "id": "tbxBillerNumber",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "maxTextLength": 35,
        "onDone": AS_TextField_ib0d5dff72414cc98b068784423fe5d0,
        "onTextChange": AS_TextField_ce496fa840164973ba123d6e3cf29c51,
        "onTouchEnd": AS_TextField_c1403db7d267490d866fff0d95ad8c7d,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onEndEditing": AS_TextField_i02861539fa04e83b2bdc9f414b8ad43,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblBillerNumber = new kony.ui.Label({
        "id": "lblBillerNumber",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.BillerNumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "26%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblserviceTypeHint = new kony.ui.Label({
        "bottom": 0,
        "id": "lblserviceTypeHint",
        "isVisible": true,
        "left": "2%",
        "right": 0,
        "skin": "sknLblWhite100",
        "text": "jhkvtyyufg ",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "73%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxUnderlineBillerNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineBillerNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "skin": "sknFlxGreyLine",
        "top": "75%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineBillerNumber.setDefaultUnit(kony.flex.DP);
    flxUnderlineBillerNumber.add();
    flxBillerNumber.add(tbxBillerNumber, lblBillerNumber, lblserviceTypeHint, flxUnderlineBillerNumber);
    var flxAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50.04%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxAmount",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "2.00%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAmount.setDefaultUnit(kony.flex.DP);
    var tbxAmount = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxAmount",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "maxTextLength": null,
        "onDone": AS_TextField_b0353dcc28354bb0bc88a269253cfc62,
        "onTextChange": AS_TextField_ce2ef31edf574aa889ed1c2f8e38cc19,
        "onTouchEnd": AS_TextField_g1dd4ab139364e20b9e41c948a7cc907,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "100%",
        "zIndex": 2
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onEndEditing": AS_TextField_b8d1a98a62de4383ac38e785ffa22c2d,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblUnderline = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderline",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderline.setDefaultUnit(kony.flex.DP);
    lblUnderline.add();
    var flxAmountTypeHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxAmountTypeHolder",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAmountTypeHolder.setDefaultUnit(kony.flex.DP);
    var CopylblArrowPaymentMode0a6c9e8837c8e4b = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblArrowPaymentMode0a6c9e8837c8e4b",
        "isVisible": true,
        "left": "90%",
        "skin": "sknBackIconDisabled",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAmountTypeHolder.add(CopylblArrowPaymentMode0a6c9e8837c8e4b);
    var lblAmount = new kony.ui.Label({
        "id": "lblAmount",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Amount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAmount.add(tbxAmount, lblUnderline, flxAmountTypeHolder, lblAmount);
    var flxIDType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxIDType",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxIDType.setDefaultUnit(kony.flex.DP);
    var tbxIDType = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxIDType",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": null,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlineIDType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineIDType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineIDType.setDefaultUnit(kony.flex.DP);
    flxUnderlineIDType.add();
    var flxIDTypeHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxIDTypeHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_a047aa93737947a380d3b7659dd1b931,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxIDTypeHolder.setDefaultUnit(kony.flex.DP);
    var lblIDType = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblIDType",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": "ID Type",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowIDType = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowIDType",
        "isVisible": true,
        "left": "90%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIDTypeHolder.add(lblIDType, lblArrowIDType);
    flxIDType.add(tbxIDType, flxUnderlineIDType, flxIDTypeHolder);
    var flxBillerID = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxBillerID",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxBillerID.setDefaultUnit(kony.flex.DP);
    var tbxBillerID = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxBillerID",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": 35,
        "onDone": AS_TextField_d3e1f5fc7e814cfe9d1873476b769383,
        "onTextChange": AS_TextField_de8fe5e4b5c342ffb3711d57b765d501,
        "onTouchEnd": AS_TextField_g59610233d7a4562a2bb30b953a7956a,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onEndEditing": AS_TextField_cfb3186419c24b1db3a561c25d804905,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlineBillerID = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineBillerID",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineBillerID.setDefaultUnit(kony.flex.DP);
    flxUnderlineBillerID.add();
    var lblBillerID = new kony.ui.Label({
        "id": "lblBillerID",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": "ID Number",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBillerID.add(tbxBillerID, flxUnderlineBillerID, lblBillerID);
    var flxNationality = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxNationality",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNationality.setDefaultUnit(kony.flex.DP);
    var tbxNationality = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxNationality",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": null,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlineNationality = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineNationality",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineNationality.setDefaultUnit(kony.flex.DP);
    flxUnderlineNationality.add();
    var flxNationalityHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxNationalityHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_b8c3c56493d1467bae05394e4832555c,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxNationalityHolder.setDefaultUnit(kony.flex.DP);
    var lblNationality = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblNationality",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": "Nationality",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowNationality = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowNationality",
        "isVisible": true,
        "left": "90%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNationalityHolder.add(lblNationality, lblArrowNationality);
    flxNationality.add(tbxNationality, flxUnderlineNationality, flxNationalityHolder);
    var flxAddress = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxAddress",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAddress.setDefaultUnit(kony.flex.DP);
    var tbxAddress = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxAddress",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": 150,
        "onDone": AS_TextField_f1ac2ac9f6fb440a90e246f54cf51fd7,
        "onTextChange": AS_TextField_f5089583a899463cb3089cbd705749c7,
        "onTouchEnd": AS_TextField_ge7a0f5f5e0d42b2997eaaa01f0ae946,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onEndEditing": AS_TextField_a837e8a0b1c749ee9385f93b4ac516d3,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlineAddress = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineAddress",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineAddress.setDefaultUnit(kony.flex.DP);
    flxUnderlineAddress.add();
    var lblAddress = new kony.ui.Label({
        "id": "lblAddress",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": "Address",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAddress.add(tbxAddress, flxUnderlineAddress, lblAddress);
    var flxPhoneNo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxPhoneNo",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxPhoneNo.setDefaultUnit(kony.flex.DP);
    var tbxPhoneNo = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxPhoneNo",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": 10,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlinePhoneNo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlinePhoneNo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlinePhoneNo.setDefaultUnit(kony.flex.DP);
    flxUnderlinePhoneNo.add();
    var lblPhoneNo = new kony.ui.Label({
        "id": "lblPhoneNo",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": "Mobile Number",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPhoneNo.add(tbxPhoneNo, flxUnderlinePhoneNo, lblPhoneNo);
    var flxSpace = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxSpace",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSpace.setDefaultUnit(kony.flex.DP);
    flxSpace.add();
    flxMain.add(flxNickName, flxBillerCategory, flxBillerName, flxServiceType, flxDenomination, flxBillerNumber, flxAmount, flxIDType, flxBillerID, flxNationality, flxAddress, flxPhoneNo, flxSpace);
    flxFormMain.add(flxHeader, flxMain);
    var flxConfirmPopUp = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxConfirmPopUp",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "sknFlexBlueBen",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxConfirmPopUp.setDefaultUnit(kony.flex.DP);
    var flxConfirmHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirmHeader.setDefaultUnit(kony.flex.DP);
    var flxHeaderBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxHeaderBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_ec48bfba94c64fc6bb16f30c6919079a,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxHeaderBack.setDefaultUnit(kony.flex.DP);
    var lblHeaderBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblHeaderBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblHeaderBack = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblHeaderBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeaderBack.add(lblHeaderBackIcon, lblHeaderBack);
    var lblHeaderTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": true,
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnClose = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknDeleteFocus",
        "height": "80%",
        "id": "btnClose",
        "isVisible": true,
        "left": "85%",
        "onClick": AS_Button_edbc1adefe694db69d2155efa3041eee,
        "skin": "sknBtnBack",
        "text": "O",
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxConfirmHeader.add(flxHeaderBack, lblHeaderTitle, btnClose);
    var flxImpDetail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "25%",
        "id": "flxImpDetail",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxImpDetail.setDefaultUnit(kony.flex.DP);
    var flxIcon1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxIcon1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxToIcon",
        "top": "7%",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxIcon1.setDefaultUnit(kony.flex.DP);
    var lblInitial = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblInitial",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "text": "OF",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIcon1.add(lblInitial);
    var lblBillerNamePopup = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBillerNamePopup",
        "isVisible": true,
        "skin": "sknBeneTitle",
        "text": "Orange Fixed",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "3%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBillerNumPopup = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBillerNumPopup",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblNextDisabled",
        "text": "064385497",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxImpDetail.add(flxIcon1, lblBillerNamePopup, lblBillerNumPopup);
    var flxOtherDetails = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "50%",
        "horizontalScrollIndicator": true,
        "id": "flxOtherDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxOtherDetails.setDefaultUnit(kony.flex.DP);
    var flxBillerCategoryPopup = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxBillerCategoryPopup",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBillerCategoryPopup.setDefaultUnit(kony.flex.DP);
    var lblConfirmBillerCategoryTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmBillerCategoryTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.BillerCategory"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmBillerCategory = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmBillerCategory",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": "TDFfXYD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBillerCategoryPopup.add(lblConfirmBillerCategoryTitle, lblConfirmBillerCategory);
    var flxServiceTypePopup = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxServiceTypePopup",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxServiceTypePopup.setDefaultUnit(kony.flex.DP);
    var lblConfirmServiceTypeTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmServiceTypeTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmServiceType = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmServiceType",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": "Fixed",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxServiceTypePopup.add(lblConfirmServiceTypeTitle, lblConfirmServiceType);
    var flxIDTypePopup = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxIDTypePopup",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxIDTypePopup.setDefaultUnit(kony.flex.DP);
    var lblConfirmIDTypeTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmIDTypeTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": "ID Type",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmIDType = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmIDType",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": "National ID",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIDTypePopup.add(lblConfirmIDTypeTitle, lblConfirmIDType);
    var flxIDNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxIDNumber",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxIDNumber.setDefaultUnit(kony.flex.DP);
    var lblConfirmIDNumberTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmIDNumberTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": "ID Number",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmIDNumber = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmIDNumber",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": "38129048293",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIDNumber.add(lblConfirmIDNumberTitle, lblConfirmIDNumber);
    var flxNationalityPopup = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxNationalityPopup",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNationalityPopup.setDefaultUnit(kony.flex.DP);
    var lblConfirmNationalityTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmNationalityTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": "Nationality",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmNationality = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmNationality",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": "Jordan",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNationalityPopup.add(lblConfirmNationalityTitle, lblConfirmNationality);
    var flxAddressPopup = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxAddressPopup",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAddressPopup.setDefaultUnit(kony.flex.DP);
    var lblConfirmAddressTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmAddressTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": "Address",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmAddress = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmAddress",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": "Amman - Haidar Abad",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAddressPopup.add(lblConfirmAddressTitle, lblConfirmAddress);
    var flxNickNamePopup = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxNickNamePopup",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNickNamePopup.setDefaultUnit(kony.flex.DP);
    var lblConfirmNickNameTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmNickNameTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Bene.NickName"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblConfirmNickName = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmNickName",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": "Some random nick name",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNickNamePopup.add(lblConfirmNickNameTitle, lblConfirmNickName);
    var flxAmountConf = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxAmountConf",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAmountConf.setDefaultUnit(kony.flex.DP);
    var lblAmountTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblAmountTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Amount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAmountConf = new kony.ui.Label({
        "height": "50%",
        "id": "lblAmountConf",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAmountConf.add(lblAmountTitle, lblAmountConf);
    var flxDenominationConf = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxDenominationConf",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDenominationConf.setDefaultUnit(kony.flex.DP);
    var lblDenominationTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblDenominationTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.prepaid.denomination"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDenominationConf = new kony.ui.Label({
        "height": "50%",
        "id": "lblDenominationConf",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDenominationConf.add(lblDenominationTitle, lblDenominationConf);
    flxOtherDetails.add(flxBillerCategoryPopup, flxServiceTypePopup, flxIDTypePopup, flxIDNumber, flxNationalityPopup, flxAddressPopup, flxNickNamePopup, flxAmountConf, flxDenominationConf);
    var flxButtonHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "18%",
        "id": "flxButtonHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxButtonHolder.setDefaultUnit(kony.flex.DP);
    var btnConfirm = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "50dp",
        "id": "btnConfirm",
        "isVisible": true,
        "onClick": AS_Button_ia7e5f7d4aab4974a5b785f185433b65,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxButtonHolder.add(btnConfirm);
    var lblLanguage = new kony.ui.Label({
        "id": "lblLanguage",
        "isVisible": false,
        "left": "-140dp",
        "skin": "slLabel",
        "text": "eng",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-700dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblType = new kony.ui.Label({
        "id": "lblType",
        "isVisible": false,
        "left": "-130dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-690dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConfirmPopUp.add(flxConfirmHeader, flxImpDetail, flxOtherDetails, flxButtonHolder, lblLanguage, lblType);
    var lblHiddenAmount = new kony.ui.Label({
        "id": "lblHiddenAmount",
        "isVisible": false,
        "left": "245dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "448dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    frmAddNewPrePayeeKA.add(flxFormMain, flxConfirmPopUp, lblHiddenAmount);
};

function frmAddNewPrePayeeKAGlobals() {
    frmAddNewPrePayeeKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmAddNewPrePayeeKA,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmAddNewPrePayeeKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_d82a391412284033ae1fa6653ec594c5,
        "preShow": AS_Form_ff4e1e3ec9274d0ab2e7e3576d5c25db,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": true,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_f41f939b6dcd4dc19a554c68fc969abb,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};