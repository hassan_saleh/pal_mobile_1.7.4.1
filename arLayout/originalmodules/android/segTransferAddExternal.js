function initializesegTransferAddExternal() {
    Copycontainer03f00119dae464d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "Copycontainer03f00119dae464d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknyourAccountCard"
    }, {}, {});
    Copycontainer03f00119dae464d.setDefaultUnit(kony.flex.DP);
    var lblContact = new kony.ui.Label({
        "id": "lblContact",
        "isVisible": true,
        "left": "5%",
        "skin": "skn",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblType = new kony.ui.Label({
        "id": "lblType",
        "isVisible": false,
        "left": "5%",
        "skin": "sknRegisterMobileBank",
        "top": 32,
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumberKA = new kony.ui.Label({
        "id": "lblAccountNumberKA",
        "isVisible": false,
        "left": "224dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRowSeparator = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "lblRowSeparator",
        "isVisible": true,
        "left": "4%",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    Copycontainer03f00119dae464d.add(lblContact, lblType, lblAccountNumberKA, lblRowSeparator);
}