function initializeCopyFBox0764caac34d1b46() {
    CopyFBox0764caac34d1b46 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "CopyFBox0764caac34d1b46",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    CopyFBox0764caac34d1b46.setDefaultUnit(kony.flex.DP);
    var contactListLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "contactListLabel",
        "isVisible": true,
        "left": "5%",
        "text": "Label",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknsegmentDivider",
        "top": "49dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "containerWeight": 100
    }, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    CopyFBox0764caac34d1b46.add(contactListLabel, contactListDivider);
}