function initializetmpAccountLandingKA() {
    yourAccount1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknflxTransprnt",
        "height": "90dp",
        "id": "yourAccount1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "onClick": AS_FlexContainer_b70ab343f2154834a459e23563641473,
        "skin": "flxsegSknblue",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    yourAccount1.setDefaultUnit(kony.flex.DP);
    var nameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "nameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknslFbox",
        "top": "0%",
        "width": "85%",
        "zIndex": 1
    }, {}, {});
    nameContainer.setDefaultUnit(kony.flex.DP);
    var dummyAccountName = new kony.ui.Label({
        "id": "dummyAccountName",
        "isVisible": false,
        "left": "25dp",
        "maxWidth": "90%",
        "skin": "sknaccountName",
        "top": "22dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var dummyAccountNumber = new kony.ui.Label({
        "id": "dummyAccountNumber",
        "isVisible": false,
        "left": "35dp",
        "maxWidth": "90%",
        "skin": "sknaccountName",
        "top": "32dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblColorKA = new kony.ui.Label({
        "bottom": "0dp",
        "height": "100%",
        "id": "lblColorKA",
        "isVisible": false,
        "left": "1dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "6dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var isPFMLabel = new kony.ui.Label({
        "id": "isPFMLabel",
        "isVisible": false,
        "right": "12dp",
        "skin": "sknaccountAmount",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBankName = new kony.ui.Label({
        "id": "lblBankName",
        "isVisible": true,
        "left": "15dp",
        "maxWidth": "90%",
        "skin": "sknsegmentHeaderText",
        "top": "29dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountID = new kony.ui.Label({
        "id": "lblAccountID",
        "isVisible": false,
        "left": "19dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxSet = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxSet",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "15dp",
        "skin": "slFbox",
        "top": "16%",
        "width": "92%",
        "zIndex": 10
    }, {}, {});
    flxSet.setDefaultUnit(kony.flex.DP);
    var nameAccount1 = new kony.ui.RichText({
        "height": "98%",
        "id": "nameAccount1",
        "isVisible": true,
        "left": "0%",
        "skin": "sknRT100",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var nameAccount12 = new kony.ui.Label({
        "id": "nameAccount12",
        "isVisible": false,
        "left": "0%",
        "skin": "sknlbleType11",
        "text": "Account Name",
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccNumber = new kony.ui.Label({
        "id": "lblAccNumber",
        "isVisible": false,
        "left": "5dp",
        "skin": "lblAccountType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMobile = new kony.ui.Label({
        "id": "lblMobile",
        "isVisible": false,
        "left": "5dp",
        "skin": "lblPhone",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSet.add(nameAccount1, nameAccount12, lblAccNumber, lblMobile);
    nameContainer.add(dummyAccountName, dummyAccountNumber, lblColorKA, isPFMLabel, lblBankName, lblAccountID, flxSet);
    var amtOutsatndingBal = new kony.ui.Label({
        "id": "amtOutsatndingBal",
        "isVisible": false,
        "right": "12dp",
        "skin": "sknaccountAmount",
        "text": "$00.00",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var amountcurrBal = new kony.ui.Label({
        "id": "amountcurrBal",
        "isVisible": false,
        "right": "12dp",
        "skin": "sknaccountAmount",
        "text": "$00.00",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var typeAccount = new kony.ui.Label({
        "bottom": "12dp",
        "id": "typeAccount",
        "isVisible": false,
        "right": "12dp",
        "skin": "sknaccountAvailableBalanceLabel",
        "text": "Available Balance",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var typeKA = new kony.ui.Label({
        "bottom": "2dp",
        "centerX": "50%",
        "id": "typeKA",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknaccountAvailableBalanceLabel",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRowSeparator = new kony.ui.Label({
        "bottom": "0dp",
        "height": "10px",
        "id": "lblRowSeparator",
        "isVisible": true,
        "left": "-0.03%",
        "skin": "CopyslLabel02015112aac2345",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100.03%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Label0bed5640cdb4b46 = new kony.ui.Label({
        "id": "Label0bed5640cdb4b46",
        "isVisible": false,
        "right": 20,
        "skin": "blsegtextrightsmall",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "31dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnNav = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnRightArw",
        "height": "60dp",
        "id": "btnNav",
        "isVisible": false,
        "onClick": AS_Button_h48f571d5a8746eb8cf3214344000957,
        "right": "5dp",
        "skin": "btnRightArw",
        "text": "L",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxamount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "48dp",
        "id": "flxamount",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "15dp",
        "skin": "slFbox",
        "top": "34%",
        "width": "75%",
        "zIndex": 1
    }, {}, {});
    flxamount.setDefaultUnit(kony.flex.DP);
    var amountAccount1 = new kony.ui.Label({
        "height": "48dp",
        "id": "amountAccount1",
        "isVisible": true,
        "right": "0dp",
        "skin": "lblAmount",
        "top": "3%",
        "width": "100%",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCurrency = new kony.ui.Label({
        "height": "48dp",
        "id": "lblCurrency",
        "isVisible": false,
        "right": "0dp",
        "skin": "lblAmountCurrency",
        "text": "JOD",
        "top": "1%",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxamount.add(amountAccount1, lblCurrency);
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknflxLineblueop",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    var btnNav1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "btnNav1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5dp",
        "skin": "slFbox",
        "top": 0,
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    btnNav1.setDefaultUnit(kony.flex.DP);
    var lblIncommingRing = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "60%",
        "id": "lblIncommingRing",
        "isVisible": true,
        "skin": "sknBOJttfLightBlue",
        "text": "s",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblIncommingRing0ac2c9e9c52d147 = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "60%",
        "id": "CopylblIncommingRing0ac2c9e9c52d147",
        "isVisible": true,
        "skin": "CopysknBOJttfLightBlue0f2f2a5a952b945",
        "text": kony.i18n.getLocalizedString("i18n.common.reverseback"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    btnNav1.add(lblIncommingRing, CopylblIncommingRing0ac2c9e9c52d147);
    var flxCardless = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxCardless",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_i7db0d32e0214f83941c452994e4b025,
        "right": "15%",
        "skin": "slFbox",
        "top": 0,
        "width": "14%",
        "zIndex": 5
    }, {}, {});
    flxCardless.setDefaultUnit(kony.flex.DP);
    var lblCardlessIcon = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "60%",
        "id": "lblCardlessIcon",
        "isVisible": true,
        "skin": "sknBlueRNDIcon70",
        "text": "D",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCardless.add(lblCardlessIcon);
    yourAccount1.add(nameContainer, amtOutsatndingBal, amountcurrBal, typeAccount, typeKA, lblRowSeparator, Label0bed5640cdb4b46, btnNav, flxamount, flxLine, btnNav1, flxCardless);
}