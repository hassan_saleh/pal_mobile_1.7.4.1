//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:40 EEST 2020
function addWidgetsfrmFacialAuthEnableAr() {
frmFacialAuthEnable.setDefaultUnit(kony.flex.DP);
var flxMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxHeaderKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "50dp",
"id": "flxHeaderKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntitleBarGradient",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeaderKA.setDefaultUnit(kony.flex.DP);
var flxHeaderContainerKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50.00%",
"clipBounds": true,
"height": "50dp",
"id": "flxHeaderContainerKA",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"top": "0dp",
"width": "280dp",
"zIndex": 1
}, {}, {});
flxHeaderContainerKA.setDefaultUnit(kony.flex.DP);
var imgAuthMode1KA = new kony.ui.Image2({
"centerY": "50%",
"height": "40dp",
"id": "imgAuthMode1KA",
"isVisible": true,
"right": "0%",
"skin": "slImage",
"src": "touchiconactive.png",
"width": "45dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxLine1KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "3.50%",
"id": "flxLine1KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "-2dp",
"skin": "sknFlxLine",
"top": "0dp",
"width": "25%",
"zIndex": 1
}, {}, {});
flxLine1KA.setDefaultUnit(kony.flex.DP);
flxLine1KA.add();
var imgAuthMode2KA = new kony.ui.Image2({
"centerY": "50%",
"height": "40dp",
"id": "imgAuthMode2KA",
"isVisible": true,
"right": "0dp",
"skin": "slImage",
"src": "pin.png",
"width": "45dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxLine2KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "3.50%",
"id": "flxLine2KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "-3dp",
"skin": "sknFlxLine",
"top": "0dp",
"width": "25%",
"zIndex": 1
}, {}, {});
flxLine2KA.setDefaultUnit(kony.flex.DP);
flxLine2KA.add();
var imgAuthMode3KA = new kony.ui.Image2({
"centerY": "50%",
"height": "40dp",
"id": "imgAuthMode3KA",
"isVisible": true,
"right": "-6dp",
"skin": "slImage",
"src": "face.png",
"width": "45dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxHeaderContainerKA.add( imgAuthMode3KA, flxLine2KA, imgAuthMode2KA, flxLine1KA,imgAuthMode1KA);
var flxHeaderText = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "50dp",
"id": "flxHeaderText",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"top": "10dp",
"width": "280dp",
"zIndex": 1
}, {}, {});
flxHeaderText.setDefaultUnit(kony.flex.DP);
var Label0i10bb778338948 = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "Label0i10bb778338948",
"isVisible": true,
"right": "0dp",
"skin": "sknnavBarTitle",
"text": kony.i18n.getLocalizedString("i18n.EnableFaceIdHeader"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHeaderText.add(Label0i10bb778338948);
flxHeaderKA.add(flxHeaderContainerKA, flxHeaderText);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "48%",
"clipBounds": true,
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "50dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var lblSetFaceID = new kony.ui.Label({
"centerX": "50%",
"height": "10%",
"id": "lblSetFaceID",
"isVisible": true,
"right": "0%",
"skin": "sknFaceIDHeader",
"text": kony.i18n.getLocalizedString("i18n.FACEID"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lbldesc = new kony.ui.Label({
"centerX": "50%",
"height": "8%",
"id": "lbldesc",
"isVisible": true,
"right": "0dp",
"skin": "sknLatoRegularlbl",
"text": kony.i18n.getLocalizedString("i18n.xyzBankFaceIDauthenticationfor"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "21%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lbldesc2 = new kony.ui.Label({
"centerX": "50%",
"height": "8%",
"id": "lbldesc2",
"isVisible": true,
"right": "0dp",
"skin": "sknLatoRegularlbl",
"text": kony.i18n.getLocalizedString("i18n.secureandconveinentsignin."),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "30%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var imgsetFaceID = new kony.ui.Image2({
"centerX": "50.00%",
"height": "39%",
"id": "imgsetFaceID",
"isVisible": true,
"right": "0dp",
"skin": "slImage",
"src": "facebig.png",
"top": "47%",
"width": "28%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxHeader.add(lblSetFaceID, lbldesc, lbldesc2, imgsetFaceID);
var flxSetFaceID = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "50%",
"id": "flxSetFaceID",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSetFaceID.setDefaultUnit(kony.flex.DP);
var lblfooterdec1 = new kony.ui.Label({
"centerX": "51.02%",
"id": "lblfooterdec1",
"isVisible": true,
"right": "0%",
"skin": "sknLatoRegularlbl",
"text": kony.i18n.getLocalizedString("i18n.UsingFACEIDisoptional.YoucanturnitONorOFFatanytimefromsettingsmenu."),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": "88%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblTips = new kony.ui.Label({
"centerX": "50.01%",
"id": "lblTips",
"isVisible": true,
"skin": "sknLatoRegularlbl",
"text": kony.i18n.getLocalizedString("i18n.Tips"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "30%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblTipsDesc = new kony.ui.Label({
"centerX": "50.00%",
"id": "lblTipsDesc",
"isVisible": true,
"skin": "sknLatoRegularlbl",
"text": kony.i18n.getLocalizedString("i18n.Holdyourphone8to20inchesfromyourface.PlaceyourfaceincircleandBlinkyoureyes."),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": "94%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnEnableFaceID = new kony.ui.Button({
"centerX": "49.97%",
"focusSkin": "sknprimaryActionFocus",
"height": "42dp",
"id": "btnEnableFaceID",
"isVisible": true,
"onClick": AS_Button_b69d69548b7a43558038b31ed3598c63,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.ENABLEFACEID"),
"top": "65%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnLater = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "10%",
"id": "btnLater",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_ba8c464b75b243589f143b62870823bf,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.DoitLater"),
"top": "82%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSetFaceID.add(lblfooterdec1, lblTips, lblTipsDesc, btnEnableFaceID, btnLater);
flxMain.add(flxHeaderKA, flxHeader, flxSetFaceID);
frmFacialAuthEnable.add(flxMain);
};
function frmFacialAuthEnableGlobalsAr() {
frmFacialAuthEnableAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmFacialAuthEnableAr,
"bounces": false,
"enabledForIdleTimeout": true,
"id": "frmFacialAuthEnable",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_abaff60417014dd2b4d5f8945f5023b8,
"skin": "sknfrmbkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_ef9ceaacc5df4dbc9fa8db7689d0d135,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
