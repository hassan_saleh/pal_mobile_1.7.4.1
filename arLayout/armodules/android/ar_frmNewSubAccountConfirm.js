//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:41 EEST 2020
function addWidgetsfrmNewSubAccountConfirmAr() {
frmNewSubAccountConfirm.setDefaultUnit(kony.flex.DP);
var flxnewSubAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxnewSubAccount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxnewSubAccount.setDefaultUnit(kony.flex.DP);
var lblJoMoPay = new kony.ui.Label({
"centerX": "50%",
"centerY": "45%",
"id": "lblJoMoPay",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.transfers.CONFIRM"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnBack = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0e73a02c4810645",
"height": "100%",
"id": "btnBack",
"isVisible": false,
"left": "0%",
"onClick": AS_Button_d3e393accbca4a2888ade40c48e70a00,
"skin": "CopyslButtonGlossBlue0e73a02c4810645",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0%",
"width": "15%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_d27ddd881c2b4a3496e57169b09d003c,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
flxnewSubAccount.add(lblJoMoPay, btnBack, flxBack);
var flxTransfer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxTransfer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "15%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTransfer.setDefaultUnit(kony.flex.DP);
var lblTransferTypeStatic = new kony.ui.Label({
"height": "35%",
"id": "lblTransferTypeStatic",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.common.accountType"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxTransferType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxTransferType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "3%",
"skin": "slFbox",
"width": "96%",
"zIndex": 1
}, {}, {});
flxTransferType.setDefaultUnit(kony.flex.DP);
var lblFre = new kony.ui.Label({
"id": "lblFre",
"isVisible": true,
"right": "2%",
"skin": "sknLblWhike125",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTransferType.add(lblFre);
flxTransfer.add(lblTransferTypeStatic, flxTransferType);
var flxAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxAccount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "28%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccount.setDefaultUnit(kony.flex.DP);
var lblAccountStaticText = new kony.ui.Label({
"height": "35%",
"id": "lblAccountStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.common.currencyType"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCurr = new kony.ui.Label({
"id": "lblCurr",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhike125",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAccount.add(lblAccountStaticText, lblCurr);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonWhite",
"height": "8%",
"id": "btnConfirm",
"isVisible": true,
"right": "10%",
"onClick": AS_Button_hc140283e13d49269ba4347b31663091,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.P2P.confirm"),
"top": "82%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxCheckBox1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxCheckBox1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "50%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxCheckBox1.setDefaultUnit(kony.flex.DP);
var lblCheck1 = new kony.ui.Label({
"id": "lblCheck1",
"isVisible": true,
"right": "0dp",
"onTouchStart": AS_Label_baa7392de904447c98af90b421ddff8d,
"skin": "sknBOJttfwhitee150",
"text": "p",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxtnc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxtnc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "15%",
"onClick": AS_RichText_he340b8241e3466eb1e0049dacd979c2,
"skin": "slFbox",
"top": "0dp",
"width": "85%",
"zIndex": 1
}, {}, {});
flxtnc.setDefaultUnit(kony.flex.DP);
var richtxtTermsandConditions = new kony.ui.RichText({
"id": "richtxtTermsandConditions",
"isVisible": true,
"right": "0%",
"skin": "sknrichTxtWhite100",
"text": "<u>RichText</u>",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxtnc.add(richtxtTermsandConditions);
flxCheckBox1.add(lblCheck1, flxtnc);
var Button0e0efca8e93bc4a = new kony.ui.Button({
"focusSkin": "slButtonGlossRed",
"height": "1dp",
"id": "Button0e0efca8e93bc4a",
"isVisible": false,
"right": "150%",
"onClick": AS_Button_d346385a5e8b43c0a65b6199f8a3606a,
"skin": "slButtonGlossBlue",
"text": "Button",
"top": "250%",
"width": "1dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
frmNewSubAccountConfirm.add(flxnewSubAccount, flxTransfer, flxAccount, btnConfirm, flxCheckBox1, Button0e0efca8e93bc4a);
};
function frmNewSubAccountConfirmGlobalsAr() {
frmNewSubAccountConfirmAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmNewSubAccountConfirmAr,
"enabledForIdleTimeout": true,
"id": "frmNewSubAccountConfirm",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_be65bad5ec5c4613b2d2a21ed0f51055,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
