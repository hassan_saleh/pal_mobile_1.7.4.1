//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:41 EEST 2020
function initializetmpExchangeRatesAr() {
    flxExchangeRatestmpAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "75dp",
        "id": "flxExchangeRatestmp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxExchangeRatestmpAr.setDefaultUnit(kony.flex.DP);
    var flxMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "4%",
        "skin": "slFbox",
        "width": "28%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var flxFlag = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "30dp",
        "id": "flxFlag",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "10%",
        "skin": "sknflxroundflag",
        "width": "30dp",
        "zIndex": 1
    }, {}, {});
    flxFlag.setDefaultUnit(kony.flex.DP);
    var imgFlag = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "105%",
        "id": "imgFlag",
        "isVisible": true,
        "skin": "slImage",
        "src": "bahrainflag.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxFlag.add(imgFlag);
    flxMain.add(flxFlag);
    var lblCurrency = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCurrency",
        "isVisible": true,
        "right": "28%",
        "skin": "sknlblWhitecariolight135",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "18%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBuyRate = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBuyRate",
        "isVisible": true,
        "right": "38%",
        "skin": "sknlblWhitecariolightNew90",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSellRate = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSellRate",
        "isVisible": true,
        "right": "68%",
        "skin": "sknlblWhitecariolightNew90",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCountry = new kony.ui.Label({
        "centerY": "80%",
        "id": "lblCountry",
        "isVisible": true,
        "right": "4%",
        "skin": "sknlblWhitecariolightNew90",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxExchangeRatestmpAr.add(flxMain, lblCurrency, lblBuyRate, lblSellRate, lblCountry);
}
