//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:41 EEST 2020
function addWidgetsfrmRegisterUserAr() {
frmRegisterUser.setDefaultUnit(kony.flex.DP);
var flxMain = new kony.ui.FlexContainer({
"clipBounds": true,
"height": "100%",
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopysknFlxBackgrounf0hf3c1bfbde664f",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var imgBackground = new kony.ui.Image2({
"height": "100%",
"id": "imgBackground",
"isVisible": false,
"right": "0dp",
"skin": "slImage",
"src": "bg04.png",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxHead = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxHead",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHead.setDefaultUnit(kony.flex.DP);
var flxHeadMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80%",
"id": "flxHeadMain",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxHeadMain.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_f0cbca532d314d1c92b4b22a0de45384,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"height": "100%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"height": "100%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
var lblTitle = new kony.ui.Label({
"height": "100%",
"id": "lblTitle",
"isVisible": true,
"left": "20%",
"skin": "lblAmountCurrency",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxNext = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxNext",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_f9d0cdfd81f943a68b13e5cb50e3d05e,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxNext.setDefaultUnit(kony.flex.DP);
var lblNext = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Next Page"
},
"height": "100%",
"id": "lblNext",
"isVisible": true,
"left": "0dp",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxNext.add(lblNext);
flxHeadMain.add(flxBack, lblTitle, flxNext);
var flxProgress1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "11%",
"id": "flxProgress1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "CopysknFlxProgress0fd0051ac013643",
"width": "100%",
"zIndex": 2
}, {}, {});
flxProgress1.setDefaultUnit(kony.flex.DP);
flxProgress1.add();
var flxProgress2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "11%",
"id": "flxProgress2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknFlxProgress",
"width": "100%",
"zIndex": 1
}, {}, {});
flxProgress2.setDefaultUnit(kony.flex.DP);
flxProgress2.add();
flxHead.add(flxHeadMain, flxProgress1, flxProgress2);
var flxDebitCard = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "86%",
"horizontalScrollIndicator": true,
"id": "flxDebitCard",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "14%",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
flxDebitCard.setDefaultUnit(kony.flex.DP);
var lblInstruction1 = new kony.ui.Label({
"centerX": "50%",
"id": "lblInstruction1",
"isVisible": true,
"right": "141dp",
"skin": "sknLblWhite",
"text": kony.i18n.getLocalizedString("i18n.reg.EnterDebit"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxEnterCardDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "52dp",
"id": "flxEnterCardDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxEnterCardDetails.setDefaultUnit(kony.flex.DP);
var flxText = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "42dp",
"id": "flxText",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "4dp",
"width": "87%",
"zIndex": 1
}, {}, {});
flxText.setDefaultUnit(kony.flex.DP);
var flxCard = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "42dp",
"id": "flxCard",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "CopyslFbox0b72106319d6145",
"top": "0%",
"width": "13%",
"zIndex": 1
}, {}, {});
flxCard.setDefaultUnit(kony.flex.DP);
var cardImg = new kony.ui.Image2({
"centerY": "50%",
"height": "100%",
"id": "cardImg",
"isVisible": false,
"right": "0%",
"skin": "slImage",
"src": "card.png",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnCardReg = new kony.ui.Button({
"focusSkin": "sknbtncard",
"height": "100%",
"id": "btnCardReg",
"isVisible": true,
"right": "0%",
"skin": "sknbtncard",
"text": "G",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxCard.add(cardImg, btnCardReg);
var txtCardNum = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Please Enter Debit Card Number"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "sknDebitCardNum",
"height": "40dp",
"id": "txtCardNum",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
"right": "14%",
"maxTextLength": 16,
"onDone": AS_TextField_a57dcb695d9f4919a18fb21739eac607,
"onTextChange": AS_TextField_e286e2c0112f48a8bfdb8a664fe2f20a,
"onTouchStart": AS_TextField_ce47004cd4674692adae12ac3408141c,
"placeholder": kony.i18n.getLocalizedString("i18n.common.Enter16Digit"),
"secureTextEntry": false,
"skin": "sknDebitCardNum",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0dp",
"width": "86%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"placeholderSkin": "sknTxtplacehodler",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblClose = new kony.ui.Label({
"height": "100%",
"id": "lblClose",
"isVisible": false,
"right": "90%",
"onTouchEnd": AS_Label_eef163aeeaec492fb08d8009c75c5d53,
"skin": "sknClose",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxText.add(flxCard, txtCardNum, lblClose);
var flxphoto = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "42dp",
"id": "flxphoto",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "88%",
"skin": "CopyslFbox0b72106319d6145",
"top": "4dp",
"width": "12%",
"zIndex": 1
}, {}, {});
flxphoto.setDefaultUnit(kony.flex.DP);
var imgScan = new kony.ui.Image2({
"centerY": "50%",
"height": "100%",
"id": "imgScan",
"isVisible": true,
"right": "0%",
"onTouchStart": AS_Image_h9f117c2109f4e76a16eeb3f647cd29f,
"skin": "slImage",
"src": "scan.png",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxphoto.add(imgScan);
var flxLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2dp",
"id": "flxLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "47dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLine.setDefaultUnit(kony.flex.DP);
flxLine.add();
flxEnterCardDetails.add(flxText, flxphoto, flxLine);
var lblInstruction2 = new kony.ui.Label({
"centerX": "50%",
"id": "lblInstruction2",
"isVisible": true,
"right": "151dp",
"skin": "sknLblWhite100",
"text": kony.i18n.getLocalizedString("i18n.common.CardPin"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxPIN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "60dp",
"id": "flxPIN",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_fac03e0d581146919bd54a60614ce2b8,
"skin": "slFbox",
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {}, {});
flxPIN.setDefaultUnit(kony.flex.DP);
var lblCircle1 = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Enter"
},
"height": "80%",
"id": "lblCircle1",
"isVisible": true,
"right": "30.50%",
"skin": "sknlblPinStar",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCircle2 = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "the"
},
"height": "80%",
"id": "lblCircle2",
"isVisible": true,
"right": "42.50%",
"skin": "sknlblPinStar",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCircle3 = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "PIN"
},
"height": "80%",
"id": "lblCircle3",
"isVisible": true,
"right": "54.50%",
"skin": "sknlblPinStar",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCircle4 = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Number"
},
"height": "80%",
"id": "lblCircle4",
"isVisible": true,
"right": "66.50%",
"skin": "sknlblPinStar",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var txtPIN = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Please enter your PIN and click next"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "sknFlxPIN",
"height": "40dp",
"id": "txtPIN",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
"right": "300%",
"maxTextLength": 4,
"onTextChange": AS_TextField_a9b6aaf7339746899319bafac5bfe440,
"secureTextEntry": false,
"skin": "sknFlxPIN",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0dp",
"width": "260dp",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknFlxPIN",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxLinee1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "8%",
"clipBounds": true,
"height": "2dp",
"id": "flxLinee1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "28%",
"skin": "sknFlxGreyLine",
"width": "9%",
"zIndex": 10
}, {}, {});
flxLinee1.setDefaultUnit(kony.flex.DP);
flxLinee1.add();
var flxLinee2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "8%",
"clipBounds": true,
"height": "2dp",
"id": "flxLinee2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "40%",
"skin": "sknFlxGreyLine",
"width": "9%",
"zIndex": 10
}, {}, {});
flxLinee2.setDefaultUnit(kony.flex.DP);
flxLinee2.add();
var flxLinee3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "8%",
"clipBounds": true,
"height": "2dp",
"id": "flxLinee3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "52%",
"skin": "sknFlxGreyLine",
"width": "9%",
"zIndex": 10
}, {}, {});
flxLinee3.setDefaultUnit(kony.flex.DP);
flxLinee3.add();
var flxLinee4 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "8%",
"clipBounds": true,
"height": "2dp",
"id": "flxLinee4",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "64%",
"skin": "sknFlxGreyLine",
"width": "9%",
"zIndex": 10
}, {}, {});
flxLinee4.setDefaultUnit(kony.flex.DP);
flxLinee4.add();
flxPIN.add(lblCircle1, lblCircle2, lblCircle3, lblCircle4, txtPIN, flxLinee1, flxLinee2, flxLinee3, flxLinee4);
var btnDummy = new kony.ui.Button({
"focusSkin": "slButtonGlossRed",
"height": "1dp",
"id": "btnDummy",
"isVisible": true,
"right": "5000dp",
"skin": "slButtonGlossBlue",
"text": "Button",
"top": "10dp",
"width": "260dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblInvalidCredentialsKA = new kony.ui.Label({
"centerX": "50%",
"id": "lblInvalidCredentialsKA",
"isVisible": false,
"left": "5%",
"skin": "sknInvalidCredKA",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxTxtMobileNukber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "350dp",
"id": "flxTxtMobileNukber",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"skin": "slFbox",
"top": "-5dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxTxtMobileNukber.setDefaultUnit(kony.flex.DP);
var lblEnterMobileNumber = new kony.ui.Label({
"centerX": "50%",
"id": "lblEnterMobileNumber",
"isVisible": true,
"skin": "sknLblWhite",
"text": kony.i18n.getLocalizedString("i18n.settings.enterYourMobileNumberPlh"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxMobContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60dp",
"id": "flxMobContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMobContainer.setDefaultUnit(kony.flex.DP);
var lblCountryCodeTitle = new kony.ui.Label({
"id": "lblCountryCodeTitle",
"isVisible": false,
"right": "0dp",
"skin": "sknLblWhite100",
"text": kony.i18n.getLocalizedString("i18n.common.countrycode"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var txtCountryCode = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "sknDebitCardNum",
"height": "40dp",
"id": "txtCountryCode",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
"right": "4%",
"maxTextLength": 3,
"onTextChange": AS_TextField_e1f51df4168c47acb1420746e8be8c28,
"placeholder": "962",
"secureTextEntry": false,
"skin": "sknDebitCardNum",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "15dp",
"width": "25%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"placeholderSkin": "sknTxtplacehodler",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxLine2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2dp",
"id": "flxLine2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxGreyLine",
"top": "55dp",
"width": "28%",
"zIndex": 1
}, {}, {});
flxLine2.setDefaultUnit(kony.flex.DP);
flxLine2.add();
var lblMobileNumberTitle = new kony.ui.Label({
"id": "lblMobileNumberTitle",
"isVisible": false,
"right": "32%",
"skin": "sknLblWhite100",
"text": kony.i18n.getLocalizedString("i18n.myprofile.MobileNumber"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var txtMobileNumber = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "sknDebitCardNum",
"height": "40dp",
"id": "txtMobileNumber",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
"right": "32%",
"maxTextLength": 20,
"onTextChange": AS_TextField_e9288e87d81c4499834424923a3d70c6,
"placeholder": kony.i18n.getLocalizedString("i18n.myprofile.MobileNumber"),
"secureTextEntry": false,
"skin": "sknDebitCardNum",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "15dp",
"width": "69%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"placeholderSkin": "sknTxtplacehodler",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyflxLine0a4214bb8a7c146 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2dp",
"id": "CopyflxLine0a4214bb8a7c146",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "32%",
"skin": "sknFlxGreyLine",
"top": "55dp",
"width": "68%",
"zIndex": 1
}, {}, {});
CopyflxLine0a4214bb8a7c146.setDefaultUnit(kony.flex.DP);
CopyflxLine0a4214bb8a7c146.add();
var btnMobileCode = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"height": "42dp",
"id": "btnMobileCode",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_bbbc2c65e1f44ad1ab5535752d14ec6e,
"skin": "CopysglossblueArrow140",
"text": "d",
"top": "15dp",
"width": "27%",
"zIndex": 5
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblMobileNumberClear = new kony.ui.Label({
"id": "lblMobileNumberClear",
"isVisible": false,
"onTouchEnd": AS_Label_g5a8dde9d55f42b9a57da8c2e7e11e6d,
"left": "3%",
"skin": "sknClose",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 3
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMobContainer.add(lblCountryCodeTitle, txtCountryCode, flxLine2, lblMobileNumberTitle, txtMobileNumber, CopyflxLine0a4214bb8a7c146, btnMobileCode, lblMobileNumberClear);
var lblHint = new kony.ui.Label({
"id": "lblHint",
"isVisible": true,
"right": "32%",
"skin": "CopysknCaironew70number",
"text": kony.i18n.getLocalizedString("i18n.common.currentmobileno"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnMobileNumberConfirm = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonWhite",
"height": "50dp",
"id": "btnMobileNumberConfirm",
"isVisible": true,
"right": "25dp",
"onClick": AS_Button_ae0c8fa3fc1d4b758e389919082b4370,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
"top": "15%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxTxtMobileNukber.add(lblEnterMobileNumber, flxMobContainer, lblHint, btnMobileNumberConfirm);
flxDebitCard.add(lblInstruction1, flxEnterCardDetails, lblInstruction2, flxPIN, btnDummy, lblInvalidCredentialsKA, flxTxtMobileNukber);
var lblCard = new kony.ui.Label({
"centerX": "59.98%",
"height": "0%",
"id": "lblCard",
"isVisible": true,
"right": "0dp",
"skin": "sknLblTitle",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "0%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCode = new kony.ui.Label({
"centerX": "69.97999999999999%",
"height": "0%",
"id": "lblCode",
"isVisible": true,
"right": "0dp",
"skin": "sknLblTitle",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "0%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMsg = new kony.ui.Label({
"centerX": "79.97999999999999%",
"height": "0%",
"id": "lblMsg",
"isVisible": true,
"right": "0dp",
"skin": "sknLblTitle",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "0%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblStatus = new kony.ui.Label({
"centerX": "89.97999999999999%",
"height": "0%",
"id": "lblStatus",
"isVisible": true,
"right": "0dp",
"skin": "sknLblTitle",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "0%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPin = new kony.ui.Label({
"centerX": "69.97999999999999%",
"height": "0%",
"id": "lblPin",
"isVisible": true,
"right": "0dp",
"skin": "sknLblTitle",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "0%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLanguage = new kony.ui.Label({
"centerX": "79.97999999999999%",
"height": "0%",
"id": "lblLanguage",
"isVisible": true,
"right": "0dp",
"skin": "sknLblTitle",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "0%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMain.add(imgBackground, flxHead, flxDebitCard, lblCard, lblCode, lblMsg, lblStatus, lblPin, lblLanguage);
frmRegisterUser.add(flxMain);
};
function frmRegisterUserGlobalsAr() {
frmRegisterUserAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmRegisterUserAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmRegisterUser",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"postShow": AS_Form_fecba2b4ec254548803e4e064bddfbbc,
"preShow": AS_Form_c0412af2d68c4c6589c8ca4f2ea7f006,
"skin": "CopysknBackground0ac38f681673446"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_g53cc23ca45343a68b0346061ed6ec57,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
