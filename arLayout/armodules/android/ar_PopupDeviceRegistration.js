//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:41 EEST 2020
function addWidgetsPopupDeviceRegistrationAr() {
var HBxContent1 = new kony.ui.Box({
"id": "HBxContent1",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var lblTitle = new kony.ui.Label({
"id": "lblTitle",
"isVisible": true,
"skin": "sknLblPopupTitle",
"text": "Are you sure you want to\nde-register this device ?",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
}
}, {
"containerWeight": 100,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"hExpand": true,
"margin": [ 1, 1,1, 1],
"marginInPixel": false,
"padding": [ 3, 2,3, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"textCopyable": false
});
HBxContent1.add(lblTitle);
var HBxContent2 = new kony.ui.Box({
"id": "HBxContent2",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var lblMessage = new kony.ui.Label({
"id": "lblMessage",
"isVisible": true,
"skin": "sknLblPopupMessage",
"text": "Are you sure you want to\nde-register this device ?",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
}
}, {
"containerWeight": 100,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"hExpand": true,
"margin": [ 1, 1,1, 1],
"marginInPixel": false,
"padding": [ 3, 0,3, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"textCopyable": false
});
HBxContent2.add(lblMessage);
var HBxSubmit = new kony.ui.Box({
"id": "HBxSubmit",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var btnYes = new kony.ui.Button({
"focusSkin": "slButtonDBlue",
"id": "btnYes",
"isVisible": true,
"skin": "slButtonDBlue",
"text": "Yes"
}, {
"containerWeight": 50,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"hExpand": true,
"margin": [ 6, 6,6, 6],
"marginInPixel": false,
"padding": [ 3, 2,3, 2],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
var btnNo = new kony.ui.Button({
"focusSkin": "slButtonDBlue",
"id": "btnNo",
"isVisible": true,
"skin": "slButtonDBlue",
"text": "No"
}, {
"containerWeight": 50,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"hExpand": true,
"margin": [ 6, 6,6, 6],
"marginInPixel": false,
"padding": [ 3, 2,3, 2],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
HBxSubmit.add( btnNo,btnYes);
PopupDeviceRegistration.add(HBxContent1, HBxContent2, HBxSubmit);
};
function PopupDeviceRegistrationGlobalsAr() {
PopupDeviceRegistrationAr = new kony.ui.Popup({
"addWidgets": addWidgetsPopupDeviceRegistrationAr,
"id": "PopupDeviceRegistration",
"isModal": true,
"skin": "sknWTBGWTBRDRND",
"transparencyBehindThePopup": 100
}, {
"containerWeight": 80,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"windowSoftInputMode": constants.POPUP_ADJUST_PAN
});
};
