//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:40 EEST 2020
function addWidgetsfrmConfirmTransferKAAr() {
frmConfirmTransferKA.setDefaultUnit(kony.flex.DP);
var titleBarWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "titleBarWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "s",
"top": "0dp",
"width": "100%",
"zIndex": 2
}, {}, {});
titleBarWrapper.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_h4a7072c5b6c47589d5a01b32c405b84,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
var transferPayTitleLabel = new kony.ui.Label({
"centerX": "49.97%",
"centerY": "50.00%",
"height": "100%",
"id": "transferPayTitleLabel",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
"width": "70%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblClose0i58055dea37541 = new kony.ui.Label({
"height": "100%",
"id": "CopylblClose0i58055dea37541",
"isVisible": true,
"onTouchEnd": AS_Label_e5b45601fa5c48f4b7a16f3a6ff70664,
"right": "2%",
"skin": "sknCloseConfirm",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
titleBarWrapper.add(flxBack, transferPayTitleLabel, CopylblClose0i58055dea37541);
var flxTransfers = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": true,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "92%",
"horizontalScrollIndicator": false,
"id": "flxTransfers",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknConfirmTransfer",
"top": "8%",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
flxTransfers.setDefaultUnit(kony.flex.DP);
var FlexContainer042b0725667e643 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "115dp",
"id": "FlexContainer042b0725667e643",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer042b0725667e643.setDefaultUnit(kony.flex.DP);
var FlexContainer0f615b714fea843 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100%",
"id": "FlexContainer0f615b714fea843",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer0f615b714fea843.setDefaultUnit(kony.flex.DP);
var transactionDate = new kony.ui.Label({
"centerX": "50%",
"id": "transactionDate",
"isVisible": true,
"skin": "skndetailPageDate",
"text": "Transfer to",
"top": "3dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider04de6f2742ada45 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider04de6f2742ada45",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": 10,
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider04de6f2742ada45.setDefaultUnit(kony.flex.DP);
Copydivider04de6f2742ada45.add();
FlexContainer0f615b714fea843.add(transactionDate, Copydivider04de6f2742ada45);
FlexContainer042b0725667e643.add(FlexContainer0f615b714fea843);
var FlexContainer0gcfd60c2f05d42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "FlexContainer0gcfd60c2f05d42",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer0gcfd60c2f05d42.setDefaultUnit(kony.flex.DP);
var lblheading = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblheading",
"isVisible": true,
"right": "0dp",
"skin": "sknlblBigTitles",
"text": kony.i18n.getLocalizedString("i18n.Transfers.WithinAcc"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
FlexContainer0gcfd60c2f05d42.add(lblheading);
var additionalDetails1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "additionalDetails1",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
additionalDetails1.setDefaultUnit(kony.flex.DP);
var fromAccountNumberKA = new kony.ui.Label({
"id": "fromAccountNumberKA",
"isVisible": false,
"left": "15%",
"skin": "sknNumber",
"top": "50dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var divider2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "divider2",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
divider2.setDefaultUnit(kony.flex.DP);
divider2.add();
var flxFrom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxFrom",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFrom.setDefaultUnit(kony.flex.DP);
var Label0e7a26666ebf141 = new kony.ui.Label({
"id": "Label0e7a26666ebf141",
"isVisible": true,
"right": "4.97%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.from"),
"top": "0dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionFrom = new kony.ui.Label({
"id": "transactionFrom",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"top": "1dp",
"width": "92%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionFromNumber = new kony.ui.Label({
"id": "transactionFromNumber",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"top": "1dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFrom.add(Label0e7a26666ebf141, transactionFrom, transactionFromNumber);
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "5dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var CopyLabel0db3e28ce417344 = new kony.ui.Label({
"id": "CopyLabel0db3e28ce417344",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"top": "2dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var amountTransaction = new kony.ui.Label({
"id": "amountTransaction",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "123.000 JOD",
"top": "30dp",
"width": "45%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionAmount = new kony.ui.Label({
"id": "transactionAmount",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transFlag = new kony.ui.Label({
"id": "transFlag",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var payMode = new kony.ui.Label({
"id": "payMode",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"text": "0",
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var LblConvertedAmt = new kony.ui.Label({
"id": "LblConvertedAmt",
"isVisible": true,
"right": "55%",
"skin": "sknNumber",
"text": "173.48 USD",
"top": "30dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAmount.add(CopyLabel0db3e28ce417344, amountTransaction, transactionAmount, transFlag, payMode, LblConvertedAmt);
var toAccountNumberKA = new kony.ui.Label({
"id": "toAccountNumberKA",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"text": "12345678901234567890",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var frmAccBranchCode = new kony.ui.Label({
"id": "frmAccBranchCode",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"text": "12345678901234567890",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var toAccBranchCode = new kony.ui.Label({
"id": "toAccBranchCode",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"text": "12345678901234567890",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxTo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxTo",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "5dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTo.setDefaultUnit(kony.flex.DP);
var Label0d028d0f8f55941 = new kony.ui.Label({
"id": "Label0d028d0f8f55941",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.common.To"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var toAcc = new kony.ui.Label({
"id": "toAcc",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"top": "1dp",
"width": "92%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountNumber = new kony.ui.Label({
"id": "lblAccountNumber",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"top": "1dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTo.add(Label0d028d0f8f55941, toAcc, lblAccountNumber);
additionalDetails1.add(fromAccountNumberKA, divider2, flxFrom, flxAmount, toAccountNumberKA, frmAccBranchCode, toAccBranchCode, flxTo);
var flxReccurrence = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxReccurrence",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxReccurrence.setDefaultUnit(kony.flex.DP);
var lblRecurrenceFreq = new kony.ui.Label({
"height": "30dp",
"id": "lblRecurrenceFreq",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"text": "Next 5 Times",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0d794f3c52b2a4f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0d794f3c52b2a4f",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0d794f3c52b2a4f.setDefaultUnit(kony.flex.DP);
Copydivider0d794f3c52b2a4f.add();
var flxFreType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxFreType",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFreType.setDefaultUnit(kony.flex.DP);
var CopyLabel034f372f1f2cb44 = new kony.ui.Label({
"id": "CopyLabel034f372f1f2cb44",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.transfer.frequencyC"),
"top": "0dp",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblReccurrenceValue = new kony.ui.Label({
"bottom": "15dp",
"id": "lblReccurrenceValue",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Once a Month",
"top": "-1dp",
"width": "35%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFreType.add(CopyLabel034f372f1f2cb44, lblReccurrenceValue);
var flxFrequency = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxFrequency",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFrequency.setDefaultUnit(kony.flex.DP);
var CopylblReccuureceFreq00b746cbdb27f45 = new kony.ui.Label({
"id": "CopylblReccuureceFreq00b746cbdb27f45",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.transfers.Recurrence"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
"padding": [ 5, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblReccuureceFreq = new kony.ui.Label({
"bottom": "15dp",
"id": "lblReccuureceFreq",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "10",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblReccuureceFreq08332f52daadb48 = new kony.ui.Label({
"height": "30dp",
"id": "CopylblReccuureceFreq08332f52daadb48",
"isVisible": false,
"left": "0dp",
"skin": "sknNumber",
"text": "Times",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,5, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFrequency.add(CopylblReccuureceFreq00b746cbdb27f45, lblReccuureceFreq, CopylblReccuureceFreq08332f52daadb48);
var flxDateDurtarion = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxDateDurtarion",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDateDurtarion.setDefaultUnit(kony.flex.DP);
var flxFromDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxFromDate",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "2.50%",
"skin": "slFbox",
"top": "0dp",
"width": "50%",
"zIndex": 1
}, {}, {});
flxFromDate.setDefaultUnit(kony.flex.DP);
var lblStartDateIndicator = new kony.ui.Label({
"id": "lblStartDateIndicator",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.Transfer.StartDate"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblfromDateKA = new kony.ui.Label({
"id": "lblfromDateKA",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "14 Feb 2018",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFromDate.add(lblStartDateIndicator, lblfromDateKA);
var flxToDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxToDate",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "0dp",
"width": "50%",
"zIndex": 1
}, {}, {});
flxToDate.setDefaultUnit(kony.flex.DP);
var lblEndDateIndicator = new kony.ui.Label({
"id": "lblEndDateIndicator",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.Transfer.EndDate"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblToDateKA = new kony.ui.Label({
"id": "lblToDateKA",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "14 Dec 2018",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxToDate.add(lblEndDateIndicator, lblToDateKA);
flxDateDurtarion.add( flxToDate,flxFromDate);
flxReccurrence.add(lblRecurrenceFreq, Copydivider0d794f3c52b2a4f, flxFreType, flxFrequency, flxDateDurtarion);
var flxSchedule = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxSchedule",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "-3%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSchedule.setDefaultUnit(kony.flex.DP);
var CopyLabel05d0720c3bd5d45 = new kony.ui.Label({
"id": "CopyLabel05d0720c3bd5d45",
"isVisible": false,
"right": "5%",
"skin": "sknlblBodytxt",
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblScheduledDate = new kony.ui.Label({
"height": "50%",
"id": "lblScheduledDate",
"isVisible": true,
"right": "5.00%",
"skin": "sknNumber",
"text": "14 Feb 2018",
"top": "-5%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider058b92c2528494d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider058b92c2528494d",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider058b92c2528494d.setDefaultUnit(kony.flex.DP);
Copydivider058b92c2528494d.add();
flxSchedule.add(CopyLabel05d0720c3bd5d45, lblScheduledDate, Copydivider058b92c2528494d);
var CopynotesWrapper0fa62da5ca9ef4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "CopynotesWrapper0fa62da5ca9ef4b",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopynotesWrapper0fa62da5ca9ef4b.setDefaultUnit(kony.flex.DP);
var CopyLabel0eac9047f054a44 = new kony.ui.Label({
"id": "CopyLabel0eac9047f054a44",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.Transfer.PoT"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPurposeofTransfer = new kony.ui.Label({
"bottom": 10,
"id": "lblPurposeofTransfer",
"isVisible": true,
"right": "5.00%",
"skin": "sknNumber",
"text": "8932939393939393939",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0e7613f0a3b0e46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0e7613f0a3b0e46",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0e7613f0a3b0e46.setDefaultUnit(kony.flex.DP);
Copydivider0e7613f0a3b0e46.add();
CopynotesWrapper0fa62da5ca9ef4b.add(CopyLabel0eac9047f054a44, lblPurposeofTransfer, Copydivider0e7613f0a3b0e46);
var CopynotesWrapper0c85909efb23b48 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "CopynotesWrapper0c85909efb23b48",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopynotesWrapper0c85909efb23b48.setDefaultUnit(kony.flex.DP);
var CopyLabel0e44e6a90da3c49 = new kony.ui.Label({
"id": "CopyLabel0e44e6a90da3c49",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.opening_account.fees"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopytransactionNotes0eac121a850b84c = new kony.ui.Label({
"bottom": "15dp",
"id": "CopytransactionNotes0eac121a850b84c",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "00.060JOD",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0j0dc5e62c25449 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0j0dc5e62c25449",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0j0dc5e62c25449.setDefaultUnit(kony.flex.DP);
Copydivider0j0dc5e62c25449.add();
CopynotesWrapper0c85909efb23b48.add(CopyLabel0e44e6a90da3c49, CopytransactionNotes0eac121a850b84c, Copydivider0j0dc5e62c25449);
var CopynotesWrapper03de47f39b8bc46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "CopynotesWrapper03de47f39b8bc46",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopynotesWrapper03de47f39b8bc46.setDefaultUnit(kony.flex.DP);
var CopyLabel07f3ee643ff4e47 = new kony.ui.Label({
"id": "CopyLabel07f3ee643ff4e47",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.Transfer.Desc"),
"top": 0,
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionNotes = new kony.ui.Label({
"bottom": "15dp",
"id": "transactionNotes",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Note details appear here. This can be a muliple line descripsion",
"top": 0,
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider06e184b7d586e4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bottom": 0,
"clipBounds": true,
"id": "Copydivider06e184b7d586e4a",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider06e184b7d586e4a.setDefaultUnit(kony.flex.DP);
Copydivider06e184b7d586e4a.add();
CopynotesWrapper03de47f39b8bc46.add(CopyLabel07f3ee643ff4e47, transactionNotes, Copydivider06e184b7d586e4a);
var flxCommisionRate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxCommisionRate",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCommisionRate.setDefaultUnit(kony.flex.DP);
var CopyLabel0h8971697c12446 = new kony.ui.Label({
"id": "CopyLabel0h8971697c12446",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.Transfers.commissionRate"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCommRate = new kony.ui.Label({
"bottom": "15dp",
"id": "lblCommRate",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var hiddenlblCommRate = new kony.ui.Label({
"bottom": "15dp",
"id": "hiddenlblCommRate",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"text": "0.060 JOD",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0bb7f27ddcc4246 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0bb7f27ddcc4246",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0bb7f27ddcc4246.setDefaultUnit(kony.flex.DP);
Copydivider0bb7f27ddcc4246.add();
flxCommisionRate.add(CopyLabel0h8971697c12446, lblCommRate, hiddenlblCommRate, Copydivider0bb7f27ddcc4246);
var flxExchangeRate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxExchangeRate",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0.30%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxExchangeRate.setDefaultUnit(kony.flex.DP);
var CopyLabel0d35893ecd56642 = new kony.ui.Label({
"id": "CopyLabel0d35893ecd56642",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.Transfers.exchangeRate"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblExchRate = new kony.ui.Label({
"bottom": "10dp",
"id": "lblExchRate",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "0.060 JOD",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var hiddenlblExchRate = new kony.ui.Label({
"bottom": "10dp",
"id": "hiddenlblExchRate",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"text": "0.060 JOD",
"top": 0,
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0h1147ffd66df45 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0h1147ffd66df45",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0h1147ffd66df45.setDefaultUnit(kony.flex.DP);
Copydivider0h1147ffd66df45.add();
flxExchangeRate.add(CopyLabel0d35893ecd56642, lblExchRate, hiddenlblExchRate, Copydivider0h1147ffd66df45);
var flxExchangeComm = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxExchangeComm",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxExchangeComm.setDefaultUnit(kony.flex.DP);
var CopyLabel0hb0ce7e1fae34b = new kony.ui.Label({
"id": "CopyLabel0hb0ce7e1fae34b",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.Transfers.exchangeCommision"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblExchComm = new kony.ui.Label({
"bottom": "15dp",
"id": "lblExchComm",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "231",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var hiddenlblExchComm = new kony.ui.Label({
"bottom": "15dp",
"id": "hiddenlblExchComm",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0ibe5b052b3574d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0ibe5b052b3574d",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0ibe5b052b3574d.setDefaultUnit(kony.flex.DP);
Copydivider0ibe5b052b3574d.add();
flxExchangeComm.add(CopyLabel0hb0ce7e1fae34b, lblExchComm, hiddenlblExchComm, Copydivider0ibe5b052b3574d);
var flxOtherComm = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxOtherComm",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxOtherComm.setDefaultUnit(kony.flex.DP);
var LabelOtherComm = new kony.ui.Label({
"id": "LabelOtherComm",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.transfer.otherComm"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblOtherComm = new kony.ui.Label({
"bottom": "15dp",
"id": "lblOtherComm",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "1",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var hiddenlblOtherComm = new kony.ui.Label({
"bottom": "15dp",
"id": "hiddenlblOtherComm",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var dividerOtherComm = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "dividerOtherComm",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
dividerOtherComm.setDefaultUnit(kony.flex.DP);
dividerOtherComm.add();
flxOtherComm.add(LabelOtherComm, lblOtherComm, hiddenlblOtherComm, dividerOtherComm);
var flxExchangeCommTra = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxExchangeCommTra",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxExchangeCommTra.setDefaultUnit(kony.flex.DP);
var lableExchangeCommTra = new kony.ui.Label({
"id": "lableExchangeCommTra",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.Transfers.exchangeCommision"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblExchangeCommTra = new kony.ui.Label({
"bottom": "15dp",
"id": "lblExchangeCommTra",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "1",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var hiddenlblExchangeCommTra = new kony.ui.Label({
"bottom": "15dp",
"id": "hiddenlblExchangeCommTra",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var divider = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "divider",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
divider.setDefaultUnit(kony.flex.DP);
divider.add();
flxExchangeCommTra.add(lableExchangeCommTra, lblExchangeCommTra, hiddenlblExchangeCommTra, divider);
var flxCreditAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxCreditAmount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCreditAmount.setDefaultUnit(kony.flex.DP);
var CopyLabel0e27eb63f9b7f40 = new kony.ui.Label({
"id": "CopyLabel0e27eb63f9b7f40",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.Transfers.creditAmount"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCrAmt = new kony.ui.Label({
"bottom": "15dp",
"id": "lblCrAmt",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "46",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var hiddenlblCrAmt = new kony.ui.Label({
"bottom": "15dp",
"id": "hiddenlblCrAmt",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0de45f2706a4a42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0de45f2706a4a42",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0de45f2706a4a42.setDefaultUnit(kony.flex.DP);
Copydivider0de45f2706a4a42.add();
flxCreditAmount.add(CopyLabel0e27eb63f9b7f40, lblCrAmt, hiddenlblCrAmt, Copydivider0de45f2706a4a42);
var flxLocalAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxLocalAmount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLocalAmount.setDefaultUnit(kony.flex.DP);
var CopyLabel0fe77340414014e = new kony.ui.Label({
"id": "CopyLabel0fe77340414014e",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.Transfers.LocalAmount"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLclAmt = new kony.ui.Label({
"bottom": "15dp",
"id": "lblLclAmt",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "5653",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var hiddenlblLclAmt = new kony.ui.Label({
"bottom": "15dp",
"id": "hiddenlblLclAmt",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0aa107d9a8b0748 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0aa107d9a8b0748",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0aa107d9a8b0748.setDefaultUnit(kony.flex.DP);
Copydivider0aa107d9a8b0748.add();
flxLocalAmount.add(CopyLabel0fe77340414014e, lblLclAmt, hiddenlblLclAmt, Copydivider0aa107d9a8b0748);
var FlexContainer002f7e6e2ce6b49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "FlexContainer002f7e6e2ce6b49",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "4%",
"width": "100%",
"zIndex": 100
}, {}, {});
FlexContainer002f7e6e2ce6b49.setDefaultUnit(kony.flex.DP);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "sknprimaryAction",
"height": "90%",
"id": "btnConfirm",
"isVisible": true,
"onClick": AS_Button_6b9fcdd57f9145a9b4e38255741be7fb,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.P2P.confirm"),
"top": "5%",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnEditTranscation = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "35dp",
"id": "btnEditTranscation",
"isVisible": false,
"onClick": AS_Button_c5ad31cef3c74b7fb0242e9ef8d6a744,
"skin": "sknsecondaryAction",
"text": "Edit",
"top": "5dp",
"width": "260dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlexContainer002f7e6e2ce6b49.add(btnConfirm, btnEditTranscation);
var CopyFlexContainer0i17dd2c8b09a4d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "4%",
"id": "CopyFlexContainer0i17dd2c8b09a4d",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0i17dd2c8b09a4d.setDefaultUnit(kony.flex.DP);
var CopybtnConfirm0g06ca389eb4a48 = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknprimaryActionFocus",
"height": "100%",
"id": "CopybtnConfirm0g06ca389eb4a48",
"isVisible": false,
"skin": "CopysknprimaryAction0h63c91801c9c45",
"top": "0%",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopybtnEditTranscation0a85b0932ffde4d = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "35dp",
"id": "CopybtnEditTranscation0a85b0932ffde4d",
"isVisible": false,
"onClick": AS_Button_bc19240db2904a37be83dc1379869b87,
"skin": "sknsecondaryAction",
"text": "Edit",
"top": "5dp",
"width": "260dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyFlexContainer0i17dd2c8b09a4d.add(CopybtnConfirm0g06ca389eb4a48, CopybtnEditTranscation0a85b0932ffde4d);
var flxNote = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxNote",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxNote.setDefaultUnit(kony.flex.DP);
var lblNote = new kony.ui.Label({
"id": "lblNote",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.common.notec"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblLclAmt0i2d80d89363f48 = new kony.ui.Label({
"bottom": "15dp",
"id": "CopylblLclAmt0i2d80d89363f48",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.common.NoteHoliday"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyhiddenlblLclAmt0jd967a47aeab40 = new kony.ui.Label({
"bottom": "15dp",
"id": "CopyhiddenlblLclAmt0jd967a47aeab40",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0b8a9a59fb2d942 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0b8a9a59fb2d942",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0b8a9a59fb2d942.setDefaultUnit(kony.flex.DP);
Copydivider0b8a9a59fb2d942.add();
flxNote.add(lblNote, CopylblLclAmt0i2d80d89363f48, CopyhiddenlblLclAmt0jd967a47aeab40, Copydivider0b8a9a59fb2d942);
var flxImage = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "3%",
"id": "flxImage",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxImage.setDefaultUnit(kony.flex.DP);
var flxToAccountImage = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxToAccountImage",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknIconTitles",
"top": "5%",
"width": "70dp",
"zIndex": 1
}, {}, {});
flxToAccountImage.setDefaultUnit(kony.flex.DP);
var Label0d39c11c1364846 = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "Label0d39c11c1364846",
"isVisible": true,
"skin": "sknIconTitlesConfirm",
"text": "SK",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxToAccountImage.add(Label0d39c11c1364846);
var transactionName = new kony.ui.Label({
"centerX": "50.00%",
"id": "transactionName",
"isVisible": false,
"skin": "sknlblBigTitles",
"text": "Sai Kadari",
"top": "5dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxImage.add(flxToAccountImage, transactionName);
var lblFreType = new kony.ui.Label({
"bottom": "0%",
"centerX": "60%",
"height": "48%",
"id": "lblFreType",
"isVisible": false,
"skin": "sknLblWhike125",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "38%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var hiddenLblFromDate = new kony.ui.Label({
"bottom": "0%",
"centerX": "60%",
"height": "48%",
"id": "hiddenLblFromDate",
"isVisible": false,
"skin": "sknLblWhike125",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "38%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var hiddenLblToDate = new kony.ui.Label({
"bottom": "0%",
"centerX": "60%",
"height": "48%",
"id": "hiddenLblToDate",
"isVisible": false,
"skin": "sknLblWhike125",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "38%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBenName = new kony.ui.Label({
"id": "lblBenName",
"isVisible": false,
"right": "132dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "263dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblRemmit = new kony.ui.Label({
"id": "lblRemmit",
"isVisible": false,
"right": "153dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBenAdr1 = new kony.ui.Label({
"id": "lblBenAdr1",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBenAdr2 = new kony.ui.Label({
"id": "lblBenAdr2",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBenAdr3 = new kony.ui.Label({
"id": "lblBenAdr3",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBenCity = new kony.ui.Label({
"id": "lblBenCity",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBenCountry = new kony.ui.Label({
"id": "lblBenCountry",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCrearCode = new kony.ui.Label({
"id": "lblCrearCode",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCrearType = new kony.ui.Label({
"id": "lblCrearType",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBenBankName = new kony.ui.Label({
"id": "lblBenBankName",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBenBankAdr1 = new kony.ui.Label({
"id": "lblBenBankAdr1",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBenBankAdr2 = new kony.ui.Label({
"id": "lblBenBankAdr2",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBenBankCity = new kony.ui.Label({
"id": "lblBenBankCity",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBenBankCountry = new kony.ui.Label({
"id": "lblBenBankCountry",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblTransCur = new kony.ui.Label({
"id": "lblTransCur",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPmt1 = new kony.ui.Label({
"id": "lblPmt1",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPmt2 = new kony.ui.Label({
"id": "lblPmt2",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblSwift = new kony.ui.Label({
"id": "lblSwift",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblNarr = new kony.ui.Label({
"id": "lblNarr",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblImbAdr1 = new kony.ui.Label({
"id": "lblImbAdr1",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCorCharges = new kony.ui.Label({
"id": "lblCorCharges",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblImbAdr2 = new kony.ui.Label({
"id": "lblImbAdr2",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblImbCity = new kony.ui.Label({
"id": "lblImbCity",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCustomerID = new kony.ui.Label({
"id": "lblCustomerID",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBenBranchCode = new kony.ui.Label({
"id": "lblBenBranchCode",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblIBAN = new kony.ui.Label({
"id": "lblIBAN",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblRefID = new kony.ui.Label({
"id": "lblRefID",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPmtDetails3 = new kony.ui.Label({
"id": "lblPmtDetails3",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPmtDetails4 = new kony.ui.Label({
"id": "lblPmtDetails4",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblToAccType = new kony.ui.Label({
"id": "lblToAccType",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblImbName = new kony.ui.Label({
"id": "lblImbName",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblImbCoun = new kony.ui.Label({
"id": "lblImbCoun",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblImbSC = new kony.ui.Label({
"id": "lblImbSC",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDealRefNo = new kony.ui.Label({
"id": "lblDealRefNo",
"isVisible": false,
"right": "18dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1095dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTransfers.add(FlexContainer042b0725667e643, FlexContainer0gcfd60c2f05d42, additionalDetails1, flxReccurrence, flxSchedule, CopynotesWrapper0fa62da5ca9ef4b, CopynotesWrapper0c85909efb23b48, CopynotesWrapper03de47f39b8bc46, flxCommisionRate, flxExchangeRate, flxExchangeComm, flxOtherComm, flxExchangeCommTra, flxCreditAmount, flxLocalAmount, FlexContainer002f7e6e2ce6b49, CopyFlexContainer0i17dd2c8b09a4d, flxNote, flxImage, lblFreType, hiddenLblFromDate, hiddenLblToDate, lblBenName, lblRemmit, lblBenAdr1, lblBenAdr2, lblBenAdr3, lblBenCity, lblBenCountry, lblCrearCode, lblCrearType, lblBenBankName, lblBenBankAdr1, lblBenBankAdr2, lblBenBankCity, lblBenBankCountry, lblTransCur, lblPmt1, lblPmt2, lblSwift, lblNarr, lblImbAdr1, lblCorCharges, lblImbAdr2, lblImbCity, lblCustomerID, lblBenBranchCode, lblIBAN, lblRefID, lblPmtDetails3, lblPmtDetails4, lblToAccType, lblImbName, lblImbCoun, lblImbSC, lblDealRefNo);
var flxWebCharge = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "92%",
"id": "flxWebCharge",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknFlxBackgrounf",
"top": "8%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxWebCharge.setDefaultUnit(kony.flex.DP);
var flxCurrBal = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxCurrBal",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "3%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCurrBal.setDefaultUnit(kony.flex.DP);
var CopyLabel0cf60348636d54e = new kony.ui.Label({
"id": "CopyLabel0cf60348636d54e",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": "Current Balance on your card",
"top": "0dp",
"width": "90%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCurrBal = new kony.ui.Label({
"bottom": "15dp",
"id": "lblCurrBal",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": " JOD",
"top": "5dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var toMaskNum = new kony.ui.Label({
"id": "toMaskNum",
"isVisible": false,
"right": "58%",
"skin": "sknlblBodytxt",
"text": "***114",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,2, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0gb800f5b8f8b40 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0gb800f5b8f8b40",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0gb800f5b8f8b40.setDefaultUnit(kony.flex.DP);
Copydivider0gb800f5b8f8b40.add();
flxCurrBal.add(CopyLabel0cf60348636d54e, lblCurrBal, toMaskNum, Copydivider0gb800f5b8f8b40);
var flxFrmAc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxFrmAc",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFrmAc.setDefaultUnit(kony.flex.DP);
var lblAcc = new kony.ui.Label({
"id": "lblAcc",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.settings.account"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var frmAccount = new kony.ui.Label({
"bottom": "15dp",
"id": "frmAccount",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Saving Account",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyhiddenlblCommRate0de2320062bd544 = new kony.ui.Label({
"bottom": "15dp",
"id": "CopyhiddenlblCommRate0de2320062bd544",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"text": "0.060 JOD",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0h5988ee13e044b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0h5988ee13e044b",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0h5988ee13e044b.setDefaultUnit(kony.flex.DP);
Copydivider0h5988ee13e044b.add();
flxFrmAc.add(lblAcc, frmAccount, CopyhiddenlblCommRate0de2320062bd544, Copydivider0h5988ee13e044b);
var flxWebAmt = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxWebAmt",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxWebAmt.setDefaultUnit(kony.flex.DP);
var CopyLabel0h30257ef236946 = new kony.ui.Label({
"id": "CopyLabel0h30257ef236946",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblWebAmt = new kony.ui.Label({
"bottom": "15dp",
"id": "lblWebAmt",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "234.333 JOD",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyhiddenlblCommRate0fcfdd7a94fe948 = new kony.ui.Label({
"bottom": "15dp",
"id": "CopyhiddenlblCommRate0fcfdd7a94fe948",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"text": "0.060 JOD",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0c568e44b746c42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0c568e44b746c42",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0c568e44b746c42.setDefaultUnit(kony.flex.DP);
Copydivider0c568e44b746c42.add();
flxWebAmt.add(CopyLabel0h30257ef236946, lblWebAmt, CopyhiddenlblCommRate0fcfdd7a94fe948, Copydivider0c568e44b746c42);
var flxWebFee = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxWebFee",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxWebFee.setDefaultUnit(kony.flex.DP);
var CopyLabel0ba1e751c6ab748 = new kony.ui.Label({
"id": "CopyLabel0ba1e751c6ab748",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.opening_account.fees"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblWebFees = new kony.ui.Label({
"bottom": "15dp",
"id": "lblWebFees",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "0.060 JOD",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyhiddenlblCommRate0ifd396d99e134f = new kony.ui.Label({
"bottom": "15dp",
"id": "CopyhiddenlblCommRate0ifd396d99e134f",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"text": "0.060 JOD",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0aaab09d7c62546 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0aaab09d7c62546",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0aaab09d7c62546.setDefaultUnit(kony.flex.DP);
Copydivider0aaab09d7c62546.add();
flxWebFee.add(CopyLabel0ba1e751c6ab748, lblWebFees, CopyhiddenlblCommRate0ifd396d99e134f, Copydivider0aaab09d7c62546);
var flxAfterTopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxAfterTopup",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAfterTopup.setDefaultUnit(kony.flex.DP);
var lblAfterTopup = new kony.ui.Label({
"id": "lblAfterTopup",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.webcharge.balaftettopup"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAfterBal = new kony.ui.Label({
"bottom": "15dp",
"id": "lblAfterBal",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "1,231.000 JOD",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyhiddenlblCommRate0f56bc71ae91f48 = new kony.ui.Label({
"bottom": "15dp",
"id": "CopyhiddenlblCommRate0f56bc71ae91f48",
"isVisible": false,
"right": "5%",
"skin": "sknNumber",
"text": "0.060 JOD",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0d6a0898aed774d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0d6a0898aed774d",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "95%",
"zIndex": 1
}, {}, {});
Copydivider0d6a0898aed774d.setDefaultUnit(kony.flex.DP);
Copydivider0d6a0898aed774d.add();
flxAfterTopup.add(lblAfterTopup, lblAfterBal, CopyhiddenlblCommRate0f56bc71ae91f48, Copydivider0d6a0898aed774d);
var CopyFlexContainer0fc7f7685c2ae49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "CopyFlexContainer0fc7f7685c2ae49",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "4%",
"width": "100%",
"zIndex": 100
}, {}, {});
CopyFlexContainer0fc7f7685c2ae49.setDefaultUnit(kony.flex.DP);
var btnWebConfirm = new kony.ui.Button({
"centerX": "50.03%",
"focusSkin": "sknprimaryAction",
"height": "90%",
"id": "btnWebConfirm",
"isVisible": true,
"onClick": AS_Button_b9c6e60dc0c6499abc6f04d3423699cd,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.P2P.confirm"),
"top": "5%",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopybtnEditTranscation0g2d647ca0dd94c = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "35dp",
"id": "CopybtnEditTranscation0g2d647ca0dd94c",
"isVisible": false,
"onClick": AS_Button_ecab9899355944a9855254a0183c3082,
"skin": "sknsecondaryAction",
"text": "Edit",
"top": "5dp",
"width": "260dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyFlexContainer0fc7f7685c2ae49.add(btnWebConfirm, CopybtnEditTranscation0g2d647ca0dd94c);
flxWebCharge.add(flxCurrBal, flxFrmAc, flxWebAmt, flxWebFee, flxAfterTopup, CopyFlexContainer0fc7f7685c2ae49);
frmConfirmTransferKA.add(titleBarWrapper, flxTransfers, flxWebCharge);
};
function frmConfirmTransferKAGlobalsAr() {
frmConfirmTransferKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmConfirmTransferKAAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmConfirmTransferKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_5661b0311bfe4bc694c99238cbe0a7f5,
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
