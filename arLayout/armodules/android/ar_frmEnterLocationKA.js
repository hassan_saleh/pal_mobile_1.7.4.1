//Do not Modify!! This is an auto generated module for 'android'. Generated on Tue Sep 15 00:13:40 EEST 2020
function addWidgetsfrmEnterLocationKAAr() {
frmEnterLocationKA.setDefaultUnit(kony.flex.DP);
var overview = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "overview",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
overview.setDefaultUnit(kony.flex.DP);
var titleBarAccountInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "85dp",
"id": "titleBarAccountInfo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarAccountInfo.setDefaultUnit(kony.flex.DP);
var flxAndroidTittleBarKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "53dp",
"id": "flxAndroidTittleBarKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAndroidTittleBarKA.setDefaultUnit(kony.flex.DP);
var titleBarLabel = new kony.ui.Label({
"centerX": "50%",
"id": "titleBarLabel",
"isVisible": true,
"skin": "skniconButtonLabel",
"text": kony.i18n.getLocalizedString("i18n.common.openinganAccount"),
"top": "12dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var androidBack = new kony.ui.Button({
"focusSkin": "sknandroidBackButtonFocus",
"height": "50dp",
"id": "androidBack",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_6bba69e6f8b647208571c47215dd549f,
"skin": "sknandroidBackButton",
"top": "0dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxAndroidTittleBarKA.add(titleBarLabel, androidBack);
var lblPickAProductKA = new kony.ui.Label({
"centerX": "50%",
"id": "lblPickAProductKA",
"isVisible": true,
"skin": "skniconButtonLabel",
"text": kony.i18n.getLocalizedString("i18n.opening_account.enterLocation"),
"top": "53dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxTransitionKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50.00%",
"clipBounds": true,
"height": "20dp",
"id": "flxTransitionKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"top": "35dp",
"width": "200dp",
"zIndex": 1
}, {}, {});
flxTransitionKA.setDefaultUnit(kony.flex.DP);
var flx1KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx1KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx1KA.setDefaultUnit(kony.flex.DP);
flx1KA.add();
var flx2KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx2KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": "0dp",
"width": "20dp"
}, {}, {});
flx2KA.setDefaultUnit(kony.flex.DP);
flx2KA.add();
var flx3KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "49.44%",
"clipBounds": true,
"height": "5dp",
"id": "flx3KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "skncontainerBkgWhite",
"top": 0,
"width": "20dp"
}, {}, {});
flx3KA.setDefaultUnit(kony.flex.DP);
flx3KA.add();
var flx4KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx4KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx4KA.setDefaultUnit(kony.flex.DP);
flx4KA.add();
var flx5KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx5KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx5KA.setDefaultUnit(kony.flex.DP);
flx5KA.add();
var flx6KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx6KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx6KA.setDefaultUnit(kony.flex.DP);
flx6KA.add();
var flx7KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx7KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx7KA.setDefaultUnit(kony.flex.DP);
flx7KA.add();
var flx8KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx8KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx8KA.setDefaultUnit(kony.flex.DP);
flx8KA.add();
flxTransitionKA.add( flx8KA, flx7KA, flx6KA, flx5KA, flx4KA, flx3KA, flx2KA,flx1KA);
titleBarAccountInfo.add(flxAndroidTittleBarKA, lblPickAProductKA, flxTransitionKA);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": 0,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkgGray",
"top": "0dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var challengeQuestionContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "challengeQuestionContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
challengeQuestionContainer.setDefaultUnit(kony.flex.DP);
var lblQuestion = new kony.ui.Label({
"height": "32dp",
"id": "lblQuestion",
"isVisible": true,
"right": "5%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.opening_account.yourLocation"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var answerFIeldWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "answerFIeldWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {}, {});
answerFIeldWrapper.setDefaultUnit(kony.flex.DP);
var ListState = new kony.ui.ListBox({
"centerY": "50.00%",
"focusSkin": "sknslListBox",
"height": "100%",
"id": "ListState",
"isVisible": true,
"right": "4.97%",
"masterData": [["lb1", "Listbox One"],["lb2", "Listbox Two"],["lb3", "Listbox Three"]],
"skin": "sknslListBox",
"top": "33dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"applySkinsToPopup": true,
"dropDownImage": "dropdown.png",
"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
});
var lblSepKA = new kony.ui.Label({
"bottom": "0dp",
"height": "1dp",
"id": "lblSepKA",
"isVisible": true,
"left": "0dp",
"skin": "sknLineEDEDEDKA",
"text": kony.i18n.getLocalizedString("i18n.opening_account.frmEnterLocationKA.lblSepKA"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
answerFIeldWrapper.add(ListState, lblSepKA);
challengeQuestionContainer.add(lblQuestion, answerFIeldWrapper);
var enableTouchID = new kony.ui.Button({
"centerX": "50.03%",
"focusSkin": "sknprimaryActionFocus",
"height": "42dp",
"id": "enableTouchID",
"isVisible": true,
"onClick": AS_Button_8db87bb4a1424a639a2ea55c0137630a,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.settings.cProceed"),
"top": "40dp",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var noThanks = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "35dp",
"id": "noThanks",
"isVisible": true,
"onClick": AS_Button_fab1e1477c8f4e5bb1d486d7e4103a38,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.common.cancel"),
"top": "4dp",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
mainContent.add(challengeQuestionContainer, enableTouchID, noThanks);
overview.add(titleBarAccountInfo, mainContent);
frmEnterLocationKA.add(overview);
};
function frmEnterLocationKAGlobalsAr() {
frmEnterLocationKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmEnterLocationKAAr,
"bounces": true,
"enableScrolling": true,
"enabledForIdleTimeout": true,
"id": "frmEnterLocationKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_28e06581c3f14a90921e2454a2613f21,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"inTransitionConfig": {
"formAnimation": 0
},
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_2814761571104ec7b9db67d2aa174c36,
"outTransitionConfig": {
"formAnimation": 0
},
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
