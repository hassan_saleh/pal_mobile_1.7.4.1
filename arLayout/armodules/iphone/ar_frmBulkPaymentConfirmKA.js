//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:57 EEST 2020
function addWidgetsfrmBulkPaymentConfirmKAAr() {
frmBulkPaymentConfirmKA.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFlxHeaderImg",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_aff5468e20d94fb7b4cc147501dd2dee,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, lblBack);
var lblBackArrow = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackArrow",
"isVisible": false,
"left": "1%",
"skin": "sknBackIcon",
"text": "j",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBackBtn = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackBtn",
"isVisible": false,
"left": "7%",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "45%",
"height": "70%",
"id": "lblTitle",
"isVisible": true,
"maxNumberOfLines": 1,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.bulkpayment.title"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxHeader.add(flxBack, lblBackArrow, lblBackBtn, lblTitle);
var flxMainContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxMainContainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "9%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMainContainer.setDefaultUnit(kony.flex.DP);
var bulkpaymentsegmant = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"accountnumber": "",
"btnBillsPayAccounts": "q",
"dueAmount": "",
"lblBillerType": "",
"lblBulkSelection": "r",
"lblInitial": "",
"lblpaidAmount": "asdfghj98765432",
"paidAmount": "Label",
"payeename": "",
"payeenickname": "",
"txtpaidAmount": "TextBox2"
}],
"groupCells": false,
"height": "85%",
"id": "bulkpaymentsegmant",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "slSegSendMoney",
"rowTemplate": flxBulkPaymentSeg,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": false,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"accountnumber": "accountnumber",
"btnBillsPayAccounts": "btnBillsPayAccounts",
"contactListDivider": "contactListDivider",
"dueAmount": "dueAmount",
"flxAnimate": "flxAnimate",
"flxBulkPaymentSeg": "flxBulkPaymentSeg",
"flxDetails": "flxDetails",
"flxIcon1": "flxIcon1",
"flxIconContainer": "flxIconContainer",
"flxPaidAmount": "flxPaidAmount",
"flxToAnimate": "flxToAnimate",
"hiddenDueAmount": "hiddenDueAmount",
"lblBillerType": "lblBillerType",
"lblBulkSelection": "lblBulkSelection",
"lblInitial": "lblInitial",
"lblTick": "lblTick",
"lblpaidAmount": "lblpaidAmount",
"paidAmount": "paidAmount",
"payeename": "payeename",
"payeenickname": "payeenickname",
"txtpaidAmount": "txtpaidAmount"
},
"width": "100%",
"zIndex": 2
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": true,
"editStyle": constants.SEGUI_EDITING_STYLE_SWIPE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": true
});
var btnBillsScreen = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "10%",
"id": "btnBillsScreen",
"isVisible": true,
"right": "1%",
"onClick": AS_Button_c105e4a46e8a4125817967f83e644928,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.cards.paynow"),
"top": "1%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxMainContainer.add(bulkpaymentsegmant, btnBillsScreen);
frmBulkPaymentConfirmKA.add(flxHeader, flxMainContainer);
};
function frmBulkPaymentConfirmKAGlobalsAr() {
frmBulkPaymentConfirmKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmBulkPaymentConfirmKAAr,
"allowVerticalBounce": true,
"bounces": true,
"enabledForIdleTimeout": true,
"id": "frmBulkPaymentConfirmKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": true,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": true,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
