//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetempJomopayPopupAr() {
    flxJPPopupAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxJPPopup",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxJPPopupAr.setDefaultUnit(kony.flex.DP);
    var lblJPTransfer = new kony.ui.Label({
        "height": "100%",
        "id": "lblJPTransfer",
        "isVisible": true,
        "right": "0%",
        "skin": "sknTransferTypeDrop",
        "top": "0%",
        "width": "100%",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,5, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblJomopayType = new kony.ui.Label({
        "height": "1%",
        "id": "lblJomopayType",
        "isVisible": true,
        "right": "0%",
        "skin": "sknJPType",
        "text": "Label",
        "top": "0%",
        "width": "1%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxJPPopupAr.add(lblJPTransfer, lblJomopayType);
}
