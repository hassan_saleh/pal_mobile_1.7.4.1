//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetempSectionHeaderP2PAr() {
CopyFlexContainer0i47e9b94f6b74fAr = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40dp",
"id": "CopyFlexContainer0i47e9b94f6b74f",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"skin": "sknCopyslFbox07d05709853a74d"
}, {}, {});
CopyFlexContainer0i47e9b94f6b74fAr.setDefaultUnit(kony.flex.DP);
var lblSepKA = new kony.ui.Label({
"height": "1dp",
"id": "lblSepKA",
"isVisible": true,
"left": "0dp",
"skin": "sknLineEDEDEDKA",
"top": "0dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxP2PphonePayeeNameKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "41.97%",
"id": "flxP2PphonePayeeNameKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxP2PphonePayeeNameKA.setDefaultUnit(kony.flex.DP);
var payeeFirstName = new kony.ui.Label({
"centerY": "50%",
"height": "20dp",
"id": "payeeFirstName",
"isVisible": true,
"right": "5%",
"skin": "skn383838LatoRegular107KA",
"top": "30dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var payeeLastName = new kony.ui.Label({
"centerY": "50%",
"height": "20dp",
"id": "payeeLastName",
"isVisible": true,
"right": "2%",
"skin": "skn383838LatoRegular107KA",
"top": "10dp",
"width": "65%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxP2PphonePayeeNameKA.add( payeeLastName,payeeFirstName);
CopyFlexContainer0i47e9b94f6b74fAr.add(lblSepKA, flxP2PphonePayeeNameKA);
}
