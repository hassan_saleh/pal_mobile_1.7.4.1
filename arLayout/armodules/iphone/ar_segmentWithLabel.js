//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializesegmentWithLabelAr() {
    Copycontainer09742046d0d2944Ar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "Copycontainer09742046d0d2944",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    Copycontainer09742046d0d2944Ar.setDefaultUnit(kony.flex.DP);
    var lblSettingsNameKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSettingsNameKA",
        "isVisible": true,
        "right": "5%",
        "skin": "skn",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblSettingsStatusKA = new kony.ui.Label({
        "centerY": "48.72%",
        "id": "lblSettingsStatusKA",
        "isVisible": true,
        "left": "35dp",
        "skin": "sknSegSecondLabel",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgProgressKey = new kony.ui.Image2({
        "centerY": "50%",
        "height": "20dp",
        "id": "imgProgressKey",
        "isVisible": true,
        "left": "4%",
        "skin": "sknslImage",
        "src": "right_chevron_icon.png",
        "width": "20dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var Hidden = new kony.ui.Label({
        "id": "Hidden",
        "isVisible": false,
        "right": "145dp",
        "skin": "slLabel",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    Copycontainer09742046d0d2944Ar.add(lblSettingsNameKA, lblSettingsStatusKA, imgProgressKey, Hidden);
}
