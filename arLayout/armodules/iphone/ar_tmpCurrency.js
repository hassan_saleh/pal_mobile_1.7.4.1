//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetmpCurrencyAr() {
tmpFlxCurrencyAr = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "tmpFlxCurrency",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox"
}, {}, {});
tmpFlxCurrencyAr.setDefaultUnit(kony.flex.DP);
var flxCountryIcon = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "80%",
"id": "flxCountryIcon",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "slFbox",
"top": "0%",
"width": "15%",
"zIndex": 1
}, {}, {});
flxCountryIcon.setDefaultUnit(kony.flex.DP);
var imgCountryIcon = new kony.ui.Image2({
"height": "100%",
"id": "imgCountryIcon",
"isVisible": true,
"right": "0%",
"skin": "slImage",
"src": "imagedrag.png",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxCountryIcon.add(imgCountryIcon);
var lblMobileCountry = new kony.ui.Label({
"centerY": "50%",
"id": "lblMobileCountry",
"isVisible": false,
"right": "23%",
"skin": "sknLblWhike125",
"top": "0%",
"width": "72%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxCurrencydetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxCurrencydetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "23%",
"skin": "slFbox",
"top": "0dp",
"width": "54%",
"zIndex": 1
}, {}, {});
flxCurrencydetails.setDefaultUnit(kony.flex.DP);
var lblCurrency = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblCurrency",
"isVisible": true,
"right": "10%",
"skin": "sknLblWhike125",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblCurrency0c4373e11aa8641 = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblCurrency0c4373e11aa8641",
"isVisible": true,
"right": "1%",
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18n.common.open"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblDescriptionEn = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblDescriptionEn",
"isVisible": true,
"right": "1%",
"skin": "sknLblWhike125",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblDescriptionAr = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblDescriptionAr",
"isVisible": true,
"right": "0%",
"skin": "sknLblWhike125",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblDescriptionAr0ca1638a7c3ee4b = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblDescriptionAr0ca1638a7c3ee4b",
"isVisible": true,
"right": "0%",
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18n.common.close"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblDecimal = new kony.ui.Label({
"centerY": "50%",
"id": "lblDecimal",
"isVisible": false,
"right": "5%",
"skin": "sknLblWhike125",
"text": "EUR",
"top": "16dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCurrCode = new kony.ui.Label({
"centerY": "50%",
"id": "lblCurrCode",
"isVisible": false,
"right": "5%",
"skin": "sknLblWhike125",
"text": "EUR",
"top": "16dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCurrencydetails.add( lblCurrCode, lblDecimal, CopylblDescriptionAr0ca1638a7c3ee4b, lblDescriptionAr, lblDescriptionEn, CopylblCurrency0c4373e11aa8641,lblCurrency);
var imgTickIcon = new kony.ui.Label({
"height": "100%",
"id": "imgTickIcon",
"isVisible": false,
"right": "80%",
"skin": "sknBOJttf200NOOPC",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
tmpFlxCurrencyAr.add(flxCountryIcon, lblMobileCountry, flxCurrencydetails, imgTickIcon);
}
