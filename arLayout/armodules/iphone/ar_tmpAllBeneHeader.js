//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetmpAllBeneHeaderAr() {
    flxTmpHeaderAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxTmpHeader",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "slFboxTemplateHeader"
    }, {}, {});
    flxTmpHeaderAr.setDefaultUnit(kony.flex.DP);
    var lbltmpTitle = new kony.ui.Label({
        "height": "99%",
        "id": "lbltmpTitle",
        "isVisible": true,
        "right": "5%",
        "skin": "slLabelTitle",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 2,0, 2],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxUnderline = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "flxUnderline",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderline.setDefaultUnit(kony.flex.DP);
    flxUnderline.add();
    flxTmpHeaderAr.add(lbltmpTitle, flxUnderline);
}
