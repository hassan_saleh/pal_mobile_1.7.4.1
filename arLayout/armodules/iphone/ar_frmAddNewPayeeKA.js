//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:56 EEST 2020
function addWidgetsfrmAddNewPayeeKAAr() {
frmAddNewPayeeKA.setDefaultUnit(kony.flex.DP);
var mainScrollContainer = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": "0dp",
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"horizontalScrollIndicator": true,
"id": "mainScrollContainer",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkgGray",
"top": "50dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
mainScrollContainer.setDefaultUnit(kony.flex.DP);
var flxUpperContainerKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "75%",
"id": "flxUpperContainerKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxUpperContainerKA.setDefaultUnit(kony.flex.DP);
var payeeNameContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "payeeNameContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "5dp",
"width": "100%",
"zIndex": 1
}, {}, {});
payeeNameContainer.setDefaultUnit(kony.flex.DP);
var CopyLabel0b353ede7efad4c = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel0b353ede7efad4c",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.manage_payee.companyName"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyusernameContainer0c123209f151c45 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "CopyusernameContainer0c123209f151c45",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyusernameContainer0c123209f151c45.setDefaultUnit(kony.flex.DP);
var newPayeeNameTextfield = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_WORDS,
"focusSkin": "skngeneralTextFieldFocus",
"height": 37,
"id": "newPayeeNameTextfield",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0dp",
"onTextChange": AS_TextField_ec5cb8f5d08e4c79b30beded2d060103,
"placeholder": kony.i18n.getLocalizedString("i18n.manage_payee.enterCompanyNameHerePlh"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxDivider1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "flxDivider1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDivider1.setDefaultUnit(kony.flex.DP);
flxDivider1.add();
CopyusernameContainer0c123209f151c45.add(newPayeeNameTextfield, flxDivider1);
payeeNameContainer.add(CopyLabel0b353ede7efad4c, CopyusernameContainer0c123209f151c45);
var flxPayeeNickNameContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "flxPayeeNickNameContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"width": "100%",
"zIndex": 1
}, {}, {});
flxPayeeNickNameContainer.setDefaultUnit(kony.flex.DP);
var CopyLabel0927085edf80e42 = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel0927085edf80e42",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.manage_payee.payeeNickName"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyusernameContainer0b764e42673504b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "CopyusernameContainer0b764e42673504b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyusernameContainer0b764e42673504b.setDefaultUnit(kony.flex.DP);
var nickName = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_WORDS,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "nickName",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.manage_payee.enterPayeeNickNameHere"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyflxDivider0b294261c43344c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyflxDivider0b294261c43344c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxDivider0b294261c43344c.setDefaultUnit(kony.flex.DP);
CopyflxDivider0b294261c43344c.add();
CopyusernameContainer0b764e42673504b.add(nickName, CopyflxDivider0b294261c43344c);
flxPayeeNickNameContainer.add(CopyLabel0927085edf80e42, CopyusernameContainer0b764e42673504b);
var payeeAccountContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "150dp",
"id": "payeeAccountContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"width": "100%",
"zIndex": 1
}, {}, {});
payeeAccountContainer.setDefaultUnit(kony.flex.DP);
var CopyLabel0200c64cb34cb44 = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel0200c64cb34cb44",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyusernameContainer09871c009dbc247 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "84dp",
"id": "CopyusernameContainer09871c009dbc247",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyusernameContainer09871c009dbc247.setDefaultUnit(kony.flex.DP);
var newPayeeAccountNumberTextField = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "newPayeeAccountNumberTextField",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.manage_payee.enterAmountNumberHere"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyflxDivider07de2109b1c0a4d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyflxDivider07de2109b1c0a4d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxDivider07de2109b1c0a4d.setDefaultUnit(kony.flex.DP);
CopyflxDivider07de2109b1c0a4d.add();
var tboxReenterAccountNumberKA = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "tboxReenterAccountNumberKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.manage_payee.reEnterAccountNumberPlh"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyflxDivider0a70939b9027043 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyflxDivider0a70939b9027043",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxDivider0a70939b9027043.setDefaultUnit(kony.flex.DP);
CopyflxDivider0a70939b9027043.add();
CopyusernameContainer09871c009dbc247.add(newPayeeAccountNumberTextField, CopyflxDivider07de2109b1c0a4d, tboxReenterAccountNumberKA, CopyflxDivider0a70939b9027043);
payeeAccountContainer.add(CopyLabel0200c64cb34cb44, CopyusernameContainer09871c009dbc247);
flxUpperContainerKA.add(payeeNameContainer, flxPayeeNickNameContainer, payeeAccountContainer);
var payeeAddressContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100dp",
"id": "payeeAddressContainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "55%",
"width": "100%",
"zIndex": 1
}, {}, {});
payeeAddressContainer.setDefaultUnit(kony.flex.DP);
var CopyLabel0b1ac08ce375a4c = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel0b1ac08ce375a4c",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.manage_payee.payeeAddressC"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyusernameContainer012a8b92d34cd4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "36dp",
"id": "CopyusernameContainer012a8b92d34cd4b",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyusernameContainer012a8b92d34cd4b.setDefaultUnit(kony.flex.DP);
var CopyusernameTextField0f7d7f563446f42 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 32,
"id": "CopyusernameTextField0f7d7f563446f42",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.manage_payee.streetPlh"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyFlexContainer09fbd523e9a6c43 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyFlexContainer09fbd523e9a6c43",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer09fbd523e9a6c43.setDefaultUnit(kony.flex.DP);
CopyFlexContainer09fbd523e9a6c43.add();
CopyusernameContainer012a8b92d34cd4b.add(CopyusernameTextField0f7d7f563446f42, CopyFlexContainer09fbd523e9a6c43);
var CopyusernameContainer0eea4f72ba00743 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "38dp",
"id": "CopyusernameContainer0eea4f72ba00743",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "6dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyusernameContainer0eea4f72ba00743.setDefaultUnit(kony.flex.DP);
var CopyusernameTextField08388e41c88cd4a = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "CopyusernameTextField08388e41c88cd4a",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0dp",
"placeholder": "Apt, P.O. Box",
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var FlexContainer0da5c3167759a45 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "FlexContainer0da5c3167759a45",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer0da5c3167759a45.setDefaultUnit(kony.flex.DP);
FlexContainer0da5c3167759a45.add();
CopyusernameContainer0eea4f72ba00743.add(CopyusernameTextField08388e41c88cd4a, FlexContainer0da5c3167759a45);
var CopyusernameContainer0740d926171f149 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "38dp",
"id": "CopyusernameContainer0740d926171f149",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "6dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyusernameContainer0740d926171f149.setDefaultUnit(kony.flex.DP);
var CopyusernameTextField01fe47525d95442 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "CopyusernameTextField01fe47525d95442",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.manage_payee.cityPlh"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyFlexContainer0f3582dde3eac40 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyFlexContainer0f3582dde3eac40",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0f3582dde3eac40.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0f3582dde3eac40.add();
CopyusernameContainer0740d926171f149.add(CopyusernameTextField01fe47525d95442, CopyFlexContainer0f3582dde3eac40);
var CopyusernameContainer083753b47661045 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "38dp",
"id": "CopyusernameContainer083753b47661045",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "6dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyusernameContainer083753b47661045.setDefaultUnit(kony.flex.DP);
var CopyusernameTextField0411be02f9f4548 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "CopyusernameTextField0411be02f9f4548",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.manage_payee.statePlh"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyFlexContainer0e65f2830703a49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyFlexContainer0e65f2830703a49",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0e65f2830703a49.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0e65f2830703a49.add();
CopyusernameContainer083753b47661045.add(CopyusernameTextField0411be02f9f4548, CopyFlexContainer0e65f2830703a49);
var CopyusernameContainer09da656c676db42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "38dp",
"id": "CopyusernameContainer09da656c676db42",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "6dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyusernameContainer09da656c676db42.setDefaultUnit(kony.flex.DP);
var zipcode = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "zipcode",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.manage_payee.zipCode"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyFlexContainer02077f851ad8c46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyFlexContainer02077f851ad8c46",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer02077f851ad8c46.setDefaultUnit(kony.flex.DP);
CopyFlexContainer02077f851ad8c46.add();
CopyusernameContainer09da656c676db42.add(zipcode, CopyFlexContainer02077f851ad8c46);
payeeAddressContainer.add(CopyLabel0b1ac08ce375a4c, CopyusernameContainer012a8b92d34cd4b, CopyusernameContainer0eea4f72ba00743, CopyusernameContainer0740d926171f149, CopyusernameContainer083753b47661045, CopyusernameContainer09da656c676db42);
var buttonWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "350dp",
"id": "buttonWrapper",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "72%",
"width": "100%",
"zIndex": 1
}, {}, {});
buttonWrapper.setDefaultUnit(kony.flex.DP);
var saveNewPayeeButton = new kony.ui.Button({
"centerX": "50.00%",
"focusSkin": "sknprimaryActionFocus",
"height": "42dp",
"id": "saveNewPayeeButton",
"isVisible": true,
"onClick": AS_Button_3d65bbd843c24bd2b9c8aec41e7d7b66,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.common.btnAddPayee"),
"top": "40dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var deleteSavedPayeeButton = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknprimaryActionFocus",
"height": "42dp",
"id": "deleteSavedPayeeButton",
"isVisible": false,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.transfer.deletePayee"),
"top": "10dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
buttonWrapper.add(saveNewPayeeButton, deleteSavedPayeeButton);
var flxScrollCompanyListKA = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": true,
"allowVerticalBounce": true,
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"horizontalScrollIndicator": true,
"id": "flxScrollCompanyListKA",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"pagingEnabled": false,
"left": "5%",
"scrollDirection": kony.flex.SCROLL_HORIZONTAL,
"skin": "sknscrollBkgGray",
"top": "70dp",
"verticalScrollIndicator": true,
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 10
}, {}, {});
flxScrollCompanyListKA.setDefaultUnit(kony.flex.DP);
var segCompanyListKA = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"lblCompanyNameKA": "Kony",
"lblSepKA": "Label"
}, {
"lblCompanyNameKA": "Gati",
"lblSepKA": "Label"
}, {
"lblCompanyNameKA": "Adobe",
"lblSepKA": "Label"
}, {
"lblCompanyNameKA": "Amazon",
"lblSepKA": "Label"
}, {
"lblCompanyNameKA": "Microsoft",
"lblSepKA": "Label"
}, {
"lblCompanyNameKA": "Pramati",
"lblSepKA": "Label"
}, {
"lblCompanyNameKA": "Pega",
"lblSepKA": "Label"
}, {
"lblCompanyNameKA": "Factset",
"lblSepKA": "Label"
}, {
"lblCompanyNameKA": "Intel",
"lblSepKA": "Label"
}, {
"lblCompanyNameKA": "Kony Private Ltd",
"lblSepKA": "Label"
}, {
"lblCompanyNameKA": "Adobe2",
"lblSepKA": "Label"
}, {
"lblCompanyNameKA": "Amazon3",
"lblSepKA": "Label"
}, {
"lblCompanyNameKA": "Micro",
"lblSepKA": "Label"
}],
"groupCells": false,
"height": "100%",
"id": "segCompanyListKA",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_c5acee60d33647ee96e481a376cb0a0d,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": FlexContainer040c5584262324d,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "f7f7f700",
"separatorRequired": true,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"FlexContainer040c5584262324d": "FlexContainer040c5584262324d",
"lblCompanyNameKA": "lblCompanyNameKA",
"lblSepKA": "lblSepKA"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": true,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": true
});
flxScrollCompanyListKA.add(segCompanyListKA);
mainScrollContainer.add(flxUpperContainerKA, payeeAddressContainer, buttonWrapper, flxScrollCompanyListKA);
var titleBarAddNewPayee = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "titleBarAddNewPayee",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntitleBarGradient",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarAddNewPayee.setDefaultUnit(kony.flex.DP);
var billPayeeTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "billPayeeTitle",
"isVisible": true,
"skin": "sknnavBarTitle",
"text": kony.i18n.getLocalizedString("i18n.common.addNewPayee"),
"width": "70%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopybuttonLeft0a6a3590306e947 = new kony.ui.Button({
"focusSkin": "skntitleBarTextButtonFocus",
"height": "50dp",
"id": "CopybuttonLeft0a6a3590306e947",
"isVisible": true,
"right": "0dp",
"minWidth": "50dp",
"skin": "skntitleBarTextButton",
"text": kony.i18n.getLocalizedString("i18n.common.cancel"),
"top": "0dp",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
titleBarAddNewPayee.add(billPayeeTitle, CopybuttonLeft0a6a3590306e947);
var flxFormMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxFormMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFormMain.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "18%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFlxHeaderImg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxTop = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxTop",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTop.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_f3356ee011ad4123b4387eb28e9de9e3,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, lblBack);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "70%",
"id": "lblTitle",
"isVisible": true,
"maxNumberOfLines": 1,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.billsPay.AddNewBill"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblNext = new kony.ui.Label({
"centerY": "50%",
"id": "lblNext",
"isVisible": true,
"left": "86%",
"onTouchEnd": AS_Label_ef9b03e39df748ff8610232ad085c6e1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTop.add(flxBack, lblTitle, lblNext);
var flxTab = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40%",
"id": "flxTab",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "5%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTab.setDefaultUnit(kony.flex.DP);
var flxContent = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "65%",
"id": "flxContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFboxOuterRing",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxContent.setDefaultUnit(kony.flex.DP);
var btnPostPaid = new kony.ui.Button({
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnPostPaid",
"isVisible": true,
"right": "0",
"onClick": AS_Button_fa5009557c21436e9a14a2359489b282,
"skin": "slButtonWhiteTab",
"text": kony.i18n.getLocalizedString("i18n.billsPay.PostPaid"),
"top": "0dp",
"width": "50%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnPrePaid = new kony.ui.Button({
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnPrePaid",
"isVisible": true,
"right": "0",
"onClick": AS_Button_f695e3b1c8784421bd63905290e0a6d4,
"skin": "slButtonWhiteTabDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.PrePaid"),
"top": "0dp",
"width": "50%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxContent.add( btnPrePaid,btnPostPaid);
flxTab.add(flxContent);
flxHeader.add(flxTop, flxTab);
var flxMain = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "82%",
"horizontalScrollIndicator": true,
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxNickName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxNickName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxNickName.setDefaultUnit(kony.flex.DP);
var tbxNickName = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxNickName",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"maxTextLength": 35,
"onDone": AS_TextField_j79cec333d1a412fbe4debcf27b03175,
"onTextChange": AS_TextField_g7b302fd4ef845969deaa12147f337f9,
"onTouchEnd": AS_TextField_gc448713c0884fdd83ee180df39f65b0,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_b1c4c6e280d64e92a169c41c1668ce00,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineNickName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineNickName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"skin": "sknFlxGreyLine",
"top": "93%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineNickName.setDefaultUnit(kony.flex.DP);
flxUnderlineNickName.add();
var lblNickName = new kony.ui.Label({
"id": "lblNickName",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.NickName"),
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNickName.add(tbxNickName, flxUnderlineNickName, lblNickName);
var flxBillerCategory = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBillerCategory",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBillerCategory.setDefaultUnit(kony.flex.DP);
var tbxBillerCategory = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxBillerCategory",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "2%",
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBillerCategory = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBillerCategory",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"skin": "sknFlxGreyLine",
"top": "93%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBillerCategory.setDefaultUnit(kony.flex.DP);
flxUnderlineBillerCategory.add();
var flxBillerCategoryHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"clipBounds": true,
"height": "60%",
"id": "flxBillerCategoryHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"onClick": AS_FlexContainer_j718fdec73e142dc9499396d118608f5,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxBillerCategoryHolder.setDefaultUnit(kony.flex.DP);
var lblBillerCategory = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblBillerCategory",
"isVisible": true,
"right": "0%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.BillerCategory"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "88%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowBillerCategory = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowBillerCategory",
"isVisible": true,
"right": "91%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBillerCategoryHolder.add(lblBillerCategory, lblArrowBillerCategory);
flxBillerCategory.add(tbxBillerCategory, flxUnderlineBillerCategory, flxBillerCategoryHolder);
var flxBillerName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBillerName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBillerName.setDefaultUnit(kony.flex.DP);
var tbxBillerName = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxBillerName",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "2%",
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBillerName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBillerName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"skin": "sknFlxGreyLine",
"top": "93%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBillerName.setDefaultUnit(kony.flex.DP);
flxUnderlineBillerName.add();
var flxBillerNameHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"clipBounds": true,
"height": "60%",
"id": "flxBillerNameHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"onClick": AS_FlexContainer_h7875446afef460b9bbe2ae4626fb49b,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxBillerNameHolder.setDefaultUnit(kony.flex.DP);
var lblBillerName = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblBillerName",
"isVisible": true,
"right": "0%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.billerName"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "88%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowBillerName = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowBillerName",
"isVisible": true,
"right": "91%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblEditBillerNameTitile = new kony.ui.Label({
"id": "lblEditBillerNameTitile",
"isVisible": false,
"right": "0%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled80",
"text": kony.i18n.getLocalizedString("i18n.billsPay.billerName"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "-10%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBillerNameHolder.add(lblBillerName, lblArrowBillerName, lblEditBillerNameTitile);
flxBillerName.add(tbxBillerName, flxUnderlineBillerName, flxBillerNameHolder);
var flxServiceType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxServiceType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxServiceType.setDefaultUnit(kony.flex.DP);
var tbxServiceType = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxServiceType",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "2%",
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineServiceType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineServiceType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"skin": "sknFlxGreyLine",
"top": "93%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineServiceType.setDefaultUnit(kony.flex.DP);
flxUnderlineServiceType.add();
var flxServiceTypeHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"clipBounds": true,
"height": "60%",
"id": "flxServiceTypeHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"onClick": AS_FlexContainer_g26c512b8d3a441cbc0b00a95a9ea25b,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxServiceTypeHolder.setDefaultUnit(kony.flex.DP);
var lblServiceType = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblServiceType",
"isVisible": true,
"right": "0%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "88%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowServiceType = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowServiceType",
"isVisible": true,
"right": "91%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblEditBillerServiceTypeTitle = new kony.ui.Label({
"id": "lblEditBillerServiceTypeTitle",
"isVisible": false,
"right": "0%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled80",
"text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "-10%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxServiceTypeHolder.add(lblServiceType, lblArrowServiceType, lblEditBillerServiceTypeTitle);
flxServiceType.add(tbxServiceType, flxUnderlineServiceType, flxServiceTypeHolder);
var flxBillerNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "85dp",
"id": "flxBillerNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBillerNumber.setDefaultUnit(kony.flex.DP);
var tbxBillerNumber = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "22%",
"focusSkin": "sknTxtBox",
"height": "55%",
"id": "tbxBillerNumber",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"maxTextLength": 35,
"onDone": AS_TextField_c8c672a59c4341f68580bcb159640130,
"onTextChange": AS_TextField_a4791ca534d5476d8725309f588bcff1,
"onTouchEnd": AS_TextField_f3d69723274e493eb1c164bbf94ce559,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_eb6b320afa1544f49739c5f08ca4b586,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBillerNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBillerNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "1%",
"skin": "sknFlxGreyLine",
"top": "75%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBillerNumber.setDefaultUnit(kony.flex.DP);
flxUnderlineBillerNumber.add();
var lblBillerNumber = new kony.ui.Label({
"id": "lblBillerNumber",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.BillerNumber"),
"top": "35%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblserviceTypeHint = new kony.ui.Label({
"id": "lblserviceTypeHint",
"isVisible": true,
"right": "2%",
"skin": "sknLblWhite100",
"top": "75%",
"width": "96%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBillerNumber.add(tbxBillerNumber, flxUnderlineBillerNumber, lblBillerNumber, lblserviceTypeHint);
var flxIDType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxIDType",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxIDType.setDefaultUnit(kony.flex.DP);
var tbxIDType = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxIDType",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineIDType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineIDType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineIDType.setDefaultUnit(kony.flex.DP);
flxUnderlineIDType.add();
var flxIDTypeHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxIDTypeHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_i8ec933f6c884d1f8f6d1efef4cb2d06,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxIDTypeHolder.setDefaultUnit(kony.flex.DP);
var lblIDType = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblIDType",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": "ID Type",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "85%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowIDType = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowIDType",
"isVisible": true,
"right": "90%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIDTypeHolder.add(lblIDType, lblArrowIDType);
flxIDType.add(tbxIDType, flxUnderlineIDType, flxIDTypeHolder);
var flxBillerID = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBillerID",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBillerID.setDefaultUnit(kony.flex.DP);
var tbxBillerID = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxBillerID",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 35,
"onDone": AS_TextField_h16af155e1bc47138eda1342c4388ad7,
"onTextChange": AS_TextField_c0756115e64f4e1a81ddd590a8430675,
"onTouchEnd": AS_TextField_ac70cace2d8d4c09b1542711af09a37b,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_f1eaf0b38e3e4c75b46e5adf9b5f2c40,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBillerID = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBillerID",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBillerID.setDefaultUnit(kony.flex.DP);
flxUnderlineBillerID.add();
var lblBillerID = new kony.ui.Label({
"id": "lblBillerID",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": "ID Number",
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBillerID.add(tbxBillerID, flxUnderlineBillerID, lblBillerID);
var flxNationality = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxNationality",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxNationality.setDefaultUnit(kony.flex.DP);
var tbxNationality = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxNationality",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineNationality = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineNationality",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineNationality.setDefaultUnit(kony.flex.DP);
flxUnderlineNationality.add();
var flxNationalityHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxNationalityHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_ca2fa8156efd4e65b18b1ca56fdba56c,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxNationalityHolder.setDefaultUnit(kony.flex.DP);
var lblNationality = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblNationality",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": "Nationality",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "85%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowNationality = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowNationality",
"isVisible": true,
"right": "90%",
"skin": "sknBackIconDisabled",
"text": "o",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNationalityHolder.add(lblNationality, lblArrowNationality);
flxNationality.add(tbxNationality, flxUnderlineNationality, flxNationalityHolder);
var flxAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAddress",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAddress.setDefaultUnit(kony.flex.DP);
var tbxAddress = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxAddress",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 150,
"onDone": AS_TextField_hafa4cc9362c4dde9653200f7990a6f3,
"onTextChange": AS_TextField_e0a02c20e7804c4a950331627f1eac74,
"onTouchEnd": AS_TextField_aa987743490d4db2af779dcb4d8dd24c,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_b3bdc8d54ad54429af1e6994bd7992a2,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAddress",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAddress.setDefaultUnit(kony.flex.DP);
flxUnderlineAddress.add();
var lblAddress = new kony.ui.Label({
"id": "lblAddress",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": "Address",
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAddress.add(tbxAddress, flxUnderlineAddress, lblAddress);
var flxPhoneNo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxPhoneNo",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxPhoneNo.setDefaultUnit(kony.flex.DP);
var tbxPhoneNo = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxPhoneNo",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 10,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlinePhoneNo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlinePhoneNo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlinePhoneNo.setDefaultUnit(kony.flex.DP);
flxUnderlinePhoneNo.add();
var lblPhoneNo = new kony.ui.Label({
"id": "lblPhoneNo",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": "Mobile Number",
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxPhoneNo.add(tbxPhoneNo, flxUnderlinePhoneNo, lblPhoneNo);
var flxSpace = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxSpace",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSpace.setDefaultUnit(kony.flex.DP);
flxSpace.add();
flxMain.add(flxNickName, flxBillerCategory, flxBillerName, flxServiceType, flxBillerNumber, flxIDType, flxBillerID, flxNationality, flxAddress, flxPhoneNo, flxSpace);
flxFormMain.add(flxHeader, flxMain);
var flxConfirmPopUp = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxConfirmPopUp",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknFlexBlueBen",
"top": "0dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxConfirmPopUp.setDefaultUnit(kony.flex.DP);
var flxConfirmHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxConfirmHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFlxHeaderImg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmHeader.setDefaultUnit(kony.flex.DP);
var flxHeaderBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxHeaderBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_ec1ea3eaafe04d088afd72a3af5fdb8f,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxHeaderBack.setDefaultUnit(kony.flex.DP);
var lblHeaderBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblHeaderBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblHeaderBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblHeaderBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxHeaderBack.add(lblHeaderBackIcon, lblHeaderBack);
var lblHeaderTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblHeaderTitle",
"isVisible": true,
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnClose = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknDeleteFocus",
"height": "80%",
"id": "btnClose",
"isVisible": true,
"left": "85%",
"onClick": AS_Button_ce970ffb530d433395a3471d82ef648d,
"skin": "sknBtnBack",
"text": "O",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxConfirmHeader.add(flxHeaderBack, lblHeaderTitle, btnClose);
var flxImpDetail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "25%",
"id": "flxImpDetail",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxImpDetail.setDefaultUnit(kony.flex.DP);
var flxIcon1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "50dp",
"id": "flxIcon1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxToIcon",
"top": "7%",
"width": "50dp",
"zIndex": 1
}, {}, {});
flxIcon1.setDefaultUnit(kony.flex.DP);
var lblInitial = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "80%",
"id": "lblInitial",
"isVisible": true,
"skin": "sknLblFromIcon",
"text": "OF",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIcon1.add(lblInitial);
var lblBillerNamePopup = new kony.ui.Label({
"centerX": "50%",
"id": "lblBillerNamePopup",
"isVisible": true,
"skin": "sknBeneTitle",
"text": "Orange Fixed",
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBillerNumPopup = new kony.ui.Label({
"centerX": "50%",
"id": "lblBillerNumPopup",
"isVisible": true,
"right": "0dp",
"skin": "sknLblNextDisabled",
"text": "064385497",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxImpDetail.add(flxIcon1, lblBillerNamePopup, lblBillerNumPopup);
var flxOtherDetails = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "50%",
"horizontalScrollIndicator": true,
"id": "flxOtherDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxOtherDetails.setDefaultUnit(kony.flex.DP);
var flxBillerCategoryPopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxBillerCategoryPopup",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBillerCategoryPopup.setDefaultUnit(kony.flex.DP);
var lblConfirmBillerCategoryTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmBillerCategoryTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.BillerCategory"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmBillerCategory = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmBillerCategory",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "TDFfXYD",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBillerCategoryPopup.add(lblConfirmBillerCategoryTitle, lblConfirmBillerCategory);
var flxServiceTypePopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxServiceTypePopup",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxServiceTypePopup.setDefaultUnit(kony.flex.DP);
var lblConfirmServiceTypeTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmServiceTypeTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmServiceType = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmServiceType",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Fixed",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxServiceTypePopup.add(lblConfirmServiceTypeTitle, lblConfirmServiceType);
var flxIDTypePopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxIDTypePopup",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIDTypePopup.setDefaultUnit(kony.flex.DP);
var lblConfirmIDTypeTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmIDTypeTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "ID Type",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmIDType = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmIDType",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "National ID",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIDTypePopup.add(lblConfirmIDTypeTitle, lblConfirmIDType);
var flxIDNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxIDNumber",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIDNumber.setDefaultUnit(kony.flex.DP);
var lblConfirmIDNumberTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmIDNumberTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "ID Number",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmIDNumber = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmIDNumber",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "38129048293",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIDNumber.add(lblConfirmIDNumberTitle, lblConfirmIDNumber);
var flxNationalityPopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxNationalityPopup",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxNationalityPopup.setDefaultUnit(kony.flex.DP);
var lblConfirmNationalityTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmNationalityTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Nationality",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmNationality = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmNationality",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Jordan",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNationalityPopup.add(lblConfirmNationalityTitle, lblConfirmNationality);
var flxAddressPopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxAddressPopup",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAddressPopup.setDefaultUnit(kony.flex.DP);
var lblConfirmAddressTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmAddressTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Address",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmAddress = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmAddress",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Amman - Haidar Abad",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAddressPopup.add(lblConfirmAddressTitle, lblConfirmAddress);
var flxNickNamePopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxNickNamePopup",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxNickNamePopup.setDefaultUnit(kony.flex.DP);
var lblConfirmNickNameTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmNickNameTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.NickName"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmNickName = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmNickName",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Some random nick name",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNickNamePopup.add(lblConfirmNickNameTitle, lblConfirmNickName);
flxOtherDetails.add(flxBillerCategoryPopup, flxServiceTypePopup, flxIDTypePopup, flxIDNumber, flxNationalityPopup, flxAddressPopup, flxNickNamePopup);
var flxButtonHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "18%",
"id": "flxButtonHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxButtonHolder.setDefaultUnit(kony.flex.DP);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "50dp",
"id": "btnConfirm",
"isVisible": true,
"onClick": AS_Button_hddcd2208b0c4536a39260febe7402c5,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxButtonHolder.add(btnConfirm);
var lblLanguage = new kony.ui.Label({
"id": "lblLanguage",
"isVisible": false,
"right": "-140dp",
"skin": "slLabel",
"text": "eng",
"top": "-700dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblType = new kony.ui.Label({
"id": "lblType",
"isVisible": false,
"right": "-130dp",
"skin": "slLabel",
"top": "-690dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmPopUp.add(flxConfirmHeader, flxImpDetail, flxOtherDetails, flxButtonHolder, lblLanguage, lblType);
frmAddNewPayeeKA.add(mainScrollContainer, titleBarAddNewPayee, flxFormMain, flxConfirmPopUp);
};
function frmAddNewPayeeKAGlobalsAr() {
frmAddNewPayeeKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmAddNewPayeeKAAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmAddNewPayeeKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"postShow": AS_Form_b008a95f9eab4724bfabe353e0e3f53d,
"preShow": AS_Form_e1be7a4a05554298be877b4310e9fced,
"skin": "sknmainGradient",
"statusBarHidden": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": true,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": true,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NEXTPREV,
"inTransitionConfig": {
"transitionDirection": "fromLeft",
"transitionDuration": 0.3,
"transitionEffect": "none"
},
"needsIndicatorDuringPostShow": false,
"outTransitionConfig": {
"transitionDirection": "none",
"transitionDuration": 0.3,
"transitionEffect": "none"
},
"retainScrollPosition": false,
"statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
"titleBar": false
});
};
