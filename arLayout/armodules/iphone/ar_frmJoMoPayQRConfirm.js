//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:58 EEST 2020
function addWidgetsfrmJoMoPayQRConfirmAr() {
frmJoMoPayQRConfirm.setDefaultUnit(kony.flex.DP);
var flxJoMoPayHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxJoMoPayHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJoMoPayHeader.setDefaultUnit(kony.flex.DP);
var lblJoMoPay = new kony.ui.Label({
"centerX": "50%",
"centerY": "45%",
"id": "lblJoMoPay",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.jomopay.confirmdetails"),
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnBack = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0e73a02c4810645",
"height": "100%",
"id": "btnBack",
"isVisible": false,
"right": "0%",
"skin": "CopyslButtonGlossBlue0e73a02c4810645",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0%",
"width": "15%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "2%",
"onClick": AS_FlexContainer_ac304c8e320048d49b605d221e67b807,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"right": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"right": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add( CopylblBack0i5985fbe3b634e,lblBackIcon);
flxJoMoPayHeader.add(lblJoMoPay, btnBack, flxBack);
var flxJoMoPayDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxJoMoPayDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "sknDetails",
"top": "9%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJoMoPayDetails.setDefaultUnit(kony.flex.DP);
var btnProfilePhoto = new kony.ui.Button({
"centerX": "50%",
"centerY": "8%",
"focusSkin": "sknbtnProfileMobile",
"height": "10%",
"id": "btnProfilePhoto",
"isVisible": false,
"skin": "sknbtnProfileMobile",
"text": "F",
"width": "16%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var lblPhoneNo = new kony.ui.Label({
"centerX": "50%",
"id": "lblPhoneNo",
"isVisible": true,
"skin": "sknTransferType",
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxMerchantName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxMerchantName",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMerchantName.setDefaultUnit(kony.flex.DP);
var lblMerchantNameStatic = new kony.ui.Label({
"height": "51%",
"id": "lblMerchantNameStatic",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.jomopay.merchantName"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxMerchantNameInside = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxMerchantNameInside",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "3%",
"skin": "slFbox",
"top": "0",
"width": "96%",
"zIndex": 1
}, {}, {});
flxMerchantNameInside.setDefaultUnit(kony.flex.DP);
var lblMerchantName = new kony.ui.Label({
"id": "lblMerchantName",
"isVisible": true,
"right": "2%",
"skin": "sknTransferType",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxMerchantNameInside.add(lblMerchantName);
flxMerchantName.add(lblMerchantNameStatic, flxMerchantNameInside);
var flxAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxAccount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccount.setDefaultUnit(kony.flex.DP);
var lblAccountStaticText = new kony.ui.Label({
"height": "50%",
"id": "lblAccountStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.settings.account"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxAccountInside = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxAccountInside",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "95%",
"zIndex": 1
}, {}, {});
flxAccountInside.setDefaultUnit(kony.flex.DP);
var lblAccountType = new kony.ui.Label({
"id": "lblAccountType",
"isVisible": true,
"right": "0%",
"skin": "sknTransferType",
"top": "0%",
"width": "66%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrow = new kony.ui.Label({
"id": "lblArrow",
"isVisible": false,
"right": "85%",
"skin": "sknLblEditDimmed",
"text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
"top": "0",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAccBal = new kony.ui.Label({
"id": "lblAccBal",
"isVisible": true,
"left": "4%",
"skin": "sknLblSmallWhite",
"top": "3%",
"width": "35%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAccountInside.add(lblAccountType, lblArrow, lblAccBal);
flxAccount.add(lblAccountStaticText, flxAccountInside);
var flxBillNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxBillNumber",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBillNumber.setDefaultUnit(kony.flex.DP);
var flxBillNumberStaticText = new kony.ui.Label({
"height": "50%",
"id": "flxBillNumberStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.jomopay.billNumber"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBillNumberInside = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxBillNumberInside",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "3%",
"skin": "slFbox",
"top": "2%",
"width": "96%",
"zIndex": 1
}, {}, {});
flxBillNumberInside.setDefaultUnit(kony.flex.DP);
var lblBillNumber = new kony.ui.Label({
"id": "lblBillNumber",
"isVisible": true,
"right": "2%",
"skin": "sknTransferType",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBillNumberInside.add(lblBillNumber);
flxBillNumber.add(flxBillNumberStaticText, flxBillNumberInside);
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxAmount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0.00%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var lblAmountStaticText = new kony.ui.Label({
"height": "50%",
"id": "lblAmountStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxAmountInside = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxAmountInside",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "95%",
"zIndex": 1
}, {}, {});
flxAmountInside.setDefaultUnit(kony.flex.DP);
var lblAmount = new kony.ui.Label({
"id": "lblAmount",
"isVisible": true,
"right": "0%",
"skin": "sknTransferType",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblStaticJOD = new kony.ui.Label({
"id": "lblStaticJOD",
"isVisible": true,
"right": "18%",
"skin": "sknTransferType",
"text": "JOD",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAmountInside.add(lblAmount, lblStaticJOD);
flxAmount.add(lblAmountStaticText, flxAmountInside);
var flxDateandTime = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxDateandTime",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDateandTime.setDefaultUnit(kony.flex.DP);
var lblDateandTimeStaticText = new kony.ui.Label({
"height": "50%",
"id": "lblDateandTimeStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.jomopay.DateandTime"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxDateandTimeInside = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxDateandTimeInside",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "95%",
"zIndex": 1
}, {}, {});
flxDateandTimeInside.setDefaultUnit(kony.flex.DP);
var lblDateandTime = new kony.ui.Label({
"id": "lblDateandTime",
"isVisible": true,
"right": "0%",
"skin": "sknTransferType",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxDateandTimeInside.add(lblDateandTime);
flxDateandTime.add(lblDateandTimeStaticText, flxDateandTimeInside);
var flxTransactionRef = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxTransactionRef",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTransactionRef.setDefaultUnit(kony.flex.DP);
var lblTransactionRefStaticText = new kony.ui.Label({
"height": "50%",
"id": "lblTransactionRefStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxTransactionRefInside = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxTransactionRefInside",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "95%",
"zIndex": 1
}, {}, {});
flxTransactionRefInside.setDefaultUnit(kony.flex.DP);
var lblTransactionRef = new kony.ui.Label({
"id": "lblTransactionRef",
"isVisible": true,
"right": "0%",
"skin": "sknTransferType",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTransactionRefInside.add(lblTransactionRef);
flxTransactionRef.add(lblTransactionRefStaticText, flxTransactionRefInside);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonWhite",
"height": "8%",
"id": "btnConfirm",
"isVisible": true,
"right": "10%",
"onClick": AS_Button_jf16111d7bda4de98fca7581fc53ffc4,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.transfer.pay"),
"top": "6%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var lblCypherText = new kony.ui.Label({
"centerX": "50%",
"id": "lblCypherText",
"isVisible": false,
"skin": "sknTransferType",
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxJoMoPayDetails.add(btnProfilePhoto, lblPhoneNo, flxMerchantName, flxAccount, flxBillNumber, flxAmount, flxDateandTime, flxTransactionRef, btnConfirm, lblCypherText);
frmJoMoPayQRConfirm.add(flxJoMoPayHeader, flxJoMoPayDetails);
};
function frmJoMoPayQRConfirmGlobalsAr() {
frmJoMoPayQRConfirmAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmJoMoPayQRConfirmAr,
"enabledForIdleTimeout": true,
"id": "frmJoMoPayQRConfirm",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
