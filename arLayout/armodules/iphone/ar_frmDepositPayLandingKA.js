//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:57 EEST 2020
function addWidgetsfrmDepositPayLandingKAAr() {
    frmDepositPayLandingKA.setDefaultUnit(kony.flex.DP);
    var topWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "topWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    topWrapper.setDefaultUnit(kony.flex.DP);
    var iosTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "iosTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    iosTitleBar.setDefaultUnit(kony.flex.DP);
    var CopyandroidTitleLabel02a1c8891167046 = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "30%",
        "id": "CopyandroidTitleLabel02a1c8891167046",
        "isVisible": true,
        "right": "55dp",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.deposit.checkDeposits"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyhamburgerButton04a4b0742ce0647 = new kony.ui.Button({
        "focusSkin": "sknhamburgerButtonFocus",
        "height": "50dp",
        "id": "CopyhamburgerButton04a4b0742ce0647",
        "isVisible": false,
        "right": "0dp",
        "onClick": AS_Button_5d4936cc6f914befbd37d9fbc81756e8,
        "skin": "sknleftBackButtonNormal",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    iosTitleBar.add(CopyandroidTitleLabel02a1c8891167046, CopyhamburgerButton04a4b0742ce0647);
    var btnNewDepositKA = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknCopysecondaryActionReversed0f548061ffe4044",
        "height": "32dp",
        "id": "btnNewDepositKA",
        "isVisible": true,
        "right": 92,
        "onClick": AS_Button_3986869d0ba24f5091be37dc4b612b64,
        "skin": "sknbtnLatoBlack85KA",
        "text": kony.i18n.getLocalizedString("i18n.deposit.newCheckDeposit"),
        "top": "55dp",
        "width": "177dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var imgCheckDepositNavOptKA = new kony.ui.Image2({
        "id": "imgCheckDepositNavOptKA",
        "isVisible": true,
        "right": "90%",
        "onTouchEnd": AS_Image_d730564096bc4e3c93b5b9101d35476a,
        "skin": "slImage",
        "src": "more.png",
        "top": "12dp",
        "width": "44px",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    topWrapper.add(iosTitleBar, btnNewDepositKA, imgCheckDepositNavOptKA);
    var bottomContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80%",
        "id": "bottomContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkg",
        "top": "20%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    bottomContainer.setDefaultUnit(kony.flex.DP);
    var transactionListsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "88%",
        "id": "transactionListsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "12%",
        "width": "200%",
        "zIndex": 1
    }, {}, {});
    transactionListsContainer.setDefaultUnit(kony.flex.DP);
    var recentTransactions = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "ImgRecurrence": "recuurencebox.png",
            "ImgTransactionFail": "failedimage.png",
            "chevron": "",
            "lblAccountTypeKA": "",
            "lblNotesKA": "Label",
            "lblSepKA": "",
            "transactionAmount": "",
            "transactionDate": "",
            "transactionName": ""
        }],
        "groupCells": false,
        "height": "100%",
        "id": "recentTransactions",
        "isVisible": false,
        "right": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_5bf19c884ae249f5ba76b158d294c006,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": CopyFlexContainer047db9e4d3aaf43,
        "scrollingEvents": {},
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "f7f7f700",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "AccountTypeKA": "AccountTypeKA",
            "CopyFlexContainer047db9e4d3aaf43": "CopyFlexContainer047db9e4d3aaf43",
            "FlexContainer093a8eea8c55f4a": "FlexContainer093a8eea8c55f4a",
            "FlexContainer0c2ca3ef38dbd43": "FlexContainer0c2ca3ef38dbd43",
            "ImgRecurrence": "ImgRecurrence",
            "ImgTransactionFail": "ImgTransactionFail",
            "chevron": "chevron",
            "lblAccountType": "lblAccountType",
            "lblAccountTypeKA": "lblAccountTypeKA",
            "lblNotesKA": "lblNotesKA",
            "lblSepKA": "lblSepKA",
            "transactionAmount": "transactionAmount",
            "transactionDate": "transactionDate",
            "transactionName": "transactionName"
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    var lblAlerts = new kony.ui.Label({
        "centerX": "25%",
        "height": "60dp",
        "id": "lblAlerts",
        "isVisible": true,
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.alerts.NoRecentDeposits"),
        "top": "18%",
        "width": "40%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var scheduledTransactions = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "ImgRecurrence": "recuurencebox.png",
            "ImgTransactionFail": "failedimage.png",
            "chevron": "",
            "lblAccountTypeKA": "",
            "lblNotesKA": "Label",
            "lblSepKA": "",
            "transactionAmount": "",
            "transactionDate": "",
            "transactionName": ""
        }],
        "groupCells": false,
        "height": "100%",
        "id": "scheduledTransactions",
        "isVisible": true,
        "right": "50%",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_cf0c86e7eb8f443ab4e6c3470d28ca2e,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": CopyFlexContainer047db9e4d3aaf43,
        "scrollingEvents": {},
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "f7f7f700",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "AccountTypeKA": "AccountTypeKA",
            "CopyFlexContainer047db9e4d3aaf43": "CopyFlexContainer047db9e4d3aaf43",
            "FlexContainer093a8eea8c55f4a": "FlexContainer093a8eea8c55f4a",
            "FlexContainer0c2ca3ef38dbd43": "FlexContainer0c2ca3ef38dbd43",
            "ImgRecurrence": "ImgRecurrence",
            "ImgTransactionFail": "ImgTransactionFail",
            "chevron": "chevron",
            "lblAccountType": "lblAccountType",
            "lblAccountTypeKA": "lblAccountTypeKA",
            "lblNotesKA": "lblNotesKA",
            "lblSepKA": "lblSepKA",
            "transactionAmount": "transactionAmount",
            "transactionDate": "transactionDate",
            "transactionName": "transactionName"
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    var lblAlerts2 = new kony.ui.Label({
        "centerX": "75%",
        "height": "60dp",
        "id": "lblAlerts2",
        "isVisible": true,
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.alerts.NoPendingDeposits"),
        "top": "28%",
        "width": "40%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    transactionListsContainer.add(recentTransactions, lblAlerts, scheduledTransactions, lblAlerts2);
    var tabsWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "12%",
        "id": "tabsWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    tabsWrapper.setDefaultUnit(kony.flex.DP);
    var scheduledTabButton = new kony.ui.Button({
        "focusSkin": "skntabSelected",
        "height": "100%",
        "id": "scheduledTabButton",
        "isVisible": true,
        "right": "50%",
        "onClick": AS_Button_898fa5d877d64982809aa55e87fb169c,
        "skin": "skntabDeselected",
        "text": kony.i18n.getLocalizedString("i18n.deposit.pending"),
        "top": "0dp",
        "width": "50%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 1,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var recentTabButton = new kony.ui.Button({
        "focusSkin": "skntabSelected",
        "height": "100%",
        "id": "recentTabButton",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_dbbcf02af01949d980a6d577221dfa21,
        "skin": "skntabSelected",
        "text": kony.i18n.getLocalizedString("i18n.deposit.recent"),
        "top": "0dp",
        "width": "50%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 1,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var tabDeselectedIndicator = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "tabDeselectedIndicator",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknCopyslFbox00fe82e6f7cc84b",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    tabDeselectedIndicator.setDefaultUnit(kony.flex.DP);
    tabDeselectedIndicator.add();
    var tabSelectedIndicator = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "tabSelectedIndicator",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknCopyslFbox03fbc1653617542",
        "top": "0dp",
        "width": "50%"
    }, {}, {});
    tabSelectedIndicator.setDefaultUnit(kony.flex.DP);
    tabSelectedIndicator.add();
    var CopylistDivider0c9d0c3b0715a44 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopylistDivider0c9d0c3b0715a44",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopylistDivider0c9d0c3b0715a44.setDefaultUnit(kony.flex.DP);
    CopylistDivider0c9d0c3b0715a44.add();
    tabsWrapper.add(scheduledTabButton, recentTabButton, tabDeselectedIndicator, tabSelectedIndicator, CopylistDivider0c9d0c3b0715a44);
    bottomContainer.add(transactionListsContainer, tabsWrapper);
    var flxCheckDepositNavOptKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxCheckDepositNavOptKA",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "onClick": AS_FlexContainer_be08217dfeea4ccf90df6d2e53ccb590,
        "skin": "sknFlx20000000KA",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCheckDepositNavOptKA.setDefaultUnit(kony.flex.DP);
    var flxNavSegment1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "90%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxNavSegment1",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "5%",
        "skin": "sknRoundedCornerKA",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNavSegment1.setDefaultUnit(kony.flex.DP);
    var segAccountDetailNav1KA = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblMenuItem": "Prefferred Deposit Settings"
        }],
        "groupCells": false,
        "id": "segAccountDetailNav1KA",
        "isVisible": false,
        "right": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxNavigationOptKA,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxLineDividerNavOptKA": "flxLineDividerNavOptKA",
            "flxNavigationOptKA": "flxNavigationOptKA",
            "lblMenuItem": "lblMenuItem"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    var btnSignOutNavOptAccOverKA = new kony.ui.Button({
        "bottom": "0px",
        "focusSkin": "sknBtnSignOutEB5000KA",
        "height": "50dp",
        "id": "btnSignOutNavOptAccOverKA",
        "isVisible": true,
        "right": "0%",
        "onClick": AS_Button_f75838145b424722ab999f9ec26cffa3,
        "skin": "sknBtnSignOutEB5000KA",
        "text": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxNavSegment1.add(segAccountDetailNav1KA, btnSignOutNavOptAccOverKA);
    flxCheckDepositNavOptKA.add(flxNavSegment1);
    frmDepositPayLandingKA.add(topWrapper, bottomContainer, flxCheckDepositNavOptKA);
};
function frmDepositPayLandingKAGlobalsAr() {
    frmDepositPayLandingKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmDepositPayLandingKAAr,
        "bounces": false,
        "enabledForIdleTimeout": true,
        "footers": [footerBack],
        "id": "frmDepositPayLandingKA",
        "init": AS_Form_9e12fd31e41c42368c6b96405c1ca672,
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": AS_Form_766c40a00e5c4b11a360358469dc89e9,
        "skin": "sknmainGradient",
        "statusBarHidden": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "bouncesZoom": true,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "transitionFade"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "transitionFade"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false
    });
};
