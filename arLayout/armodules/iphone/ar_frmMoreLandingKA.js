//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:58 EEST 2020
function addWidgetsfrmMoreLandingKAAr() {
frmMoreLandingKA.setDefaultUnit(kony.flex.DP);
var moreScrollContainer = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": "0dp",
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "moreScrollContainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"onScrolling": AS_FlexScrollContainer_5744197d64f64bf4a8a994cfc51b1885,
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknslFSbox",
"top": "0dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
moreScrollContainer.setDefaultUnit(kony.flex.DP);
var topContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "25%",
"id": "topContainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"top": "0dp",
"width": "100%"
}, {}, {});
topContainer.setDefaultUnit(kony.flex.DP);
var greetingWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "30%",
"id": "greetingWrapper",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "45dp",
"width": "100%",
"zIndex": 1
}, {}, {});
greetingWrapper.setDefaultUnit(kony.flex.DP);
var greetingName = new kony.ui.Label({
"centerX": "50%",
"id": "greetingName",
"isVisible": true,
"skin": "sknCopyslLabel0385c0f68cb1d41",
"text": "Hi, Kyle",
"top": "0",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lastSignOnLabel = new kony.ui.Label({
"centerX": "50%",
"id": "lastSignOnLabel",
"isVisible": true,
"skin": "sknCopyslLabel0fecb00298a7744",
"text": "You last signed on at 08/31/2015 07:46 AM ET",
"top": "3dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
greetingWrapper.add(greetingName, lastSignOnLabel);
var locatorContactWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "65%",
"id": "locatorContactWrapper",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "sknslFbox",
"top": "45dp",
"width": "100%",
"zIndex": 1
}, {}, {});
locatorContactWrapper.setDefaultUnit(kony.flex.DP);
var flxMessageWrapperKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxMessageWrapperKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknCopyslFbox0027a026e702e4d",
"top": "0",
"width": "30%",
"zIndex": 1
}, {}, {});
flxMessageWrapperKA.setDefaultUnit(kony.flex.DP);
var imgMessageIconKA = new kony.ui.Image2({
"centerX": "50%",
"height": "42%",
"id": "imgMessageIconKA",
"isVisible": true,
"right": "23dp",
"skin": "sknslImage",
"src": "message.png",
"top": "10%",
"width": "42%"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblMessageKA = new kony.ui.Label({
"bottom": "14%",
"centerX": "50%",
"id": "lblMessageKA",
"isVisible": true,
"skin": "skniconButtonLabel",
"text": kony.i18n.getLocalizedString("i18n.more.messages"),
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnMessageKA = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus",
"height": "100%",
"id": "btnMessageKA",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_1532035ca83244f0a4b1e6c66329f8b4,
"skin": "skninvisibleButtonNormal",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
flxMessageWrapperKA.add(imgMessageIconKA, lblMessageKA, btnMessageKA);
var buttonDivider = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "buttonDivider",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknwhiteDivider",
"width": "1dp",
"zIndex": 1
}, {}, {});
buttonDivider.setDefaultUnit(kony.flex.DP);
buttonDivider.add();
var locatorWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "locatorWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknCopyslFbox0027a026e702e4d",
"top": "0dp",
"width": "30%",
"zIndex": 1
}, {}, {});
locatorWrapper.setDefaultUnit(kony.flex.DP);
var locatorIcon = new kony.ui.Image2({
"centerX": "50%",
"height": "42%",
"id": "locatorIcon",
"isVisible": true,
"right": "23dp",
"skin": "sknslImage",
"src": "locator_pin_icon.png",
"top": "10%",
"width": "42%"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var locatorLabel = new kony.ui.Label({
"bottom": "14%",
"centerX": "50%",
"id": "locatorLabel",
"isVisible": true,
"skin": "skniconButtonLabel",
"text": kony.i18n.getLocalizedString("i18n.common.locateUs"),
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var locatorButton = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus",
"height": "100%",
"id": "locatorButton",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_5232d8e11dfb437cbdd57db58b6922d1,
"skin": "skninvisibleButtonNormal",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
locatorWrapper.add(locatorIcon, locatorLabel, locatorButton);
var buttonDivider1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "buttonDivider1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "1%",
"skin": "sknwhiteDivider",
"width": "1dp",
"zIndex": 1
}, {}, {});
buttonDivider1.setDefaultUnit(kony.flex.DP);
buttonDivider1.add();
var contactWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "contactWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknCopyslFbox02b728f12fdc342",
"top": "0dp",
"width": "30%",
"zIndex": 1
}, {}, {});
contactWrapper.setDefaultUnit(kony.flex.DP);
var contactIcon = new kony.ui.Image2({
"centerX": "50%",
"height": "42%",
"id": "contactIcon",
"isVisible": true,
"skin": "sknslImage",
"src": "phone_white_icon.png",
"top": "10%",
"width": "42%"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var contactLabel = new kony.ui.Label({
"bottom": "14%",
"centerX": "50%",
"id": "contactLabel",
"isVisible": true,
"skin": "skniconButtonLabel",
"text": kony.i18n.getLocalizedString("i18n.common.contactUs"),
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var contactButton = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus",
"height": "100%",
"id": "contactButton",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_8a7939b2cdbd4c65a66a4f69f4379e58,
"skin": "skninvisibleButtonNormal",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
contactWrapper.add(contactIcon, contactLabel, contactButton);
locatorContactWrapper.add( contactWrapper, buttonDivider1, locatorWrapper, buttonDivider,flxMessageWrapperKA);
topContainer.add(greetingWrapper, locatorContactWrapper);
var bottomContainerr = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "65%",
"id": "bottomContainerr",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknCopyslFbox019f84c8a6c5541",
"top": "37%",
"width": "100%",
"zIndex": 1
}, {}, {});
bottomContainerr.setDefaultUnit(kony.flex.DP);
bottomContainerr.add();
var flxOuterBottonContainerKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "75%",
"id": "flxOuterBottonContainerKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxOuterBottonContainerKA.setDefaultUnit(kony.flex.DP);
var resourcesLabel = new kony.ui.Label({
"bottom": "0dp",
"height": "50dp",
"id": "resourcesLabel",
"isVisible": true,
"right": "0dp",
"skin": "sknsecondaryHeader",
"text": kony.i18n.getLocalizedString("i18n.more.resources"),
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var bottomContainer = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": true,
"allowVerticalBounce": true,
"bottom": "0dp",
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"horizontalScrollIndicator": true,
"id": "bottomContainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknCopyslFSbox013d08d6060bd4b",
"top": "0dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
bottomContainer.setDefaultUnit(kony.flex.DP);
var moreResourcesSegment = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bottom": 0,
"data": [{
"HiddenLbl": "",
"imgicontick": "",
"lblPageNameKA": ""
}],
"groupCells": false,
"id": "moreResourcesSegment",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_9032cf7905794b79a7870c2ca9f51c2d,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowTemplate": container,
"scrollingEvents": {},
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "f7f7f700",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"container": "container",
"imgicontick": "imgicontick",
"lblPageNameKA": "lblPageNameKA"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": false
});
var segmentBorderBottom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "segmentBorderBottom",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
segmentBorderBottom.setDefaultUnit(kony.flex.DP);
segmentBorderBottom.add();
bottomContainer.add(moreResourcesSegment, segmentBorderBottom);
flxOuterBottonContainerKA.add(resourcesLabel, bottomContainer);
moreScrollContainer.add(topContainer, bottomContainerr, flxOuterBottonContainerKA);
var iosTitleBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "iosTitleBar",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntitleBarGradient",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
iosTitleBar.setDefaultUnit(kony.flex.DP);
var settingsButtonIos = new kony.ui.Button({
"focusSkin": "sknCopyinvisibleButtonFocus0a3a732524ff54e",
"height": "50dp",
"id": "settingsButtonIos",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_7f73adf27b3d425596d1a71e16536614,
"skin": "sknCopyinvisibleButtonNormal09ba09890a67c4b",
"top": 0,
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var signOutButton = new kony.ui.Button({
"focusSkin": "skntitleBarTextButtonFocus",
"height": "100%",
"id": "signOutButton",
"isVisible": true,
"onClick": AS_Button_7704ef2413594df0a30a1120cc5b04ce,
"left": "0.00%",
"skin": "skntitleBarTextButton",
"text": kony.i18n.getLocalizedString("i18n.common.signOut"),
"top": "0dp",
"width": "90dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var moreLandingTitle = new kony.ui.Label({
"centerX": "50%",
"height": "22dp",
"id": "moreLandingTitle",
"isVisible": true,
"skin": "sknnavBarTitle",
"text": "Hi, Kyle",
"top": "55dp",
"width": "60%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
iosTitleBar.add(settingsButtonIos, signOutButton, moreLandingTitle);
var accountLabelScroll = new kony.ui.Label({
"height": "50dp",
"id": "accountLabelScroll",
"isVisible": false,
"right": "0dp",
"skin": "sknCopysecondaryHeader07cfb9398604f4f",
"text": "Resources",
"top": "50dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
frmMoreLandingKA.add(moreScrollContainer, iosTitleBar, accountLabelScroll);
};
function frmMoreLandingKAGlobalsAr() {
frmMoreLandingKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmMoreLandingKAAr,
"allowHorizontalBounce": false,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"footers": [footerBack],
"id": "frmMoreLandingKA",
"init": AS_Form_27c15e07f1dd42489132334199d90467,
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_156ebd28571f47dfa876957728039d2a,
"skin": "sknmainGradient",
"statusBarHidden": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": true,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"inTransitionConfig": {
"transitionDirection": "none",
"transitionEffect": "transitionFade"
},
"needsIndicatorDuringPostShow": false,
"outTransitionConfig": {
"transitionDirection": "none",
"transitionEffect": "transitionFade"
},
"retainScrollPosition": false,
"statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
"titleBar": false
});
};
