//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:58 EEST 2020
function addWidgetsfrmLocatorATMDetailsKAAr() {
    frmLocatorATMDetailsKA.setDefaultUnit(kony.flex.DP);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": 0,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkg",
        "top": "50dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {
        "bouncesZoom": false
    });
    mainContent.setDefaultUnit(kony.flex.DP);
    var FlexScrollContainermainKA = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": true,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "contentOffset": {
            "x": "0dp",
            "y": "0dp"
        },
        "enableScrolling": true,
        "height": "91%",
        "horizontalScrollIndicator": true,
        "id": "FlexScrollContainermainKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {
        "bouncesZoom": false
    });
    FlexScrollContainermainKA.setDefaultUnit(kony.flex.DP);
    var atmExampleDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "120dp",
        "id": "atmExampleDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    atmExampleDetails.setDefaultUnit(kony.flex.DP);
    var address1 = new kony.ui.Label({
        "centerX": "50%",
        "id": "address1",
        "isVisible": true,
        "skin": "sknNumber",
        "text": "221 W Sixth St",
        "top": "65dp",
        "width": "96%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var address2 = new kony.ui.Label({
        "centerX": "50%",
        "id": "address2",
        "isVisible": false,
        "skin": "sknNumber",
        "text": "Austin, TX 78704",
        "top": "85dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var distance = new kony.ui.Label({
        "centerX": "50%",
        "id": "distance",
        "isVisible": true,
        "skin": "skndetailPageNumber",
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var dividerDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "dividerDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    dividerDetails.setDefaultUnit(kony.flex.DP);
    dividerDetails.add();
    atmExampleDetails.add(address1, address2, distance, dividerDetails);
    var hoursContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "290dp",
        "id": "hoursContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    hoursContainer.setDefaultUnit(kony.flex.DP);
    var branchHoursLabel = new kony.ui.Label({
        "id": "branchHoursLabel",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.common.Hours"),
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var hoursWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "85%",
        "id": "hoursWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": 0,
        "skin": "sknslFbox",
        "top": "50dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    hoursWrapper.setDefaultUnit(kony.flex.DP);
 
    var segOperationHours = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "lblOperationHrs": "",
            "lblOperationHrsValue": ""
        }],
        "groupCells": false,
        "height": "100%",
        "id": "segOperationHours",
        "isVisible": true,
        "right": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": CopyFBox052ae3f35f03445,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "ffffff00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "lblOperationHrs": "lblOperationHrs",
            "lblOperationHrsValue": "lblOperationHrsValue"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    hoursWrapper.add(segOperationHours);
    var Copydivider0baad3824020b41 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0baad3824020b41",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider0baad3824020b41.setDefaultUnit(kony.flex.DP);
    Copydivider0baad3824020b41.add();
    hoursContainer.add(branchHoursLabel, hoursWrapper, Copydivider0baad3824020b41);
    var servicesContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "380dp",
        "id": "servicesContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    servicesContainer.setDefaultUnit(kony.flex.DP);
    var atmServicesLabel = new kony.ui.Label({
        "id": "atmServicesLabel",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.locateus.atmServices"),
        "top": "30dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
 
    var segServiceList = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "lblServiceList": ""
        }],
        "groupCells": false,
        "height": "85%",
        "id": "segServiceList",
        "isVisible": true,
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "left": "0dp",
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": CopyFBox0103104335ef34f,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "ffffff00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "50dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "lblServiceList": "lblServiceList"
        },
        "width": "95%",
        "zIndex": 1
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    var divider1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "divider1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    divider1.setDefaultUnit(kony.flex.DP);
    divider1.add();
    servicesContainer.add(atmServicesLabel, segServiceList, divider1);
    FlexScrollContainermainKA.add(atmExampleDetails, hoursContainer, servicesContainer);
    var getDirectionsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "getDirectionsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "-10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    getDirectionsContainer.setDefaultUnit(kony.flex.DP);
    var getDirectionsButton = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "getDirectionsButton",
        "isVisible": true,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.locateus.getDirections"),
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var divider02 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "divider02",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    divider02.setDefaultUnit(kony.flex.DP);
    divider02.add();
    getDirectionsContainer.add(getDirectionsButton, divider02);
    mainContent.add(FlexScrollContainermainKA, getDirectionsContainer);
    var iosTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "iosTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    iosTitleBar.setDefaultUnit(kony.flex.DP);
    var exampleTitleLabel = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "exampleTitleLabel",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.locateus.frmLocatorATMDetailsKA.atmExampleTitleLabel"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var backButton = new kony.ui.Button({
        "focusSkin": "sknleftBackButtonFocus",
        "height": "50dp",
        "id": "backButton",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_72c093ae3a9f4e539a10ec3b79a98605,
        "skin": "sknleftBackButtonNormal",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    iosTitleBar.add(exampleTitleLabel, backButton);
    frmLocatorATMDetailsKA.add(mainContent, iosTitleBar);
};
function frmLocatorATMDetailsKAGlobalsAr() {
    frmLocatorATMDetailsKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmLocatorATMDetailsKAAr,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmLocatorATMDetailsKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient",
        "statusBarHidden": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false
    });
};
