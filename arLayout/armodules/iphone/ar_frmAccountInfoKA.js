//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:56 EEST 2020
function addWidgetsfrmAccountInfoKAAr() {
frmAccountInfoKA.setDefaultUnit(kony.flex.DP);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": "0dp",
"bounces": false,
"clipBounds": true,
"enableScrolling": false,
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknAccountsInfo",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var CopypreferredAccountEnable0d7ab026f4a5845 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "CopypreferredAccountEnable0d7ab026f4a5845",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopypreferredAccountEnable0d7ab026f4a5845.setDefaultUnit(kony.flex.DP);
var CopyLabel0a393696e101f42 = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel0a393696e101f42",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.overview.accountNickname"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var usernameContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "usernameContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "100%",
"zIndex": 1
}, {}, {});
usernameContainer.setDefaultUnit(kony.flex.DP);
var accountNicknameTextfield = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "accountNicknameTextfield",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0dp",
"onDone": AS_TextField_d31569470d484932b93f5e87ff79247b,
"placeholder": kony.i18n.getLocalizedString("i18n.overview.accountNicknameTextfield_placeholder"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": false,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var successIcon = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "39.14%",
"clipBounds": false,
"height": "30dp",
"id": "successIcon",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "40dp",
"skin": "sknsuccessIcon",
"top": "4dp",
"width": "30dp",
"zIndex": 1
}, {}, {});
successIcon.setDefaultUnit(kony.flex.DP);
var successImage = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"height": "50%",
"id": "successImage",
"isVisible": true,
"skin": "sknslImage",
"src": "success_large_check.png",
"width": "50%"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
successIcon.add(successImage);
usernameContainer.add(accountNicknameTextfield, successIcon);
var saveNickname = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "skneditFormFocus",
"height": "60dp",
"id": "saveNickname",
"isVisible": false,
"onClick": AS_Button_ae14d5a58ea04e4ca928dfe39b7bfedc,
"left": -0,
"skin": "skneditForm",
"text": kony.i18n.getLocalizedString("i18n.common.save"),
"top": "0dp",
"width": "56dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var accountInfoEditBtn = new kony.ui.Button({
"focusSkin": "editBtnOnFocusSkn",
"height": "35dp",
"id": "accountInfoEditBtn",
"isVisible": true,
"onClick": AS_Button_gf59aa372c9e4e7cbd40844d09b6328d,
"left": "5dp",
"skin": "editBtnOnFocusSkn",
"text": "Edit",
"top": "36dp",
"width": "60dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var CopyeditNameBorder0c0fccfd1823c4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyeditNameBorder0c0fccfd1823c4b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"left": "0%",
"skin": "sknsegmentDivider",
"top": "79dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyeditNameBorder0c0fccfd1823c4b.setDefaultUnit(kony.flex.DP);
CopyeditNameBorder0c0fccfd1823c4b.add();
CopypreferredAccountEnable0d7ab026f4a5845.add(CopyLabel0a393696e101f42, usernameContainer, saveNickname, accountInfoEditBtn, CopyeditNameBorder0c0fccfd1823c4b);
var titleBarAccountInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "titleBarAccountInfo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarAccountInfo.setDefaultUnit(kony.flex.DP);
var androidTitleBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45dp",
"id": "androidTitleBar",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
androidTitleBar.setDefaultUnit(kony.flex.DP);
var lblLoanLoanIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblLoanLoanIcon",
"isVisible": false,
"left": "8%",
"skin": "accountDealLoanBackSkin",
"text": "Loans",
"top": "10dp",
"width": "70dp"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var androidBack = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknandroidBackButton",
"height": "90%",
"id": "androidBack",
"isVisible": true,
"left": "2%",
"onClick": AS_Button_b270e8d09ceb4c868c1f1873326adb1b,
"skin": "sknandroidBackButton",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"width": "90dp",
"zIndex": 5
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var lblAccountInfoMsg = new kony.ui.Label({
"id": "lblAccountInfoMsg",
"isVisible": false,
"right": "15%",
"skin": "CopyslLabel0if62558c2f444d",
"text": "Y",
"top": "7dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAccountInfoUser = new kony.ui.Label({
"id": "lblAccountInfoUser",
"isVisible": false,
"right": "2%",
"skin": "CopyslLabel0cbb44b1e4e6f4a",
"text": "F",
"top": "4dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblLoanDealIcon = new kony.ui.Label({
"height": "30dp",
"id": "lblLoanDealIcon",
"isVisible": false,
"left": "8%",
"skin": "accountDealLoanBackSkin",
"text": "Deals",
"top": "9dp",
"width": "70dp"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblLoanAccountInfo = new kony.ui.Label({
"centerY": "50%",
"height": "90%",
"id": "lblLoanAccountInfo",
"isVisible": true,
"left": "7.50%",
"skin": "accountDealLoanBackSkin",
"text": "Accounts",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var FlexContainer0b9fde1e61d3c44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlexContainer0b9fde1e61d3c44",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "slFbox",
"top": "0dp",
"width": "100dp",
"zIndex": 1
}, {}, {});
FlexContainer0b9fde1e61d3c44.setDefaultUnit(kony.flex.DP);
var btnNotification = new kony.ui.Button({
"focusSkin": "BtnNotificationMail",
"height": "45dp",
"id": "btnNotification",
"isVisible": false,
"left": "10dp",
"skin": "BtnNotificationMail",
"text": "Y",
"top": "0dp",
"width": "42dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": false,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnProfile = new kony.ui.Button({
"focusSkin": "btnUser",
"height": "50dp",
"id": "btnProfile",
"isVisible": true,
"left": "54dp",
"onClick": AS_Button_dbaa6fc3af1e43539db2d7f0868f4f43,
"skin": "btnUser",
"text": "F",
"top": "0dp",
"width": "40dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
FlexContainer0b9fde1e61d3c44.add(btnNotification, btnProfile);
androidTitleBar.add(lblLoanLoanIcon, androidBack, lblAccountInfoMsg, lblAccountInfoUser, lblLoanDealIcon, lblLoanAccountInfo, FlexContainer0b9fde1e61d3c44);
var imgAccountInfoMoreKA = new kony.ui.Image2({
"id": "imgAccountInfoMoreKA",
"isVisible": false,
"left": "90%",
"skin": "slImage",
"src": "more.png",
"top": "10dp",
"width": "44px",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
titleBarAccountInfo.add(androidTitleBar, imgAccountInfoMoreKA);
var accountDetailsContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"id": "accountDetailsContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "45dp",
"width": "100%",
"zIndex": 1
}, {}, {});
accountDetailsContainer.setDefaultUnit(kony.flex.DP);
var flxAccountNameMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "30dp",
"id": "flxAccountNameMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "sknslFbox",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxAccountNameMain.setDefaultUnit(kony.flex.DP);
var accountNumberLabel = new kony.ui.Label({
"centerX": "50%",
"id": "accountNumberLabel",
"isVisible": true,
"skin": "sknoverviewTypeLabel",
"text": "Car loan *** 454",
"top": "10%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var Label013c1b33ed19c4a = new kony.ui.Label({
"id": "Label013c1b33ed19c4a",
"isVisible": false,
"right": "0dp",
"skin": "sknstandardTextBold",
"text": kony.i18n.getLocalizedString("i18n.common.NumberC"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblMobile = new kony.ui.Label({
"centerY": "50%",
"id": "lblMobile",
"isVisible": false,
"right": "15dp",
"skin": "lblPhone",
"text": "S",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAccountNameMain.add( lblMobile, Label013c1b33ed19c4a,accountNumberLabel);
var flxAmountmain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxAmountmain",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "sknslFbox",
"top": "16dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmountmain.setDefaultUnit(kony.flex.DP);
var CopyLabel01225a607629c49 = new kony.ui.Label({
"id": "CopyLabel01225a607629c49",
"isVisible": false,
"right": "0dp",
"skin": "sknstandardTextBold",
"text": kony.i18n.getLocalizedString("i18n.cards.CurrentBalancec"),
"top": "0dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyaccountNumberLabel0e1c2ed08e25c42 = new kony.ui.Label({
"centerX": "50%",
"height": "100%",
"id": "CopyaccountNumberLabel0e1c2ed08e25c42",
"isVisible": true,
"skin": "sknAccBalance",
"text": "2,734.00",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCurrency = new kony.ui.Label({
"height": "100%",
"id": "lblCurrency",
"isVisible": false,
"right": "5dp",
"skin": "lblAmountCurrency",
"text": "JOD",
"top": "3dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAmountmain.add( lblCurrency, CopyaccountNumberLabel0e1c2ed08e25c42,CopyLabel01225a607629c49);
var flxNextPayment = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxNextPayment",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "9%",
"width": "45%",
"zIndex": 1
}, {}, {});
flxNextPayment.setDefaultUnit(kony.flex.DP);
var lblNextPayment = new kony.ui.Label({
"id": "lblNextPayment",
"isVisible": true,
"right": "0dp",
"skin": "sknstandardTextBold",
"text": kony.i18n.getLocalizedString("i18n.loans.nextPaymentOn"),
"top": "5dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblNextPayment1 = new kony.ui.Label({
"id": "lblNextPayment1",
"isVisible": true,
"right": "0dp",
"skin": "sknlblwhite40",
"text": "20 Feb 2018",
"top": "24dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNextPayment.add(lblNextPayment, lblNextPayment1);
var flxLoanPaymentAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxLoanPaymentAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "45%",
"skin": "sknslFbox",
"top": "9%",
"width": "55%",
"zIndex": 1
}, {}, {});
flxLoanPaymentAmount.setDefaultUnit(kony.flex.DP);
var lblPaymentAmount = new kony.ui.Label({
"id": "lblPaymentAmount",
"isVisible": true,
"right": "0dp",
"skin": "sknstandardTextBold",
"text": kony.i18n.getLocalizedString("i18n.loans.paymentAmount"),
"top": "5dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyaccountNumberLabel014bda555f8c040 = new kony.ui.Label({
"id": "CopyaccountNumberLabel014bda555f8c040",
"isVisible": true,
"right": "0dp",
"left": "0dp",
"skin": "sknlblwhite40",
"text": "12,724.200 JOD ",
"top": "24dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxLoanPaymentAmount.add(lblPaymentAmount, CopyaccountNumberLabel014bda555f8c040);
var flxLowerInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"id": "flxLowerInfo",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "195dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLowerInfo.setDefaultUnit(kony.flex.DP);
var flxInfo1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60dp",
"id": "flxInfo1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxInfo1.setDefaultUnit(kony.flex.DP);
var lblIcon1 = new kony.ui.Label({
"id": "lblIcon1",
"isVisible": true,
"right": "2%",
"skin": "slLabel",
"text": "Label",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblName1 = new kony.ui.Label({
"height": "20dp",
"id": "lblName1",
"isVisible": true,
"right": "30%",
"skin": "CopyslLabel0e1c7e2a9dbc24e",
"text": "Loan Amount",
"top": "0dp",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAmount1 = new kony.ui.Label({
"height": "40dp",
"id": "lblAmount1",
"isVisible": true,
"right": "30%",
"skin": "slLabel",
"text": "Label",
"top": "28dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxInfo1.add(lblIcon1, lblName1, lblAmount1);
var CopyFlexContainer0c70ac18acc484d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "55dp",
"id": "CopyFlexContainer0c70ac18acc484d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxsegBg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0c70ac18acc484d.setDefaultUnit(kony.flex.DP);
var lbInfolAvailable = new kony.ui.Label({
"height": "20dp",
"id": "lbInfolAvailable",
"isVisible": true,
"right": "15%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.cards.AvailablePointsc"),
"top": "10dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var routingNumberLabel = new kony.ui.Label({
"height": "30dp",
"id": "routingNumberLabel",
"isVisible": true,
"right": "15%",
"left": "0dp",
"skin": "sknNumber",
"text": "830039378",
"top": "23dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxInfoLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flxInfoLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "54dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxInfoLine.setDefaultUnit(kony.flex.DP);
flxInfoLine.add();
CopyFlexContainer0c70ac18acc484d.add(lbInfolAvailable, routingNumberLabel, flxInfoLine);
var CopyFlexContainer03936eaf9d8bc46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "55dp",
"id": "CopyFlexContainer03936eaf9d8bc46",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxsegBg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer03936eaf9d8bc46.setDefaultUnit(kony.flex.DP);
var CopyLabel08fc47965766d47 = new kony.ui.Label({
"height": "20dp",
"id": "CopyLabel08fc47965766d47",
"isVisible": true,
"right": "15%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.cards.AvailableCreditc"),
"top": "10dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyaccountNumberLabel045821e897a8646 = new kony.ui.Label({
"height": "30dp",
"id": "CopyaccountNumberLabel045821e897a8646",
"isVisible": true,
"right": "15%",
"left": "0dp",
"skin": "sknNumber",
"text": "27349310947",
"top": "23dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxInfoLine2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flxInfoLine2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "54dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxInfoLine2.setDefaultUnit(kony.flex.DP);
flxInfoLine2.add();
CopyFlexContainer03936eaf9d8bc46.add(CopyLabel08fc47965766d47, CopyaccountNumberLabel045821e897a8646, flxInfoLine2);
var CopyFlexContainer04eaae0c4373d4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "55dp",
"id": "CopyFlexContainer04eaae0c4373d4b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxsegBg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer04eaae0c4373d4b.setDefaultUnit(kony.flex.DP);
var CopyLabel0b8c157d2d8024d = new kony.ui.Label({
"height": "20dp",
"id": "CopyLabel0b8c157d2d8024d",
"isVisible": true,
"right": "15%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.account.lastStatementBalanceC"),
"top": "10dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var interestRateLabel = new kony.ui.Label({
"height": "30dp",
"id": "interestRateLabel",
"isVisible": true,
"right": "15%",
"left": "0dp",
"skin": "sknNumber",
"text": "0.25%",
"top": "23dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxInfoLine3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2dp",
"id": "flxInfoLine3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "54dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxInfoLine3.setDefaultUnit(kony.flex.DP);
flxInfoLine3.add();
CopyFlexContainer04eaae0c4373d4b.add(CopyLabel0b8c157d2d8024d, interestRateLabel, flxInfoLine3);
var CopyFlexContainer06c7adcdd8ed04c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "55dp",
"id": "CopyFlexContainer06c7adcdd8ed04c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer06c7adcdd8ed04c.setDefaultUnit(kony.flex.DP);
var CopyLabel035b37ff751a748 = new kony.ui.Label({
"height": "20dp",
"id": "CopyLabel035b37ff751a748",
"isVisible": true,
"right": "15%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.cards.MinimumPaymentDuec"),
"top": "10dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var interestEarnedLabel = new kony.ui.Label({
"height": "30dp",
"id": "interestEarnedLabel",
"isVisible": true,
"right": "15%",
"left": "0dp",
"skin": "sknNumber",
"text": "$5.24",
"top": "23dp",
"width": "20%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxInfoLine4 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flxInfoLine4",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "54dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxInfoLine4.setDefaultUnit(kony.flex.DP);
flxInfoLine4.add();
CopyFlexContainer06c7adcdd8ed04c.add(CopyLabel035b37ff751a748, interestEarnedLabel, flxInfoLine4);
var flxLoans5 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "55dp",
"id": "flxLoans5",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxsegBg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLoans5.setDefaultUnit(kony.flex.DP);
var lblLoanTypeInfo = new kony.ui.Label({
"height": "20dp",
"id": "lblLoanTypeInfo",
"isVisible": true,
"right": "15%",
"skin": "loansDealsTextSkin",
"text": "Loan type",
"top": "10dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblLoanType = new kony.ui.Label({
"height": "30dp",
"id": "lblLoanType",
"isVisible": true,
"right": "15%",
"skin": "sknNumber",
"text": "Car Loan",
"top": "23dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxLoans5.add(lblLoanTypeInfo, lblLoanType);
flxLowerInfo.add(flxInfo1, CopyFlexContainer0c70ac18acc484d, CopyFlexContainer03936eaf9d8bc46, CopyFlexContainer04eaae0c4373d4b, CopyFlexContainer06c7adcdd8ed04c, flxLoans5);
var flxLoanInfo = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": "0%",
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"horizontalScrollIndicator": true,
"id": "flxLoanInfo",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "37%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 10
}, {}, {});
flxLoanInfo.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0d6e67691a5a640 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0d6e67691a5a640",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0d6e67691a5a640.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0d6e67691a5a640.add();
var flxLoanAccNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxLoanAccNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0.00%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLoanAccNumber.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0e4a7e0b29cc14d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0e4a7e0b29cc14d",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "54dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0e4a7e0b29cc14d.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0e4a7e0b29cc14d.add();
var CopylblListInfoInfo0g18f05f1b4ea4a = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblListInfoInfo0g18f05f1b4ea4a",
"isVisible": true,
"right": "15dp",
"skin": "lblListOn",
"text": "N",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0i26409437d9340 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0i26409437d9340",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "50dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0i26409437d9340.setDefaultUnit(kony.flex.DP);
var CopylblLoanamt0b5bab77d350d4d = new kony.ui.Label({
"height": "25dp",
"id": "CopylblLoanamt0b5bab77d350d4d",
"isVisible": true,
"right": "0dp",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.loan.loanNumber"),
"top": "8dp",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblLoanAccNumber = new kony.ui.Label({
"height": "25dp",
"id": "lblLoanAccNumber",
"isVisible": true,
"right": "0dp",
"skin": "sknNumber",
"text": "123456789021",
"top": "24dp",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0i26409437d9340.add(CopylblLoanamt0b5bab77d350d4d, lblLoanAccNumber);
flxLoanAccNumber.add(CopyflxInfoLine0e4a7e0b29cc14d, CopylblListInfoInfo0g18f05f1b4ea4a, CopyflxRight0i26409437d9340);
var CopyflxInfoLine0c853c09ec1c64d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0c853c09ec1c64d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0c853c09ec1c64d.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0c853c09ec1c64d.add();
var flxLoanAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxLoanAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0.00%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLoanAmount.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0abffedfc28b748 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0abffedfc28b748",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "54dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0abffedfc28b748.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0abffedfc28b748.add();
var CopylblListInfoInfo0f3e6bc30864748 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblListInfoInfo0f3e6bc30864748",
"isVisible": true,
"right": "15dp",
"skin": "lblListOn",
"text": "!",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0b6cffc7208ab4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0b6cffc7208ab4a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "50dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0b6cffc7208ab4a.setDefaultUnit(kony.flex.DP);
var lblLoanamt = new kony.ui.Label({
"height": "25dp",
"id": "lblLoanamt",
"isVisible": true,
"right": "0dp",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.loans.loanAmount"),
"top": "8dp",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblLoanAmount = new kony.ui.Label({
"height": "25dp",
"id": "lblLoanAmount",
"isVisible": true,
"right": "0dp",
"skin": "sknNumber",
"text": "1,245.215 JOD",
"top": "24dp",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0b6cffc7208ab4a.add(lblLoanamt, lblLoanAmount);
flxLoanAmount.add(CopyflxInfoLine0abffedfc28b748, CopylblListInfoInfo0f3e6bc30864748, CopyflxRight0b6cffc7208ab4a);
var CopyflxInfoLine0be0e6baa380c4d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0be0e6baa380c4d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0be0e6baa380c4d.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0be0e6baa380c4d.add();
var flxInterestRate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxInterestRate",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxInterestRate.setDefaultUnit(kony.flex.DP);
var CopylblListInfoInfo0b9eafbde447947 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblListInfoInfo0b9eafbde447947",
"isVisible": true,
"right": "15dp",
"skin": "sknlblwhite40",
"text": "%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxRight5 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxRight5",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "50dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxRight5.setDefaultUnit(kony.flex.DP);
var lblInterestRateLoan = new kony.ui.Label({
"height": "30dp",
"id": "lblInterestRateLoan",
"isVisible": true,
"right": "0dp",
"skin": "sknNumber",
"text": "3.825%",
"top": "24dp",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel071e930018a534f = new kony.ui.Label({
"height": "25dp",
"id": "CopyLabel071e930018a534f",
"isVisible": true,
"right": "0dp",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.overview.interestRate"),
"top": "8dp",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxRight5.add(lblInterestRateLoan, CopyLabel071e930018a534f);
flxInterestRate.add(CopylblListInfoInfo0b9eafbde447947, flxRight5);
var CopyflxInfoLine0c0e888d26f014c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0c0e888d26f014c",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0c0e888d26f014c.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0c0e888d26f014c.add();
var flxPaymentDue = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxPaymentDue",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxPaymentDue.setDefaultUnit(kony.flex.DP);
var CopylblListInfoInfo0e950196987044f = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblListInfoInfo0e950196987044f",
"isVisible": true,
"right": "5%",
"skin": "lblListOn",
"text": "I",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0ab9abae4877a40 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0ab9abae4877a40",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxRight0ab9abae4877a40.setDefaultUnit(kony.flex.DP);
var lblPaymentDue = new kony.ui.Label({
"height": "30dp",
"id": "lblPaymentDue",
"isVisible": true,
"right": "5%",
"left": "0dp",
"skin": "sknNumber",
"text": "2,254.200 JOD",
"top": "24dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblPaymentDue1 = new kony.ui.Label({
"height": "25dp",
"id": "lblPaymentDue1",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": "Payment due",
"top": "8dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0ab9abae4877a40.add(lblPaymentDue, lblPaymentDue1);
flxPaymentDue.add( CopyflxRight0ab9abae4877a40,CopylblListInfoInfo0e950196987044f);
var CopyflxInfoLine0e10508070cbc4e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2dp",
"id": "CopyflxInfoLine0e10508070cbc4e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0e10508070cbc4e.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0e10508070cbc4e.add();
var flxNextPaymentDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxNextPaymentDate",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxNextPaymentDate.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0ac3193bd5f514b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0ac3193bd5f514b",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "60dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0ac3193bd5f514b.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0ac3193bd5f514b.add();
var CopylblopenDate0c6bd7c387cb144 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblopenDate0c6bd7c387cb144",
"isVisible": true,
"right": "15dp",
"skin": "lblListOn",
"text": "T",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0e6cbaf47c87c4c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0e6cbaf47c87c4c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "50dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0e6cbaf47c87c4c.setDefaultUnit(kony.flex.DP);
var CopylblStartingDate0ec9e899d10ca42 = new kony.ui.Label({
"height": "25dp",
"id": "CopylblStartingDate0ec9e899d10ca42",
"isVisible": true,
"right": "0%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.loanInfo.nextPaymentDate"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblNextPaymentDate = new kony.ui.Label({
"height": "30dp",
"id": "lblNextPaymentDate",
"isVisible": true,
"right": "0dp",
"skin": "sknNumber",
"text": "11 May 2018",
"top": "24dp",
"width": "60%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxLoanPostPoneNextPayment = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "51%",
"clipBounds": true,
"height": "100%",
"id": "flxLoanPostPoneNextPayment",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_b1329283a171497e9c7614143aa791eb,
"left": "4%",
"skin": "slFbox",
"top": "20dp",
"width": "40%",
"zIndex": 3
}, {}, {});
flxLoanPostPoneNextPayment.setDefaultUnit(kony.flex.DP);
var CopylblopenDate0e9624340ea2743 = new kony.ui.Label({
"centerX": "46%",
"centerY": "45%",
"id": "CopylblopenDate0e9624340ea2743",
"isVisible": true,
"left": "5%",
"skin": "lblListOn",
"text": "8",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 3
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblCcPaynowIcon0j794bd32570846 = new kony.ui.Label({
"centerX": "46%",
"centerY": "43%",
"id": "CopylblCcPaynowIcon0j794bd32570846",
"isVisible": true,
"left": "7%",
"skin": "lblListOn100",
"text": "T",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblLoanpostPoneNextPayment = new kony.ui.Label({
"centerX": "50%",
"id": "lblLoanpostPoneNextPayment",
"isVisible": true,
"right": "7dp",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.loanpostpone.lblloanpostpone"),
"top": "60%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 3
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxLoanPostPoneNextPayment.add(CopylblopenDate0e9624340ea2743, CopylblCcPaynowIcon0j794bd32570846, lblLoanpostPoneNextPayment);
CopyflxRight0e6cbaf47c87c4c.add(CopylblStartingDate0ec9e899d10ca42, lblNextPaymentDate, flxLoanPostPoneNextPayment);
flxNextPaymentDate.add(CopyflxInfoLine0ac3193bd5f514b, CopylblopenDate0c6bd7c387cb144, CopyflxRight0e6cbaf47c87c4c);
var CopyflxInfoLine0ce193160eeb346 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0ce193160eeb346",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0ce193160eeb346.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0ce193160eeb346.add();
var flxFlatRate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxFlatRate",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFlatRate.setDefaultUnit(kony.flex.DP);
var CopylblListInfoInfo0b1333fb8fdd24c = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblListInfoInfo0b1333fb8fdd24c",
"isVisible": true,
"right": "15dp",
"skin": "sknlblwhite40",
"text": "%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0bd051a43cb3043 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0bd051a43cb3043",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "50dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0bd051a43cb3043.setDefaultUnit(kony.flex.DP);
var lblFlatRate = new kony.ui.Label({
"height": "30dp",
"id": "lblFlatRate",
"isVisible": true,
"right": "0dp",
"skin": "sknNumber",
"text": " 5,525.200 JOD",
"top": "24dp",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0hccbea4ff47f4d = new kony.ui.Label({
"height": "25dp",
"id": "CopyLabel0hccbea4ff47f4d",
"isVisible": true,
"right": "0dp",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18.loans.flatRate"),
"top": "8dp",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0bd051a43cb3043.add(lblFlatRate, CopyLabel0hccbea4ff47f4d);
flxFlatRate.add(CopylblListInfoInfo0b1333fb8fdd24c, CopyflxRight0bd051a43cb3043);
var CopyflxInfoLine0gab94217d3824c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0gab94217d3824c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0gab94217d3824c.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0gab94217d3824c.add();
var flxReducedInterestRate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxReducedInterestRate",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxReducedInterestRate.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0af6341a04dc247 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0af6341a04dc247",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "60dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0af6341a04dc247.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0af6341a04dc247.add();
var CopylblopenDate0e48492e5fe7c47 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblopenDate0e48492e5fe7c47",
"isVisible": true,
"right": "15dp",
"skin": "lblListOn",
"text": "8",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0d706163a6d1944 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0d706163a6d1944",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "50dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0d706163a6d1944.setDefaultUnit(kony.flex.DP);
var CopylblStartingDate0fed6fff41ad440 = new kony.ui.Label({
"height": "25dp",
"id": "CopylblStartingDate0fed6fff41ad440",
"isVisible": true,
"right": "0dp",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18.loans.Reducedinterestrate"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblReducedInterestRate = new kony.ui.Label({
"height": "30dp",
"id": "lblReducedInterestRate",
"isVisible": true,
"right": "0dp",
"skin": "sknNumber",
"text": "1,222.200 JOD",
"top": "24dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0d706163a6d1944.add(CopylblStartingDate0fed6fff41ad440, lblReducedInterestRate);
flxReducedInterestRate.add(CopyflxInfoLine0af6341a04dc247, CopylblopenDate0e48492e5fe7c47, CopyflxRight0d706163a6d1944);
var CopyflxInfoLine0cdfaf4d915374f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0cdfaf4d915374f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0cdfaf4d915374f.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0cdfaf4d915374f.add();
var flxRemaingBalanceLoan = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxRemaingBalanceLoan",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxRemaingBalanceLoan.setDefaultUnit(kony.flex.DP);
var CopylblListInfoInfo0dbab92c3046d4e = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblListInfoInfo0dbab92c3046d4e",
"isVisible": true,
"right": "15dp",
"skin": "lblListOnCairoLight250",
"text": "$",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0d5e4618c1f5140 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0d5e4618c1f5140",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "55dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0d5e4618c1f5140.setDefaultUnit(kony.flex.DP);
var CopyinterestRateLabel0bfae983d84904f = new kony.ui.Label({
"height": "30dp",
"id": "CopyinterestRateLabel0bfae983d84904f",
"isVisible": true,
"right": "0dp",
"skin": "sknNumber",
"text": " 9,525.222 JOD",
"top": "24dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblRemainingBalanceLoan = new kony.ui.Label({
"height": "25dp",
"id": "lblRemainingBalanceLoan",
"isVisible": true,
"right": "0dp",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.loans.remainingBalance"),
"top": "8dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0d5e4618c1f5140.add(CopyinterestRateLabel0bfae983d84904f, lblRemainingBalanceLoan);
flxRemaingBalanceLoan.add(CopylblListInfoInfo0dbab92c3046d4e, CopyflxRight0d5e4618c1f5140);
var CopyflxInfoLine0h1ac2e91a3aa48 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0h1ac2e91a3aa48",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0h1ac2e91a3aa48.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0h1ac2e91a3aa48.add();
var flxStartingDateLoan = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxStartingDateLoan",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxStartingDateLoan.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0afc94ceb50c743 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0afc94ceb50c743",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "60dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0afc94ceb50c743.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0afc94ceb50c743.add();
var CopylblopenDate0a2d58ebd269f40 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblopenDate0a2d58ebd269f40",
"isVisible": true,
"right": "15dp",
"skin": "lblListOn",
"text": "T",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0fcdbb8be9db040 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0fcdbb8be9db040",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "50dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0fcdbb8be9db040.setDefaultUnit(kony.flex.DP);
var CopylblStartingDate0h26bb7c4100a4c = new kony.ui.Label({
"height": "25dp",
"id": "CopylblStartingDate0h26bb7c4100a4c",
"isVisible": true,
"right": "0dp",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.loans.startingDate"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblStartingDateLoan = new kony.ui.Label({
"height": "30dp",
"id": "lblStartingDateLoan",
"isVisible": true,
"right": "0dp",
"skin": "sknNumber",
"text": "11 May 2017",
"top": "24dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0fcdbb8be9db040.add(CopylblStartingDate0h26bb7c4100a4c, lblStartingDateLoan);
flxStartingDateLoan.add(CopyflxInfoLine0afc94ceb50c743, CopylblopenDate0a2d58ebd269f40, CopyflxRight0fcdbb8be9db040);
flxLoanInfo.add(CopyflxInfoLine0d6e67691a5a640, flxLoanAccNumber, CopyflxInfoLine0c853c09ec1c64d, flxLoanAmount, CopyflxInfoLine0be0e6baa380c4d, flxInterestRate, CopyflxInfoLine0c0e888d26f014c, flxPaymentDue, CopyflxInfoLine0e10508070cbc4e, flxNextPaymentDate, CopyflxInfoLine0ce193160eeb346, flxFlatRate, CopyflxInfoLine0gab94217d3824c, flxReducedInterestRate, CopyflxInfoLine0cdfaf4d915374f, flxRemaingBalanceLoan, CopyflxInfoLine0h1ac2e91a3aa48, flxStartingDateLoan);
var flxDeposits = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": "0%",
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"horizontalScrollIndicator": true,
"id": "flxDeposits",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "32%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 10
}, {}, {});
flxDeposits.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0e7d73762b7c04d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0e7d73762b7c04d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0e7d73762b7c04d.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0e7d73762b7c04d.add();
var flxDepositNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxDepositNumber",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDepositNumber.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0c7b24ac933eb46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0c7b24ac933eb46",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "54dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0c7b24ac933eb46.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0c7b24ac933eb46.add();
var CopylblListInfoInfo0bcccd59a6f434e = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblListInfoInfo0bcccd59a6f434e",
"isVisible": true,
"right": "5%",
"skin": "lblListOn",
"text": "N",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0ff6720b2fbd848 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0ff6720b2fbd848",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0ff6720b2fbd848.setDefaultUnit(kony.flex.DP);
var CopylblType0jbef84e5496547 = new kony.ui.Label({
"height": "25dp",
"id": "CopylblType0jbef84e5496547",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.Deposit.depositNumber"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblTDDepositNumber = new kony.ui.Label({
"height": "25dp",
"id": "lblTDDepositNumber",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "214578954154014",
"top": "24dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0ff6720b2fbd848.add(CopylblType0jbef84e5496547, lblTDDepositNumber);
flxDepositNumber.add( CopyflxRight0ff6720b2fbd848, CopylblListInfoInfo0bcccd59a6f434e,CopyflxInfoLine0c7b24ac933eb46);
var flxLine11 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flxLine11",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLine11.setDefaultUnit(kony.flex.DP);
flxLine11.add();
var flxDepositAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxDepositAmount",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDepositAmount.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0eec646268bf545 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0eec646268bf545",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "54dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0eec646268bf545.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0eec646268bf545.add();
var CopylblListInfoInfo0ee3bbbb4f9514f = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblListInfoInfo0ee3bbbb4f9514f",
"isVisible": true,
"right": "5%",
"skin": "lblListOn",
"text": "9",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0b4f352b79b504f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0b4f352b79b504f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0b4f352b79b504f.setDefaultUnit(kony.flex.DP);
var lblDepositAmount = new kony.ui.Label({
"height": "25dp",
"id": "lblDepositAmount",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18.deposit.DepositAmount"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblLoanType0dc1c0e442dc34b = new kony.ui.Label({
"height": "25dp",
"id": "CopylblLoanType0dc1c0e442dc34b",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "2,734.00 JOD",
"top": "24dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0b4f352b79b504f.add(lblDepositAmount, CopylblLoanType0dc1c0e442dc34b);
flxDepositAmount.add( CopyflxRight0b4f352b79b504f, CopylblListInfoInfo0ee3bbbb4f9514f,CopyflxInfoLine0eec646268bf545);
var CopyflxInfoLine0ea1c3ca227f645 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0ea1c3ca227f645",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0ea1c3ca227f645.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0ea1c3ca227f645.add();
var CopyflxInfoLine0ea4b3ab31a1744 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0ea4b3ab31a1744",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0ea4b3ab31a1744.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0ea4b3ab31a1744.add();
var flxMaturityDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxMaturityDate",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMaturityDate.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0bc582acddac14c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0bc582acddac14c",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "60dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0bc582acddac14c.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0bc582acddac14c.add();
var CopylblopenDate0e5959115dca849 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblopenDate0e5959115dca849",
"isVisible": true,
"right": "5%",
"skin": "lblListOn",
"text": "T",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxRight7 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxRight7",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxRight7.setDefaultUnit(kony.flex.DP);
var CopyLabel0b620c6a0e0784f = new kony.ui.Label({
"height": "25dp",
"id": "CopyLabel0b620c6a0e0784f",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.overview.maturityDate"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyinterestEarnedLabel0daf983bec94d4c = new kony.ui.Label({
"height": "30dp",
"id": "CopyinterestEarnedLabel0daf983bec94d4c",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "20 Jun 2017",
"top": "24dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxRight7.add(CopyLabel0b620c6a0e0784f, CopyinterestEarnedLabel0daf983bec94d4c);
flxMaturityDate.add( flxRight7, CopylblopenDate0e5959115dca849,CopyflxInfoLine0bc582acddac14c);
var CopyflxInfoLine0dad23a0ef8484b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0dad23a0ef8484b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0dad23a0ef8484b.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0dad23a0ef8484b.add();
var flxInterestAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxInterestAccount",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "flxsegBg",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxInterestAccount.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0g00333057ac844 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0g00333057ac844",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "60dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0g00333057ac844.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0g00333057ac844.add();
var CopylblopenDate0ecde8496625f40 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblopenDate0ecde8496625f40",
"isVisible": true,
"right": "5%",
"skin": "lblListOn",
"text": "T",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0fcff36c6b26249 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0fcff36c6b26249",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0fcff36c6b26249.setDefaultUnit(kony.flex.DP);
var lblInterestAcc = new kony.ui.Label({
"height": "25dp",
"id": "lblInterestAcc",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": "Interest account",
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyinterestEarnedLabel0d63a4ac42f5d48 = new kony.ui.Label({
"height": "30dp",
"id": "CopyinterestEarnedLabel0d63a4ac42f5d48",
"isVisible": true,
"right": "5%",
"left": "0dp",
"skin": "sknNumber",
"text": "6,452.200 JOD",
"top": "24dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0fcff36c6b26249.add(lblInterestAcc, CopyinterestEarnedLabel0d63a4ac42f5d48);
flxInterestAccount.add( CopyflxRight0fcff36c6b26249, CopylblopenDate0ecde8496625f40,CopyflxInfoLine0g00333057ac844);
var CopyflxInfoLine0e81b079b008d44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0e81b079b008d44",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0e81b079b008d44.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0e81b079b008d44.add();
var flxInterestDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxInterestDate",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxInterestDate.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0jd76d36e5f624c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0jd76d36e5f624c",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "60dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0jd76d36e5f624c.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0jd76d36e5f624c.add();
var CopylblopenDate0cb861180b9d040 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblopenDate0cb861180b9d040",
"isVisible": true,
"right": "5%",
"skin": "lblListOn",
"text": "T",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0h09d3d719f594a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0h09d3d719f594a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0h09d3d719f594a.setDefaultUnit(kony.flex.DP);
var lblInterestDate = new kony.ui.Label({
"height": "25dp",
"id": "lblInterestDate",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18.deposit.Interestdate"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblInterestDate1 = new kony.ui.Label({
"height": "30dp",
"id": "lblInterestDate1",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "01 Jan 2018",
"top": "24dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0h09d3d719f594a.add(lblInterestDate, lblInterestDate1);
flxInterestDate.add( CopyflxRight0h09d3d719f594a, CopylblopenDate0cb861180b9d040,CopyflxInfoLine0jd76d36e5f624c);
var CopyflxInfoLine0i0e98f35621043 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0i0e98f35621043",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0i0e98f35621043.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0i0e98f35621043.add();
var flxNextInterestDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxNextInterestDate",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxNextInterestDate.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0a3caef694d2b44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0a3caef694d2b44",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "60dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0a3caef694d2b44.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0a3caef694d2b44.add();
var CopylblopenDate0b6e9ee9d58774b = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblopenDate0b6e9ee9d58774b",
"isVisible": true,
"right": "5%",
"skin": "lblListOn",
"text": "!",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0fae56ea7ad994f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0fae56ea7ad994f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0fae56ea7ad994f.setDefaultUnit(kony.flex.DP);
var lblNextInterestDate = new kony.ui.Label({
"height": "25dp",
"id": "lblNextInterestDate",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.deposit.interestamount"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblNextInterestDate1 = new kony.ui.Label({
"height": "30dp",
"id": "lblNextInterestDate1",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "11 Jul 2018",
"top": "24dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0fae56ea7ad994f.add(lblNextInterestDate, lblNextInterestDate1);
flxNextInterestDate.add( CopyflxRight0fae56ea7ad994f, CopylblopenDate0b6e9ee9d58774b,CopyflxInfoLine0a3caef694d2b44);
var CopyflxInfoLine0e55dcd1af9254e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0e55dcd1af9254e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0e55dcd1af9254e.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0e55dcd1af9254e.add();
var flxInterestRateDeposit = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxInterestRateDeposit",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxInterestRateDeposit.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0f978867fc31441 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0f978867fc31441",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "60dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0f978867fc31441.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0f978867fc31441.add();
var CopylblopenDate0i9af6500aacd45 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblopenDate0i9af6500aacd45",
"isVisible": true,
"right": "5%",
"skin": "sknlblwhite40",
"text": "%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0b613659e3ceb4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0b613659e3ceb4b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0b613659e3ceb4b.setDefaultUnit(kony.flex.DP);
var lblInterestRate = new kony.ui.Label({
"height": "25dp",
"id": "lblInterestRate",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.overview.interestRate"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblInterestRateValue = new kony.ui.Label({
"height": "30dp",
"id": "lblInterestRateValue",
"isVisible": true,
"right": "5%",
"left": "0dp",
"skin": "sknNumber",
"text": "11 Jul 2018",
"top": "24dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0b613659e3ceb4b.add(lblInterestRate, lblInterestRateValue);
flxInterestRateDeposit.add( CopyflxRight0b613659e3ceb4b, CopylblopenDate0i9af6500aacd45,CopyflxInfoLine0f978867fc31441);
var CopyflxInfoLine0b7458d8e600846 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0b7458d8e600846",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0b7458d8e600846.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0b7458d8e600846.add();
var flxDepositType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxDepositType",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDepositType.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0c306eae953a34f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0c306eae953a34f",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "54dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0c306eae953a34f.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0c306eae953a34f.add();
var CopylblListInfoInfo0b33d19e128a44a = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblListInfoInfo0b33d19e128a44a",
"isVisible": true,
"right": "5%",
"skin": "lblListOn",
"text": "9",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0b16a76581e0540 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0b16a76581e0540",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
CopyflxRight0b16a76581e0540.setDefaultUnit(kony.flex.DP);
var lblType = new kony.ui.Label({
"height": "25dp",
"id": "lblType",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18.deposit.Deposittype"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblLoanType1 = new kony.ui.Label({
"height": "25dp",
"id": "lblLoanType1",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Term Deposit",
"top": "24dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0b16a76581e0540.add(lblType, lblLoanType1);
flxDepositType.add( CopyflxRight0b16a76581e0540, CopylblListInfoInfo0b33d19e128a44a,CopyflxInfoLine0c306eae953a34f);
flxDeposits.add(CopyflxInfoLine0e7d73762b7c04d, flxDepositNumber, flxLine11, flxDepositAmount, CopyflxInfoLine0ea1c3ca227f645, CopyflxInfoLine0ea4b3ab31a1744, flxMaturityDate, CopyflxInfoLine0dad23a0ef8484b, flxInterestAccount, CopyflxInfoLine0e81b079b008d44, flxInterestDate, CopyflxInfoLine0i0e98f35621043, flxNextInterestDate, CopyflxInfoLine0e55dcd1af9254e, flxInterestRateDeposit, CopyflxInfoLine0b7458d8e600846, flxDepositType);
accountDetailsContainer.add(flxAccountNameMain, flxAmountmain, flxNextPayment, flxLoanPaymentAmount, flxLowerInfo, flxLoanInfo, flxDeposits);
var flxaccountdetailsSavings = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxaccountdetailsSavings",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxaccountdetailsSavings.setDefaultUnit(kony.flex.DP);
var CopyFlexContainer0a6739d0cd9f646 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "18dp",
"id": "CopyFlexContainer0a6739d0cd9f646",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "sknslFbox",
"top": "20dp",
"width": "80%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0a6739d0cd9f646.setDefaultUnit(kony.flex.DP);
var CopyaccountNumberLabel0b9e42672fcff47 = new kony.ui.Label({
"id": "CopyaccountNumberLabel0b9e42672fcff47",
"isVisible": true,
"left": "0dp",
"skin": "sknNumber",
"text": "90010020177801",
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0e1156277669a43 = new kony.ui.Label({
"id": "CopyLabel0e1156277669a43",
"isVisible": true,
"right": "0dp",
"skin": "sknstandardTextBold",
"text": kony.i18n.getLocalizedString("i18n.account.AvailableBalance"),
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0a6739d0cd9f646.add(CopyaccountNumberLabel0b9e42672fcff47, CopyLabel0e1156277669a43);
var CopyFlexContainer0d10cd6555dbe4f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "18dp",
"id": "CopyFlexContainer0d10cd6555dbe4f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "sknslFbox",
"top": "20dp",
"width": "80%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0d10cd6555dbe4f.setDefaultUnit(kony.flex.DP);
var CopyroutingNumberLabel07e049f0450604c = new kony.ui.Label({
"id": "CopyroutingNumberLabel07e049f0450604c",
"isVisible": true,
"left": "0dp",
"skin": "sknNumber",
"text": "3.825%",
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel07fa0c9be280242 = new kony.ui.Label({
"id": "CopyLabel07fa0c9be280242",
"isVisible": true,
"right": "0dp",
"skin": "sknstandardTextBold",
"text": kony.i18n.getLocalizedString("i18n.cards.CurrentBalancec"),
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0d10cd6555dbe4f.add(CopyroutingNumberLabel07e049f0450604c, CopyLabel07fa0c9be280242);
var CopyFlexContainer0afd6376da0b84b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "20dp",
"clipBounds": true,
"height": "18dp",
"id": "CopyFlexContainer0afd6376da0b84b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "sknslFbox",
"top": "20dp",
"width": "80%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0afd6376da0b84b.setDefaultUnit(kony.flex.DP);
var CopyinterestRateLabel0cf90d54edd014c = new kony.ui.Label({
"id": "CopyinterestRateLabel0cf90d54edd014c",
"isVisible": true,
"left": "0dp",
"skin": "sknNumber",
"text": " $25,525",
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel08f49cee622914b = new kony.ui.Label({
"id": "CopyLabel08f49cee622914b",
"isVisible": true,
"right": "0dp",
"skin": "sknstandardTextBold",
"text": kony.i18n.getLocalizedString("i18n.overview.accountNumber"),
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0afd6376da0b84b.add(CopyinterestRateLabel0cf90d54edd014c, CopyLabel08f49cee622914b);
var CopyFlexContainer03766269e7e5c4f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "18dp",
"id": "CopyFlexContainer03766269e7e5c4f",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "sknslFbox",
"top": "20dp",
"width": "80%",
"zIndex": 1
}, {}, {});
CopyFlexContainer03766269e7e5c4f.setDefaultUnit(kony.flex.DP);
var CopyinterestRateLabel0808b4dc25b1340 = new kony.ui.Label({
"id": "CopyinterestRateLabel0808b4dc25b1340",
"isVisible": true,
"left": "0dp",
"skin": "sknNumber",
"text": "24 months",
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0aafc222d94f24a = new kony.ui.Label({
"id": "CopyLabel0aafc222d94f24a",
"isVisible": true,
"right": "0dp",
"skin": "sknstandardTextBold",
"text": kony.i18n.getLocalizedString("i18n.overview.term"),
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer03766269e7e5c4f.add(CopyinterestRateLabel0808b4dc25b1340, CopyLabel0aafc222d94f24a);
var CopyFlexContainer0b39f93d6332343 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "18dp",
"id": "CopyFlexContainer0b39f93d6332343",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "sknslFbox",
"top": "10dp",
"width": "80%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0b39f93d6332343.setDefaultUnit(kony.flex.DP);
var CopyinterestEarnedLabel00e3fd0f28dad48 = new kony.ui.Label({
"id": "CopyinterestEarnedLabel00e3fd0f28dad48",
"isVisible": true,
"left": "0dp",
"skin": "sknNumber",
"text": "11/20/2015",
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel018e532fcf36a49 = new kony.ui.Label({
"id": "CopyLabel018e532fcf36a49",
"isVisible": true,
"right": "0dp",
"skin": "sknstandardTextBold",
"text": kony.i18n.getLocalizedString("i18n.overview.dateOpened"),
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0b39f93d6332343.add(CopyinterestEarnedLabel00e3fd0f28dad48, CopyLabel018e532fcf36a49);
var CopyFlexContainer0b60b2baaaa414a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "18dp",
"id": "CopyFlexContainer0b60b2baaaa414a",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "sknslFbox",
"top": "10dp",
"width": "80%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0b60b2baaaa414a.setDefaultUnit(kony.flex.DP);
var CopyinterestEarnedLabel024c1176c902f49 = new kony.ui.Label({
"id": "CopyinterestEarnedLabel024c1176c902f49",
"isVisible": true,
"left": "0dp",
"skin": "sknNumber",
"text": "11/20/2017",
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0aa24694b0b8143 = new kony.ui.Label({
"id": "CopyLabel0aa24694b0b8143",
"isVisible": true,
"right": "0dp",
"skin": "sknstandardTextBold",
"text": kony.i18n.getLocalizedString("i18n.overview.maturityDate"),
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyFlexContainer0b60b2baaaa414a.add(CopyinterestEarnedLabel024c1176c902f49, CopyLabel0aa24694b0b8143);
flxaccountdetailsSavings.add(CopyFlexContainer0a6739d0cd9f646, CopyFlexContainer0d10cd6555dbe4f, CopyFlexContainer0afd6376da0b84b, CopyFlexContainer03766269e7e5c4f, CopyFlexContainer0b39f93d6332343, CopyFlexContainer0b60b2baaaa414a);
var preferredAccountEnable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "95dp",
"id": "preferredAccountEnable",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
preferredAccountEnable.setDefaultUnit(kony.flex.DP);
var accountPreviewEnableSwitch = new kony.ui.Switch({
"height": "32dp",
"id": "accountPreviewEnableSwitch",
"isVisible": true,
"rightSideText": "ON",
"left": "5%",
"leftSideText": "OFF",
"selectedIndex": 0,
"skin": "sknCopyslSwitch060ce78731a4840",
"top": "10dp",
"width": "53dp",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyLabel0426d83d0ac414e = new kony.ui.Label({
"bottom": "15dp",
"id": "CopyLabel0426d83d0ac414e",
"isVisible": true,
"right": "5%",
"maxWidth": "80%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.overview.noteDeposits"),
"top": "40dp",
"width": "73.44%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel03ee59176c1854c = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel03ee59176c1854c",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.common.PreferredCheckingAccount"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
preferredAccountEnable.add(accountPreviewEnableSwitch, CopyLabel0426d83d0ac414e, CopyLabel03ee59176c1854c);
var addExternalAccount = new kony.ui.Button({
"centerX": "50.00%",
"focusSkin": "sknsecondaryActionFocus",
"height": "58dp",
"id": "addExternalAccount",
"isVisible": false,
"onClick": AS_Button_3c136696e8e74a3ebfae050310848b72,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.checkReOrder.CheckReOrder"),
"top": "8dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var contactSegmentList = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50.00%",
"data": [{
"imgicontick": "",
"lblNameKA": ""
}],
"groupCells": false,
"height": "100dp",
"id": "contactSegmentList",
"isVisible": false,
"needPageIndicator": true,
"onRowClick": AS_Segment_75c7a6f6d9684307974e6feee495e49a,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowTemplate": CopyFlexContainer0b2b1c26ffbf74f,
"scrollingEvents": {},
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "64646400",
"separatorRequired": false,
"separatorThickness": 1,
"showScrollbars": false,
"top": "20dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer0b2b1c26ffbf74f": "CopyFlexContainer0b2b1c26ffbf74f",
"contactListDivider": "contactListDivider",
"imgicontick": "imgicontick",
"lblNameKA": "lblNameKA"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": false
});
var callBankForEnquiry = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100dp",
"id": "callBankForEnquiry",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"onClick": AS_FlexContainer_c01e71ea931e456295ecf652702251b6,
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
callBankForEnquiry.setDefaultUnit(kony.flex.DP);
var lblCallBank = new kony.ui.Label({
"id": "lblCallBank",
"isVisible": true,
"right": "5%",
"skin": "sknstandardTextBold",
"text": "Call bank for inquiries",
"top": "24dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBankPhn = new kony.ui.Label({
"id": "lblBankPhn",
"isVisible": true,
"right": "5%",
"skin": "sknstandardTextBold",
"text": "284-123-3465",
"top": "52dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var Image0j0f41ac61f1444 = new kony.ui.Image2({
"height": "30dp",
"id": "Image0j0f41ac61f1444",
"isVisible": true,
"left": "57dp",
"skin": "slImage",
"src": "phone_icon.png",
"top": "29dp",
"width": "30dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
callBankForEnquiry.add(lblCallBank, lblBankPhn, Image0j0f41ac61f1444);
var CopycallBankForEnquiry0b9b1a0be4e0b49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100dp",
"id": "CopycallBankForEnquiry0b9b1a0be4e0b49",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_c067d32432474269a5ae2e03a7d4fff0,
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopycallBankForEnquiry0b9b1a0be4e0b49.setDefaultUnit(kony.flex.DP);
var CopyeditNameBorder0d5425112e66f48 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyeditNameBorder0d5425112e66f48",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"left": "0%",
"skin": "sknsegmentDivider",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyeditNameBorder0d5425112e66f48.setDefaultUnit(kony.flex.DP);
CopyeditNameBorder0d5425112e66f48.add();
var innerflexcallBank = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "innerflexcallBank",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "10dp",
"width": "100%",
"zIndex": 1
}, {}, {});
innerflexcallBank.setDefaultUnit(kony.flex.DP);
var CopylblCallBank0a18e60b0a64b42 = new kony.ui.Label({
"id": "CopylblCallBank0a18e60b0a64b42",
"isVisible": true,
"right": "5%",
"skin": "sknstandardTextBold",
"text": "Call bank for inquiries",
"top": "15dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblBankPhn0d1a01dbf93f742 = new kony.ui.Label({
"id": "CopylblBankPhn0d1a01dbf93f742",
"isVisible": true,
"right": "5%",
"skin": "sknstandardTextBold",
"text": "284-123-3465",
"top": "40dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyImage0d4152ce2ebfb4c = new kony.ui.Image2({
"height": "30dp",
"id": "CopyImage0d4152ce2ebfb4c",
"isVisible": true,
"left": "0dp",
"skin": "slImage",
"src": "phone_icon.png",
"top": "20dp",
"width": "20%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
innerflexcallBank.add(CopylblCallBank0a18e60b0a64b42, CopylblBankPhn0d1a01dbf93f742, CopyImage0d4152ce2ebfb4c);
CopycallBankForEnquiry0b9b1a0be4e0b49.add(CopyeditNameBorder0d5425112e66f48, innerflexcallBank);
var segmentBorderBottom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "segmentBorderBottom",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
segmentBorderBottom.setDefaultUnit(kony.flex.DP);
segmentBorderBottom.add();
var flxaccountdetailsfordeposits = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": "0dp",
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"horizontalScrollIndicator": true,
"id": "flxaccountdetailsfordeposits",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "40%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 5
}, {}, {});
flxaccountdetailsfordeposits.setDefaultUnit(kony.flex.DP);
var flxOpeningDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxOpeningDate",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxOpeningDate.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0i9acc3be91bd40 = new kony.ui.Label({
"centerY": "50%",
"id": "CopyflxInfoLine0i9acc3be91bd40",
"isVisible": false,
"right": "5%",
"skin": "lblListOn",
"text": "I",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxRight = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxRight",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxRight.setDefaultUnit(kony.flex.DP);
var CopyinterestEarnedLabel00b76261e56e741 = new kony.ui.Label({
"height": "30dp",
"id": "CopyinterestEarnedLabel00b76261e56e741",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": " 25,525 JOD",
"top": "26dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel01f52e7df8d504e = new kony.ui.Label({
"height": "25dp",
"id": "CopyLabel01f52e7df8d504e",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.accounts.OpeningDate"),
"top": "8dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxRight.add(CopyinterestEarnedLabel00b76261e56e741, CopyLabel01f52e7df8d504e);
flxOpeningDate.add(CopyflxInfoLine0i9acc3be91bd40, flxRight);
var flxOpenedIn = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxOpenedIn",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxOpenedIn.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0b8847aa4c3b749 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0b8847aa4c3b749",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "54dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0b8847aa4c3b749.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0b8847aa4c3b749.add();
var lblopen = new kony.ui.Label({
"centerY": "50%",
"id": "lblopen",
"isVisible": false,
"right": "5%",
"skin": "lblListOn",
"text": "T",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxright1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxright1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "85%",
"zIndex": 1
}, {}, {});
flxright1.setDefaultUnit(kony.flex.DP);
var CopyLabel00465c266f9f248 = new kony.ui.Label({
"height": "25dp",
"id": "CopyLabel00465c266f9f248",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.locateus.branch"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyinterestRateLabel0b14fb4af446e45 = new kony.ui.Label({
"height": "25dp",
"id": "CopyinterestRateLabel0b14fb4af446e45",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Abdali Mall Branch",
"top": "26dp",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxright1.add(CopyLabel00465c266f9f248, CopyinterestRateLabel0b14fb4af446e45);
var Copylblopen0d5a9d725b32e44 = new kony.ui.Label({
"centerY": "50%",
"id": "Copylblopen0d5a9d725b32e44",
"isVisible": false,
"left": "6%",
"skin": "sknBojBlueFont",
"text": "&",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxOpenedIn.add(CopyflxInfoLine0b8847aa4c3b749, lblopen, flxright1, Copylblopen0d5a9d725b32e44);
var flxStatusInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxStatusInfo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxStatusInfo.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0g08c7fbbd54d4e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0g08c7fbbd54d4e",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "54dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0g08c7fbbd54d4e.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0g08c7fbbd54d4e.add();
var lblStatusinfo = new kony.ui.Label({
"centerY": "50%",
"id": "lblStatusinfo",
"isVisible": false,
"right": "5%",
"skin": "lblListOn",
"text": "T",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxRight2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxRight2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxRight2.setDefaultUnit(kony.flex.DP);
var CopyLabel0e0961549274149 = new kony.ui.Label({
"height": "25dp",
"id": "CopyLabel0e0961549274149",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.common.accountstatus"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyinterestRateLabel0h623a08258c745 = new kony.ui.Label({
"height": "25dp",
"id": "CopyinterestRateLabel0h623a08258c745",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Active",
"top": "26dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxRight2.add(CopyLabel0e0961549274149, CopyinterestRateLabel0h623a08258c745);
flxStatusInfo.add(CopyflxInfoLine0g08c7fbbd54d4e, lblStatusinfo, flxRight2);
var flxIBAN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxIBAN",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIBAN.setDefaultUnit(kony.flex.DP);
var CopyLabel0beabfcfbe34248 = new kony.ui.Label({
"height": "25dp",
"id": "CopyLabel0beabfcfbe34248",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.common.IBAN"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyinterestRateLabel0b24dd6f4438b4a = new kony.ui.Label({
"height": "25dp",
"id": "CopyinterestRateLabel0b24dd6f4438b4a",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "J0898 32433 234 34",
"top": "26dp",
"width": "81%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxLineIBAN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flxLineIBAN",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "54dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLineIBAN.setDefaultUnit(kony.flex.DP);
flxLineIBAN.add();
var IbanShareIcon = new kony.ui.Label({
"centerY": "50%",
"id": "IbanShareIcon",
"isVisible": true,
"onTouchEnd": AS_Label_hc5f4d9763df43749ac3c031a0e9c26c,
"left": "6%",
"skin": "sknBojBlueFont",
"text": "'",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIBAN.add(CopyLabel0beabfcfbe34248, CopyinterestRateLabel0b24dd6f4438b4a, flxLineIBAN, IbanShareIcon);
var flxAccountNumberInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxAccountNumberInfo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccountNumberInfo.setDefaultUnit(kony.flex.DP);
var CopyLabel08f2d764e1add4c = new kony.ui.Label({
"height": "25dp",
"id": "CopyLabel08f2d764e1add4c",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyaccountNumberLabel036c71ac511b84d = new kony.ui.Label({
"height": "25dp",
"id": "CopyaccountNumberLabel036c71ac511b84d",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "90010020177801",
"top": "24dp",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxInfoLine0afc2797a3f4f43 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0afc2797a3f4f43",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0j28652e02edc44",
"top": "54dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0afc2797a3f4f43.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0afc2797a3f4f43.add();
var AccNoShareIcon = new kony.ui.Label({
"centerY": "50%",
"id": "AccNoShareIcon",
"isVisible": true,
"onTouchEnd": AS_Label_df431890dcf1494294aa4260cfb71b8d,
"left": "6%",
"skin": "sknBojBlueFont",
"text": "'",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAccountNumberInfo.add(CopyLabel08f2d764e1add4c, CopyaccountNumberLabel036c71ac511b84d, CopyflxInfoLine0afc2797a3f4f43, AccNoShareIcon);
var flxCurrency = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxCurrency",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCurrency.setDefaultUnit(kony.flex.DP);
var lblCur = new kony.ui.Label({
"centerY": "50%",
"id": "lblCur",
"isVisible": false,
"right": "5%",
"skin": "lblListOn",
"text": "T",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxright3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxright3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxright3.setDefaultUnit(kony.flex.DP);
var CopyinterestRateLabel0dbfe2220200e49 = new kony.ui.Label({
"height": "25dp",
"id": "CopyinterestRateLabel0dbfe2220200e49",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "JOD(Jordanian Dinar)",
"top": "26dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel0e630e73d580f43 = new kony.ui.Label({
"height": "25dp",
"id": "CopyLabel0e630e73d580f43",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.more.currency"),
"top": "8dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxright3.add(CopyinterestRateLabel0dbfe2220200e49, CopyLabel0e630e73d580f43);
flxCurrency.add(lblCur, flxright3);
var flxCurrentBalance = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxCurrentBalance",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCurrentBalance.setDefaultUnit(kony.flex.DP);
var CopylblListInfoInfo0b6c3c2c5e04b4f = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblListInfoInfo0b6c3c2c5e04b4f",
"isVisible": false,
"right": "5%",
"skin": "lblListOn",
"text": "I",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxRight4 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxRight4",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxRight4.setDefaultUnit(kony.flex.DP);
var CopyinterestRateLabel09f06e1600f0e43 = new kony.ui.Label({
"height": "30dp",
"id": "CopyinterestRateLabel09f06e1600f0e43",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": " 25,525 JOD",
"top": "26dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyLabel052d12dab31224a = new kony.ui.Label({
"height": "25dp",
"id": "CopyLabel052d12dab31224a",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.deposit.availableBalance"),
"top": "8dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxRight4.add(CopyinterestRateLabel09f06e1600f0e43, CopyLabel052d12dab31224a);
flxCurrentBalance.add(CopylblListInfoInfo0b6c3c2c5e04b4f, flxRight4);
var CopyflxInfoLine0b9b914b0795e4f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxInfoLine0b9b914b0795e4f",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLineBlue",
"top": "60dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0b9b914b0795e4f.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0b9b914b0795e4f.add();
flxaccountdetailsfordeposits.add(flxOpeningDate, flxOpenedIn, flxStatusInfo, flxIBAN, flxAccountNumberInfo, flxCurrency, flxCurrentBalance, CopyflxInfoLine0b9b914b0795e4f);
var flxTabsWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "flxTabsWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "30%",
"width": "100%",
"zIndex": 11
}, {}, {});
flxTabsWrapper.setDefaultUnit(kony.flex.DP);
var lblListOnInfo = new kony.ui.Label({
"centerY": "49.72%",
"id": "lblListOnInfo",
"isVisible": true,
"right": "15%",
"onTouchEnd": AS_Label_i6716b8e1a5242968a4ca100220c4696,
"skin": "lblListOff",
"text": "W",
"top": "7dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblListInfoInfo = new kony.ui.Label({
"centerY": "50%",
"id": "lblListInfoInfo",
"isVisible": true,
"onTouchEnd": AS_Label_i15289511e734af8a0e39b1fbef37dfb,
"left": "9%",
"skin": "lblListOn",
"text": "E",
"top": "4dp",
"width": "20%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAccountOptions = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblAccountOptions",
"isVisible": false,
"onTouchEnd": AS_Label_b0ec57afef88412b8352a761dd0300fc,
"left": "9%",
"skin": "lblFontSettIconsFont2",
"text": "Q",
"top": "4dp",
"width": "20%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTabsWrapper.add(lblListOnInfo, lblListInfoInfo, lblAccountOptions);
var imgDiv = new kony.ui.Image2({
"centerY": "38.70%",
"id": "imgDiv",
"isVisible": true,
"left": "9%",
"skin": "slImage",
"src": "searcha.png",
"width": "20%",
"zIndex": 10
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var imgDivSett = new kony.ui.Image2({
"centerY": "38.70%",
"id": "imgDivSett",
"isVisible": false,
"left": "9%",
"skin": "slImage",
"src": "searcha.png",
"width": "20%",
"zIndex": 10
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxAccountOptions = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"id": "flxAccountOptions",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "40%",
"width": "100%",
"zIndex": 110
}, {}, {});
flxAccountOptions.setDefaultUnit(kony.flex.DP);
var flxRequestStatement = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxRequestStatement",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "1dp",
"width": "100%",
"zIndex": 10
}, {}, {});
flxRequestStatement.setDefaultUnit(kony.flex.DP);
var CopyflxRight0d26c9310984f45 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0d26c9310984f45",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_d1631a5fba564f339c1b506352218c72,
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxRight0d26c9310984f45.setDefaultUnit(kony.flex.DP);
var CopyinterestEarnedLabel0g3277f09dbea43 = new kony.ui.Label({
"centerY": "50%",
"height": "30dp",
"id": "CopyinterestEarnedLabel0g3277f09dbea43",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.accounts.requestStatement"),
"top": "26dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblCcPaynowIcon0g68c0fdb1f2049 = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblCcPaynowIcon0g68c0fdb1f2049",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "S",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblEdit0cb8bdbb5180c41 = new kony.ui.Label({
"height": "100%",
"id": "CopylblEdit0cb8bdbb5180c41",
"isVisible": true,
"left": "0%",
"skin": "sknLblEditDimmed",
"text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
"top": "0dp",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0d26c9310984f45.add(CopyinterestEarnedLabel0g3277f09dbea43, CopylblCcPaynowIcon0g68c0fdb1f2049, CopylblEdit0cb8bdbb5180c41);
var CopyflxInfoLine0h03e27421c7942 = new kony.ui.Label({
"centerY": "50%",
"id": "CopyflxInfoLine0h03e27421c7942",
"isVisible": false,
"right": "5%",
"skin": "lblListOn",
"text": "I",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxRequestStatement.add(CopyflxRight0d26c9310984f45, CopyflxInfoLine0h03e27421c7942);
var flxOrderCheckBook = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxOrderCheckBook",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxOrderCheckBook.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0gafe4c0d61ef4f = new kony.ui.Label({
"centerY": "50%",
"id": "CopyflxInfoLine0gafe4c0d61ef4f",
"isVisible": false,
"right": "5%",
"skin": "lblListOn",
"text": "I",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyflxRight0e87efefcf3084c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0e87efefcf3084c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_df6856490c32423b900f80ccb323a7c6,
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 10
}, {}, {});
CopyflxRight0e87efefcf3084c.setDefaultUnit(kony.flex.DP);
var CopyinterestEarnedLabel0h59544fc55f948 = new kony.ui.Label({
"centerY": "50%",
"height": "30dp",
"id": "CopyinterestEarnedLabel0h59544fc55f948",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.accounts.orderchequebook"),
"top": "26dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblCcPaynowIcon0ia9bd60362544a = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblCcPaynowIcon0ia9bd60362544a",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "F",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblEdit0b4a3f2847e3d44 = new kony.ui.Label({
"height": "100%",
"id": "CopylblEdit0b4a3f2847e3d44",
"isVisible": true,
"left": "0%",
"skin": "sknLblEditDimmed",
"text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
"top": "0dp",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxRight0e87efefcf3084c.add(CopyinterestEarnedLabel0h59544fc55f948, CopylblCcPaynowIcon0ia9bd60362544a, CopylblEdit0b4a3f2847e3d44);
flxOrderCheckBook.add(CopyflxInfoLine0gafe4c0d61ef4f, CopyflxRight0e87efefcf3084c);
var flxLoanPostPone = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"focusSkin": "flxsegBg",
"height": "65dp",
"id": "flxLoanPostPone",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_e5c906bd97224a0cbf51b4d9798b332a,
"skin": "flxsegBg",
"top": "1dp",
"width": "100%",
"zIndex": 10
}, {}, {});
flxLoanPostPone.setDefaultUnit(kony.flex.DP);
var flxLoanPostPoneAction = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxLoanPostPoneAction",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLoanPostPoneAction.setDefaultUnit(kony.flex.DP);
var CopylblCcPaynowIcon0cd9c3d7611af4b = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblCcPaynowIcon0cd9c3d7611af4b",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "F",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyinterestEarnedLabel0a386e97ffafd44 = new kony.ui.Label({
"centerY": "50%",
"height": "30dp",
"id": "CopyinterestEarnedLabel0a386e97ffafd44",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.loanpostpone.title"),
"top": "26dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblEdit0a54be50984ac43 = new kony.ui.Label({
"height": "100%",
"id": "CopylblEdit0a54be50984ac43",
"isVisible": true,
"left": "0%",
"skin": "sknLblEditDimmed",
"text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
"top": "0dp",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxLoanPostPoneAction.add(CopylblCcPaynowIcon0cd9c3d7611af4b, CopyinterestEarnedLabel0a386e97ffafd44, CopylblEdit0a54be50984ac43);
flxLoanPostPone.add(flxLoanPostPoneAction);
flxAccountOptions.add(flxRequestStatement, flxOrderCheckBook, flxLoanPostPone);
mainContent.add(CopypreferredAccountEnable0d7ab026f4a5845, titleBarAccountInfo, accountDetailsContainer, flxaccountdetailsSavings, preferredAccountEnable, addExternalAccount, contactSegmentList, callBankForEnquiry, CopycallBankForEnquiry0b9b1a0be4e0b49, segmentBorderBottom, flxaccountdetailsfordeposits, flxTabsWrapper, imgDiv, imgDivSett, flxAccountOptions);
var flxAccountInfoNavOptKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxAccountInfoNavOptKA",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"onClick": AS_FlexContainer_594ad1bb128b46be92ff65d036fd994d,
"skin": "sknFlx20000000KA",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccountInfoNavOptKA.setDefaultUnit(kony.flex.DP);
var flxInnerAccntInfoNavOptKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bottom": "0dp",
"centerY": "70%",
"clipBounds": true,
"id": "flxInnerAccntInfoNavOptKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "5%",
"skin": "sknRoundedCornerKA",
"width": "90%",
"zIndex": 1
}, {}, {});
flxInnerAccntInfoNavOptKA.setDefaultUnit(kony.flex.DP);
var segAccntInfoNavOptKA = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [{
"lblMenuItem": "Edit Nick Name"
}],
"groupCells": false,
"id": "segAccntInfoNavOptKA",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_1153b1419cbf405cbd8956ac91363cb0,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowSkin": "seg2Normal",
"rowTemplate": flxNavigationOptKA,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": false,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"flxLineDividerNavOptKA": "flxLineDividerNavOptKA",
"flxNavigationOptKA": "flxNavigationOptKA",
"lblMenuItem": "lblMenuItem"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": true,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": true
});
var btnAccountInfoNavOptSignOutKA = new kony.ui.Button({
"focusSkin": "slButtonGlossRed",
"height": "50dp",
"id": "btnAccountInfoNavOptSignOutKA",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_34687d3b67d3474e8b0c17ee9a2248a2,
"skin": "sknBtnSignOutEB5000KA",
"text": kony.i18n.getLocalizedString("i18n.common.signOut"),
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxInnerAccntInfoNavOptKA.add(segAccntInfoNavOptKA, btnAccountInfoNavOptSignOutKA);
flxAccountInfoNavOptKA.add(flxInnerAccntInfoNavOptKA);
var flxPageIndicator = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": false,
"bounces": false,
"centerX": "51%",
"centerY": "4%",
"clipBounds": true,
"enableScrolling": true,
"height": "4%",
"horizontalScrollIndicator": false,
"id": "flxPageIndicator",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_HORIZONTAL,
"skin": "slFSbox",
"verticalScrollIndicator": true,
"width": "48%",
"zIndex": 10
}, {}, {});
flxPageIndicator.setDefaultUnit(kony.flex.DP);
var flxPageIndicator0 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "20%",
"id": "flxPageIndicator0",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknPageIndicatorFlexnoOpc",
"width": "8%",
"zIndex": 1
}, {}, {});
flxPageIndicator0.setDefaultUnit(kony.flex.DP);
flxPageIndicator0.add();
flxPageIndicator.add(flxPageIndicator0);
frmAccountInfoKA.add(mainContent, flxAccountInfoNavOptKA, flxPageIndicator);
};
function frmAccountInfoKAGlobalsAr() {
frmAccountInfoKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmAccountInfoKAAr,
"bounces": false,
"enableScrolling": true,
"enabledForIdleTimeout": true,
"footers": [footerBack],
"id": "frmAccountInfoKA",
"init": AS_Form_hf0544f811ac401cb073b17ab1469d46,
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_5eedd91880c2459ba4fcdb82178fec87,
"skin": "CopysknSuccessBkg0abb718ccac584f",
"statusBarHidden": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": true,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"inTransitionConfig": {
"transitionDirection": "none",
"transitionEffect": "none"
},
"needsIndicatorDuringPostShow": false,
"outTransitionConfig": {
"transitionDirection": "none",
"transitionEffect": "none"
},
"retainScrollPosition": false,
"statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
"titleBar": false
});
};
