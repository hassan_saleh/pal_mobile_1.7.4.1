//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetmpQuickBalanceSegAr() {
    flxSegQuickBalanceAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "65dp",
        "id": "flxSegQuickBalance",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxSegQuickBalanceAr.setDefaultUnit(kony.flex.DP);
    var lblAccount = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAccount",
        "isVisible": true,
        "right": "5%",
        "skin": "sknlblTouchIdsmall",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAccNumber = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAccNumber",
        "isVisible": false,
        "right": "0%",
        "skin": "sknlblTouchIdsmall",
        "text": "Label",
        "top": "101dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblIncommingTick = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblIncommingTick",
        "isVisible": true,
        "left": "3.80%",
        "skin": "sknBOJttfwhiteeSmall",
        "text": "r",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblIncommingRing = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblIncommingRing",
        "isVisible": true,
        "left": "2%",
        "skin": "sknBOJttfwhitee",
        "text": "s",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAccountName = new kony.ui.Label({
        "id": "lblAccountName",
        "isVisible": false,
        "right": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxSegQuickBalanceAr.add(lblAccount, lblAccNumber, lblIncommingTick, lblIncommingRing, lblAccountName);
}
