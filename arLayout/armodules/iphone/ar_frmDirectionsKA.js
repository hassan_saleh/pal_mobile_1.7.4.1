//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:57 EEST 2020
function addWidgetsfrmDirectionsKAAr() {
frmDirectionsKA.setDefaultUnit(kony.flex.DP);
var titleBarLocator = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "titleBarLocator",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntitleBarGradient",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarLocator.setDefaultUnit(kony.flex.DP);
var backButton = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknleftBackButtonFocus",
"height": "50dp",
"id": "backButton",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_75d8008cfbfe4781b9b0a3125938152a,
"skin": "sknleftBackButtonNormal",
"top": "0dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var flxTitleKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "38dp",
"id": "flxTitleKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "15%",
"skin": "sknslFbox",
"width": "70%",
"zIndex": 1
}, {}, {});
flxTitleKA.setDefaultUnit(kony.flex.DP);
var lblTitleKA = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblTitleKA",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.locateUs.BranchorATM"),
"width": "70%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTitleKA.add(lblTitleKA);
var cancelButton = new kony.ui.Button({
"focusSkin": "skntitleBarTextButtonFocus",
"height": "50dp",
"id": "cancelButton",
"isVisible": false,
"minWidth": "50dp",
"left": "-15%",
"skin": "skntitleBarTextButton",
"text": kony.i18n.getLocalizedString("i18n.common.cancel"),
"top": "0dp",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var btnBackKA = new kony.ui.Button({
"focusSkin": "sknleftBackButtonFocus",
"height": "50dp",
"id": "btnBackKA",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_dd661676647f44a58948235bbced06f3,
"skin": "sknleftBackButtonNormal",
"top": "0dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
titleBarLocator.add(backButton, flxTitleKA, cancelButton, btnBackKA);
var mainContent = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"id": "mainContent",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkg",
"top": "50dp",
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var locatorListView = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70%",
"id": "locatorListView",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "30%",
"width": "100%",
"zIndex": 1
}, {}, {});
locatorListView.setDefaultUnit(kony.flex.DP);
var dividerKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "dividerKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
dividerKA.setDefaultUnit(kony.flex.DP);
dividerKA.add();
var locatorSegmentList = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50.00%",
"data": [{
"imgDirKA": "imagedrag.png",
"lblAddress1KA": "Address1",
"lblAddress2KA": "Address2",
"lblMilesKA": "20 Feet Right"
}, {
"imgDirKA": "imagedrag.png",
"lblAddress1KA": "Address1",
"lblAddress2KA": "Address2",
"lblMilesKA": "20 Feet Right"
}, {
"imgDirKA": "imagedrag.png",
"lblAddress1KA": "Address1",
"lblAddress2KA": "Address2",
"lblMilesKA": "20 Feet Right"
}, {
"imgDirKA": "imagedrag.png",
"lblAddress1KA": "Address1",
"lblAddress2KA": "Address2",
"lblMilesKA": "20 Feet Right"
}, {
"imgDirKA": "imagedrag.png",
"lblAddress1KA": "Address1",
"lblAddress2KA": "Address2",
"lblMilesKA": "20 Feet Right"
}, {
"imgDirKA": "imagedrag.png",
"lblAddress1KA": "Address1",
"lblAddress2KA": "Address2",
"lblMilesKA": "20 Feet Right"
}, {
"imgDirKA": "imagedrag.png",
"lblAddress1KA": "Address1",
"lblAddress2KA": "Address2",
"lblMilesKA": "20 Feet Right"
}, {
"imgDirKA": "imagedrag.png",
"lblAddress1KA": "Address1",
"lblAddress2KA": "Address2",
"lblMilesKA": "20 Feet Right"
}, {
"imgDirKA": "imagedrag.png",
"lblAddress1KA": "Address1",
"lblAddress2KA": "Address2",
"lblMilesKA": "20 Feet Right"
}],
"groupCells": false,
"height": "100%",
"id": "locatorSegmentList",
"isVisible": true,
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": CopyFlexContainer08af3de49f89842,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": true,
"showScrollbars": false,
"top": "1dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer08af3de49f89842": "CopyFlexContainer08af3de49f89842",
"contactListDivider": "contactListDivider",
"imgDirKA": "imgDirKA",
"informationListDivider": "informationListDivider",
"lblAddress1KA": "lblAddress1KA",
"lblAddress2KA": "lblAddress2KA",
"lblMilesKA": "lblMilesKA"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": true,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": false
});
var bottomListDivider = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "bottomListDivider",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
bottomListDivider.setDefaultUnit(kony.flex.DP);
bottomListDivider.add();
var noSearchResults = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "noSearchResults",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
noSearchResults.setDefaultUnit(kony.flex.DP);
var CopyImage0c9980dffa3f24b = new kony.ui.Image2({
"centerX": "50%",
"height": "100dp",
"id": "CopyImage0c9980dffa3f24b",
"isVisible": true,
"skin": "sknslImage",
"src": "search_empty.png",
"top": "60dp",
"width": "100dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyLabel01cf823c2e98e4a = new kony.ui.Label({
"centerX": "50%",
"id": "CopyLabel01cf823c2e98e4a",
"isVisible": true,
"right": 0,
"skin": "sknnoSearchResultsText",
"text": kony.i18n.getLocalizedString("i18n.locateus.enterZipCodeOrCity"),
"top": "10dp",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var atmLink = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"id": "atmLink",
"isVisible": true,
"onClick": AS_Button_07065ee5644946d7a9a0e9e5c5f6befb,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.locateus.atmLink"),
"top": "20dp",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
noSearchResults.add(CopyImage0c9980dffa3f24b, CopyLabel01cf823c2e98e4a, atmLink);
locatorListView.add(dividerKA, locatorSegmentList, bottomListDivider, noSearchResults);
var locatorMapView = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "locatorMapView",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
locatorMapView.setDefaultUnit(kony.flex.DP);
var locatorMap = new kony.ui.Map({
"calloutWidth": 100,
"defaultPinImage": "atm_icon_inactive.png",
"height": "100%",
"id": "locatorMap",
"isVisible": true,
"right": "0dp",
"locationData": [{
"desc": "206 Guadalupe St",
"lat": "17.447326",
"lon": "78.371358",
"name": "Guadalupe ATM"
}, {
"desc": "2119 E Seventh St",
"lat": "17.441839",
"lon": "78.380928",
"name": "East Seventh"
}, {
"desc": "Austin, TX 78701",
"lat": "28.449340",
"lon": "-81.481519",
"name": "South Lamar ATM"
}, {
"desc": "Colorado, TX 78701",
"lat": "28.449340",
"lon": "-81.481519",
"name": "Lake Austin"
}, {
"desc": "Alaska, TX 78701",
"lat": "60.441839",
"lon": "31.481519",
"name": "Marcos Lavino"
}],
"provider": constants.MAP_PROVIDER_GOOGLE,
"top": "0dp",
"width": "100%"
}, {}, {
"mode": constants.MAP_VIEW_MODE_NORMAL,
"showCurrentLocation": constants.MAP_VIEW_SHOW_CURRENT_LOCATION_AS_CIRCLE,
"zoomLevel": 12
});
var mapCurrentLocationWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "3%",
"clipBounds": false,
"height": "60dp",
"id": "mapCurrentLocationWrapper",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "slFbox",
"width": "60dp",
"zIndex": 1
}, {}, {});
mapCurrentLocationWrapper.setDefaultUnit(kony.flex.DP);
var Image02e834c4c0b774d = new kony.ui.Image2({
"centerX": "52%",
"centerY": "52%",
"height": "60dp",
"id": "Image02e834c4c0b774d",
"isVisible": true,
"skin": "sknslImage",
"src": "locate.png",
"width": "60dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var currentLocationButton = new kony.ui.Button({
"focusSkin": "sknCopyslButtonGlossBlue0f07d963e023c4d",
"height": "100%",
"id": "currentLocationButton",
"isVisible": true,
"right": "0dp",
"skin": "sknCopyslButtonGlossBlue0773d36c6abc444",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
mapCurrentLocationWrapper.add(Image02e834c4c0b774d, currentLocationButton);
var closeCalloutButton = new kony.ui.Button({
"focusSkin": "skninvisibleButton",
"height": "100%",
"id": "closeCalloutButton",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_46f1ccc7c20f4fa6a70b40fa974e7abc,
"skin": "skninvisibleButton",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var flxMapBGShadeKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "150dp",
"id": "flxMapBGShadeKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknFlxBGWhiteShadeKA",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMapBGShadeKA.setDefaultUnit(kony.flex.DP);
flxMapBGShadeKA.add();
locatorMapView.add(locatorMap, mapCurrentLocationWrapper, closeCalloutButton, flxMapBGShadeKA);
var searchFocus = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "searchFocus",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknCopyslFbox061e616a387ca49",
"top": "29.98%",
"width": "100%",
"zIndex": 1
}, {}, {});
searchFocus.setDefaultUnit(kony.flex.DP);
var searchCurrentLocationWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "searchCurrentLocationWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgBorderKA",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
searchCurrentLocationWrapper.setDefaultUnit(kony.flex.DP);
var Image03409386204724e = new kony.ui.Image2({
"centerY": "50%",
"height": "25dp",
"id": "Image03409386204724e",
"isVisible": true,
"right": "5%",
"skin": "sknslImage",
"src": "locator_icon_outline_blue.png",
"width": "25dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyLabel05dfd79fd44784e = new kony.ui.Label({
"centerY": "50%",
"id": "CopyLabel05dfd79fd44784e",
"isVisible": true,
"right": "18%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.locateus.searchByCurrentLocation"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var Button0de7cd327cd5942 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonGreyFocus",
"height": "100%",
"id": "Button0de7cd327cd5942",
"isVisible": true,
"right": "0dp",
"skin": "skninvisibleButtonNormal",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
searchCurrentLocationWrapper.add(Image03409386204724e, CopyLabel05dfd79fd44784e, Button0de7cd327cd5942);
searchFocus.add(searchCurrentLocationWrapper);
var MapListControllerWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "37dp",
"id": "MapListControllerWrapper",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "flxBGKA",
"top": "2%",
"width": "50%",
"zIndex": 20
}, {}, {});
MapListControllerWrapper.setDefaultUnit(kony.flex.DP);
var btnMapKA = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknaccountFilterButtonFocus",
"height": "100%",
"id": "btnMapKA",
"isVisible": true,
"onClick": AS_Button_b2c5fe7cfef043f6907c68c8674fd467,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.Map.Map"),
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 6, 0,6, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var btnListKA = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknaccountFilterButtonFocus",
"height": "100%",
"id": "btnListKA",
"isVisible": true,
"onClick": AS_Button_9393bf8279db4ea0a5e2bd95354d736c,
"skin": "sknandroidSegmentedTextInactive",
"text": "Directions",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 2, 0,2, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
MapListControllerWrapper.add( btnListKA,btnMapKA);
var flxDirectionDetailsKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100dp",
"id": "flxDirectionDetailsKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"skin": "slFbox",
"top": "67dp",
"width": "100%",
"zIndex": 5
}, {}, {});
flxDirectionDetailsKA.setDefaultUnit(kony.flex.DP);
var lblDistanceKA = new kony.ui.Label({
"id": "lblDistanceKA",
"isVisible": true,
"right": 5,
"skin": "sknLblMilesKA",
"text": "2.2 Miles / 10 Minutes",
"top": "3dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAddress1KA = new kony.ui.Label({
"id": "lblAddress1KA",
"isVisible": true,
"right": "0",
"skin": "sknaccountAmount",
"text": "504 , Austin Market street",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAddress2KA = new kony.ui.Label({
"id": "lblAddress2KA",
"isVisible": true,
"right": 0,
"skin": "sknaccountAmount",
"text": "Austin 50001",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxDirectionDetailsKA.add(lblDistanceKA, lblAddress1KA, lblAddress2KA);
mainContent.add(locatorListView, locatorMapView, searchFocus, MapListControllerWrapper, flxDirectionDetailsKA);
frmDirectionsKA.add(titleBarLocator, mainContent);
};
function frmDirectionsKAGlobalsAr() {
frmDirectionsKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmDirectionsKAAr,
"bounces": true,
"enableScrolling": false,
"enabledForIdleTimeout": false,
"id": "frmDirectionsKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"onHide": AS_Form_b2d63282523142bf9f93fd44f4a3330f,
"preShow": AS_Form_be06ffc5478f47eeb20a5e2a394a7ebd,
"skin": "sknmainGradient",
"statusBarHidden": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": true,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": true,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"inTransitionConfig": {
"transitionDirection": "fromBottom",
"transitionEffect": "none"
},
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
"titleBar": false
});
};
