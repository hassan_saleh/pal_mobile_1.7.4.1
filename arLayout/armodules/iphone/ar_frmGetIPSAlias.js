//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:58 EEST 2020
function addWidgetsfrmGetIPSAliasAr() {
frmGetIPSAlias.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknBlueBGheader",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_c555fc96803048f8b90b9d3c93e2a54e,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "3dp",
"skin": "sknBackIcon",
"text": "j",
"top": "9dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0%",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, lblBack);
var lblTitle = new kony.ui.Label({
"centerY": "50%",
"height": "90%",
"id": "lblTitle",
"isVisible": true,
"left": "20%",
"minHeight": "90%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.CLIQ.YourAliases"),
"top": "8dp",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxHeader.add(flxBack, lblTitle);
var flxMain = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "90%",
"horizontalScrollIndicator": true,
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "9%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var segGetAllIPSAliases = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [{
"lblAliasSeg": "",
"lblAliasTitleSeg": kony.i18n.getLocalizedString("i18n.jomopay.aliastype"),
"lblAliasTypeSeg": "",
"lblAliasTypeTitleSeg": "Alias Type"
}, {
"lblAliasSeg": "",
"lblAliasTitleSeg": kony.i18n.getLocalizedString("i18n.jomopay.aliastype"),
"lblAliasTypeSeg": "",
"lblAliasTypeTitleSeg": "Alias Type"
}, {
"lblAliasSeg": "",
"lblAliasTitleSeg": kony.i18n.getLocalizedString("i18n.jomopay.aliastype"),
"lblAliasTypeSeg": "",
"lblAliasTypeTitleSeg": "Alias Type"
}],
"groupCells": false,
"id": "segGetAllIPSAliases",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "slSegSendMoney",
"rowTemplate": flxMainSeg,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"flxIPSAlias": "flxIPSAlias",
"flxMainSeg": "flxMainSeg",
"lblAliasSeg": "lblAliasSeg",
"lblAliasTitleSeg": "lblAliasTitleSeg",
"lblAliasTypeSeg": "lblAliasTypeSeg",
"lblAliasTypeTitleSeg": "lblAliasTypeTitleSeg"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": true,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": true
});
flxMain.add(segGetAllIPSAliases);
frmGetIPSAlias.add(flxHeader, flxMain);
};
function frmGetIPSAliasGlobalsAr() {
frmGetIPSAliasAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmGetIPSAliasAr,
"enabledForIdleTimeout": false,
"id": "frmGetIPSAlias",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": true,
"skin": "sknBackground"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
