//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializesegmanagecardstmpltAr() {
    flxCreditCardTemplateKAAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "flxsegSknblue",
        "height": "90%",
        "id": "flxCreditCardTemplateKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flexTransparent"
    }, {}, {});
    flxCreditCardTemplateKAAr.setDefaultUnit(kony.flex.DP);
    var cardType = new kony.ui.Label({
        "centerX": "50%",
        "height": "12%",
        "id": "cardType",
        "isVisible": true,
        "skin": "sknLblBack",
        "top": "6%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxCardDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0%",
        "clipBounds": true,
        "height": "81%",
        "id": "flxCardDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "14%",
        "skin": "slFbox",
        "width": "72%",
        "zIndex": 1
    }, {}, {});
    flxCardDetails.setDefaultUnit(kony.flex.DP);
    var cardImage = new kony.ui.Image2({
        "centerX": "50%",
        "height": "100%",
        "id": "cardImage",
        "isVisible": true,
        "skin": "slImage",
        "src": "card_affluent_debit.png",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var cardNumber = new kony.ui.Label({
        "height": "30dp",
        "id": "cardNumber",
        "isVisible": true,
        "right": "26%",
        "skin": "sknLblWhiteTmp",
        "top": "24%",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CardHolder = new kony.ui.Label({
        "bottom": "1%",
        "id": "CardHolder",
        "isVisible": true,
        "right": "8%",
        "skin": "sknLblWhiteTmp",
        "width": "84%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var ValidThru = new kony.ui.Label({
        "id": "ValidThru",
        "isVisible": true,
        "right": "42%",
        "skin": "sknLblWhiteTmp",
        "top": "62%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxCardDetails.add(cardImage, cardNumber, CardHolder, ValidThru);
    flxCreditCardTemplateKAAr.add(cardType, flxCardDetails);
}
