//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:57 EEST 2020
function addWidgetsfrmEPSAr() {
frmEPS.setDefaultUnit(kony.flex.DP);
var flxMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 10
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxHeaderIPS = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeaderIPS",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "s",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeaderIPS.setDefaultUnit(kony.flex.DP);
var Label0g6814cba9e7841 = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "Label0g6814cba9e7841",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.CLIQ.Title"),
"textStyle": {},
"top": "15dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxNext = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "31dp",
"id": "flxNext",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_dc39a67e81024e4daf25ad9a95043fb3,
"right": "2.04%",
"skin": "slFbox",
"top": "11.75%",
"width": "17.33%",
"zIndex": 1
}, {}, {});
flxNext.setDefaultUnit(kony.flex.DP);
var lblNext = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Next Page"
},
"centerY": "50%",
"height": "100%",
"id": "lblNext",
"isVisible": true,
"left": "0dp",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"textStyle": {},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNext.add(lblNext);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_jc5faecf6023498da0ce1eadb2aa04d1,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, lblBack);
flxHeaderIPS.add(Label0g6814cba9e7841, flxNext, flxBack);
var FlexScrollContainer0f98b54f0d87740 = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "90%",
"horizontalScrollIndicator": true,
"id": "FlexScrollContainer0f98b54f0d87740",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 10
}, {}, {});
FlexScrollContainer0f98b54f0d87740.setDefaultUnit(kony.flex.DP);
var flxAccounFrom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxAccounFrom",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 10
}, {}, {});
flxAccounFrom.setDefaultUnit(kony.flex.DP);
var lblFrom = new kony.ui.Label({
"height": "35%",
"id": "lblFrom",
"isVisible": true,
"right": "20dp",
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.from"),
"textStyle": {},
"top": "-5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblSplit2 = new kony.ui.Label({
"height": "1dp",
"id": "lblSplit2",
"isVisible": true,
"right": "0dp",
"skin": "lblsknToandFromAccLine",
"top": "0dp",
"width": "100%",
"zIndex": 10
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxAcc1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70%",
"id": "flxAcc1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "-0.09%",
"skin": "slFbox",
"top": "0.00%",
"width": "100%",
"zIndex": 10
}, {}, {});
flxAcc1.setDefaultUnit(kony.flex.DP);
var flxIcon1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "45dp",
"id": "flxIcon1",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "6%",
"skin": "CopysknFlxToIcon0i50cfcc9869a40",
"top": "10%",
"width": "45dp",
"zIndex": 100
}, {}, {});
flxIcon1.setDefaultUnit(kony.flex.DP);
var lblIcon1 = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "80%",
"id": "lblIcon1",
"isVisible": true,
"right": "19dp",
"skin": "sknLblFromIcon",
"text": "BH",
"textStyle": {},
"top": "13dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIcon1.add(lblIcon1);
var lblAccountName1 = new kony.ui.Label({
"bottom": "0%",
"height": "40%",
"id": "lblAccountName1",
"isVisible": true,
"right": "21%",
"skin": "sknLblAccNum",
"textStyle": {},
"top": "10%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAccountNumber1 = new kony.ui.Label({
"height": "35%",
"id": "lblAccountNumber1",
"isVisible": true,
"right": "21%",
"skin": "sknSmallForIban",
"textStyle": {},
"top": "50%",
"width": "72%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnForward1 = new kony.ui.Button({
"focusSkin": "btnBackFoc0a4aadf8ca3bb4a",
"height": "100%",
"id": "btnForward1",
"isVisible": true,
"right": "5%",
"onClick": AS_Button_ebc84f2a29e0468bae5a79580ca5f6e6,
"left": 0,
"skin": "sknBtnForwardDimmed",
"text": kony.i18n.getLocalizedString("i18n.common.reverseback"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxAcc1.add(flxIcon1, lblAccountName1, lblAccountNumber1, btnForward1);
var lblSelectanAccount1 = new kony.ui.Label({
"centerY": "-35%",
"id": "lblSelectanAccount1",
"isVisible": true,
"right": "8%",
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18n.Transfer.selectAcc"),
"textStyle": {},
"top": "19dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblFromAccCurr = new kony.ui.Label({
"id": "lblFromAccCurr",
"isVisible": true,
"right": "0dp",
"skin": "sknTrans",
"textStyle": {},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAccounFrom.add(lblFrom, lblSplit2, flxAcc1, lblSelectanAccount1, lblFromAccCurr);
var lblSplit1 = new kony.ui.Label({
"height": "1dp",
"id": "lblSplit1",
"isVisible": true,
"right": "0dp",
"skin": "lblsknToandFromAccLine",
"top": "0dp",
"width": "100%",
"zIndex": 10
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxAccountTo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxAccountTo",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 10
}, {}, {});
flxAccountTo.setDefaultUnit(kony.flex.DP);
var lblTo = new kony.ui.Label({
"height": "35%",
"id": "lblTo",
"isVisible": true,
"right": "20dp",
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18.CLIQ.selectRegType"),
"textStyle": {},
"top": "-5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxRadioIbanAliasSelection = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": false,
"height": "35%",
"id": "flxRadioIbanAliasSelection",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxRadioIbanAliasSelection.setDefaultUnit(kony.flex.DP);
var flxIBANAlias = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100%",
"id": "flxIBANAlias",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "3%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIBANAlias.setDefaultUnit(kony.flex.DP);
var btnAlias = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "75%",
"id": "btnAlias",
"isVisible": true,
"right": "2%",
"onClick": AS_Button_i384728c2537420891282a51bfcb923d,
"skin": "sknOrangeBGRNDBOJ",
"text": kony.i18n.getLocalizedString("i18n.jomopay.aliastype"),
"top": "2%",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnMob = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "75%",
"id": "btnMob",
"isVisible": true,
"right": "35%",
"onClick": AS_Button_c0dfd3117c3f449bb29108b40418bd2a,
"skin": "slButtonBlueFocus",
"text": kony.i18n.getLocalizedString("i18n.jomopay.mobiletype"),
"top": "2%",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnIBAN = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "75%",
"id": "btnIBAN",
"isVisible": true,
"onClick": AS_Button_h05db8b9899c48469a3e8409ea660b75,
"left": "2%",
"skin": "slButtonBlueFocus",
"text": kony.i18n.getLocalizedString("i18.CLIQ.IBAN"),
"top": "2%",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxIBANAlias.add(btnAlias, btnMob, btnIBAN);
flxRadioIbanAliasSelection.add(flxIBANAlias);
flxAccountTo.add(lblTo, flxRadioIbanAliasSelection);
var lblSplit3 = new kony.ui.Label({
"height": "1dp",
"id": "lblSplit3",
"isVisible": true,
"right": "0dp",
"skin": "lblsknToandFromAccLine",
"top": "0dp",
"width": "100%",
"zIndex": 10
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxDetailsAlias = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxDetailsAlias",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDetailsAlias.setDefaultUnit(kony.flex.DP);
var flxAliasType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "110dp",
"id": "flxAliasType",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAliasType.setDefaultUnit(kony.flex.DP);
var lblAliasType = new kony.ui.Label({
"id": "lblAliasType",
"isVisible": false,
"right": "2%",
"skin": "slLabelTitle",
"text": "Alias Type",
"textStyle": {},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxAliasTypes = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxAliasTypes",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "3%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAliasTypes.setDefaultUnit(kony.flex.DP);
var btnEmail = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "75%",
"id": "btnEmail",
"isVisible": false,
"right": "2%",
"onClick": AS_Button_d17df9e376f645b3a43b885263a3d52d,
"skin": "slButtonBlueFocus",
"text": "Email",
"top": "2%",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnMobile = new kony.ui.Button({
"centerY": "49.83%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "75%",
"id": "btnMobile",
"isVisible": true,
"right": "2.00%",
"onClick": AS_Button_b7a3abb29df0425f8c2d47b1793568a1,
"skin": "slButtonBlueFocus",
"text": "Mobile",
"top": "2%",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btntext = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "75%",
"id": "btntext",
"isVisible": true,
"right": "35%",
"onClick": AS_Button_a3ecc24341644f45aef4b5c73ef13d47,
"skin": "slButtonBlueFocus",
"text": "Text",
"top": "2%",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxAliasTypes.add( btntext, btnMobile,btnEmail);
flxAliasType.add(lblAliasType, flxAliasTypes);
flxDetailsAlias.add(flxAliasType);
var flxDetailsIBAN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxDetailsIBAN",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDetailsIBAN.setDefaultUnit(kony.flex.DP);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"id": "lblTitle",
"isVisible": false,
"right": "0dp",
"skin": "slLabelTitle",
"text": kony.i18n.getLocalizedString("i18n.common.IBAN"),
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBeneNameIBAN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBeneNameIBAN",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBeneNameIBAN.setDefaultUnit(kony.flex.DP);
var lblBeneNameIBANTitle = new kony.ui.Label({
"id": "lblBeneNameIBANTitle",
"isVisible": true,
"right": "2%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.transfer.BeneficiaryName"),
"textStyle": {},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBeneNameIBAN = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "50%",
"id": "lblBeneNameIBAN",
"isVisible": true,
"onTouchEnd": AS_Label_g57a3faf961241e984be682b29b54a8b,
"skin": "slLabelTitle",
"textStyle": {},
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxUnderlineBeneNameIBAN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBeneNameIBAN",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95.30%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBeneNameIBAN.setDefaultUnit(kony.flex.DP);
flxUnderlineBeneNameIBAN.add();
flxBeneNameIBAN.add(lblBeneNameIBANTitle, lblBeneNameIBAN, flxUnderlineBeneNameIBAN);
var flxAmountIBAN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAmountIBAN",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAmountIBAN.setDefaultUnit(kony.flex.DP);
var lblAmount = new kony.ui.Label({
"id": "lblAmount",
"isVisible": true,
"right": "2%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 10
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCurrCodeIBAN = new kony.ui.Label({
"centerY": "70%",
"id": "lblCurrCodeIBAN",
"isVisible": true,
"right": "88%",
"skin": "lblAccountStaticText",
"text": "JOD",
"textStyle": {},
"top": "42%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 5
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxUnderlineAmountIBAN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAmountIBAN",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95.30%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAmountIBAN.setDefaultUnit(kony.flex.DP);
flxUnderlineAmountIBAN.add();
var txtAmountIBAN = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "50%",
"id": "txtAmountIBAN",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
"right": "0%",
"onDone": AS_TextField_e543fd73147b406ea7fe4a2b6f9f239b,
"onTextChange": AS_TextField_i1fd46864d6b4ee3a9d16a036ec1e7b7,
"placeholder": "0.000",
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxAmountIBAN.add(lblAmount, lblCurrCodeIBAN, flxUnderlineAmountIBAN, txtAmountIBAN);
var flxIbanDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxIbanDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxIbanDetails.setDefaultUnit(kony.flex.DP);
var lblIBANAlias = new kony.ui.Label({
"id": "lblIBANAlias",
"isVisible": true,
"right": "2%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18.CLIQ.IBAN"),
"textStyle": {},
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtIBANAlias = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_ALL,
"bottom": "4.00%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "50%",
"id": "txtIBANAlias",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 30,
"onDone": AS_TextField_cc66c1332ea24c2588f20ac1e41f8b34,
"onTextChange": AS_TextField_a34cd033ff6e4437b2b1f8d6c7437e63,
"onTouchEnd": AS_TextField_a1cde35b94d94560b115e14e3058bd30,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onBeginEditing": AS_TextField_g92d026c82524f0e920f4378c238e534,
"onEndEditing": AS_TextField_f12bcec5edad4681927e22735dc52927,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineIBAN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineIBAN",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "94%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineIBAN.setDefaultUnit(kony.flex.DP);
flxUnderlineIBAN.add();
flxIbanDetails.add(lblIBANAlias, txtIBANAlias, flxUnderlineIBAN);
var lblHintIBAN = new kony.ui.Label({
"id": "lblHintIBAN",
"isVisible": false,
"right": "5%",
"skin": "sknInline",
"text": kony.i18n.getLocalizedString("i18n.beneficiary.invalidIBAN"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxIbanBeneName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxIbanBeneName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxIbanBeneName.setDefaultUnit(kony.flex.DP);
var lblIBANBeneName = new kony.ui.Label({
"id": "lblIBANBeneName",
"isVisible": true,
"right": "2%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.transfer.BeneficiaryName"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtIBANBeneName = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "50%",
"id": "txtIBANBeneName",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onDone": AS_TextField_i521b8e7d6914fb4996712235b5a91b1,
"onTextChange": AS_TextField_b57335955b5a47fc85b052226f26adff,
"onTouchEnd": AS_TextField_f9a4f13593d74daa8f90f1856c3a0708,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onBeginEditing": AS_TextField_j258c48a208e47b9a798b54ea402f3f1,
"onEndEditing": AS_TextField_b5b57d87d5f644cd9ab52d808ee74515,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var IBANBeneNameDiv = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "IBANBeneNameDiv",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 1
}, {}, {});
IBANBeneNameDiv.setDefaultUnit(kony.flex.DP);
IBANBeneNameDiv.add();
flxIbanBeneName.add(lblIBANBeneName, txtIBANBeneName, IBANBeneNameDiv);
var flxAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": false,
"height": "70dp",
"id": "flxAddress",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAddress.setDefaultUnit(kony.flex.DP);
var lblAddress = new kony.ui.Label({
"id": "lblAddress",
"isVisible": true,
"right": "2%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.common.address"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtAddressAlias = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_ALL,
"bottom": "4.00%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "50%",
"id": "txtAddressAlias",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 35,
"onDone": AS_TextField_b002c944fe4148aab6030510b2b41ced,
"onTextChange": AS_TextField_a47a540d44684d0f99eb0ce952ec724c,
"onTouchEnd": AS_TextField_b801087c581b40cea58a07516e2b7337,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onBeginEditing": AS_TextField_e34d184f6cc149198507b7b25364dc17,
"onEndEditing": AS_TextField_e2b4722045e043ccaa6b4866efe852de,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineAddressBene = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAddressBene",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAddressBene.setDefaultUnit(kony.flex.DP);
flxUnderlineAddressBene.add();
flxAddress.add(lblAddress, txtAddressAlias, flxUnderlineAddressBene);
var flxBank = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": false,
"height": "70dp",
"id": "flxBank",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": 0,
"width": "90%",
"zIndex": 1
}, {}, {});
flxBank.setDefaultUnit(kony.flex.DP);
var lblBank = new kony.ui.Label({
"id": "lblBank",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": "Bank",
"textStyle": {},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtBankAlias = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_ALL,
"bottom": "4.00%",
"centerX": "49.98%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtBankAlias",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 35,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineBank = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBank",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBank.setDefaultUnit(kony.flex.DP);
flxUnderlineBank.add();
flxBank.add(lblBank, txtBankAlias, flxUnderlineBank);
var flxBankName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBankName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBankName.setDefaultUnit(kony.flex.DP);
var lblBankNameStat = new kony.ui.Label({
"id": "lblBankNameStat",
"isVisible": false,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknlblanimated75",
"text": kony.i18n.getLocalizedString("i18n.Bene.Bankname"),
"textStyle": {},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "14%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var tbxBankName = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "tbxBankName",
"isVisible": true,
"skin": "sknlblWhitecarioRegular135",
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxUnderlineBankName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBankName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBankName.setDefaultUnit(kony.flex.DP);
flxUnderlineBankName.add();
var flxBankInside = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "5%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxBankInside",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_a184579df15240ddbeaf7066f713277c,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxBankInside.setDefaultUnit(kony.flex.DP);
var lblBankName = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblBankName",
"isVisible": true,
"right": "3%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Bankname"),
"textStyle": {},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowBank = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowBank",
"isVisible": true,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.reverseback"),
"textStyle": {},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBankInside.add(lblBankName, lblArrowBank);
var lblSwiftCode = new kony.ui.Label({
"id": "lblSwiftCode",
"isVisible": false,
"right": "0dp",
"skin": "slLabel",
"top": "6dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBankName.add(lblBankNameStat, tbxBankName, flxUnderlineBankName, flxBankInside, lblSwiftCode);
flxDetailsIBAN.add(lblTitle, flxBeneNameIBAN, flxAmountIBAN, flxIbanDetails, lblHintIBAN, flxIbanBeneName, flxAddress, flxBank, flxBankName);
var flxDetailsMobileScroll = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxDetailsMobileScroll",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDetailsMobileScroll.setDefaultUnit(kony.flex.DP);
var flxAliassNameMob = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": false,
"height": "80dp",
"id": "flxAliassNameMob",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAliassNameMob.setDefaultUnit(kony.flex.DP);
var lblAliasNameMob = new kony.ui.Label({
"id": "lblAliasNameMob",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.mobiletype"),
"textStyle": {},
"top": "35%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtAliasNameMob = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_ALL,
"bottom": "4.00%",
"centerX": "49.98%",
"focusSkin": "sknTxtBox",
"height": "45%",
"id": "txtAliasNameMob",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 15,
"onDone": AS_TextField_ibfd8dea3864460e97c89938b65b5871,
"onTouchEnd": AS_TextField_ef2396f2621d47888126f1422be57b3b,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "30%",
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_d2f30648a977481bb558e0b593b89a2b,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineAliasNameMob = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAliasNameMob",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "75%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAliasNameMob.setDefaultUnit(kony.flex.DP);
flxUnderlineAliasNameMob.add();
var lblMobileHintEPS = new kony.ui.Label({
"id": "lblMobileHintEPS",
"isVisible": true,
"right": "0%",
"skin": "latoRegular24px",
"text": kony.i18n.getLocalizedString("i18n.jomopay.mobilenumberhint"),
"top": "77%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAliassNameMob.add(lblAliasNameMob, txtAliasNameMob, flxUnderlineAliasNameMob, lblMobileHintEPS);
var flxBeneNameMob = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBeneNameMob",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "10dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBeneNameMob.setDefaultUnit(kony.flex.DP);
var lblBeneNameMobTitle = new kony.ui.Label({
"id": "lblBeneNameMobTitle",
"isVisible": true,
"right": 0,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.transfer.BeneficiaryName"),
"textStyle": {},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBeneNameMob = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "50%",
"id": "lblBeneNameMob",
"isVisible": true,
"onTouchEnd": AS_Label_g57a3faf961241e984be682b29b54a8b,
"skin": "slLabelTitle",
"textStyle": {},
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxUnderlineBeneNameMob = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBeneNameMob",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95.30%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBeneNameMob.setDefaultUnit(kony.flex.DP);
flxUnderlineBeneNameMob.add();
flxBeneNameMob.add(lblBeneNameMobTitle, lblBeneNameMob, flxUnderlineBeneNameMob);
var flxAmountMob = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAmountMob",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAmountMob.setDefaultUnit(kony.flex.DP);
var fllxUnderlineAmountMob = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "fllxUnderlineAmountMob",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
fllxUnderlineAmountMob.setDefaultUnit(kony.flex.DP);
fllxUnderlineAmountMob.add();
var lblAmountAliasMob = new kony.ui.Label({
"id": "lblAmountAliasMob",
"isVisible": true,
"right": 0,
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.cards.amount"),
"textStyle": {},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "15%",
"width": "20%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCurrCodeMob = new kony.ui.Label({
"id": "lblCurrCodeMob",
"isVisible": true,
"right": "88%",
"skin": "lblAccountStaticText",
"text": "JOD",
"textStyle": {},
"top": "42%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtAmountMob = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtAmountMob",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 35,
"onDone": AS_TextField_c3c8364f0d43400584afd1d57525b1a2,
"placeholder": "0.000",
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxAmountMob.add(fllxUnderlineAmountMob, lblAmountAliasMob, lblCurrCodeMob, txtAmountMob);
var flxIbanDetailsMob = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxIbanDetailsMob",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxIbanDetailsMob.setDefaultUnit(kony.flex.DP);
var lblIBANMob = new kony.ui.Label({
"id": "lblIBANMob",
"isVisible": true,
"right": 0,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18.CLIQ.IBAN"),
"textStyle": {},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblIBANMobtxt = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "lblIBANMobtxt",
"isVisible": true,
"skin": "slLabelTitle",
"textStyle": {},
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxUnderlineIBANMob = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineIBANMob",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineIBANMob.setDefaultUnit(kony.flex.DP);
flxUnderlineIBANMob.add();
flxIbanDetailsMob.add(lblIBANMob, lblIBANMobtxt, flxUnderlineIBANMob);
var flxAddressMob = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAddressMob",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAddressMob.setDefaultUnit(kony.flex.DP);
var lblAddressMob = new kony.ui.Label({
"id": "lblAddressMob",
"isVisible": true,
"right": 0,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.address"),
"textStyle": {},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAddressMobtxt = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "lblAddressMobtxt",
"isVisible": true,
"skin": "slLabelTitle",
"textStyle": {},
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxUnderlineAddressMob = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAddressMob",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAddressMob.setDefaultUnit(kony.flex.DP);
flxUnderlineAddressMob.add();
flxAddressMob.add(lblAddressMob, lblAddressMobtxt, flxUnderlineAddressMob);
var flxBankMob = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBankMob",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBankMob.setDefaultUnit(kony.flex.DP);
var lblBankMob = new kony.ui.Label({
"id": "lblBankMob",
"isVisible": true,
"right": 0,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Bankname"),
"textStyle": {},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBankMobAliastxt = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "lblBankMobAliastxt",
"isVisible": true,
"skin": "slLabelTitle",
"textStyle": {},
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxUnderlineBankMob = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBankMob",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBankMob.setDefaultUnit(kony.flex.DP);
flxUnderlineBankMob.add();
flxBankMob.add(lblBankMob, lblBankMobAliastxt, flxUnderlineBankMob);
flxDetailsMobileScroll.add(flxAliassNameMob, flxBeneNameMob, flxAmountMob, flxIbanDetailsMob, flxAddressMob, flxBankMob);
var flxDetailsAliasScroll = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxDetailsAliasScroll",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDetailsAliasScroll.setDefaultUnit(kony.flex.DP);
var lblTitleAlias = new kony.ui.Label({
"centerX": "50%",
"id": "lblTitleAlias",
"isVisible": false,
"right": "0dp",
"skin": "slLabelTitle",
"text": kony.i18n.getLocalizedString("i18n.jomopay.aliastype"),
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxAliassName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": false,
"height": "70dp",
"id": "flxAliassName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAliassName.setDefaultUnit(kony.flex.DP);
var lblAliasName = new kony.ui.Label({
"id": "lblAliasName",
"isVisible": true,
"right": 0,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.benificiaryalias"),
"textStyle": {},
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtAliasName = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_ALL,
"bottom": "4.00%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtAliasName",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 15,
"onDone": AS_TextField_bbda995544a54b028734549a89352363,
"onTouchEnd": AS_TextField_a3e6c9fe63bc41fe8449f9e446116974,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_ac714b94f11d449084c84019bf0e886b,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineAliasName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAliasName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAliasName.setDefaultUnit(kony.flex.DP);
flxUnderlineAliasName.add();
flxAliassName.add(lblAliasName, txtAliasName, flxUnderlineAliasName);
var flxBeneName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxBeneName",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBeneName.setDefaultUnit(kony.flex.DP);
var lblBeneNameTitle = new kony.ui.Label({
"id": "lblBeneNameTitle",
"isVisible": true,
"right": 0,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.benificiaryalias"),
"textStyle": {},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBeneName = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "lblBeneName",
"isVisible": true,
"onTouchEnd": AS_Label_g57a3faf961241e984be682b29b54a8b,
"skin": "slLabelTitle",
"textStyle": {},
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxUnderlineBeneName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineBeneName",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95.30%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineBeneName.setDefaultUnit(kony.flex.DP);
flxUnderlineBeneName.add();
flxBeneName.add(lblBeneNameTitle, lblBeneName, flxUnderlineBeneName);
var flxAmountAlias = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50.00%",
"clipBounds": true,
"height": "70dp",
"id": "flxAmountAlias",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAmountAlias.setDefaultUnit(kony.flex.DP);
var flxUnderlineAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAmount.setDefaultUnit(kony.flex.DP);
flxUnderlineAmount.add();
var lblAmountAlias = new kony.ui.Label({
"id": "lblAmountAlias",
"isVisible": true,
"right": 0,
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"textStyle": {},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "15%",
"width": "25%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCurrCode = new kony.ui.Label({
"id": "lblCurrCode",
"isVisible": true,
"right": "88%",
"skin": "lblAccountStaticText",
"text": "JOD",
"textStyle": {},
"top": "42%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtAmountAlias = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "50%",
"id": "txtAmountAlias",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
"maxTextLength": 35,
"onDone": AS_TextField_f7622948d8544ff9b1c52f4693e5c646,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxAmountAlias.add(flxUnderlineAmount, lblAmountAlias, lblCurrCode, txtAmountAlias);
var flxIBANAliass = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxIBANAliass",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxIBANAliass.setDefaultUnit(kony.flex.DP);
var lbIBANAlias = new kony.ui.Label({
"id": "lbIBANAlias",
"isVisible": true,
"right": 0,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18.CLIQ.IBAN"),
"textStyle": {},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblIBANAliastxt = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "lblIBANAliastxt",
"isVisible": true,
"onTouchEnd": AS_Label_d4704500168b422b8b5e57e24fda0c19,
"skin": "slLabelTitle",
"textStyle": {},
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxUnderlineIBANAlias = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineIBANAlias",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineIBANAlias.setDefaultUnit(kony.flex.DP);
flxUnderlineIBANAlias.add();
flxIBANAliass.add(lbIBANAlias, lblIBANAliastxt, flxUnderlineIBANAlias);
var flxAliasAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAliasAddress",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": 0,
"width": "90%",
"zIndex": 1
}, {}, {});
flxAliasAddress.setDefaultUnit(kony.flex.DP);
var lblAliasAdress = new kony.ui.Label({
"id": "lblAliasAdress",
"isVisible": true,
"right": 0,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.address"),
"textStyle": {},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lbAliasAddresstxt = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "lbAliasAddresstxt",
"isVisible": true,
"onTouchEnd": AS_Label_d8d0648bccdf4292afc7eef51b6b2ea7,
"skin": "slLabelTitle",
"textStyle": {},
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxUnderlineAliasAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAliasAddress",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAliasAddress.setDefaultUnit(kony.flex.DP);
flxUnderlineAliasAddress.add();
flxAliasAddress.add(lblAliasAdress, lbAliasAddresstxt, flxUnderlineAliasAddress);
var flxAliasBank = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAliasBank",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAliasBank.setDefaultUnit(kony.flex.DP);
var lblAliasBank = new kony.ui.Label({
"id": "lblAliasBank",
"isVisible": true,
"right": 0,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Bankname"),
"textStyle": {},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAliasBanktxt = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "lblAliasBanktxt",
"isVisible": true,
"onTouchEnd": AS_Label_ba71786db87349c2a33061b5dfd2946b,
"skin": "slLabelTitle",
"textStyle": {},
"width": "100%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxUnderlineAliasBank = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAliasBank",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAliasBank.setDefaultUnit(kony.flex.DP);
flxUnderlineAliasBank.add();
flxAliasBank.add(lblAliasBank, lblAliasBanktxt, flxUnderlineAliasBank);
flxDetailsAliasScroll.add(lblTitleAlias, flxAliassName, flxBeneName, flxAmountAlias, flxIBANAliass, flxAliasAddress, flxAliasBank);
FlexScrollContainer0f98b54f0d87740.add(flxAccounFrom, lblSplit1, flxAccountTo, lblSplit3, flxDetailsAlias, flxDetailsIBAN, flxDetailsMobileScroll, flxDetailsAliasScroll);
flxMain.add(flxHeaderIPS, FlexScrollContainer0f98b54f0d87740);
var flxConfirmEPS = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxConfirmEPS",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 10
}, {}, {});
flxConfirmEPS.setDefaultUnit(kony.flex.DP);
var flxConfirmHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxConfirmHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFlxHeaderImg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmHeader.setDefaultUnit(kony.flex.DP);
var flxHeaderBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxHeaderBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_cae25eee82c344a3ac5e2c4cb4627302,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxHeaderBack.setDefaultUnit(kony.flex.DP);
var lblHeaderBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblHeaderBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblHeaderBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblHeaderBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxHeaderBack.add(lblHeaderBackIcon, lblHeaderBack);
var lblHeaderTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblHeaderTitle",
"isVisible": true,
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
"textStyle": {},
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnClose = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknDeleteFocus",
"height": "80%",
"id": "btnClose",
"isVisible": true,
"left": "85%",
"onClick": AS_Button_a78d3c26030e45c4b3c8a3ca02d1eee7,
"skin": "sknBtnBack",
"text": "O",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxConfirmHeader.add(flxHeaderBack, lblHeaderTitle, btnClose);
var flxImpDetail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "23%",
"id": "flxImpDetail",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxImpDetail.setDefaultUnit(kony.flex.DP);
var CopyflxIcon0bd2e3679c4ec42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "50dp",
"id": "CopyflxIcon0bd2e3679c4ec42",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxToIcon",
"top": "7%",
"width": "50dp",
"zIndex": 1
}, {}, {});
CopyflxIcon0bd2e3679c4ec42.setDefaultUnit(kony.flex.DP);
var lblInitial = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "80%",
"id": "lblInitial",
"isVisible": true,
"skin": "sknLblFromIcon",
"text": "C N",
"textStyle": {},
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
CopyflxIcon0bd2e3679c4ec42.add(lblInitial);
var lblCustomerNameConfirmation = new kony.ui.Label({
"centerX": "50%",
"id": "lblCustomerNameConfirmation",
"isVisible": true,
"skin": "sknBeneTitle",
"text": "Customer Name",
"textStyle": {},
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBeneAccNum = new kony.ui.Label({
"centerX": "50%",
"id": "lblBeneAccNum",
"isVisible": true,
"right": "0dp",
"skin": "sknLblNextDisabled",
"text": "Account number 7705152",
"textStyle": {},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxImpDetail.add(CopyflxIcon0bd2e3679c4ec42, lblCustomerNameConfirmation, lblBeneAccNum);
var flxOtherDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxOtherDetails",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxOtherDetails.setDefaultUnit(kony.flex.DP);
var flxConfirmEmail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxConfirmEmail",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmEmail.setDefaultUnit(kony.flex.DP);
var lblConfirmEmailTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmEmailTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Beneficiaryemail"),
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmEmail = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmEmail",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "someone@something.somecom",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmEmail.add(lblConfirmEmailTitle, lblConfirmEmail);
var flxConfirmCountry = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxConfirmCountry",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmCountry.setDefaultUnit(kony.flex.DP);
var lblConfirmCountryTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmCountryTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.NUO.Country"),
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmCountry = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmCountry",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Sweden",
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmCountry.add(lblConfirmCountryTitle, lblConfirmCountry);
flxOtherDetails.add(flxConfirmEmail, flxConfirmCountry);
var flxOtherDetailScroll = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "75%",
"id": "flxOtherDetailScroll",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxOtherDetailScroll.setDefaultUnit(kony.flex.DP);
var flxFromAccountsConf = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "flxFromAccountsConf",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFromAccountsConf.setDefaultUnit(kony.flex.DP);
var lblFromAccConfTitle = new kony.ui.Label({
"height": "50%",
"id": "lblFromAccConfTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.termDeposit.fundDeductionAccountNumberTitle"),
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblFromAccConfText = new kony.ui.Label({
"height": "50%",
"id": "lblFromAccConfText",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxFromAccountsConf.add(lblFromAccConfTitle, lblFromAccConfText);
var ToAccConfirmation = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60dp",
"id": "ToAccConfirmation",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
ToAccConfirmation.setDefaultUnit(kony.flex.DP);
var ToAccConfirmationTitle = new kony.ui.Label({
"height": "50%",
"id": "ToAccConfirmationTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.ToC"),
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var ToAccConfirmationText = new kony.ui.Label({
"height": "50%",
"id": "ToAccConfirmationText",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
ToAccConfirmation.add(ToAccConfirmationTitle, ToAccConfirmationText);
var flxConfirmBankName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60dp",
"id": "flxConfirmBankName",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmBankName.setDefaultUnit(kony.flex.DP);
var lblConfirmBankNameTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmBankNameTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Bankname"),
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmBankName = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmBankName",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmBankName.add(lblConfirmBankNameTitle, lblConfirmBankName);
var flxConfirmAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60dp",
"id": "flxConfirmAddress",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmAddress.setDefaultUnit(kony.flex.DP);
var lblConfirmAddTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmAddTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.address"),
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmAddr = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmAddr",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmAddress.add(lblConfirmAddTitle, lblConfirmAddr);
var flxConfirmIBANBeneName = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60dp",
"id": "flxConfirmIBANBeneName",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmIBANBeneName.setDefaultUnit(kony.flex.DP);
var lblConfirmIBANBeneNameTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmIBANBeneNameTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.transfer.BeneficiaryName"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmIBANBeneName = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmIBANBeneName",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmIBANBeneName.add(lblConfirmIBANBeneNameTitle, lblConfirmIBANBeneName);
var flxIBAN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60dp",
"id": "flxIBAN",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIBAN.setDefaultUnit(kony.flex.DP);
var lblIBANConfTitle = new kony.ui.Label({
"height": "50%",
"id": "lblIBANConfTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "IBAN",
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblIBANConftext = new kony.ui.Label({
"height": "50%",
"id": "lblIBANConftext",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxIBAN.add(lblIBANConfTitle, lblIBANConftext);
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60dp",
"id": "flxAmount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var lblAmountConfirm = new kony.ui.Label({
"height": "50%",
"id": "lblAmountConfirm",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.cards.amount"),
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAmountConfirmValue = new kony.ui.Label({
"height": "50%",
"id": "lblAmountConfirmValue",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAmount.add(lblAmountConfirm, lblAmountConfirmValue);
var flxConfirmFees = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60dp",
"id": "flxConfirmFees",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmFees.setDefaultUnit(kony.flex.DP);
var lblConfirmFeesTitle = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmFeesTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.bills.FeeAmount"),
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmFees = new kony.ui.Label({
"height": "50%",
"id": "lblConfirmFees",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "0.000",
"textStyle": {},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmFees.add(lblConfirmFeesTitle, lblConfirmFees);
flxOtherDetailScroll.add(flxFromAccountsConf, ToAccConfirmation, flxConfirmBankName, flxConfirmAddress, flxConfirmIBANBeneName, flxIBAN, flxAmount, flxConfirmFees);
var flxButtonHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxButtonHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxButtonHolder.setDefaultUnit(kony.flex.DP);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "50dp",
"id": "btnConfirm",
"isVisible": true,
"onClick": AS_Button_bbb2dca332654386bea221c9e48bc752,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxButtonHolder.add(btnConfirm);
var lblCurrency = new kony.ui.Label({
"id": "lblCurrency",
"isVisible": true,
"right": "-120dp",
"skin": "slLabel",
"text": "USD",
"textStyle": {},
"top": "-680dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBranchNumber = new kony.ui.Label({
"id": "lblBranchNumber",
"isVisible": true,
"right": "-120dp",
"skin": "slLabel",
"text": "1232435",
"textStyle": {},
"top": "-680dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblFavourite = new kony.ui.Label({
"id": "lblFavourite",
"isVisible": true,
"right": "-130dp",
"skin": "slLabel",
"text": "true",
"textStyle": {},
"top": "-690dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblLanguage = new kony.ui.Label({
"id": "lblLanguage",
"isVisible": true,
"right": "-140dp",
"skin": "slLabel",
"text": "EN",
"textStyle": {},
"top": "-700dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmEPS.add(flxConfirmHeader, flxImpDetail, flxOtherDetails, flxOtherDetailScroll, flxButtonHolder, lblCurrency, lblBranchNumber, lblFavourite, lblLanguage);
frmEPS.add(flxMain, flxConfirmEPS);
};
function frmEPSGlobalsAr() {
frmEPSAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmEPSAr,
"enabledForIdleTimeout": false,
"id": "frmEPS",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": true,
"skin": "slFormCommon"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NEXTPREV,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
