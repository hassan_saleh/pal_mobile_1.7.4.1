//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetmpAuthHeaderAr() {
flxMainAr = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntitleBarGradient",
"top": "90%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMainAr.setDefaultUnit(kony.flex.DP);
var flxHead3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100%",
"id": "flxHead3",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHead3.setDefaultUnit(kony.flex.DP);
var imgAuthMode1 = new kony.ui.Image2({
"centerY": "50%",
"height": "40dp",
"id": "imgAuthMode1",
"isVisible": true,
"right": "7%",
"skin": "slImage",
"src": "touchiconactive.png",
"width": "35dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxLine1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "3.50%",
"id": "flxLine1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "-2dp",
"skin": "sknFlxLine",
"top": "0dp",
"width": "30%",
"zIndex": 1
}, {}, {});
flxLine1.setDefaultUnit(kony.flex.DP);
flxLine1.add();
var imgAuthMode2 = new kony.ui.Image2({
"centerY": "50%",
"height": "40dp",
"id": "imgAuthMode2",
"isVisible": true,
"right": "-2dp",
"skin": "slImage",
"src": "pin.png",
"width": "35dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxLine2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "3.50%",
"id": "flxLine2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "-3dp",
"skin": "sknFlxLine",
"top": "0dp",
"width": "30%",
"zIndex": 1
}, {}, {});
flxLine2.setDefaultUnit(kony.flex.DP);
flxLine2.add();
var imgAuthMode3 = new kony.ui.Image2({
"centerY": "50%",
"height": "35dp",
"id": "imgAuthMode3",
"isVisible": true,
"right": "-6dp",
"skin": "slImage",
"src": "face.png",
"width": "40dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxHead3.add( imgAuthMode3, flxLine2, imgAuthMode2, flxLine1,imgAuthMode1);
var flxHead2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "flxHead2",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"width": "80%",
"zIndex": 1
}, {}, {});
flxHead2.setDefaultUnit(kony.flex.DP);
var imgAuthMode12 = new kony.ui.Image2({
"centerY": "50%",
"height": "40dp",
"id": "imgAuthMode12",
"isVisible": true,
"right": "15%",
"skin": "slImage",
"src": "touchicontick.png",
"width": "35dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyflxLine0ga6685876f5d42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "3.50%",
"id": "CopyflxLine0ga6685876f5d42",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "-2dp",
"skin": "sknFlxLine",
"top": "0dp",
"width": "54%",
"zIndex": 1
}, {}, {});
CopyflxLine0ga6685876f5d42.setDefaultUnit(kony.flex.DP);
CopyflxLine0ga6685876f5d42.add();
var imgAuthMode22 = new kony.ui.Image2({
"centerY": "50%",
"height": "40dp",
"id": "imgAuthMode22",
"isVisible": true,
"right": "-12dp",
"skin": "slImage",
"src": "facetickwhite.png",
"width": "40dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxHead2.add( imgAuthMode22, CopyflxLine0ga6685876f5d42,imgAuthMode12);
var flxHead1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxHead1",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"width": "80%",
"zIndex": 1
}, {}, {});
flxHead1.setDefaultUnit(kony.flex.DP);
var imgAuthMode111 = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"height": "40dp",
"id": "imgAuthMode111",
"isVisible": true,
"right": 7,
"skin": "slImage",
"src": "touchicontick.png",
"width": "35dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyflxLine0i11e7f54e79145 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "3.50%",
"id": "CopyflxLine0i11e7f54e79145",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "-2dp",
"skin": "sknFlxLine",
"top": "0dp",
"width": "30%",
"zIndex": 1
}, {}, {});
CopyflxLine0i11e7f54e79145.setDefaultUnit(kony.flex.DP);
CopyflxLine0i11e7f54e79145.add();
var CopyflxLine0a3103bee98754e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "3.50%",
"id": "CopyflxLine0a3103bee98754e",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "-3dp",
"skin": "sknFlxLine",
"top": "0dp",
"width": "30%",
"zIndex": 1
}, {}, {});
CopyflxLine0a3103bee98754e.setDefaultUnit(kony.flex.DP);
CopyflxLine0a3103bee98754e.add();
flxHead1.add( CopyflxLine0a3103bee98754e, CopyflxLine0i11e7f54e79145,imgAuthMode111);
flxMainAr.add(flxHead3, flxHead2, flxHead1);
}
