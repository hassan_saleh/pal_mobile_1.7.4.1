//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:57 EEST 2020
function addWidgetsfrmContactUsKAAr() {
frmContactUsKA.setDefaultUnit(kony.flex.DP);
var iosTitleBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "iosTitleBar",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntitleBarGradient",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
iosTitleBar.setDefaultUnit(kony.flex.DP);
var transferPayTitleLabel = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "transferPayTitleLabel",
"isVisible": true,
"skin": "sknnavBarTitle",
"text": kony.i18n.getLocalizedString("i18n.common.contactUs"),
"width": "70%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var backButton = new kony.ui.Button({
"focusSkin": "sknleftBackButtonFocus",
"height": "50dp",
"id": "backButton",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_090d523b438b4da2ae190d0b439736d4,
"skin": "sknleftBackButtonNormal",
"top": "0dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
iosTitleBar.add(transferPayTitleLabel, backButton);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "80%",
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknFlxBGBlue",
"top": "20%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var contactListDivider = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1%",
"id": "contactListDivider",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
contactListDivider.setDefaultUnit(kony.flex.DP);
contactListDivider.add();
var contactSegmentList = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50.00%",
"data": [{
"imgicontick": "",
"lblNameKA": ""
}],
"groupCells": false,
"id": "contactSegmentList",
"isVisible": false,
"needPageIndicator": true,
"onRowClick": AS_Segment_aae2b1aa35dc464082b4c2955a66212f,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowTemplate": CopyFlexContainer0b2b1c26ffbf74f,
"scrollingEvents": {},
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "64646400",
"separatorRequired": false,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer0b2b1c26ffbf74f": "CopyFlexContainer0b2b1c26ffbf74f",
"contactListDivider": "contactListDivider",
"imgicontick": "imgicontick",
"lblNameKA": "lblNameKA"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": false
});
var flxCallUS = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "15%",
"id": "flxCallUS",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxBgBlueGradientRound8",
"top": "8%",
"width": "88%",
"zIndex": 1
}, {}, {});
flxCallUS.setDefaultUnit(kony.flex.DP);
var lblCallUS = new kony.ui.Label({
"height": "40%",
"id": "lblCallUS",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128CR",
"text": kony.i18n.getLocalizedString("i18n.common.callus24"),
"top": "1%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblMobile = new kony.ui.Label({
"id": "lblMobile",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128C",
"top": "50%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblMobileIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblMobileIcon",
"isVisible": true,
"left": "1%",
"skin": "sknBOJttf170Yellow",
"text": "X",
"width": "25%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCallUS.add(lblCallUS, lblMobile, lblMobileIcon);
var flxSendUS = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "15%",
"id": "flxSendUS",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxBgBlueGradientRound8",
"top": "5%",
"width": "88%",
"zIndex": 1
}, {}, {});
flxSendUS.setDefaultUnit(kony.flex.DP);
var lblSendus = new kony.ui.Label({
"height": "40%",
"id": "lblSendus",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128CR",
"text": kony.i18n.getLocalizedString("i18n.common.sendusanemail"),
"top": "2%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblEmail = new kony.ui.Label({
"id": "lblEmail",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128C",
"top": "50%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblEmailIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblEmailIcon",
"isVisible": true,
"left": "1%",
"skin": "sknBOJttf170Yellow",
"text": "Y",
"width": "25%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxSendUS.add(lblSendus, lblEmail, lblEmailIcon);
var flxVisitBranch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "15%",
"id": "flxVisitBranch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxBgBlueGradientRound8",
"top": "5%",
"width": "88%",
"zIndex": 1
}, {}, {});
flxVisitBranch.setDefaultUnit(kony.flex.DP);
var lblVisitBranch = new kony.ui.Label({
"centerY": "50%",
"height": "40%",
"id": "lblVisitBranch",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128CR",
"text": kony.i18n.getLocalizedString("i18n.common.visitourbranch"),
"top": "1%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBranchIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblBranchIcon",
"isVisible": true,
"left": "1%",
"skin": "sknBOJttf170Yellow",
"text": "B",
"width": "25%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxVisitBranch.add(lblVisitBranch, lblBranchIcon);
var flxHomepage = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "15%",
"id": "flxHomepage",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxBgBlueGradientRound8",
"top": "5%",
"width": "88%",
"zIndex": 1
}, {}, {});
flxHomepage.setDefaultUnit(kony.flex.DP);
var lblHomePage = new kony.ui.Label({
"centerY": "50.00%",
"height": "40%",
"id": "lblHomePage",
"isVisible": true,
"right": "4.96%",
"skin": "sknLblWhite128CR",
"text": kony.i18n.getLocalizedString("i18n.common.gotohomepage"),
"top": "2%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblHomepageIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblHomepageIcon",
"isVisible": true,
"left": "1%",
"skin": "sknBOJttf170Yellow",
"text": "Z",
"width": "25%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxHomepage.add(lblHomePage, lblHomepageIcon);
var lblFollowUs = new kony.ui.Label({
"centerX": "50%",
"height": "33dp",
"id": "lblFollowUs",
"isVisible": false,
"right": "0dp",
"skin": "sknLblWhite128CR",
"text": "Follow Us",
"top": "48dp",
"width": "73dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxFollowUs = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "39dp",
"clipBounds": true,
"height": "39dp",
"id": "flxFollowUs",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFollowUs.setDefaultUnit(kony.flex.DP);
var lblFb = new kony.ui.Label({
"height": "45dp",
"id": "lblFb",
"isVisible": true,
"right": "90dp",
"skin": "sknLblBoj205White",
"text": "e",
"top": "0dp",
"width": "45dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblTwitter = new kony.ui.Label({
"height": "45dp",
"id": "lblTwitter",
"isVisible": true,
"right": "30dp",
"skin": "sknLblBoj205White",
"text": "f",
"top": "0dp",
"width": "45dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblLinkedIn = new kony.ui.Label({
"height": "45dp",
"id": "lblLinkedIn",
"isVisible": true,
"right": "30dp",
"skin": "sknLblBoj205White",
"text": "g",
"top": "0dp",
"width": "45dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxFollowUs.add( lblLinkedIn, lblTwitter,lblFb);
mainContent.add(contactListDivider, contactSegmentList, flxCallUS, flxSendUS, flxVisitBranch, flxHomepage, lblFollowUs, flxFollowUs);
frmContactUsKA.add(iosTitleBar, mainContent);
};
function frmContactUsKAGlobalsAr() {
frmContactUsKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmContactUsKAAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmContactUsKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_d86821a21bcb4d96b49bd44254326b7c,
"skin": "sknmainGradient",
"statusBarHidden": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": true,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": true,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
"titleBar": false
});
};
