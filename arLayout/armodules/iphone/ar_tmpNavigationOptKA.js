//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetmpNavigationOptKAAr() {
    flxNavigationOptKAAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxNavigationOptKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "skncontainerBkgWhite"
    }, {}, {});
    flxNavigationOptKAAr.setDefaultUnit(kony.flex.DP);
    var lblMenuItem = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblMenuItem",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknNavOptLatoMediumKA",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxLineDividerNavOptKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLineDividerNavOptKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknflxD5D5D5KA",
        "top": "49dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLineDividerNavOptKA.setDefaultUnit(kony.flex.DP);
    flxLineDividerNavOptKA.add();
    flxNavigationOptKAAr.add(lblMenuItem, flxLineDividerNavOptKA);
}
