//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetmpChooseAccountsSettingsAr() {
    flxSegChooseAccountsAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "95dp",
        "id": "flxSegChooseAccounts",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknflxtmpAccountDetailsScreen"
    }, {}, {});
    flxSegChooseAccountsAr.setDefaultUnit(kony.flex.DP);
    var lblAccountName = new kony.ui.Label({
        "id": "lblAccountName",
        "isVisible": true,
        "right": "8%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "top": "35dp",
        "width": "78%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var Symbol = new kony.ui.Label({
        "centerY": "50%",
        "id": "Symbol",
        "isVisible": true,
        "left": "4%",
        "skin": "sknBackIconDisabled",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAccountNumber = new kony.ui.Label({
        "id": "lblAccountNumber",
        "isVisible": true,
        "right": "8%",
        "skin": "lblsegtextsmall0b5a3b38d4be646",
        "top": "17dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDefaultAccPayment = new kony.ui.Label({
        "height": "45%",
        "id": "lblDefaultAccPayment",
        "isVisible": false,
        "left": "5%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Balance",
        "top": "5%",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAmount = new kony.ui.Label({
        "height": "45%",
        "id": "lblAmount",
        "isVisible": false,
        "left": "3%",
        "skin": "Copylblsegtextsmall0a16789105f8b49",
        "text": "2,7453 JOD",
        "top": "5%",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDefaultAccTransfer = new kony.ui.Label({
        "id": "lblDefaultAccTransfer",
        "isVisible": false,
        "right": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Salary Account",
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblACCHideShow = new kony.ui.Label({
        "id": "lblACCHideShow",
        "isVisible": false,
        "right": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Salary Account",
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAccNickname = new kony.ui.Label({
        "id": "lblAccNickname",
        "isVisible": false,
        "right": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Salary Account",
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAccName = new kony.ui.Label({
        "id": "lblAccName",
        "isVisible": false,
        "right": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxSegChooseAccountsAr.add(lblAccountName, Symbol, lblAccountNumber, lblDefaultAccPayment, lblAmount, lblDefaultAccTransfer, lblACCHideShow, lblAccNickname, lblAccName);
}
