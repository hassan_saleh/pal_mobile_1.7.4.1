//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:57 EEST 2020
function addWidgetsfrmCardLinkedAccountsConfirmAr() {
frmCardLinkedAccountsConfirm.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_c06d5d941d5d41d586d6310445a80163,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblBack0h2fe661f120d45 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblBack0h2fe661f120d45",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, CopylblBack0h2fe661f120d45);
var lblApplyCardTitle = new kony.ui.Label({
"centerX": "50%",
"height": "90%",
"id": "lblApplyCardTitle",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxHeader.add(flxBack, lblApplyCardTitle);
var flxBody = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBody",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "10%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBody.setDefaultUnit(kony.flex.DP);
var lblDefaultAccountTitle = new kony.ui.Label({
"id": "lblDefaultAccountTitle",
"isVisible": false,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.linkcard.defaultaccounts"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblDefaultAccount = new kony.ui.Label({
"id": "lblDefaultAccount",
"isVisible": false,
"right": "5%",
"skin": "sknLblWhike125",
"text": "0013010052132001",
"top": "4%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblLinkedAccountsTitle = new kony.ui.Label({
"id": "lblLinkedAccountsTitle",
"isVisible": false,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.debitcardlinkedacc.linkedacc"),
"top": "10%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var segLinkedAccounts = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [
[{
"lblTitle": "Label"
},
[{
"lblAccountName": "test",
"lblAccountNickName": "test",
"lblAccountNumber": "test",
"lblDefaultAccounts": "",
"lblLinkAccounts": "",
"lblTick": "r"
}, {
"lblAccountName": "test",
"lblAccountNickName": "test",
"lblAccountNumber": "test",
"lblDefaultAccounts": "",
"lblLinkAccounts": "",
"lblTick": "r"
}]
],
[{
"lblTitle": "Label"
},
[{
"lblAccountName": "test",
"lblAccountNickName": "test",
"lblAccountNumber": "test",
"lblDefaultAccounts": "",
"lblLinkAccounts": "",
"lblTick": "r"
}, {
"lblAccountName": "test",
"lblAccountNickName": "test",
"lblAccountNumber": "test",
"lblDefaultAccounts": "",
"lblLinkAccounts": "",
"lblTick": "r"
}]
]
],
"groupCells": false,
"height": "85%",
"id": "segLinkedAccounts",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "sknsegAcc",
"rowSkin": "sknsegAcc",
"rowTemplate": flxtemplateLinkedAccounts,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"sectionHeaderTemplate": FlexContainer06d150d0ad52f42,
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": false,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"Copydivider0c263b496ea8345": "Copydivider0c263b496ea8345",
"FlexContainer06d150d0ad52f42": "FlexContainer06d150d0ad52f42",
"flxAccountDetails": "flxAccountDetails",
"flxAccountLinkOptions": "flxAccountLinkOptions",
"flxDefaultAccounts": "flxDefaultAccounts",
"flxDivider": "flxDivider",
"flxILineDisabled": "flxILineDisabled",
"flxIcon1": "flxIcon1",
"flxInnerDisabled": "flxInnerDisabled",
"flxInnerEnabled": "flxInnerEnabled",
"flxLineEnabled": "flxLineEnabled",
"flxLinkAccounts": "flxLinkAccounts",
"flxLinkedAccountsDisabled": "flxLinkedAccountsDisabled",
"flxLinkedAccountsEnable": "flxLinkedAccountsEnable",
"flxtemplateLinkedAccounts": "flxtemplateLinkedAccounts",
"lblAccountName": "lblAccountName",
"lblAccountNickName": "lblAccountNickName",
"lblAccountNumber": "lblAccountNumber",
"lblDefaultAccounts": "lblDefaultAccounts",
"lblLinkAccounts": "lblLinkAccounts",
"lblTick": "lblTick",
"lblTitle": "lblTitle"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": false
});
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"centerY": "90%",
"focusSkin": "slButtonWhiteFocus",
"height": "9%",
"id": "btnConfirm",
"isVisible": true,
"onClick": AS_Button_f7c31b7e26e34a6c86cbdc68fd07f88f,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.transfers.CONFIRM"),
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxBody.add(lblDefaultAccountTitle, lblDefaultAccount, lblLinkedAccountsTitle, segLinkedAccounts, btnConfirm);
frmCardLinkedAccountsConfirm.add(flxHeader, flxBody);
};
function frmCardLinkedAccountsConfirmGlobalsAr() {
frmCardLinkedAccountsConfirmAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmCardLinkedAccountsConfirmAr,
"bounces": false,
"enabledForIdleTimeout": true,
"id": "frmCardLinkedAccountsConfirm",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": true,
"skin": "slFormCommon",
"verticalScrollIndicator": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"bouncesZoom": false,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
"inTransitionConfig": {
"transitionDirection": "none",
"transitionDuration": 0.3,
"transitionEffect": "none"
},
"needsIndicatorDuringPostShow": false,
"outTransitionConfig": {
"transitionDirection": "none",
"transitionDuration": 0.3,
"transitionEffect": "none"
},
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
