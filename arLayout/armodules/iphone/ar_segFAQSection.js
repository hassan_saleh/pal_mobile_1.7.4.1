//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializesegFAQSectionAr() {
    flxFAQSectionAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "5%",
        "id": "flxFAQSection",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "skncontainerBkg"
    }, {}, {});
    flxFAQSectionAr.setDefaultUnit(kony.flex.DP);
    var lblSectionName = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSectionName",
        "isVisible": true,
        "right": "20%",
        "skin": "sknLblWhike150",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgSection = new kony.ui.Image2({
        "centerY": "50%",
        "height": "50%",
        "id": "imgSection",
        "isVisible": true,
        "right": "5%",
        "skin": "slImage",
        "src": "appicon.png",
        "top": "8dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxFAQSectionAr.add(lblSectionName, imgSection);
}
