//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetmpSegRequestStatusAr() {
    flxtmpSegRequestStatusAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxtmpSegRequestStatus",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "slFbox"
    }, {}, {});
    flxtmpSegRequestStatusAr.setDefaultUnit(kony.flex.DP);
    var lblJoMoPay = new kony.ui.Label({
        "id": "lblJoMoPay",
        "isVisible": true,
        "right": "0dp",
        "skin": "lblAmountCurrency",
        "top": "20dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStatus = new kony.ui.Label({
        "id": "lblStatus",
        "isVisible": true,
        "right": "0dp",
        "skin": "lblAccountStaticText",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStatusDesc = new kony.ui.Label({
        "id": "lblStatusDesc",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblWhike125",
        "top": "-5dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRequestDate = new kony.ui.Label({
        "id": "lblRequestDate",
        "isVisible": true,
        "right": "0dp",
        "skin": "lblAccountStaticText",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRequestDateVale = new kony.ui.Label({
        "id": "lblRequestDateVale",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblWhike125",
        "top": "-5dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDelDate = new kony.ui.Label({
        "id": "lblDelDate",
        "isVisible": true,
        "right": "0dp",
        "skin": "lblAccountStaticText",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDelDateValue = new kony.ui.Label({
        "id": "lblDelDateValue",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblWhike125",
        "top": "-5dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknflxLinewhiteOp",
        "top": "12dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    flxtmpSegRequestStatusAr.add(lblJoMoPay, lblStatus, lblStatusDesc, lblRequestDate, lblRequestDateVale, lblDelDate, lblDelDateValue, flxLine);
}
