//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:58 EEST 2020
function addWidgetsfrmIPSManageBeneAr() {
frmIPSManageBene.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_be673e4f00254a4fb3f6e23caf690a85,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblBack0h2fe661f120d45 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblBack0h2fe661f120d45",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, CopylblBack0h2fe661f120d45);
var lblIPSBeneficiaryTitle = new kony.ui.Label({
"height": "90%",
"id": "lblIPSBeneficiaryTitle",
"isVisible": true,
"left": "20%",
"minHeight": "90%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.CLIQ.Title"),
"top": "0%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnGetAllAliases = new kony.ui.Button({
"focusSkin": "CopyslButtonGlosssBlue0hcfda7e5f61c48",
"height": "90%",
"id": "btnGetAllAliases",
"isVisible": true,
"left": "90%",
"onClick": AS_Button_d71e185c3d9c44e8bb8653a3093c21a8,
"skin": "CopyslButtonGlosssBlue0hcfda7e5f61c48",
"text": "Q",
"top": "0dp",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxHeader.add(flxBack, lblIPSBeneficiaryTitle, btnGetAllAliases);
var flxIPSBeneList = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxIPSBeneList",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "9%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIPSBeneList.setDefaultUnit(kony.flex.DP);
var lblStatus = new kony.ui.Label({
"centerX": "50%",
"centerY": "35%",
"id": "lblStatus",
"isVisible": true,
"skin": "sknBeneTitle",
"text": kony.i18n.getLocalizedString("i18n.bene.noBeneError"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnIPSTransfer = new kony.ui.Button({
"centerX": "50%",
"centerY": "90%",
"focusSkin": "slButtonWhiteFocus",
"height": "8%",
"id": "btnIPSTransfer",
"isVisible": true,
"onClick": AS_Button_g0acc7751ef84dcd87f81eb1beb8e342,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.CLIQ.Title"),
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnAddAlias = new kony.ui.Button({
"centerX": "50%",
"centerY": "94%",
"focusSkin": "slButtonWhiteFocus",
"height": "8%",
"id": "btnAddAlias",
"isVisible": false,
"onClick": AS_Button_abc66eba16eb4614bb3280f10bd1cda6,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.CLIQ.addAlias"),
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxIPSBeneList.add(lblStatus, btnIPSTransfer, btnAddAlias);
frmIPSManageBene.add(flxHeader, flxIPSBeneList);
};
function frmIPSManageBeneGlobalsAr() {
frmIPSManageBeneAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmIPSManageBeneAr,
"bounces": false,
"enabledForIdleTimeout": true,
"id": "frmIPSManageBene",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": true,
"skin": "sknBackground",
"verticalScrollIndicator": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
