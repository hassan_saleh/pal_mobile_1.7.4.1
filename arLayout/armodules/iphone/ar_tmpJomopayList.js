//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetmpJomopayListAr() {
    flxJomoPayPastDetailsAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "12%",
        "id": "flxJomoPayPastDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxJomoPayPastDetailsAr.setDefaultUnit(kony.flex.DP);
    var flxProfilePic = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxProfilePic",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxProfilePic.setDefaultUnit(kony.flex.DP);
    var btnPic = new kony.ui.Button({
        "height": "60%",
        "id": "btnPic",
        "isVisible": true,
        "right": "20%",
        "skin": "CopyslButtonGlossBlue0gc9fc8149f6943",
        "top": "20%",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxProfilePic.add(btnPic);
    var flxUserDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxUserDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "slFbox",
        "top": "3%",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    flxUserDetails.setDefaultUnit(kony.flex.DP);
    var lblTransactiondate = new kony.ui.Label({
        "height": "33%",
        "id": "lblTransactiondate",
        "isVisible": true,
        "right": "5%",
        "skin": "sknSIDate",
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblPayeeName = new kony.ui.Label({
        "height": "33%",
        "id": "lblPayeeName",
        "isVisible": true,
        "right": "5%",
        "skin": "CopyslLabel0e2302468ce1241",
        "top": "33%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDescription = new kony.ui.Label({
        "height": "33%",
        "id": "lblDescription",
        "isVisible": true,
        "right": "5%",
        "skin": "CopyslLabel0e2302468ce1241",
        "top": "66%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxUserDetails.add(lblTransactiondate, lblPayeeName, lblDescription);
    var flxTAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxTAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "70%",
        "skin": "slFbox",
        "top": "0%",
        "width": "30%",
        "zIndex": 1
    }, {}, {});
    flxTAmount.setDefaultUnit(kony.flex.DP);
    var lblAmount = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAmount",
        "isVisible": true,
        "right": "10%",
        "skin": "CopyslLabel0a115c078436a40",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblCurrency = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCurrency",
        "isVisible": true,
        "left": "10%",
        "skin": "CopyslLabel0a115c078436a40",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxTAmount.add(lblAmount, lblCurrency);
    var lblSeparator = new kony.ui.Label({
        "height": "1px",
        "id": "lblSeparator",
        "isVisible": true,
        "right": "0%",
        "skin": "sknDividerWhite",
        "text": "Label",
        "top": "98%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxJomoPayPastDetailsAr.add(flxProfilePic, flxUserDetails, flxTAmount, lblSeparator);
}
