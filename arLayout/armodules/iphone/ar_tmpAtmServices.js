//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetmpAtmServicesAr() {
    flxAtmServicesAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxAtmServices",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "CopyslFbox0bd1a0e0f205646"
    }, {}, {});
    flxAtmServicesAr.setDefaultUnit(kony.flex.DP);
    var lblServiceName = new kony.ui.Label({
        "centerY": "50%",
        "height": "60%",
        "id": "lblServiceName",
        "isVisible": true,
        "right": "15dp",
        "skin": "CopyslLabel0h0731bfc921144",
        "top": "29dp",
        "width": "85%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgCheck = new kony.ui.Image2({
        "centerY": "53%",
        "height": "20dp",
        "id": "imgCheck",
        "isVisible": true,
        "left": "15dp",
        "skin": "slImage",
        "src": "checkbox_on.png",
        "top": "14dp",
        "width": "20dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxAtmServicesAr.add(lblServiceName, imgCheck);
}
