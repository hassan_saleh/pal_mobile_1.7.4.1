//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:57 EEST 2020
function addWidgetsfrmEditPayeeSuccessBillPayKAAr() {
    frmEditPayeeSuccessBillPayKA.setDefaultUnit(kony.flex.DP);
    var flxContainerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxContainerKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "60dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContainerKA.setDefaultUnit(kony.flex.DP);
    var lblFormTitleKA = new kony.ui.Label({
        "id": "lblFormTitleKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknonboardingHeader",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.editPayee"),
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxSuccessIconContainerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "30dp",
        "id": "flxSuccessIconContainerKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknsuccessIcon",
        "top": "40dp",
        "width": "30dp",
        "zIndex": 1
    }, {}, {});
    flxSuccessIconContainerKA.setDefaultUnit(kony.flex.DP);
    var imgSuccessIconKA = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "50%",
        "id": "imgSuccessIconKA",
        "isVisible": true,
        "skin": "sknslImage",
        "src": "check.png",
        "width": "50%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxSuccessIconContainerKA.add(imgSuccessIconKA);
    var lblSuccessTitleKA = new kony.ui.Label({
        "id": "lblSuccessTitleKA",
        "isVisible": true,
        "right": "8dp",
        "skin": "sknonboardingHeader",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.payeeUpdatedSuccessfully"),
        "top": "7dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblSuccessNotesKA = new kony.ui.Label({
        "centerX": "50.03%",
        "id": "lblSuccessNotesKA",
        "isVisible": true,
        "right": "20%",
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.manage_payee.updateThePayeeSuccess"),
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnPayBIllKA = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "btnPayBIllKA",
        "isVisible": true,
        "onClick": AS_Button_e6d52d0654034c76bc0545968790fd65,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.common.btnContinuee"),
        "top": "60dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxContainerKA.add(lblFormTitleKA, flxSuccessIconContainerKA, lblSuccessTitleKA, lblSuccessNotesKA, btnPayBIllKA);
    frmEditPayeeSuccessBillPayKA.add(flxContainerKA);
};
function frmEditPayeeSuccessBillPayKAGlobalsAr() {
    frmEditPayeeSuccessBillPayKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmEditPayeeSuccessBillPayKAAr,
        "enabledForIdleTimeout": true,
        "id": "frmEditPayeeSuccessBillPayKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": true,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "titleBar": false
    });
};
