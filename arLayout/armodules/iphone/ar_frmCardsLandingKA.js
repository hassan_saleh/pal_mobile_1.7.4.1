//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:57 EEST 2020
function addWidgetsfrmCardsLandingKAAr() {
frmCardsLandingKA.setDefaultUnit(kony.flex.DP);
var overview = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "92%",
"id": "overview",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "CopyskncardsBGNew0h3f106bb85484c",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
overview.setDefaultUnit(kony.flex.DP);
var flxHeaderDashboard = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeaderDashboard",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeaderDashboard.setDefaultUnit(kony.flex.DP);
var FlexContainer0b9fde1e61d3c44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlexContainer0b9fde1e61d3c44",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "slFbox",
"top": "0dp",
"width": "100dp",
"zIndex": 1
}, {}, {});
FlexContainer0b9fde1e61d3c44.setDefaultUnit(kony.flex.DP);
var btnNotification = new kony.ui.Button({
"focusSkin": "BtnNotificationMail",
"height": "100%",
"id": "btnNotification",
"isVisible": false,
"left": "10dp",
"skin": "BtnNotificationMail",
"text": "Y",
"top": "0dp",
"width": "42dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var btnProfile = new kony.ui.Button({
"focusSkin": "btnUser",
"height": "50dp",
"id": "btnProfile",
"isVisible": true,
"left": "54dp",
"onClick": AS_Button_bba8c120bf874164a97988b5fe6f9a83,
"skin": "btnUser",
"text": "F",
"top": "0dp",
"width": "40dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
FlexContainer0b9fde1e61d3c44.add(btnNotification, btnProfile);
var Image0c5157670502748 = new kony.ui.Image2({
"height": "30dp",
"id": "Image0c5157670502748",
"isVisible": false,
"left": "15dp",
"skin": "slImage",
"src": "logo03.png",
"top": "5dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_b816679eeaf947ea96c7d33caded6d6e,
"skin": "slFbox",
"top": "0%",
"width": "26%",
"zIndex": 10
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.accounts.accounts"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, lblBack);
var btnApplyCards = new kony.ui.Button({
"centerX": "82%",
"focusSkin": "sknTransparentBGRNDFontBOJ",
"height": "50dp",
"id": "btnApplyCards",
"isVisible": true,
"onClick": AS_Button_a24c520c0ea5412dbf6638899cccdcd2,
"skin": "sknTransparentBGRNDFontBOJ",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 5
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxHeaderDashboard.add(FlexContainer0b9fde1e61d3c44, Image0c5157670502748, flxBack, btnApplyCards);
var flxTabAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "6%",
"id": "flxTabAccounts",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "-1%",
"width": "90%",
"zIndex": 100
}, {}, {});
flxTabAccounts.setDefaultUnit(kony.flex.DP);
var btnAllCards = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "btnAccounts",
"height": "100%",
"id": "btnAllCards",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_d8e6be846ae24849bc31be2444d44206,
"skin": "btnAccounts",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
"top": "0dp",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxTabAccounts.add(btnAllCards);
var flxScrollMain = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "84%",
"horizontalScrollIndicator": true,
"id": "flxScrollMain",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "3%",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 100
}, {}, {
"bouncesZoom": false
});
flxScrollMain.setDefaultUnit(kony.flex.DP);
var segCardsLanding = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50%",
"data": [{
"CardHolder": "",
"CopylblIncommingRing0db621a02819242": "",
"CopylblIncommingRing0h6f8267705004b": "",
"ValidThru": "",
"accountNumber": "",
"btnNav": "",
"cardImage": "",
"cardNumber": "",
"cardType": "",
"cardTypeFlag": "",
"lblIncommingRing": "",
"reference_no": ""
}],
"groupCells": false,
"id": "segCardsLanding",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_d6dda1de5c674a6481ff4c6914d16d8e,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "sknsegAcc",
"rowSkin": "sknsegAcc",
"rowTemplate": flxCardsLandingTemplate,
"scrollingEvents": {},
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "ffffff64",
"separatorRequired": true,
"separatorThickness": 0,
"showScrollbars": true,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CardHolder": "CardHolder",
"CopybtnNav0b0169acff9c84a": "CopybtnNav0b0169acff9c84a",
"CopylblIncommingRing0db621a02819242": "CopylblIncommingRing0db621a02819242",
"CopylblIncommingRing0h6f8267705004b": "CopylblIncommingRing0h6f8267705004b",
"ValidThru": "ValidThru",
"btnNav": "btnNav",
"btnNav1": "btnNav1",
"cardImage": "cardImage",
"cardNumber": "cardNumber",
"cardType": "cardType",
"cardTypeFlag": "cardTypeFlag",
"flxCardDetails": "flxCardDetails",
"flxCardsLandingTemplate": "flxCardsLandingTemplate",
"lblIncommingRing": "lblIncommingRing"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": false
});
var LabelNoRecordsKA = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "LabelNoRecordsKA",
"isVisible": false,
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.cards.nocards"),
"width": "65%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxScrollMain.add(segCardsLanding, LabelNoRecordsKA);
var btnApplyNewCard = new kony.ui.Button({
"centerY": "-10%",
"focusSkin": "sknOrangeBGRNDFontBOJ2",
"height": "60dp",
"id": "btnApplyNewCard",
"isVisible": false,
"onClick": AS_Button_he29dc34a4024cb3ab60bf4e7d31a84e,
"left": "5%",
"skin": "sknOrangeBGRNDFontBOJ2",
"text": "C",
"width": "60dp",
"zIndex": 200
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var flxApplyNewCards = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "85%",
"id": "flxApplyNewCards",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "12%",
"width": "100%",
"zIndex": 200
}, {}, {});
flxApplyNewCards.setDefaultUnit(kony.flex.DP);
var flxapplyCredit = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "10%",
"id": "flxapplyCredit",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_Button_he29dc34a4024cb3ab60bf4e7d31a84e,
"skin": "sknBOJWhiteRd",
"top": "6%",
"width": "70%",
"zIndex": 1
}, {}, {});
flxapplyCredit.setDefaultUnit(kony.flex.DP);
var lblCredit = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblCredit",
"isVisible": true,
"skin": "sknCaiRegWhite50Op",
"text": kony.i18n.getLocalizedString("i18n.common.creditCards"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxapplyCredit.add(lblCredit);
var flxapplyDebit = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "10%",
"id": "flxapplyDebit",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_Button_he29dc34a4024cb3ab60bf4e7d31a84e,
"skin": "sknBOJWhiteRd",
"top": "6%",
"width": "70%",
"zIndex": 1
}, {}, {});
flxapplyDebit.setDefaultUnit(kony.flex.DP);
var lblDebit = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblDebit",
"isVisible": true,
"skin": "sknCaiRegWhite50Op",
"text": kony.i18n.getLocalizedString("i18n.common.debitcards"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxapplyDebit.add(lblDebit);
var flxapplyPrepaid = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "10%",
"id": "flxapplyPrepaid",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_Button_he29dc34a4024cb3ab60bf4e7d31a84e,
"skin": "sknBOJWhiteRd",
"top": "6%",
"width": "70%",
"zIndex": 1
}, {}, {});
flxapplyPrepaid.setDefaultUnit(kony.flex.DP);
var lblPrepaidCards = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblPrepaidCards",
"isVisible": true,
"skin": "sknCaiRegWhite50Op",
"text": kony.i18n.getLocalizedString("i18n.common.prepaidcards"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxapplyPrepaid.add(lblPrepaidCards);
flxApplyNewCards.add(flxapplyCredit, flxapplyDebit, flxapplyPrepaid);
overview.add(flxHeaderDashboard, flxTabAccounts, flxScrollMain, btnApplyNewCard, flxApplyNewCards);
var footerBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": false,
"height": "7%",
"id": "footerBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0",
"skin": "skncontainerBkgWhite",
"width": "100%"
}, {}, {});
footerBack.setDefaultUnit(kony.flex.DP);
var footerBackground = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "footerBackground",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0",
"skin": "menu",
"top": "0",
"width": "100%"
}, {}, {});
footerBackground.setDefaultUnit(kony.flex.DP);
var FlxAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxAccounts",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknfocusmenu",
"top": "0dp",
"width": "25%",
"zIndex": 1
}, {}, {});
FlxAccounts.setDefaultUnit(kony.flex.DP);
var img1 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img1",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_accounts_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label03174bff69bb54c = new kony.ui.Label({
"centerX": "50%",
"id": "Label03174bff69bb54c",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.my_money.accounts"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnAccounts = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnAccounts",
"isVisible": true,
"onClick": AS_Button_be372a5cd36f487999bfedef51a75ed1,
"skin": "btnCard",
"text": "H",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
FlxAccounts.add(img1, Label03174bff69bb54c, btnAccounts);
var FlxTranfers = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxTranfers",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_d6a2df9484094ecea6ede407279b0a5d,
"skin": "slFbox",
"top": "0dp",
"width": "25%",
"zIndex": 1
}, {}, {});
FlxTranfers.setDefaultUnit(kony.flex.DP);
var img2 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img2",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_t_and_p_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label02bec01fd5baf4c = new kony.ui.Label({
"centerX": "50%",
"id": "Label02bec01fd5baf4c",
"isVisible": true,
"skin": "sknlblFootertitleFocus",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopybtnAccounts0ia3b1ff37c304b = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnfooterfont2enable",
"height": "50dp",
"id": "CopybtnAccounts0ia3b1ff37c304b",
"isVisible": true,
"skin": "sknlblFooter2Enable",
"text": "G",
"width": "100%",
"zIndex": 10
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
FlxTranfers.add(img2, Label02bec01fd5baf4c, CopybtnAccounts0ia3b1ff37c304b);
var FlxBot = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxBot",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_ca3b13678b6e420295689f6466155539,
"skin": "slFbox",
"top": "0dp",
"width": "25%",
"zIndex": 2
}, {}, {});
FlxBot.setDefaultUnit(kony.flex.DP);
var imgBot = new kony.ui.Image2({
"centerX": "50%",
"height": "40dp",
"id": "imgBot",
"isVisible": false,
"right": "13dp",
"skin": "slImage",
"src": "chaticonactive.png",
"top": "4dp",
"width": "40dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopybtnAccounts0ff48f9feb0aa42 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "CopybtnAccounts0ff48f9feb0aa42",
"isVisible": true,
"onClick": AS_Button_dcd82513b2be409fa7dc542a82d9018e,
"skin": "btnCard",
"text": "i",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var CopyLabel0i584dcaf2e8546 = new kony.ui.Label({
"centerX": "50%",
"id": "CopyLabel0i584dcaf2e8546",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.alert.transfers"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
FlxBot.add(imgBot, CopybtnAccounts0ff48f9feb0aa42, CopyLabel0i584dcaf2e8546);
var FlxDeposits = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxDeposits",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_i288c2e1a4fb4e35b94231bb5c8df667,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxDeposits.setDefaultUnit(kony.flex.DP);
var img3 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img3",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_deposits_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label04221a71494e848 = new kony.ui.Label({
"centerX": "50%",
"id": "Label04221a71494e848",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.deposits"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopybtnAccounts0bc68a97c14fb49 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCard",
"height": "50dp",
"id": "CopybtnAccounts0bc68a97c14fb49",
"isVisible": true,
"skin": "btnCard",
"text": "J",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
FlxDeposits.add(img3, Label04221a71494e848, CopybtnAccounts0bc68a97c14fb49);
var flxEfawatercoom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxEfawatercoom",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "23%",
"zIndex": 2
}, {}, {});
flxEfawatercoom.setDefaultUnit(kony.flex.DP);
var btnEfawatercoom = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnEfawatercoom",
"isVisible": true,
"onClick": AS_Button_ecbe0c3165f64efda7ffa17b740ff070,
"skin": "btnCard",
"text": "x",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var lblEfawatercoom = new kony.ui.Label({
"centerX": "50%",
"id": "lblEfawatercoom",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.footerBill.BillPayment"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxEfawatercoom.add(btnEfawatercoom, lblEfawatercoom);
var FlxMore = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxMore",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_e70fdb18e17b4e03b4526a94d1343562,
"skin": "sknslFbox",
"top": "0dp",
"width": "25%",
"zIndex": 1
}, {}, {});
FlxMore.setDefaultUnit(kony.flex.DP);
var img4 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img4",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_more_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label0e5331028c2ef41 = new kony.ui.Label({
"centerX": "50%",
"id": "Label0e5331028c2ef41",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.common.more"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopybtnAccounts0ec4d23080f0146 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "CopybtnAccounts0ec4d23080f0146",
"isVisible": true,
"onClick": AS_Button_j8c267b87d2d408391bc5583057c0373,
"skin": "btnCard",
"text": "K",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
FlxMore.add(img4, Label0e5331028c2ef41, CopybtnAccounts0ec4d23080f0146);
footerBackground.add( FlxMore, flxEfawatercoom, FlxDeposits, FlxBot, FlxTranfers,FlxAccounts);
footerBack.add(footerBackground);
frmCardsLandingKA.add(overview, footerBack);
};
function frmCardsLandingKAGlobalsAr() {
frmCardsLandingKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmCardsLandingKAAr,
"bounces": false,
"enabledForIdleTimeout": true,
"id": "frmCardsLandingKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_cbaed6899c66493eb38b1a4c21117635,
"skin": "CopyslFormCommon0eb853f78d72b43",
"verticalScrollIndicator": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": true,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
