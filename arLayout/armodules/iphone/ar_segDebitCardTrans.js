//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializesegDebitCardTransAr() {
    CopyFlexContainer0a99898df0f2a41Ar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "focusSkin": "sknflxTransprnt",
        "id": "CopyFlexContainer0a99898df0f2a41",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknflxTransprnt"
    }, {}, {});
    CopyFlexContainer0a99898df0f2a41Ar.setDefaultUnit(kony.flex.DP);
    var transactionDate = new kony.ui.Label({
        "id": "transactionDate",
        "isVisible": true,
        "right": "20dp",
        "skin": "loansDealsTextSkin",
        "top": "6dp",
        "width": "230dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var transactionAmount = new kony.ui.Label({
        "centerY": "27dp",
        "id": "transactionAmount",
        "isVisible": true,
        "left": "20dp",
        "skin": "sknNumber",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var transactionAmountJOD = new kony.ui.Label({
        "centerY": "73dp",
        "id": "transactionAmountJOD",
        "isVisible": true,
        "left": "20dp",
        "skin": "sknNumber",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblLastTransaction = new kony.ui.Label({
        "centerY": "30dp",
        "id": "lblLastTransaction",
        "isVisible": true,
        "right": "20dp",
        "skin": "sknNumber",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var transactionName = new kony.ui.Label({
        "id": "transactionName",
        "isVisible": true,
        "right": "20dp",
        "skin": "sknNumber",
        "top": "28dp",
        "width": "250dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblCardStatementmerchant = new kony.ui.Label({
        "id": "lblCardStatementmerchant",
        "isVisible": true,
        "right": "20dp",
        "skin": "sknNumber",
        "top": "45dp",
        "width": "250dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyFlexContainer0a99898df0f2a41Ar.add(transactionDate, transactionAmount, transactionAmountJOD, lblLastTransaction, transactionName, lblCardStatementmerchant);
}
