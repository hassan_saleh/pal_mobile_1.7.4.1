//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:57 EEST 2020
function addWidgetsfrmConfirmationCardKAAr() {
    frmConfirmationCardKA.setDefaultUnit(kony.flex.DP);
    var touchFeature = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "touchFeature",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    touchFeature.setDefaultUnit(kony.flex.DP);
    var FlexContainer00a3c07fadcbf4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainer00a3c07fadcbf4b",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "60dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer00a3c07fadcbf4b.setDefaultUnit(kony.flex.DP);
    var lblHeadingKA = new kony.ui.Label({
        "id": "lblHeadingKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknonboardingHeader",
        "text": kony.i18n.getLocalizedString("i18n.opening_account.frmConfirmationCardKA.lblHeadingKA"),
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopysuccessIcon0f42e785ae2344c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "30dp",
        "id": "CopysuccessIcon0f42e785ae2344c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknsuccessIcon",
        "top": "40dp",
        "width": "30dp",
        "zIndex": 1
    }, {}, {});
    CopysuccessIcon0f42e785ae2344c.setDefaultUnit(kony.flex.DP);
    var CopyImage031bb4461f4b644 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "50%",
        "id": "CopyImage031bb4461f4b644",
        "isVisible": true,
        "skin": "sknslImage",
        "src": "check.png",
        "width": "50%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    CopysuccessIcon0f42e785ae2344c.add(CopyImage031bb4461f4b644);
    var CopyLabel04e32e5c0a78845 = new kony.ui.Label({
        "id": "CopyLabel04e32e5c0a78845",
        "isVisible": true,
        "skin": "sknonboardingHeader",
        "text": kony.i18n.getLocalizedString("i18n.opening_account.applicationSubmittedSuccessfully"),
        "top": "7dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyLabel0d00f815bcc7c41 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel0d00f815bcc7c41",
        "isVisible": true,
        "right": "20%",
        "skin": "sknonboardingText",
        "text": kony.i18n.getLocalizedString("i18n.opening_account.thankYouForApplyingForOurProduct"),
        "top": "10dp",
        "width": "83%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var refID = new kony.ui.Label({
        "id": "refID",
        "isVisible": true,
        "right": "0",
        "skin": "sknsectionHeaderLabel",
        "text": kony.i18n.getLocalizedString("i18n.opening_account.frmConfirmationCardKA.CopyLabel09dd01806856b4f"),
        "top": "7dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var deleteScheduleTransactionButton = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "60dp",
        "id": "deleteScheduleTransactionButton",
        "isVisible": true,
        "onClick": AS_Button_77eaf9f6381b44a292ea6b97e1131f94,
        "skin": "sknSecondaryActionWhiteBgKA",
        "text": kony.i18n.getLocalizedString("i18n.opening_account.exploreMoreProducts"),
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var CopydeleteScheduleTransactionButton0ff2090b870b341 = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "60dp",
        "id": "CopydeleteScheduleTransactionButton0ff2090b870b341",
        "isVisible": true,
        "onClick": AS_Button_cc66c147868a4cc18abc2d950879f9cb,
        "skin": "sknSecondaryActionWhiteBgKA",
        "text": kony.i18n.getLocalizedString("i18n.opening_account.backToMenu"),
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    FlexContainer00a3c07fadcbf4b.add(lblHeadingKA, CopysuccessIcon0f42e785ae2344c, CopyLabel04e32e5c0a78845, CopyLabel0d00f815bcc7c41, refID, deleteScheduleTransactionButton, CopydeleteScheduleTransactionButton0ff2090b870b341);
    touchFeature.add(FlexContainer00a3c07fadcbf4b);
    frmConfirmationCardKA.add(touchFeature);
};
function frmConfirmationCardKAGlobalsAr() {
    frmConfirmationCardKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmConfirmationCardKAAr,
        "bounces": false,
        "enableScrolling": true,
        "enabledForIdleTimeout": true,
        "id": "frmConfirmationCardKA",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "onHide": AS_Form_0222ff20bea040e1a0e26432be64bdae,
        "pagingEnabled": false,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "transitionFade"
        },
        "retainScrollPosition": false,
        "titleBar": false
    });
};
