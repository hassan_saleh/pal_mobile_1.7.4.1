//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:57 EEST 2020
function addWidgetsfrmEstatementLandingKAAr() {
frmEstatementLandingKA.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b7d74518f07a4d",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var lblFormHeading = new kony.ui.Label({
"centerX": "50%",
"height": "100%",
"id": "lblFormHeading",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.Estmt.Header"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxNext = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "flxNext",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_e94dbed34a784f5999c266bed20524c9,
"right": "2%",
"skin": "slFbox",
"top": "5%",
"width": "18%",
"zIndex": 10
}, {}, {});
flxNext.setDefaultUnit(kony.flex.DP);
var lblNext = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Next Page"
},
"height": "100%",
"id": "lblNext",
"isVisible": true,
"left": "0dp",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"top": "0dp",
"width": "100%",
"zIndex": 5
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxNext.add(lblNext);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_a1bc11f66e524456b0396a9cc1524461,
"skin": "slFbox",
"top": "0%",
"width": "18%",
"zIndex": 5
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"height": "100%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"height": "100%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btn = new kony.ui.Button({
"focusSkin": "slButtonGlossRed",
"height": "0%",
"id": "btn",
"isVisible": true,
"left": "0%",
"skin": "btnBack0b71f859656c647",
"top": "0%",
"width": "0%",
"zIndex": 100
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxBack.add(lblBackIcon, lblBack, btn);
flxHeader.add(lblFormHeading, flxNext, flxBack);
var flxToggleEStmt = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "flxToggleEStmt",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknFlxLightGreyColor",
"top": "13%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxToggleEStmt.setDefaultUnit(kony.flex.DP);
var lblEstmtToggleText = new kony.ui.Label({
"height": "100%",
"id": "lblEstmtToggleText",
"isVisible": true,
"right": "3%",
"skin": "sknlblTouchIdsmall",
"text": kony.i18n.getLocalizedString("i18n.Estmt.settingstext"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxSwitchOffTouchLogin = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxSwitchOffTouchLogin",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_hef9268296374824b49528807f9230c3,
"left": "3%",
"skin": "sknflxGrey",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxSwitchOffTouchLogin.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlue0hfee6fe5ed994b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlue0hfee6fe5ed994b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlue0hfee6fe5ed994b.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlue0hfee6fe5ed994b.add();
var Copyflxlakeer0a0654737f4504c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "Copyflxlakeer0a0654737f4504c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
Copyflxlakeer0a0654737f4504c.setDefaultUnit(kony.flex.DP);
Copyflxlakeer0a0654737f4504c.add();
flxSwitchOffTouchLogin.add(CopyflxRoundDBlue0hfee6fe5ed994b, Copyflxlakeer0a0654737f4504c);
var flxSwitchOnTouchLogin = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxSwitchOnTouchLogin",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "3%",
"skin": "sknflxyellow",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxSwitchOnTouchLogin.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlueOff0j3222bd94cbc49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlueOff0j3222bd94cbc49",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 2
}, {}, {});
CopyflxRoundDBlueOff0j3222bd94cbc49.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlueOff0j3222bd94cbc49.add();
var CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b.setDefaultUnit(kony.flex.DP);
CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b.add();
flxSwitchOnTouchLogin.add(CopyflxRoundDBlueOff0j3222bd94cbc49, CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b);
flxToggleEStmt.add(lblEstmtToggleText, flxSwitchOffTouchLogin, flxSwitchOnTouchLogin);
var FlxEmail1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "FlxEmail1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknslFbox",
"top": "24%",
"width": "90%",
"zIndex": 1
}, {}, {});
FlxEmail1.setDefaultUnit(kony.flex.PERCENTAGE);
var txtEmail1 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtEmail1",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onDone": AS_TextField_d898582466cd4e7192a1901693dc903a,
"onTextChange": AS_TextField_d9467ca6abe6481f9374e5ad79f41d66,
"onTouchEnd": AS_TextField_acd5740226ed4651a9fbd9eeb3b817cf,
"onTouchStart": AS_TextField_cfd0f0b445de4583b335c4cf08dde1a7,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 10
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onBeginEditing": AS_TextField_d095d984e3af434fae4224de0d5e0d43,
"onEndEditing": AS_TextField_jae85ec918da466f87aceae3a67cd095,
"placeholderSkin": "sknPlaceholderKA",
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var borderBottom1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "borderBottom1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "90%",
"width": "95%",
"zIndex": 2
}, {}, {});
borderBottom1.setDefaultUnit(kony.flex.DP);
borderBottom1.add();
var lblEmail1 = new kony.ui.Label({
"id": "lblEmail1",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": kony.i18n.getLocalizedString("i18n.Estmt.Email1Text"),
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblClose1 = new kony.ui.Label({
"height": "100%",
"id": "lblClose1",
"isVisible": false,
"onTouchEnd": AS_Label_ga6b9385ff1340d98a18f45163872d8d,
"left": "2%",
"skin": "sknClose",
"text": "O",
"top": "10%",
"width": "10%",
"zIndex": 20
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
FlxEmail1.add(txtEmail1, borderBottom1, lblEmail1, lblClose1);
var lblInvalidEmail1 = new kony.ui.Label({
"centerX": "50%",
"height": "5%",
"id": "lblInvalidEmail1",
"isVisible": false,
"maxNumberOfLines": 1,
"skin": "CopysknInvalidCredKA0g10f8d36a5e842",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "35%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var FlxEmail2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "FlxEmail2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknslFbox",
"top": "40%",
"width": "90%",
"zIndex": 1
}, {}, {});
FlxEmail2.setDefaultUnit(kony.flex.PERCENTAGE);
var lblEmail2 = new kony.ui.Label({
"id": "lblEmail2",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": kony.i18n.getLocalizedString("i18n.Estmt.Email2Text"),
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var txtEmail2 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtEmail2",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onDone": AS_TextField_e9c586fa1e6a477a92f66b9d7f4fb480,
"onTextChange": AS_TextField_h56fcccb056144b4b1d494add2b2cf3b,
"onTouchEnd": AS_TextField_g3345678d58247899db56ce093064d38,
"onTouchStart": AS_TextField_jd357dbb293b467abd4f16b16897da42,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 10
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onBeginEditing": AS_TextField_j27bd31af4844a7a81caa2a47a6d8584,
"onEndEditing": AS_TextField_fd91cd4910f54e308b0741f8073565b6,
"placeholderSkin": "sknPlaceholderKA",
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblClose2 = new kony.ui.Label({
"height": "100%",
"id": "lblClose2",
"isVisible": false,
"onTouchEnd": AS_Label_fb8257a0bf804fefa1252122cc5288f1,
"left": "2%",
"skin": "sknClose",
"text": "O",
"top": "10%",
"width": "10%",
"zIndex": 20
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var borderBottom2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "borderBottom2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "90%",
"width": "95%",
"zIndex": 2
}, {}, {});
borderBottom2.setDefaultUnit(kony.flex.DP);
borderBottom2.add();
FlxEmail2.add(lblEmail2, txtEmail2, lblClose2, borderBottom2);
var lblInvalidEmail2 = new kony.ui.Label({
"centerX": "50%",
"height": "5%",
"id": "lblInvalidEmail2",
"isVisible": false,
"maxNumberOfLines": 1,
"skin": "CopysknInvalidCredKA0g10f8d36a5e842",
"text": "sfs",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "51%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxTermsandConditionCheck = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxTermsandConditionCheck",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "90%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxTermsandConditionCheck.setDefaultUnit(kony.flex.DP);
var lblTermsandConditionsCheckBox = new kony.ui.Label({
"id": "lblTermsandConditionsCheckBox",
"isVisible": true,
"right": "0%",
"onTouchEnd": AS_Label_b671ea0ecac04a45b070fe53658a3fc9,
"skin": "sknBOJttfwhitee150",
"text": "q",
"top": "4dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxTncBody = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxTncBody",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "10%",
"onClick": AS_FlexContainer_d290c367d6974f74bb85bf0353ba04b4,
"skin": "slFbox",
"top": "0dp",
"width": "85%",
"zIndex": 1
}, {}, {});
flxTncBody.setDefaultUnit(kony.flex.DP);
var richtxtTermsandCondition = new kony.ui.RichText({
"id": "richtxtTermsandCondition",
"isVisible": true,
"right": "0dp",
"skin": "sknrichTxtWhite100",
"text": "<U>RichText</U>",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTncBody.add(richtxtTermsandCondition);
flxTermsandConditionCheck.add(lblTermsandConditionsCheckBox, flxTncBody);
frmEstatementLandingKA.add(flxHeader, flxToggleEStmt, FlxEmail1, lblInvalidEmail1, FlxEmail2, lblInvalidEmail2, flxTermsandConditionCheck);
};
function frmEstatementLandingKAGlobalsAr() {
frmEstatementLandingKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmEstatementLandingKAAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": false,
"id": "frmEstatementLandingKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"postShow": AS_Form_f144f16817394b4c9392afb12483f52a,
"preShow": AS_Form_ja8256911b054d4d8d0a37aad90bd121,
"skin": "sknmainGradient",
"statusBarHidden": true
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"bouncesZoom": false,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
