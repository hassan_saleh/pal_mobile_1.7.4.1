//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializesegGetAllIPSAliasAr() {
    flxMainSegAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxMainSeg",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxMainSegAr.setDefaultUnit(kony.flex.DP);
    var flxIPSAlias = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxIPSAlias",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxIPSAlias.setDefaultUnit(kony.flex.DP);
    var lblAliasTypeTitleSeg = new kony.ui.Label({
        "id": "lblAliasTypeTitleSeg",
        "isVisible": true,
        "right": "2%",
        "skin": "sknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.AliasTypeIPS"),
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAliasTitleSeg = new kony.ui.Label({
        "id": "lblAliasTitleSeg",
        "isVisible": true,
        "right": "2%",
        "skin": "sknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.jomopay.aliastype"),
        "top": "45%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAliasTypeSeg = new kony.ui.Label({
        "id": "lblAliasTypeSeg",
        "isVisible": true,
        "right": "30%",
        "skin": "sknLblNextDisabled",
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAliasSeg = new kony.ui.Label({
        "id": "lblAliasSeg",
        "isVisible": true,
        "right": "30%",
        "skin": "sknLblNextDisabled",
        "top": "45%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxIPSAlias.add(lblAliasTypeTitleSeg, lblAliasTitleSeg, lblAliasTypeSeg, lblAliasSeg);
    flxMainSegAr.add(flxIPSAlias);
}
