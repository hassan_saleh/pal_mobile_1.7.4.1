//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:56 EEST 2020
function addWidgetsfrmAddExternalAccountHomeAr() {
frmAddExternalAccountHome.setDefaultUnit(kony.flex.DP);
var flxMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "s",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_i38f5066f4d7441a989df64f08779587,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, lblBack);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "100%",
"id": "lblTitle",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.Bene.SendmoneySmall"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxHeader.add(flxBack, lblTitle);
var flxMainContainer = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "83%",
"horizontalScrollIndicator": true,
"id": "flxMainContainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxMainContainer.setDefaultUnit(kony.flex.DP);
var imgBeneficiary = new kony.ui.Image2({
"centerX": "50%",
"height": "35%",
"id": "imgBeneficiary",
"isVisible": true,
"skin": "slImage",
"src": "icon_beneficiary.png",
"top": "10%",
"width": "90%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblMessageTitle = new kony.ui.Label({
"centerX": "50%",
"id": "lblMessageTitle",
"isVisible": true,
"skin": "sknLblMsg",
"text": kony.i18n.getLocalizedString("i18n.externalAccount.Sendmoneytobeneficiary"),
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblMessage = new kony.ui.Label({
"centerX": "50%",
"id": "lblMessage",
"isVisible": true,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.externalAccounts.sendmoneyDesc"),
"textStyle": {},
"top": "1%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnScreen = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "45dp",
"id": "btnScreen",
"isVisible": true,
"maxWidth": "90%",
"onClick": AS_Button_ac78a366be28472dac76bef8a8641a1c,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.Bene.Addbeneficiary"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 23, 0,23, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxMainContainer.add(imgBeneficiary, lblMessageTitle, lblMessage, btnScreen);
flxMain.add(flxHeader, flxMainContainer);
var flxFooter = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": false,
"height": "7%",
"id": "flxFooter",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0",
"skin": "menu",
"top": "93%",
"width": "100%"
}, {}, {});
flxFooter.setDefaultUnit(kony.flex.DP);
var FlxAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxAccounts",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_f70d36ebe6c74f569f9ffa060d5aa973,
"skin": "sknfocusmenu",
"top": "0dp",
"width": "25%",
"zIndex": 1
}, {}, {});
FlxAccounts.setDefaultUnit(kony.flex.DP);
var img1 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img1",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_accounts_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label03174bff69bb54c = new kony.ui.Label({
"centerX": "50%",
"id": "Label03174bff69bb54c",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.my_money.accounts"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnAccounts = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnAccounts",
"isVisible": true,
"onClick": AS_Button_hf9d1c8a4d6349a3973861bd541d63b2,
"skin": "btnCard",
"text": "H",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
FlxAccounts.add(img1, Label03174bff69bb54c, btnAccounts);
var FlxTranfers = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxTranfers",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_f188be557d9542c99caf381cf602efae,
"skin": "sknslFbox",
"top": "0dp",
"width": "25%",
"zIndex": 1
}, {}, {});
FlxTranfers.setDefaultUnit(kony.flex.DP);
var img2 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img2",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_t_and_p_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label02bec01fd5baf4c = new kony.ui.Label({
"centerX": "50%",
"id": "Label02bec01fd5baf4c",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.Payments"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnTransfers = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnTransfers",
"isVisible": true,
"onClick": AS_Button_aaf9aa8718264031a4fb6426a9954c15,
"skin": "btnFooterDisable",
"text": "G",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var CopyLabel0c7b8af8febe44e = new kony.ui.Label({
"centerX": "50%",
"id": "CopyLabel0c7b8af8febe44e",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
FlxTranfers.add(img2, Label02bec01fd5baf4c, btnTransfers, CopyLabel0c7b8af8febe44e);
var FlxPayment = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxPayment",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_ba1658f41383430aa16c529d1ff1d562,
"skin": "slFbox",
"top": "0dp",
"width": "25%",
"zIndex": 1
}, {}, {});
FlxPayment.setDefaultUnit(kony.flex.DP);
var imgBot = new kony.ui.Image2({
"centerX": "50%",
"height": "40dp",
"id": "imgBot",
"isVisible": false,
"right": "13dp",
"skin": "slImage",
"src": "chaticonactive.png",
"top": "4dp",
"width": "40dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnPayment = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnPayment",
"isVisible": true,
"skin": "btnFooterDisableaccount",
"text": "i",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var CopyLabel0i584dcaf2e8546 = new kony.ui.Label({
"centerX": "50%",
"id": "CopyLabel0i584dcaf2e8546",
"isVisible": true,
"skin": "sknlblFootertitleFocus",
"text": kony.i18n.getLocalizedString("i18n.alert.transfers"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
FlxPayment.add(imgBot, btnPayment, CopyLabel0i584dcaf2e8546);
var FlxDeposits = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxDeposits",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_cde86567dd5541c2bda13a5b86559f94,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxDeposits.setDefaultUnit(kony.flex.DP);
var img3 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img3",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_deposits_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label04221a71494e848 = new kony.ui.Label({
"centerX": "50%",
"id": "Label04221a71494e848",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.deposits"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnDeposit = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnDeposit",
"isVisible": true,
"skin": "btnCard",
"text": "J",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
FlxDeposits.add(img3, Label04221a71494e848, btnDeposit);
var FlxMore = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxMore",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_i84be02fad534f4f9c299feb44d283d3,
"skin": "sknslFbox",
"top": "0dp",
"width": "25%",
"zIndex": 1
}, {}, {});
FlxMore.setDefaultUnit(kony.flex.DP);
var img4 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img4",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_more_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label0e5331028c2ef41 = new kony.ui.Label({
"centerX": "50%",
"id": "Label0e5331028c2ef41",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.more"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnMore = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnMore",
"isVisible": true,
"onClick": AS_Button_a4ad7fad82de4654aefd6fcefd33edce,
"skin": "btnCard",
"text": "K",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var CopyLabel0j9886914c8a64f = new kony.ui.Label({
"centerX": "50%",
"id": "CopyLabel0j9886914c8a64f",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.common.more"),
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
FlxMore.add(img4, Label0e5331028c2ef41, btnMore, CopyLabel0j9886914c8a64f);
flxFooter.add( FlxMore, FlxDeposits, FlxPayment, FlxTranfers,FlxAccounts);
frmAddExternalAccountHome.add(flxMain, flxFooter);
};
function frmAddExternalAccountHomeGlobalsAr() {
frmAddExternalAccountHomeAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmAddExternalAccountHomeAr,
"enabledForIdleTimeout": true,
"id": "frmAddExternalAccountHome",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "sknBackground"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
