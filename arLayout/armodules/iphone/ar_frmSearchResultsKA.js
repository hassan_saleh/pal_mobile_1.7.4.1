//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function addWidgetsfrmSearchResultsKAAr() {
    frmSearchResultsKA.setDefaultUnit(kony.flex.DP);
    var mainSearch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "mainSearch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainSearch.setDefaultUnit(kony.flex.DP);
    var titleBarAccountDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarAccountDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarAccountDetails.setDefaultUnit(kony.flex.DP);
    var iOSBackButton = new kony.ui.Button({
        "focusSkin": "sknleftBackButtonFocus",
        "height": "50dp",
        "id": "iOSBackButton",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_7005aa0756094cb1958b7d7945ddf4ed,
        "skin": "sknleftBackButtonNormal",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var lblTitleKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblTitleKA",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.search.SearchResults"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "right": "10%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    titleBarAccountDetails.add(iOSBackButton, lblTitleKA, lblBack);
    var mainContentSearch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "mainContentSearch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "skncontainerBkg",
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContentSearch.setDefaultUnit(kony.flex.DP);
    var flxSectionHeaderKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxSectionHeaderKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0",
        "skin": "skncontainerBkg",
        "top": "0",
        "width": "100%"
    }, {}, {});
    flxSectionHeaderKA.setDefaultUnit(kony.flex.DP);
    var lblHeaderKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblHeaderKA",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.common.accountName"),
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxSectionHeaderKA.add(lblHeaderKA);
    var segSearchResultsKA = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "data": [{
            "lblSepKA": "Label",
            "lblTransactionAmountKA": "",
            "lblTransactionDateKA": "",
            "lblTransactionNameKA": ""
        }],
        "groupCells": false,
        "id": "segSearchResultsKA",
        "isVisible": true,
        "right": "0%",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_552bf130769c4cf5954c857c6c782b6d,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Normal",
        "rowSkin": "seg2Normal",
        "rowTemplate": CopyFlexContainer0125626bedd894d,
        "scrollingEvents": {
            "onReachingEnd": AS_Segment_9c488c1d867842b8a96e03293ba7ea7d
        },
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "40dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopyFlexContainer0125626bedd894d": "CopyFlexContainer0125626bedd894d",
            "lblSepKA": "lblSepKA",
            "lblTransactionAmountKA": "lblTransactionAmountKA",
            "lblTransactionDateKA": "lblTransactionDateKA",
            "lblTransactionNameKA": "lblTransactionNameKA"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    var LabelNoRecordsKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "40%",
        "id": "LabelNoRecordsKA",
        "isVisible": false,
        "right": "10%",
        "skin": "skn383838LatoRegular107KA",
        "text": kony.i18n.getLocalizedString("i18n.common.norecords"),
        "top": "30dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    mainContentSearch.add(flxSectionHeaderKA, segSearchResultsKA, LabelNoRecordsKA);
    mainSearch.add(titleBarAccountDetails, mainContentSearch);
    frmSearchResultsKA.add(mainSearch);
};
function frmSearchResultsKAGlobalsAr() {
    frmSearchResultsKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmSearchResultsKAAr,
        "allowHorizontalBounce": false,
        "allowVerticalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmSearchResultsKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient",
        "statusBarHidden": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": true,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": true,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false
    });
};
