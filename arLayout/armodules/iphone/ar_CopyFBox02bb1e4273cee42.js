//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:56 EEST 2020
function initializeCopyFBox02bb1e4273cee42Ar() {
    CopyFBox02bb1e4273cee42Ar = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyFBox02bb1e4273cee42",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    CopyFBox02bb1e4273cee42Ar.setDefaultUnit(kony.flex.DP);
    var lblCDTermValKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCDTermValKA",
        "isVisible": true,
        "right": "5%",
        "skin": "skn",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAPYValKA = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblAPYValKA",
        "isVisible": true,
        "skin": "skn",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblMinDepositValKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblMinDepositValKA",
        "isVisible": true,
        "left": "5%",
        "skin": "skn",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyFBox02bb1e4273cee42Ar.add(lblCDTermValKA, lblAPYValKA, lblMinDepositValKA);
}
