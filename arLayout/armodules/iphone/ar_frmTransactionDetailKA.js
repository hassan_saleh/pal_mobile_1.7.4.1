//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function addWidgetsfrmTransactionDetailKAAr() {
frmTransactionDetailKA.setDefaultUnit(kony.flex.DP);
var topWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "topWrapper",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
topWrapper.setDefaultUnit(kony.flex.DP);
var iosTitleBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "iosTitleBar",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
iosTitleBar.setDefaultUnit(kony.flex.DP);
var CopyandroidTitleLabel02a1c8891167046 = new kony.ui.Label({
"centerY": "50%",
"id": "CopyandroidTitleLabel02a1c8891167046",
"isVisible": true,
"right": "55dp",
"skin": "sknnavBarTitle",
"text": "Transaction Details",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopyhamburgerButton04a4b0742ce0647 = new kony.ui.Button({
"focusSkin": "sknhamburgerButtonFocus",
"height": "50dp",
"id": "CopyhamburgerButton04a4b0742ce0647",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_f0a5128809c5494fa35ab2ea30db2e4f,
"skin": "sknleftBackButtonNormal",
"top": "0dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
iosTitleBar.add(CopyandroidTitleLabel02a1c8891167046, CopyhamburgerButton04a4b0742ce0647);
topWrapper.add(iosTitleBar);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": 0,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkg",
"top": "0dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var FlexContainer042b0725667e643 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "145dp",
"id": "FlexContainer042b0725667e643",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer042b0725667e643.setDefaultUnit(kony.flex.DP);
var FlexContainer0f615b714fea843 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100%",
"id": "FlexContainer0f615b714fea843",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "80%",
"zIndex": 1
}, {}, {});
FlexContainer0f615b714fea843.setDefaultUnit(kony.flex.DP);
var transactionAmount = new kony.ui.Label({
"centerX": "50%",
"id": "transactionAmount",
"isVisible": true,
"skin": "skndetailPageNumber",
"text": "$500.00",
"top": "30dp",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var dummytransactionAmount = new kony.ui.Label({
"centerX": "50%",
"id": "dummytransactionAmount",
"isVisible": false,
"skin": "skndetailPageNumber",
"text": "$500.00",
"top": "30dp",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var transactionName = new kony.ui.Label({
"centerX": "50%",
"id": "transactionName",
"isVisible": true,
"skin": "sknNumber",
"text": "Payment to City of Austin",
"top": "5dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var transactionDate = new kony.ui.Label({
"centerX": "50%",
"id": "transactionDate",
"isVisible": true,
"skin": "skndetailPageDate",
"text": "Jan 29, 2016",
"top": "3dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var dummytransactionDate = new kony.ui.Label({
"centerX": "50%",
"id": "dummytransactionDate",
"isVisible": false,
"skin": "skndetailPageDate",
"text": "Jan 29, 2016",
"top": "3dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
FlexContainer0f615b714fea843.add(transactionAmount, dummytransactionAmount, transactionName, transactionDate, dummytransactionDate);
var divider = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "divider",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
divider.setDefaultUnit(kony.flex.DP);
divider.add();
FlexContainer042b0725667e643.add(FlexContainer0f615b714fea843, divider);
var additionalDetails1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "additionalDetails1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
additionalDetails1.setDefaultUnit(kony.flex.DP);
var Label0e7a26666ebf141 = new kony.ui.Label({
"id": "Label0e7a26666ebf141",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.common.fromc"),
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var transactionFrom = new kony.ui.Label({
"id": "transactionFrom",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Savings Account 3465",
"top": "40dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var divider2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "divider2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
divider2.setDefaultUnit(kony.flex.DP);
divider2.add();
additionalDetails1.add(Label0e7a26666ebf141, transactionFrom, divider2);
var CopynotesWrapper03de47f39b8bc46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "CopynotesWrapper03de47f39b8bc46",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopynotesWrapper03de47f39b8bc46.setDefaultUnit(kony.flex.DP);
var CopyLabel07f3ee643ff4e47 = new kony.ui.Label({
"id": "CopyLabel07f3ee643ff4e47",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.common.notesc"),
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var transactionNotes = new kony.ui.Label({
"id": "transactionNotes",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Note Details appear here.This can be a multiple line description",
"top": "40dp",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var Copydivider06e184b7d586e4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider06e184b7d586e4a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider06e184b7d586e4a.setDefaultUnit(kony.flex.DP);
Copydivider06e184b7d586e4a.add();
CopynotesWrapper03de47f39b8bc46.add(CopyLabel07f3ee643ff4e47, transactionNotes, Copydivider06e184b7d586e4a);
var CopynotesWrapper0accf5ab04b6940 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "85dp",
"id": "CopynotesWrapper0accf5ab04b6940",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopynotesWrapper0accf5ab04b6940.setDefaultUnit(kony.flex.DP);
var CopyLabel000cf6571804e4e = new kony.ui.Label({
"id": "CopyLabel000cf6571804e4e",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.my_money.mapToACategory"),
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var hiddencategoryIdKA = new kony.ui.Label({
"id": "hiddencategoryIdKA",
"isVisible": false,
"right": "15%",
"skin": "sknsegmentHeaderText",
"text": "Map to a category",
"top": "30dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var Copydivider0828a7be62f8a4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0828a7be62f8a4a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0828a7be62f8a4a.setDefaultUnit(kony.flex.DP);
Copydivider0828a7be62f8a4a.add();
var ListBox001b478bb90954f = new kony.ui.ListBox({
"height": "40dp",
"id": "ListBox001b478bb90954f",
"isVisible": true,
"right": "20dp",
"masterData": [["Key197", kony.i18n.getLocalizedString("i18n.my_money.frmTransactionDetailKA.ListBox001b478bb90954f.Key197")],["lb3", kony.i18n.getLocalizedString("i18n.my_money.frmTransactionDetailKA.ListBox001b478bb90954f.lb3")],["lb2", kony.i18n.getLocalizedString("i18n.my_money.frmTransactionDetailKA.ListBox001b478bb90954f.lb2")],["lb1", kony.i18n.getLocalizedString("i18n.my_money.frmTransactionDetailKA.ListBox001b478bb90954f.lb1")]],
"selectedKey": "lb1",
"selectedKeyValue": ["lb1", null],
"top": "40dp",
"width": "260dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"dropDownImage": "dropdown.png",
"groupCells": false,
"viewConfig": {
"toggleViewConfig": {
"viewStyle": constants.LISTBOX_TOGGLE_VIEW_STYLE_PLAIN
}
},
"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
});
CopynotesWrapper0accf5ab04b6940.add(CopyLabel000cf6571804e4e, hiddencategoryIdKA, Copydivider0828a7be62f8a4a, ListBox001b478bb90954f);
var preferredAccountEnable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "preferredAccountEnable",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
preferredAccountEnable.setDefaultUnit(kony.flex.DP);
var accountPreviewEnableSwitch = new kony.ui.Switch({
"height": "32dp",
"id": "accountPreviewEnableSwitch",
"isVisible": true,
"rightSideText": "ON",
"left": "5%",
"leftSideText": "OFF",
"selectedIndex": 0,
"skin": "sknCopyslSwitch060ce78731a4840",
"top": "10dp",
"width": "53dp",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyLabel03ee59176c1854c = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel03ee59176c1854c",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.my_money.includeInAnalysis"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var hiddenIncludeInAnalysis = new kony.ui.Label({
"height": "32dp",
"id": "hiddenIncludeInAnalysis",
"isVisible": false,
"right": "15%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.my_money.includeInAnalysis"),
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var Copydivider00b787fb89f2841 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider00b787fb89f2841",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider00b787fb89f2841.setDefaultUnit(kony.flex.DP);
Copydivider00b787fb89f2841.add();
preferredAccountEnable.add(accountPreviewEnableSwitch, CopyLabel03ee59176c1854c, hiddenIncludeInAnalysis, Copydivider00b787fb89f2841);
var CopypreferredAccountEnable03b3617d7d0094f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "CopypreferredAccountEnable03b3617d7d0094f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopypreferredAccountEnable03b3617d7d0094f.setDefaultUnit(kony.flex.DP);
var CopyaccountPreviewEnableSwitch0b4fab801c08546 = new kony.ui.Switch({
"height": "32dp",
"id": "CopyaccountPreviewEnableSwitch0b4fab801c08546",
"isVisible": true,
"rightSideText": "ON",
"left": "5%",
"leftSideText": "OFF",
"selectedIndex": 0,
"skin": "sknCopyslSwitch060ce78731a4840",
"top": "10dp",
"width": "53dp",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyLabel0960d147b038a48 = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel0960d147b038a48",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.my_money.mapThisMerchantAlways"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var hiddenMaptoMerchant = new kony.ui.Label({
"height": "32dp",
"id": "hiddenMaptoMerchant",
"isVisible": false,
"right": "15%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.my_money.mapThisMerchantAlways"),
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var Copydivider09e23dd99550542 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider09e23dd99550542",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider09e23dd99550542.setDefaultUnit(kony.flex.DP);
Copydivider09e23dd99550542.add();
CopypreferredAccountEnable03b3617d7d0094f.add(CopyaccountPreviewEnableSwitch0b4fab801c08546, CopyLabel0960d147b038a48, hiddenMaptoMerchant, Copydivider09e23dd99550542);
var signInButton = new kony.ui.Button({
"centerX": "50.00%",
"focusSkin": "sknprimaryActionFocus",
"height": "38dp",
"id": "signInButton",
"isVisible": true,
"onClick": AS_Button_d8d9cafbbc444d47af671a8d5632ef9c,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.my_money.map"),
"top": "14dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
mainContent.add(FlexContainer042b0725667e643, additionalDetails1, CopynotesWrapper03de47f39b8bc46, CopynotesWrapper0accf5ab04b6940, preferredAccountEnable, CopypreferredAccountEnable03b3617d7d0094f, signInButton);
var flxHeaderTransaferedStatusDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "flxHeaderTransaferedStatusDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxSearch",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeaderTransaferedStatusDetails.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_d2ad7610b0154381a21ac4a56f336fcc,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, lblBack);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblTitle",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.common.transferstatusdetails"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxHeaderTransaferedStatusDetails.add(flxBack, lblTitle);
var flxTransferedStatusDetailsBody = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "92%",
"horizontalScrollIndicator": true,
"id": "flxTransferedStatusDetailsBody",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0%",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
flxTransferedStatusDetailsBody.setDefaultUnit(kony.flex.DP);
var lblPayeeNameTitle = new kony.ui.Label({
"id": "lblPayeeNameTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.transfer.payeeNameC"),
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblPayeeName = new kony.ui.Label({
"id": "lblPayeeName",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "RAEID WAFA HASAN ABD RABU\r",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxAccounts",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccounts.setDefaultUnit(kony.flex.DP);
var lblPayeeAccountTitle = new kony.ui.Label({
"id": "lblPayeeAccountTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.payeeaccount"),
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblPayeeAccount = new kony.ui.Label({
"id": "lblPayeeAccount",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "0013010050133000",
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAccounts.add(lblPayeeAccountTitle, lblPayeeAccount);
var lblCreditAccountTitle = new kony.ui.Label({
"id": "lblCreditAccountTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": "PayeeAccount",
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCreditAccount = new kony.ui.Label({
"id": "lblCreditAccount",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "0013010050133000",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblReferenceIdTitle = new kony.ui.Label({
"id": "lblReferenceIdTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblReferenceId = new kony.ui.Label({
"id": "lblReferenceId",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "632481",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var lblAmountTitle = new kony.ui.Label({
"id": "lblAmountTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAmount = new kony.ui.Label({
"id": "lblAmount",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "928.703 JOD",
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblFeesTitle = new kony.ui.Label({
"id": "lblFeesTitle",
"isVisible": true,
"right": "55%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.opening_account.fees"),
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblFees = new kony.ui.Label({
"id": "lblFees",
"isVisible": true,
"right": "55%",
"skin": "sknLblBack",
"text": "5 JOD",
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAmount.add(lblAmountTitle, lblAmount, lblFeesTitle, lblFees);
var flxDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxDate",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDate.setDefaultUnit(kony.flex.DP);
var lblTransactionDateTitle = new kony.ui.Label({
"id": "lblTransactionDateTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblTransactionDate = new kony.ui.Label({
"id": "lblTransactionDate",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "2013-10-07",
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblTransactionEffectedDateTitle = new kony.ui.Label({
"id": "lblTransactionEffectedDateTitle",
"isVisible": true,
"right": "55%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.effecteddate"),
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblTransactionEffectedDate = new kony.ui.Label({
"id": "lblTransactionEffectedDate",
"isVisible": true,
"right": "55%",
"skin": "sknLblBack",
"text": "2013-10-07",
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxDate.add(lblTransactionDateTitle, lblTransactionDate, lblTransactionEffectedDateTitle, lblTransactionEffectedDate);
var lblPurposeTitle = new kony.ui.Label({
"id": "lblPurposeTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.confirmdetails.purposeoftransfer"),
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblPurpose = new kony.ui.Label({
"id": "lblPurpose",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "Internal Transaction",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBankTitle = new kony.ui.Label({
"id": "lblBankTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Bankname"),
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBank = new kony.ui.Label({
"id": "lblBank",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "AL RAJHI BANK",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBranchTitle = new kony.ui.Label({
"id": "lblBranchTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Map.Branchname"),
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBranch = new kony.ui.Label({
"id": "lblBranch",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxBankAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxBankAddress",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBankAddress.setDefaultUnit(kony.flex.DP);
var lblBankAddressTitle = new kony.ui.Label({
"id": "lblBankAddressTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.BankAddress"),
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBankAddress1 = new kony.ui.Label({
"id": "lblBankAddress1",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBankAddress2 = new kony.ui.Label({
"id": "lblBankAddress2",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBankAddress3 = new kony.ui.Label({
"id": "lblBankAddress3",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBankAddress.add(lblBankAddressTitle, lblBankAddress1, lblBankAddress2, lblBankAddress3);
var flxBeneficiaryAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxBeneficiaryAddress",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBeneficiaryAddress.setDefaultUnit(kony.flex.DP);
var lblBeneficaryAddressTitle = new kony.ui.Label({
"id": "lblBeneficaryAddressTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.BeneficiaryAddress"),
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBeneficaryAddress1 = new kony.ui.Label({
"id": "lblBeneficaryAddress1",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBeneficaryAddress2 = new kony.ui.Label({
"id": "lblBeneficaryAddress2",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBeneficaryAddress3 = new kony.ui.Label({
"id": "lblBeneficaryAddress3",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBeneficiaryAddress.add(lblBeneficaryAddressTitle, lblBeneficaryAddress1, lblBeneficaryAddress2, lblBeneficaryAddress3);
var lblRemarksTitle = new kony.ui.Label({
"id": "lblRemarksTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.remarks"),
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblRemarks = new kony.ui.Label({
"id": "lblRemarks",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "AL RAJHI BANK , JORDAN BRANCH",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblNotesTitle = new kony.ui.Label({
"id": "lblNotesTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.newCard.notes"),
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblNotes = new kony.ui.Label({
"id": "lblNotes",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "AL RAJHI BANK , JORDAN BRANCH",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTransferedStatusDetailsBody.add(lblPayeeNameTitle, lblPayeeName, flxAccounts, lblCreditAccountTitle, lblCreditAccount, lblReferenceIdTitle, lblReferenceId, flxAmount, flxDate, lblPurposeTitle, lblPurpose, lblBankTitle, lblBank, lblBranchTitle, lblBranch, flxBankAddress, flxBeneficiaryAddress, lblRemarksTitle, lblRemarks, lblNotesTitle, lblNotes);
frmTransactionDetailKA.add(topWrapper, mainContent, flxHeaderTransaferedStatusDetails, flxTransferedStatusDetailsBody);
};
function frmTransactionDetailKAGlobalsAr() {
frmTransactionDetailKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmTransactionDetailKAAr,
"bounces": false,
"enableScrolling": true,
"enabledForIdleTimeout": true,
"id": "frmTransactionDetailKA",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"skin": "sknmainGradient",
"statusBarHidden": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"bouncesZoom": true,
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": true,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
"inTransitionConfig": {
"transitionDirection": "none",
"transitionEffect": "transitionFade"
},
"needsIndicatorDuringPostShow": false,
"outTransitionConfig": {
"transitionDirection": "none",
"transitionEffect": "transitionFade"
},
"retainScrollPosition": false,
"statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
"titleBar": false
});
};
