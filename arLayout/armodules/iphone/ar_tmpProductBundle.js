//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetmpProductBundleAr() {
    flxMainuAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxMainu",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    flxMainuAr.setDefaultUnit(kony.flex.DP);
    var flxIconContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxIconContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "slFbox",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxIconContainer.setDefaultUnit(kony.flex.DP);
    var flxIcon1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxIcon1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxToIcon",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxIcon1.setDefaultUnit(kony.flex.DP);
    var lblInitial = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblInitial",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblTick = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblTick",
        "isVisible": true,
        "skin": "sknBOJttfwhitee150",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxIcon1.add(lblInitial, lblTick);
    flxIconContainer.add(flxIcon1);
    var lblLine1 = new kony.ui.Label({
        "height": "48%",
        "id": "lblLine1",
        "isVisible": true,
        "right": "22%",
        "skin": "CopylblAmountCurrency0f1534fe7031e4b",
        "text": "Label",
        "top": "0%",
        "width": "76%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblLine2 = new kony.ui.Label({
        "height": "48%",
        "id": "lblLine2",
        "isVisible": true,
        "right": "22%",
        "skin": "sknLblAccNumBiller",
        "text": "Label",
        "top": "49%",
        "width": "76%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknsegmentDivider",
        "top": "99%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    flxMainuAr.add(flxIconContainer, lblLine1, lblLine2, contactListDivider);
}
