//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializesegmentWithDoubleLabelAr() {
    Copycontainer018037268d2ef46Ar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "Copycontainer018037268d2ef46",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    Copycontainer018037268d2ef46Ar.setDefaultUnit(kony.flex.DP);
    var TopText = new kony.ui.Label({
        "centerY": "30%",
        "id": "TopText",
        "isVisible": true,
        "right": "5%",
        "skin": "skn",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var BottomText = new kony.ui.Label({
        "centerY": "70%",
        "id": "BottomText",
        "isVisible": true,
        "right": "5%",
        "skin": "sknSegSecondLabel",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var RightText = new kony.ui.Label({
        "centerY": "48.72%",
        "id": "RightText",
        "isVisible": true,
        "left": "35dp",
        "skin": "sknSegSecondLabel",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var InfoImg = new kony.ui.Image2({
        "centerY": "50%",
        "height": "20dp",
        "id": "InfoImg",
        "isVisible": true,
        "left": "4%",
        "skin": "sknslImage",
        "src": "right_chevron_icon.png",
        "width": "20dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var hidden1 = new kony.ui.Label({
        "id": "hidden1",
        "isVisible": false,
        "right": "116dp",
        "skin": "slLabel",
        "top": "9dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    Copycontainer018037268d2ef46Ar.add(TopText, BottomText, RightText, InfoImg, hidden1);
}
