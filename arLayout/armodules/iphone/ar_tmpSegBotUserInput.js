//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetmpSegBotUserInputAr() {
    flxBotUserMsgAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxBotUserMsg",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "CopyslFbox0i52c67271d3346"
    }, {}, {});
    flxBotUserMsgAr.setDefaultUnit(kony.flex.DP);
    var flxBotUserSwipe = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxBotUserSwipe",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "-100%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "200%",
        "zIndex": 1
    }, {}, {});
    flxBotUserSwipe.setDefaultUnit(kony.flex.DP);
    var FlexContainer0h9372881f12a46 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "FlexContainer0h9372881f12a46",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "CopyslFbox0ae5db6e620f743",
        "top": "0dp",
        "width": "51%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0h9372881f12a46.setDefaultUnit(kony.flex.DP);
    FlexContainer0h9372881f12a46.add();
    var TextField0e5adb2bfb61248 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "50dp",
        "id": "TextField0e5adb2bfb61248",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "right": "52%",
        "secureTextEntry": false,
        "skin": "slTextBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "48%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,3, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxBotUserSwipe.add(FlexContainer0h9372881f12a46, TextField0e5adb2bfb61248);
    flxBotUserMsgAr.add(flxBotUserSwipe);
}
