//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:57 EEST 2020
function addWidgetsfrmCardlessTransactionAr() {
frmCardlessTransaction.setDefaultUnit(kony.flex.DP);
var flxMainContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxMainContainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "CopyslFbox0cd4df9aea5a14b",
"top": "0.00%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMainContainer.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFlxHeaderImg",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxback = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxback",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_ja068a57b12441188611d9f8ba460ca7,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxback.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0%",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0%",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxback.add(lblBackIcon, lblBack);
var lblCardlessTitle = new kony.ui.Label({
"height": "90%",
"id": "lblCardlessTitle",
"isVisible": true,
"left": "20%",
"minHeight": "90%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.Cardless.NewCardless"),
"top": "0%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnNextCardless = new kony.ui.Button({
"focusSkin": "slButtonGlossRed",
"height": "90%",
"id": "btnNextCardless",
"isVisible": true,
"onClick": AS_Button_bc9c73dd782947a9a224ee682904a362,
"right": "0%",
"skin": "jomopaynextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"top": "0%",
"width": "20%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxHeader.add(flxback, lblCardlessTitle, btnNextCardless);
var flxBody = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxBody",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBody.setDefaultUnit(kony.flex.DP);
var flxAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90dp",
"id": "flxAccount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxAccount.setDefaultUnit(kony.flex.DP);
var flxPaymentModeBulk = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "65%",
"id": "flxPaymentModeBulk",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"left": 0,
"skin": "slFbox",
"top": "0.00%",
"width": "90%",
"zIndex": 2
}, {}, {});
flxPaymentModeBulk.setDefaultUnit(kony.flex.DP);
var tbxPaymentModeBulk = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxPaymentModeBulk",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlinePaymentModeBulk = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "flxUnderlinePaymentModeBulk",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlinePaymentModeBulk.setDefaultUnit(kony.flex.DP);
flxUnderlinePaymentModeBulk.add();
var flxPaymentModeTypeHolderBulk = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxPaymentModeTypeHolderBulk",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_d7efbb940b094c7da970be41f1d10215,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxPaymentModeTypeHolderBulk.setDefaultUnit(kony.flex.DP);
var lblPaymentMode = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblPaymentMode",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "88%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblArrowPaymentMode = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowPaymentMode",
"isVisible": true,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.baclinfo"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblBranchCode = new kony.ui.Label({
"centerY": "50%",
"id": "lblBranchCode",
"isVisible": false,
"right": "93%",
"skin": "sknBackIconDisabled",
"text": "1",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxPaymentModeTypeHolderBulk.add(lblPaymentMode, lblArrowPaymentMode, lblBranchCode);
flxPaymentModeBulk.add(tbxPaymentModeBulk, flxUnderlinePaymentModeBulk, flxPaymentModeTypeHolderBulk);
flxAccount.add(flxPaymentModeBulk);
var flxAmountDetail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "30%",
"id": "flxAmountDetail",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxAmountDetail.setDefaultUnit(kony.flex.DP);
var lblAmountStaticTexts = new kony.ui.Label({
"id": "lblAmountStaticTexts",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Cardless.amountWithdraw"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxSelectAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "50%",
"id": "flxSelectAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "25%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxSelectAmount.setDefaultUnit(kony.flex.DP);
var txtFieldAmount = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerY": "50%",
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "80%",
"id": "txtFieldAmount",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
"right": "25%",
"maxTextLength": 9,
"onDone": AS_TextField_aab002d3b3ff4972814c0b74ff660db2,
"onTextChange": AS_TextField_a13aceb3120a4f7e8074fff5bf7ee866,
"placeholder": "0",
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": 0,
"width": "30%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onBeginEditing": AS_TextField_c410129e7344461598bc0aaf4e9cddc8,
"onEndEditing": AS_TextField_d4a3d52fa79d4be6ac77201e35e01f15,
"showClearButton": false,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxBorderAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxBorderAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "25%",
"skin": "skntextFieldDivider",
"top": "75%",
"width": "45%",
"zIndex": 1
}, {}, {});
flxBorderAmount.setDefaultUnit(kony.flex.DP);
flxBorderAmount.add();
var lblHiddenAmount = new kony.ui.Label({
"centerX": "50%",
"id": "lblHiddenAmount",
"isVisible": false,
"skin": "lblAccountStaticText",
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCurrencyCode = new kony.ui.Label({
"height": "60%",
"id": "lblCurrencyCode",
"isVisible": true,
"right": "60%",
"skin": "sknSIDesc",
"text": "JOD",
"top": "23%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxPlusContainer = new kony.ui.Button({
"height": "30dp",
"id": "flxPlusContainer",
"isVisible": true,
"onClick": AS_Button_b31529ae056e447382c1c0621ad769b4,
"left": "10%",
"skin": "plusMinusSkin",
"text": "u",
"top": "18dp",
"width": "30dp",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var flxMinusContainer = new kony.ui.Button({
"height": "30dp",
"id": "flxMinusContainer",
"isVisible": true,
"right": "10%",
"onClick": AS_Button_gc6b7d93b2e64165a9f21e25995406c5,
"skin": "plusMinusSkin",
"text": "-",
"top": "18dp",
"width": "30dp",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxSelectAmount.add(txtFieldAmount, flxBorderAmount, lblHiddenAmount, lblCurrencyCode, flxPlusContainer, flxMinusContainer);
var lblAmountFieldNote = new kony.ui.Label({
"bottom": "22%",
"centerX": "50%",
"id": "lblAmountFieldNote",
"isVisible": true,
"right": "25%",
"skin": "latoRegular24px",
"text": kony.i18n.getLocalizedString("i18n.cardless.AmountFieldNote"),
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblPreDefinedAmounts = new kony.ui.Label({
"bottom": "0%",
"id": "lblPreDefinedAmounts",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Cardless.preDefined"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxAmountDetail.add(lblAmountStaticTexts, flxSelectAmount, lblAmountFieldNote, lblPreDefinedAmounts);
var flxAmounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "130dp",
"id": "flxAmounts",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"left": "4%",
"skin": "flxBGKA",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAmounts.setDefaultUnit(kony.flex.DP);
var flxFirtsRow = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxFirtsRow",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "0dp",
"skin": "slFbox",
"top": "3%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFirtsRow.setDefaultUnit(kony.flex.DP);
var btnTenJOD = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "75%",
"id": "btnTenJOD",
"isVisible": true,
"left": "2%",
"onClick": AS_Button_d3376bfe66c940a8af596956bba6fa73,
"skin": "slButtonBlueFocus",
"text": "10 JOD",
"top": "2%",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnFiJOD = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "75%",
"id": "btnFiJOD",
"isVisible": true,
"left": "2%",
"onClick": AS_Button_de71918370534d6d8e35f6734aa1cb1f,
"skin": "slButtonBlueFocus",
"text": "50 JOD",
"top": "2%",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnHuJOD = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "75%",
"id": "btnHuJOD",
"isVisible": true,
"left": "2%",
"onClick": AS_Button_j8be8ba74f2246fc9e949bbc2f2b095a,
"skin": "slButtonBlueFocus",
"text": "100 JOD",
"top": "2%",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxFirtsRow.add(btnTenJOD, btnFiJOD, btnHuJOD);
var flxSecondRow = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxSecondRow",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSecondRow.setDefaultUnit(kony.flex.DP);
var btnHuFiJOD = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "75%",
"id": "btnHuFiJOD",
"isVisible": true,
"left": "2%",
"onClick": AS_Button_bfba1d6aa0334c86a44a56e0b58b2b5d,
"skin": "slButtonBlueFocus",
"text": "150 JOD",
"top": "2%",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnTwoHJOD = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "75%",
"id": "btnTwoHJOD",
"isVisible": true,
"left": "2%",
"onClick": AS_Button_dc65fca6585d4d87a64f1a1e41494abb,
"skin": "slButtonBlueFocus",
"text": "200 JOD",
"top": "2%",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnTwoFiJOD = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "75%",
"id": "btnTwoFiJOD",
"isVisible": true,
"left": "2%",
"onClick": AS_Button_c6aa21f05d6e4a5cb7f60d381bf9ca81,
"skin": "slButtonBlueFocus",
"text": "500 JOD",
"top": "2%",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxSecondRow.add(btnHuFiJOD, btnTwoHJOD, btnTwoFiJOD);
flxAmounts.add(flxFirtsRow, flxSecondRow);
var flxPassword = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "120dp",
"id": "flxPassword",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxPassword.setDefaultUnit(kony.flex.DP);
var lblPasswordNote2 = new kony.ui.Label({
"id": "lblPasswordNote2",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.cardless.PasswordFieldNote2"),
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var tbxPassword = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "45%",
"id": "tbxPassword",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
"maxTextLength": 6,
"onDone": AS_TextField_f17beaab32c2418384c9e2c50d1b592f,
"onTextChange": AS_TextField_f5ce4076c1914ae1b76fe485b21b0d93,
"onTouchEnd": AS_TextField_cefda326474e480aac594724c468c4d6,
"placeholder": kony.i18n.getLocalizedString("i18n.cardless.PasswordField"),
"secureTextEntry": true,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "38%",
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,2, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onEndEditing": AS_TextField_j2dce94c4d9444cc9ae47ab98bc1e43f,
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlinePassword = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxUnderlinePassword",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"skin": "sknFlxGreyLine",
"top": "75%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlinePassword.setDefaultUnit(kony.flex.DP);
flxUnderlinePassword.add();
var lblPassword = new kony.ui.Label({
"id": "lblPassword",
"isVisible": false,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.cardless.PasswordField"),
"top": "20%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblPasswordNote = new kony.ui.Label({
"id": "lblPasswordNote",
"isVisible": true,
"right": "2%",
"skin": "latoRegular24px",
"text": kony.i18n.getLocalizedString("i18n.cardless.PasswordFieldNote"),
"top": "85%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxPassword.add(lblPasswordNote2, tbxPassword, flxUnderlinePassword, lblPassword, lblPasswordNote);
var flxCondition1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "5%",
"id": "flxCondition1",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxCondition1.setDefaultUnit(kony.flex.DP);
var lblConditionCheck1 = new kony.ui.Label({
"height": "100%",
"id": "lblConditionCheck1",
"isVisible": true,
"right": "0%",
"skin": "lblTick",
"text": "r",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCondition1 = new kony.ui.Label({
"height": "100%",
"id": "lblCondition1",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Label",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxCondition1.add(lblConditionCheck1, lblCondition1);
var flxTermsandConditionCheck = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "5%",
"id": "flxTermsandConditionCheck",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "slFbox",
"top": "2%",
"width": "92%",
"zIndex": 1
}, {}, {});
flxTermsandConditionCheck.setDefaultUnit(kony.flex.DP);
var lblTermsandConditionsCheckBox = new kony.ui.Label({
"height": "100%",
"id": "lblTermsandConditionsCheckBox",
"isVisible": true,
"right": "0%",
"onTouchEnd": AS_Label_c46cec92cc1b4fe0a557d19f169c1a74,
"skin": "sknLblRemCheckBox",
"text": "q",
"top": "0%",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxTncBody = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxTncBody",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "8%",
"onClick": AS_FlexContainer_i117a263066c4e1199dd6865ed037b78,
"skin": "slFbox",
"top": "2dp",
"width": "86%",
"zIndex": 1
}, {}, {});
flxTncBody.setDefaultUnit(kony.flex.DP);
var richtxtTermsandConditions = new kony.ui.RichText({
"id": "richtxtTermsandConditions",
"isVisible": true,
"right": "2%",
"skin": "sknRichtextTnC",
"text": kony.i18n.getLocalizedString("i18n.Cardless.TNC"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTncBody.add(richtxtTermsandConditions);
flxTermsandConditionCheck.add(lblTermsandConditionsCheckBox, flxTncBody);
flxBody.add(flxAccount, flxAmountDetail, flxAmounts, flxPassword, flxCondition1, flxTermsandConditionCheck);
var flxConfirm = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxConfirm",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirm.setDefaultUnit(kony.flex.DP);
var flxConfirmAccountNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxConfirmAccountNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "4%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxConfirmAccountNumber.setDefaultUnit(kony.flex.DP);
var lblAccountNumberTitle = new kony.ui.Label({
"id": "lblAccountNumberTitle",
"isVisible": true,
"right": "0%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAccountNumber = new kony.ui.Label({
"id": "lblAccountNumber",
"isVisible": true,
"right": "0%",
"skin": "sknNumber",
"top": "50%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmAccountNumber.add(lblAccountNumberTitle, lblAccountNumber);
var flxConfirmAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxConfirmAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "4%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxConfirmAmount.setDefaultUnit(kony.flex.DP);
var lblConfirmAmountTitle = new kony.ui.Label({
"id": "lblConfirmAmountTitle",
"isVisible": true,
"right": "0%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmAmount = new kony.ui.Label({
"id": "lblConfirmAmount",
"isVisible": true,
"right": "0%",
"skin": "sknNumber",
"top": "50%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblCurrencyCodeConfirm = new kony.ui.Label({
"id": "lblCurrencyCodeConfirm",
"isVisible": true,
"right": "88%",
"skin": "lblAccountStaticText",
"text": "JOD",
"top": "42%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmAmount.add(lblConfirmAmountTitle, lblConfirmAmount, lblCurrencyCodeConfirm);
var flxConfirmPass = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxConfirmPass",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "4%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxConfirmPass.setDefaultUnit(kony.flex.DP);
var lblConfirmPassTitle = new kony.ui.Label({
"id": "lblConfirmPassTitle",
"isVisible": true,
"right": "0%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.login.password"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblConfirmPass = new kony.ui.Label({
"id": "lblConfirmPass",
"isVisible": true,
"right": "0%",
"skin": "sknNumber",
"top": "50%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxConfirmPass.add(lblConfirmPassTitle, lblConfirmPass);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "50dp",
"id": "btnConfirm",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_d8a715ae17274d2fb579bee99135ec8f,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
"top": "20%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxConfirm.add(flxConfirmAccountNumber, flxConfirmAmount, flxConfirmPass, btnConfirm);
var BrowserTCCardless = new kony.ui.Browser({
"detectTelNumber": true,
"enableZoom": false,
"height": "91%",
"id": "BrowserTCCardless",
"isVisible": false,
"right": "0dp",
"requestURLConfig": {
"URL": "",
"requestMethod": constants.BROWSER_REQUEST_METHOD_POST
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMainContainer.add(flxHeader, flxBody, flxConfirm, BrowserTCCardless);
frmCardlessTransaction.add(flxMainContainer);
};
function frmCardlessTransactionGlobalsAr() {
frmCardlessTransactionAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmCardlessTransactionAr,
"enabledForIdleTimeout": true,
"id": "frmCardlessTransaction",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": true,
"postShow": AS_Form_hc6edfb735aa452885967b5931319343,
"skin": "slFormCommon"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};
