//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializesegMessagesTmplKAAr() {
flxSegContainerKAAr = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100dp",
"id": "flxSegContainerKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknCopyslFbox07d05709853a74d"
}, {}, {});
flxSegContainerKAAr.setDefaultUnit(kony.flex.DP);
var flxSegMsgSwipe = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100dp",
"id": "flxSegMsgSwipe",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "130%",
"zIndex": 1
}, {}, {});
flxSegMsgSwipe.setDefaultUnit(kony.flex.DP);
var flxMainKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100dp",
"id": "flxMainKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "77%",
"zIndex": 1
}, {}, {});
flxMainKA.setDefaultUnit(kony.flex.DP);
var lblTitleKA = new kony.ui.Label({
"id": "lblTitleKA",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknMessageTitleKA",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "2%",
"width": "90%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxDescChevronContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40dp",
"id": "flxDescChevronContainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0",
"skin": "slFbox",
"top": "25dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDescChevronContainer.setDefaultUnit(kony.flex.DP);
var lblDescKA = new kony.ui.Label({
"id": "lblDescKA",
"isVisible": true,
"right": "5.00%",
"maxNumberOfLines": 1,
"skin": "CopyslLabel031d27909a26c4a",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var imgChevronKA = new kony.ui.Image2({
"centerY": "50%",
"height": "20dp",
"id": "imgChevronKA",
"isVisible": true,
"right": 0,
"left": "4%",
"skin": "sknslImage",
"src": "right_chevron_icon.png",
"width": "10%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxDescChevronContainer.add( imgChevronKA,lblDescKA);
var lblTimestampKA = new kony.ui.Label({
"id": "lblTimestampKA",
"isVisible": true,
"right": "5%",
"skin": "sknasOfTimeLabelBlack",
"top": "60dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblMessageIdKA = new kony.ui.Label({
"id": "lblMessageIdKA",
"isVisible": false,
"right": "204dp",
"skin": "slLabel",
"text": "Label",
"top": "67dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxMainKA.add(lblTitleKA, flxDescChevronContainer, lblTimestampKA, lblMessageIdKA);
var btnDeleteKA = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknbtnDeleteKA",
"height": "100dp",
"id": "btnDeleteKA",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_b616e3930d014803a060a8d7412fffab,
"skin": "sknbtnDeleteKA",
"text": "Delete",
"width": "100dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxSegMsgSwipe.add( btnDeleteKA,flxMainKA);
flxSegContainerKAAr.add(flxSegMsgSwipe);
}
