//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Sep 14 12:08:59 EEST 2020
function initializetmpAccountTypesAr() {
    flxFrequencyAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxFrequency",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "CopyslFbox0h5adacabf4a940"
    }, {}, {});
    flxFrequencyAr.setDefaultUnit(kony.flex.DP);
    var lblfrequency = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblfrequency",
        "isVisible": true,
        "right": "22%",
        "skin": "sknLblWhike125",
        "width": "74%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblKey = new kony.ui.Label({
        "centerX": "60%",
        "centerY": "60%",
        "id": "lblKey",
        "isVisible": false,
        "right": "5%",
        "skin": "sknLblWhike125",
        "text": "Label",
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "50%",
        "id": "imgIcon",
        "isVisible": true,
        "right": "5%",
        "skin": "slImage",
        "src": "imagedrag.png",
        "width": "12%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxFrequencyAr.add(lblfrequency, lblKey, imgIcon);
}
